import {AuthProvider} from 'ngx-auth-firebaseui';

export const environment = {
  production: true,
  url: 'http://localhost:3000',
  aplicativo: {
    nombreAplicativo: 'NOMBRE_APLICATIVO',
    versionAplicativo: '1.0.0'
  },
  firebase: {
    apiKey: 'AIzaSyD1s-DooP0MzL6qOhwYkTvVL0Bxihofzos',
    authDomain: 'sos-encargos-test.firebaseapp.com',
    projectId: 'sos-encargos-test',
    storageBucket: 'sos-encargos-test.appspot.com',
    messagingSenderId: '143073202598',
    appId: '1:143073202598:web:0fd131fc31f0b8d1e5bf67',
    measurementId: 'G-7RQR9Q38JP'
  },
  firebaseAuth: {
    providers: [
      AuthProvider.EmailAndPassword
    ],
    registrationEnabled: true,
    resetPasswordEnabled: true,
    guestEnabled: true,
    tosUrl: 'https://manticore-labs.com', // URL de terminos y condiciones
    privacyPolicyUrl: 'https://manticore-labs.com', // URL políticas de privacidad
    goBackURL: '/inicio',
  },
  emuladores: {
    autenticacion: true,
    firestore: true,
    functions: true,
  },
};
