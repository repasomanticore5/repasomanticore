// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {AuthProvider} from 'ngx-auth-firebaseui';

export const environment = {
  production: false,
  url: 'http://localhost:3000',
  aplicativo: {
    nombreAplicativo: 'NOMBRE_APLICATIVO',
    versionAplicativo: '1.0.0'
  },
  firebase: {
    apiKey: 'AIzaSyD1s-DooP0MzL6qOhwYkTvVL0Bxihofzos',
    authDomain: 'sos-encargos-test.firebaseapp.com',
    projectId: 'sos-encargos-test',
    storageBucket: 'sos-encargos-test.appspot.com',
    messagingSenderId: '143073202598',
    appId: '1:143073202598:web:0fd131fc31f0b8d1e5bf67',
    measurementId: 'G-7RQR9Q38JP'
  },
  firebaseAuth: {
    providers: [
      AuthProvider.EmailAndPassword,
      AuthProvider.Google,
    ],
    registrationEnabled: true,
    resetPasswordEnabled: true,
    guestEnabled: false,
    tosUrl: 'https://manticore-labs.com', // URL de terminos y condiciones
    privacyPolicyUrl: 'https://manticore-labs.com', // URL políticas de privacidad
    goBackURL: '/inicio',
  },
  emuladores: {
    autenticacion: true,
    firestore: true,
    functions: true,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
