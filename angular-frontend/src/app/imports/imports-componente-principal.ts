import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BlockUIModule} from 'primeng/blockui';
import {ToastModule} from 'primeng/toast';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ModalCrearEditarComunModule} from '../modal/modal-crear-editar-comun/modal-crear-editar-comun.module';
import {MatDialogModule} from '@angular/material/dialog';
import {PanelMenuModule} from 'primeng/panelmenu';
import {MlCampoFormularioModule} from '@manticore-labs/ng-2021';
import {NotificacionModule} from '../servicios/notificacion/notificacion.module';
import {ComponentePrincipalModule} from '../servicios/componente-principal/componente-principal.module';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {MatMenuModule} from '@angular/material/menu';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../environments/environment';
import {AngularFireAnalyticsModule} from '@angular/fire/analytics';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {MatPasswordStrengthModule} from '@angular-material-extensions/password-strength';
import {NgxAuthFirebaseUIModule} from 'ngx-auth-firebaseui';
import {TranslocoRootModule} from '../transloco/transloco-root.module';
import {ModalRegistroUsuarioModule} from '../modal/modal-registro-usuario/modal-registro-usuario.module';
import {TranslocoModule} from '@ngneat/transloco';

export const IMPORTS_COMPONENTE_PRINCIPAL = [
  BrowserModule,
  ConfirmDialogModule,
  AppRoutingModule,
  BrowserAnimationsModule,
  NgbModule,
  BlockUIModule,
  ToastModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatButtonModule,
  MlCampoFormularioModule,
  AutoCompleteModule,
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  ModalCrearEditarComunModule,
  ModalRegistroUsuarioModule,
  MatDialogModule,
  PanelMenuModule,
  NotificacionModule,
  ComponentePrincipalModule,
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireAnalyticsModule,
  AngularFirestoreModule,
  MatPasswordStrengthModule.forRoot(),
  NgxAuthFirebaseUIModule.forRoot(environment.firebase,
    () => 'your_app_name_factory',
    {
      enableFirestoreSync: true, // enable/disable autosync users with firestore
      toastMessageOnAuthSuccess: false, // whether to open/show a snackbar message on auth success - default : true
      toastMessageOnAuthError: false, // whether to open/show a snackbar message on auth error - default : true
      authGuardFallbackURL: '/loggedout', // url for unauthenticated users - to use in combination with canActivate feature on a route
      authGuardLoggedInURL: '/loggedin', // url for authenticated users - to use in combination with canActivate feature on a route
      passwordMaxLength: 60, // `min/max` input parameters in components should be within this range.
      passwordMinLength: 8, // Password length min/max in forms independently of each componenet min/max.
      // Same as password but for the name
      nameMaxLength: 50,
      nameMinLength: 2,
      // If set, sign-in/up form is not available until email has been verified.
      // Plus protected routes are still protected even though user is connected.
      guardProtectedRoutesUntilEmailIsVerified: true,
      enableEmailVerification: true, // default: true
      useRawUserCredential: true, // If set to true outputs the UserCredential object instead of firebase.User after login and signup - Default: false
    }),
  HttpClientModule,
  TranslocoRootModule,
  TranslocoModule,
  MatMenuModule,
];
