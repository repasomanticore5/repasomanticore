import {RutaLoginComponent} from '../ruta/ruta-login/ruta-login.component';
import {RutaNoEncontradoComponent} from '../ruta/ruta-no-encontrado/ruta-no-encontrado.component';
import {RutaInicioComponent} from '../ruta/ruta-inicio/ruta-inicio.component';
import {AppComponent} from '../app.component';

export const DECLARATIONS_COMPONENTE_PRINCIPAL = [

  AppComponent,
  RutaLoginComponent,
  RutaNoEncontradoComponent,
  RutaInicioComponent,
];
