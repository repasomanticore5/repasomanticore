import {ConfirmationService, MessageService} from 'primeng/api';
import {CargandoService} from '../servicios/cargando/cargando.service';
import {MenuGeneralService} from '../servicios/menu-general/menu-general.service';
import {AuthService} from '../servicios/auth/auth.service';
import {FirebaseUtilService} from '../servicios/firebase/firebase-util.service';

import {USE_EMULATOR as EMULADOR_AUTENTICACION} from '@angular/fire/auth';
import {USE_EMULATOR as EMULADOR_FIRESTORE} from '@angular/fire/firestore';
import {USE_EMULATOR as EMULADOR_FUNCTIONS} from '@angular/fire/functions';
import {environment} from '../../environments/environment';
import {TRANSLOCO_CONFIG, TRANSLOCO_LOADER, translocoConfig} from '@ngneat/transloco';
import {TranslocoHttpLoader} from '../transloco/transloco-root.module';
import {ArchivoPrincipalService} from '../servicios/archivos/archivo-principal.service';
import {SRutaLoginService} from '../ruta/ruta-login/s-ruta-login/s-ruta-login.service';
import {SeguridadService} from '../servicios/seguridad.service';
import {INTERCEPTORS} from '../interceptors/interceptors';

export const PROVIDERS_COMPONENTE_PRINCIPAL = [
  MessageService,
  CargandoService,
  MenuGeneralService,
  ConfirmationService,
  AuthService,
  FirebaseUtilService,
  ArchivoPrincipalService,
  SRutaLoginService,
  SeguridadService,
  ...INTERCEPTORS,
  {
    provide: EMULADOR_AUTENTICACION,
    useValue: environment.emuladores.autenticacion ? ['localhost', 9099] : undefined
  },
  {
    provide: EMULADOR_FIRESTORE,
    useValue: environment.emuladores.firestore ? ['localhost', 8080] : undefined
  },
  {
    provide: EMULADOR_FUNCTIONS,
    useValue: environment.emuladores.functions ? ['localhost', 5001] : undefined
  }
];
