import {ModalCrearEditarComunComponent} from '../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';
import {ModalRegistroUsuarioComponent} from '../modal/modal-registro-usuario/modal-registro-usuario.component';

export const ENTRY_COMPONENTS = [
  ModalCrearEditarComunComponent,
  ModalRegistroUsuarioComponent,
];
