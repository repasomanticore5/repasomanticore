import {FieldValue, Timestamp} from '@firebase/firestore-types';
import {EntidadGeneralInterface} from '@manticore-labs/firebase-angular';
import {ActivoInactivo} from '../enums/activo-inactivo';

export interface EntidadComunProyectoFirebase extends EntidadGeneralInterface {
    createdAt?: Timestamp | number | FieldValue;
    updatedAt?: Timestamp | number | FieldValue;
    sisHabilitado?: ActivoInactivo;
    habilitado: boolean;

}
