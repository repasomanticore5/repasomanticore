import {EntidadComun} from '@manticore-labs/ng-2021';
import {ArchivoInterface} from '@manticore-labs/ng-2021/lib/clases-abstractas/interfaces/archivo.interface';

export interface EntidadComunProyecto extends EntidadComun {
  id?: number | string;
  sis_IM?: ArchivoInterface;
  sis_AR?: ArchivoInterface;
}
