import {FieldValue, Timestamp} from '@firebase/firestore-types';
import {FirestoreUsuarioInterface} from '@manticore-labs/firebase-angular';

export interface UsuarioAbstractoProyectoInterface extends FirestoreUsuarioInterface {
  createdAt?: Timestamp | number | FieldValue;
  updatedAt?: Timestamp | number | FieldValue;
}

