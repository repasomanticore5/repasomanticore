import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaNoTienePermisosComponent } from './ruta-no-tiene-permisos.component';

describe('RutaNoTienePermisosComponent', () => {
  let component: RutaNoTienePermisosComponent;
  let fixture: ComponentFixture<RutaNoTienePermisosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RutaNoTienePermisosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaNoTienePermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
