import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_MENU_PRINCIPAL} from '../transloco/loader-menu-principal';
import {MenuItem} from 'primeng/api';

export const MENU_PRINCIPAL_MIGAS_PAN
  : (componente: any, items?: MenuItem) => MigaPanRuta = (componente: any) => {
  // Inicializar todos los parametros de ruta con '0'
  // let parametros: SRutaRolParametros = {
  //   // parametroRutaUno: '0',
  //   // parametroRutaDos: '0',
  //   // parametroRutaTres: '0',
  // parametros = ruta.parametros as SRutaRolParametros;
  const restoDeMigas: MenuItem[] = [];
  // };
  let ruta: any = {};
  if (componente) {
    if (componente.nombreServicio) {
      ruta = componente[componente.nombreServicio] as any;
    }
  }
  return {
    // path que va a ir en el RoutingModule
    id: 'menu-principal',
    // Nombre a visualizarse


    label: 'migaPan',
    expanded: true,

    // Aqui se debe de llenar el arreglo del path de esta ruta.
    // Se puede utilizar las migas de pan de la anterior ruta.
    routerLink: [
      // ...SUBMODULO_ROLES_MIGAS_PAN(componente).routerLink, // Se llama a la miga de pan PAPA de esta ruta.
      // parametros.parametroRutaTres as string, // parametro de ruta del ultimo parametro de ruta, en este caso "parametroRutaTres"
      '/menu-principal' // si existe un path al final se lo pone aqui
    ],
    scopeTransloco: NOMBRE_SCOPE_MENU_PRINCIPAL,  // si no se quiere internacionalizar poner un string vacio Ej: ''
    items: restoDeMigas.length > 0 ? restoDeMigas : undefined,
  };
};
