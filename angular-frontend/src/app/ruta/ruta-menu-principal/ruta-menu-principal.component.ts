import { Component, OnInit } from '@angular/core';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';
import {ObjetoInternacionalizacionFormulario} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_MENU_PRINCIPAL} from './transloco/loader-menu-principal';
import {MENU_PRINCIPAL_MIGAS_PAN} from './migas-pan/menu-principal-migas-pan';
import construirMigasPanRutaCustom from '../../funciones/construir-migas-pan-custom';
import {OpcionesMenuInterface} from '../../interfaces/opciones-menu.interface';

@Component({
  selector: 'app-ruta-menu-roles',
  templateUrl: './ruta-menu-principal.component.html',
  styleUrls: ['./ruta-menu-principal.component.scss']
})
export class RutaMenuPrincipalComponent implements OnInit {


  migasPan: MigaDePanInterface[] = [];

  rutaImagenAssets = 'assets/img/';

  rutaImagenPrincipal = this.rutaImagenAssets + '/menu-principal/imagen-principal.svg';

  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
    botonAtras: 'botonAtras',
    botonLimpiarFormulario: 'botonLimpiarFormulario',
    botonSiguiente: 'botonSiguiente',
    nombreScopeTransloco: NOMBRE_SCOPE_MENU_PRINCIPAL,
  };
  opcionesMenu: OpcionesMenuInterface [] = [
    // {
    //   texto: 'opcionesMenu.submoduloEmpresa.texto',
    //   descripcion: 'opcionesMenu.submoduloEmpresa.descripcion',
    //   imagen: '/submodulo-empresa/empresa/empresa.svg',
    //   url: ['/submodulo-empresa'],
    // },
    // {
    //   texto: 'opcionesMenu.submoduloArticulo.texto',
    //   descripcion: 'opcionesMenu.submoduloArticulo.descripcion',
    //   imagen: '/submodulo-articulo/articulo/imagen-principal.svg',
    //   url: ['/submodulo-articulo'],
    // },
    {
      texto: 'opcionesMenu.submoduloRoles.texto',
      descripcion: 'opcionesMenu.submoduloRoles.descripcion',
      imagen: '/submodulo-roles/rol/imagen-principal.svg',
      url: ['/submodulo-roles'],
    },
    // {
    //   texto: 'opcionesMenu.submoduloFacturacionElectronica.texto',
    //   descripcion: 'opcionesMenu.submoduloFacturacionElectronica.descripcion',
    //   imagen: '/submodulo-facturacion-electronica/menu-facturacion-electronica/imagen-principal.svg',
    //   url: ['/submodulo-facturacion-electronica'],
    // },
    // {
    //   texto: 'opcionesMenu.submoduloVendedores.texto',
    //   descripcion: 'opcionesMenu.submoduloVendedores.descripcion',
    //   imagen: '/submodulo-vendedores/menu-vendedores/imagen-principal.svg',
    //   url: ['/submodulo-vendedores'],
    // },
    // {
    //   texto: 'opcionesMenu.submoduloRecaudos.texto',
    //   descripcion: 'opcionesMenu.submoduloRecaudos.descripcion',
    //   imagen: '/submodulo-recaudos/menu-recaudos/imagen-principal.svg',
    //   url: ['/submodulo-recaudos'],
    // },
  ];

  constructor() { }

  ngOnInit(): void {
    this.migasPan = construirMigasPanRutaCustom(
      this,
      [
        MENU_PRINCIPAL_MIGAS_PAN
      ]
    );
  }

}
