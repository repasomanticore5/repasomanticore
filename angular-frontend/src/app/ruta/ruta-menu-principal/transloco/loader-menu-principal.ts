import {NOMBRE_MODULO} from '../../../modulos/ejemplo-nest/constantes/nombre-modulo';
import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';


export const NOMBRE_SCOPE_MENU_PRINCIPAL = NOMBRE_MODULO + 'MenuPrincipal';

export const SCOPE_MENU_PRINCIPAL = (http: HttpClient) => {
  const loaderMenuRoles = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/menu-principal/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_MENU_PRINCIPAL, loader: loaderMenuRoles};
};

export const LOADER_MENU_PRINCIPAL = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_MENU_PRINCIPAL,
  multi: true
};
