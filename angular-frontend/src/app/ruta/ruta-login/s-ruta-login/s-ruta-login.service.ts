import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {RutaComun} from '@manticore-labs/ng-2021';
import {DomSanitizer} from '@angular/platform-browser';
import {CargandoService} from '../../../servicios/cargando/cargando.service';
import {NotificacionService} from '../../../servicios/notificacion/notificacion.service';
import {MenuGeneralService} from '../../../servicios/menu-general/menu-general.service';
import {ArchivoPrincipalService} from '../../../servicios/archivos/archivo-principal.service';
import {ModalCrearEditarComunComponent} from '../../../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';
import {TAKE} from '../../../constantes/numero-registros-en-tabla/take';
import {REGISTROS_POR_PAGINA} from '../../../constantes/numero-registros-en-tabla/registros-por-pagina';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

@Injectable()
export class SRutaLoginService extends RutaComun<any,
  any,
  any,
  any> {
  constructor(
    public readonly _entrenadorService: ArchivoPrincipalService,
    public readonly _cargandoService: CargandoService,
    public readonly _notificacionService: NotificacionService,
    public matDialog: MatDialog,
    private readonly _menuGeneralService: MenuGeneralService,
    private readonly _archivoPrincipalService: ArchivoPrincipalService,
    public readonly _domSanitizer: DomSanitizer,
  ) {
    super(
      _entrenadorService,
      _cargandoService,
      _notificacionService,
      matDialog,
      () => [],
      'formularios.crearEditar.formularioCrearEditar.tituloCrear',
      'formularios.crearEditar.formularioCrearEditar.descripcionCrear',
      'formularios.crearEditar.formularioCrearEditar.tituloActualizar',
      'formularios.crearEditar.formularioCrearEditar.descripcionActualizar',
      () => [],
      () => [],
      () => {
        return [
          // {
          //   nombreCampo: 'nombreCampo', // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Nombre Campò'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return  valorCampo ; // Valor a mostrarse del campo, se pueden hacer transformaciones
          //   },
          // },
          // {
          //   nombreCampo: 'campoOpciones',  // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Opciones'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: ActivoInactivo, nombreCampo: string) => {
          //     switch (valorCampo) { // se puede dar diferentes nombres dependiendo del valor del campo
          //       case 'opcionUno':
          //         return 'Uno';
          //       case 'opcionDos':
          //         return 'Dos';
          //     }
          //   },
          // },
        ];
      },
      ModalCrearEditarComunComponent,
      TAKE,
      REGISTROS_POR_PAGINA,
      ActivoInactivo as { Activo: any, Inactivo: any },
      _menuGeneralService,
      _archivoPrincipalService,
      _domSanitizer
    );
  }
}
