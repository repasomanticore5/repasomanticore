import {CampoFormulario} from '@manticore-labs/ng-2021';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export class CrearRegistro {
  // Definir todos los campos dentro del FORMULARIO de creación
  // se debe igualar a undefined, OMITIR RELACIONES
  // nombreCampo?: CampoFormulario = undefined;
  nombres?: CampoFormulario = undefined;
  apellidos?: CampoFormulario = undefined;
  identificacionPais?: CampoFormulario = undefined;
  fechaNacimiento?: CampoFormulario = undefined;
  codigoPais?: CampoFormulario = undefined;
  telefono?: CampoFormulario = undefined;
  sexo?: CampoFormulario = undefined;
  email?: CampoFormulario = undefined;
}
