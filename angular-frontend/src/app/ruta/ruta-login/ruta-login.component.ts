import {Component, OnInit} from '@angular/core';
import {AuthProvider} from 'ngx-auth-firebaseui';
import {environment} from '../../../environments/environment';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthService} from '../../servicios/auth/auth.service';
import {UsuarioFirebaseInterface} from '../../servicios/auth/interfaces/usuario-firebase.interface';
import {UsuarioFirebaseCallbackLoginInterface} from '../../servicios/auth/interfaces/usuario-firebase-callback-login.interface';
import {MatDialog} from '@angular/material/dialog';
import {ModalRegistroUsuarioComponent} from '../../modal/modal-registro-usuario/modal-registro-usuario.component';
import {
  EventoCambioAutocomplete,
  ObjetoInternacionalizacionFormulario, SetearObjeto,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_PRINCIPAL} from '../../transloco/loader-principal';
import {REGISTRO_FORMULARIO} from './formularios/registro-formulario';
import {HttpClient} from '@angular/common/http';
import {PaisInterface} from '../../interfaces/pais.interface';
import {EntrenadorInterface} from '../../modulos/ejemplo-nest/interfaces/entrenador.interface';
import {AngularFireAuth} from '@angular/fire/auth';
import {NotificacionService} from '../../servicios/notificacion/notificacion.service';
import {TranslocoService} from '@ngneat/transloco';
import {CrearEntrenador} from '../../modulos/ejemplo-nest/ruta/ruta-entrenador/clases/crear-entrenador';
import {SRutaLoginService} from './s-ruta-login/s-ruta-login.service';
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {RegistroInterface} from './interfaces/registro.interface';
import {CrearRegistro} from './clases/crear-registro';

@Component({
  selector: 'app-ruta-login',
  templateUrl: './ruta-login.component.html',
  styleUrls: ['./ruta-login.component.scss']
})
export class RutaLoginComponent implements OnInit {
  providers: AuthProvider[] = environment.firebaseAuth.providers;
  registrationEnabled = environment.firebaseAuth.registrationEnabled;
  resetPasswordEnabled = environment.firebaseAuth.resetPasswordEnabled;
  guestEnabled = environment.firebaseAuth.guestEnabled;
  tosUrl = environment.firebaseAuth.tosUrl;
  privacyPolicyUrl = environment.firebaseAuth.privacyPolicyUrl;
  goBackURL = environment.firebaseAuth.goBackURL;

  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
    botonAtras: 'botonAtras',
    botonLimpiarFormulario: 'botonLimpiarFormulario',
    botonSiguiente: 'botonSiguiente',
    nombreScopeTransloco: NOMBRE_SCOPE_PRINCIPAL,
  };

  ocultarFormulario = false;
  botonAceptarModalDisabled = true;
  formularioCrearEditar?: TodosCamposValidados;

  constructor(
    public readonly firestore: AngularFirestore,
    public readonly authService: AuthService,
    public dialog: MatDialog,
    public _httpClient: HttpClient,
    public angularFireAuth: AngularFireAuth,
    public readonly _notificacionService: NotificacionService,
    private _translocoService: TranslocoService,
    private readonly _sRutaLoginService: SRutaLoginService,
  ) {
  }

  ngOnInit(): void {
    if (!this.authService.objetoAuth.usuarioSeguridad ) {
      console.warn({
        mensaje: 'Usuario aún no es creado'
      });
    }
    // this.angularFireAuth
    //   .authState
    //   .subscribe(
    //     (usuario) => {
    //       if (usuario) {
    //         const uid = usuario.uid;
    //         this.authService
    //           .buscarDatoUsuario(uid)
    //           .subscribe(
    //             (data: any) => {
    //               if (data[1] > 0){
    //                 console.info({mensaje: 'Usuario existe, no es necesario login'});
    //                 this.authService.logearse(usuario, data[0][0]);
    //               }else{
    //                 this.authService.logearse(usuario);
    //                 this.dialog.closeAll();
    //                 this.abrirModalRegistro();
    //               }
    //             },
    //             (error) => {
    //               console.error({
    //                 mensaje: 'Error consultando datos usuario',
    //                 error
    //               });
    //               this._notificacionService.anadir({
    //                 titulo: this._translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
    //                 detalle: this._translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
    //                 severidad: 'error'
    //               });
    //             }
    //           );
    //         // this.authService.logearse(usuario);
    //       } else {
    //         this.authService.desLogearse();
    //       }
    //     }
    //   );
  }

  abrirModalRegistro() {
    this.dialog.open(ModalRegistroUsuarioComponent, {
      data: {
        componente: this,
        camposFormulario: REGISTRO_FORMULARIO
      },
      closeOnNavigation: false,
      disableClose: true,
    });
  }

  exitoEnLogarse(event: UsuarioFirebaseCallbackLoginInterface) {
    // const usuario: UsuarioFirebaseInterface = event.user.toJSON();
    this.authService.logearse(event.user);
    if (event.additionalUserInfo) {
      if (event.additionalUserInfo.isNewUser) {
        this.abrirModalRegistro();
      } else {

      }
    } else {
      console.warn({mensaje: 'No tiene información adicional'});
    }
    // this._authService.objetoAuth.usuario.usuarioFirebase = usuario;
    // console.log('evento', event);
    // console.log('usuario', event.user.toJSON());
    // this.firestore
    //   .collection('usuario')
    //   .get()
    //   .subscribe(
    //     (data) => {
    //       console.log('HABLA', data);
    //       data.forEach(
    //         (a) => {
    //           console.log(a.data());
    //         }
    //       );
    //
    //     },
    //     (error) => {
    //       console.log(error);
    //     }
    //   );
  }

  errorEnLogearse(event: any) {
    // console.log(event);
  }

  formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete) {
    this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete) {
    if (evento.campoFormulario) {
      switch (evento.campoFormulario.nombreCampo) {
        case 'codigoPais': // nombres de cada uno de los campos autocomplete
          this.buscarAutocompleteCodigoPais(evento); // ejecutar una búsqueda con los servicios pertinentes
          break;
      }
    }
  }

  buscarAutocompleteCodigoPais(evento: EventoCambioAutocomplete) {

    this._httpClient
      .get('https://restcountries.eu/rest/v2/all?fields=name;numericCode;flag;alpha3Code;')
      .toPromise()
      .then(res => res as PaisInterface[])
      .then(data => {
        const arregloDatos = data;
        // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
        // const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
        const indiceEcuador = data.findIndex((p) => p.numericCode === '218');
        const ecuador = {...data[indiceEcuador]};
        data.splice(indiceEcuador, 1);
        data.unshift(ecuador);
        if (evento.campoFormulario.autocomplete) {
          if (Array.isArray(arregloDatos)) {
            evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
          } else {
            evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
          }
        }
        return data;
      });
  }

  formularioCrearEditarValido(estaValido: TodosCamposValidados): void {
    if (estaValido.valido) {
      this.botonAceptarModalDisabled = false;
      this.formularioCrearEditar = estaValido;
    } else {
      this.botonAceptarModalDisabled = true;
      this.formularioCrearEditar = undefined;
    }
  }
  crearEditar(){
    if (this.formularioCrearEditar && this.authService.objetoAuth.usuarioSeguridad){
      const camposCrear = this._sRutaLoginService.obtenerCampos<CrearRegistro>(
        CrearRegistro,
        this.formularioCrearEditar
      );
      const arregloPropiedades: SetearObjeto[] = [
        {
          nombreCampo: 'codigoPais',
          nombrePropiedadObjeto: 'numericCode'
        },
        {
          nombreCampo: 'sexo',
          nombrePropiedadObjeto: 'sexo'
        },
        // {
        //   nombreCampo: 'sisHabilitado',
        //   nombrePropiedadObjeto: 'sisHabilitado'
        // }
      ];
      camposCrear.objetoCrear = this._sRutaLoginService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );
      camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
      camposCrear.objetoCrear.uid = this.authService.objetoAuth.usuarioSeguridad.datosUsuario.uid;
      this._sRutaLoginService._cargandoService.habilitarCargando();
      const nuevoUsuario: RegistroInterface = {
        email: camposCrear.objetoCrear.email as string,
        nombres: camposCrear.objetoCrear.nombres as string,
        apellidos: camposCrear.objetoCrear.apellidos as string,
        identificacionPais: camposCrear.objetoCrear.identificacionPais as string,
        fechaNacimiento: new Date(camposCrear.objetoCrear.fechaNacimiento).toISOString().slice(0, 10) as string,
        codigoPais: camposCrear.objetoCrear.codigoPais as string,
        telefono: camposCrear.objetoCrear.telefono as string,
        sexo: camposCrear.objetoCrear.sexo as string,
        sisHabilitado: camposCrear.objetoCrear.sisHabilitado as ActivoInactivo,
        uid: camposCrear.objetoCrear.uid as string
      };
      this.authService
        .crearNuevoUsuario(nuevoUsuario)
        .subscribe(
          (usuarioCreado) => {
            this._sRutaLoginService._cargandoService.deshabilitarCargando();
            this.authService.logearse(this.authService.objetoAuth.usuarioSeguridad as any, usuarioCreado);
            this.dialog.closeAll();
          },
          (error) => {
            console.error({
              mensaje: 'Error al crear usuario',
              error
            });
            this._notificacionService.anadir({
              titulo: this._translocoService.translate('generales.toasters.toastErrorPuedeExista.title'),
              detalle: this._translocoService.translate('generales.toasters.toastErrorPuedeExista.body'),
              severidad: 'error'
            });
            this._sRutaLoginService._cargandoService.deshabilitarCargando();
          }
        );
    }

  }


}
