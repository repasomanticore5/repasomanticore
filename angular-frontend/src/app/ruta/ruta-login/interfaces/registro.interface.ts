import {ActivoInactivo} from '../../../enums/activo-inactivo';
import {EntidadComunProyecto} from '../../../abstractos/entidad-comun-proyecto';

export interface RegistroInterface extends EntidadComunProyecto {
  nombres: string;
  apellidos: string;
  identificacionPais: string;
  fechaNacimiento?: string;
  codigoPais: string;
  telefono: string;
  sexo: string;
  email: string;
  sisHabilitado: ActivoInactivo;
  uid: string;
}
