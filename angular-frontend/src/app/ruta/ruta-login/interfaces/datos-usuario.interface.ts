import {ActivoInactivo} from '../../../enums/activo-inactivo';
import {EntidadComunProyecto} from '../../../abstractos/entidad-comun-proyecto';

export interface DatosUsuarioInterface extends EntidadComunProyecto {
  nombres: string;
  apellidos: string;
  identificacionPais: string;
  fechaNacimiento?: Date;
  codigoPais: string;
  telefono: string;
  sexo: string;
  email: string;
  sisHabilitado: ActivoInactivo;
  uid: string;
}
