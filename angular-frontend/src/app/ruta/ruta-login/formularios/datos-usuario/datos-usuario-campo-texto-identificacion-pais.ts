import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';



export const DATOS_USUARIO_CAMPO_TEXTO_IDENTIFICACION_PAIS: (

  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {

    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.identificacionPais;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.identificacionPais) {
    //   validators.push(Validators.required);
    // }



    return {
      tipoCampoHtml: 'text',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.identificacionPais,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.identificacionPais ? 6 : 12,


      validators: [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(15),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'identificacionPais',
      nombreMostrar: 'formularios.crearEditar.campoIdentificacion.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoIdentificacion.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoIdentificacion.placeholderEjemplo',




      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoIdentificacion.nombreMostrar',

        minlength: 1,
        maxlength: 15,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
