import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {DATOS_USUARIO_CAMPO_TEXTO_APELLIDOS} from './datos-usuario-campo-texto-apellidos';
import {DATOS_USUARIO_CAMPO_TEXTO_NOMBRES} from './datos-usuario-campo-texto-nombres';
import {DATOS_USUARIO_CAMPO_TEXTO_EMAIL} from './datos-usuario-campo-texto-email';
import {DATOS_USUARIO_CAMPO_SELECT_SEXO} from './datos-usuario-campo-select-sexo';
import {DATOS_USUARIO_CAMPO_TEXTO_IDENTIFICACION_PAIS} from './datos-usuario-campo-texto-identificacion-pais';
import {DATOS_USUARIO_CAMPO_TEXTO_TELEFONO} from './datos-usuario-campo-texto-telefono';
import {DATOS_USUARIO_CAMPO_TEXTO_FECHA_NACIMIENTO} from './datos-usuario-campo-texto-fecha-nacimiento';
import {DATOS_USUARIO_CAMPO_AUTOCOMPLETE_CODIGO_PAIS} from './datos-usuario-campo-autocomplete-codigo-pais';

export const DATOS_USUARIO_FORMULARIO: (claseComponente: ModalComponente) => CampoFormulario[] =
  (claseComponente: ModalComponente) => {
    return [
        // AGREGUE SUS CAMPOS AQUI
        // EJEMPLO_CAMPO_TIPO_NOMBRE(claseComponente)
      DATOS_USUARIO_CAMPO_TEXTO_NOMBRES(claseComponente),
      DATOS_USUARIO_CAMPO_TEXTO_APELLIDOS(claseComponente),
      DATOS_USUARIO_CAMPO_TEXTO_EMAIL(claseComponente),
      DATOS_USUARIO_CAMPO_SELECT_SEXO(claseComponente),
      DATOS_USUARIO_CAMPO_TEXTO_FECHA_NACIMIENTO(claseComponente),
      DATOS_USUARIO_CAMPO_AUTOCOMPLETE_CODIGO_PAIS(claseComponente),
      DATOS_USUARIO_CAMPO_TEXTO_IDENTIFICACION_PAIS(claseComponente),
      DATOS_USUARIO_CAMPO_TEXTO_TELEFONO(claseComponente),
      // DATOS_USUARIO_CAMPO_TEXTO_UID(claseComponente),
    ];
  };
// yo man-lab-yo-ng:v2-campo-formulario
// Texto:
// NombreRuta NombreCampo texto
// NombreRuta NombreCampo texto --esFormulario true
// NombreRuta NombreCampo texto --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo texto --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Password:
// NombreRuta NombreCampo password
// NombreRuta NombreCampo password --esFormulario true
// NombreRuta NombreCampo password --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo password --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Busqueda:
// NombreRuta NombreCampo busqueda
// NombreRuta NombreCampo busqueda --nombrePrefijo prefijo
// Input Number:
// NombreRuta NombreCampo inputNumber
// NombreRuta NombreCampo inputNumber --esFormulario true
// NombreRuta NombreCampo inputNumber --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputNumber --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Input Mask:
// NombreRuta NombreCampo inputMask
// NombreRuta NombreCampo inputMask --esFormulario true
// NombreRuta NombreCampo inputMask --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputMask --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Input Switch:
// NombreRuta NombreCampo inputSwitch
// NombreRuta NombreCampo inputSwitch --esFormulario true
// NombreRuta NombreCampo inputSwitch --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputSwitch --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Autocomplete:
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Select:
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M"
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M" --esFormulario true
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M" "Ningún día de la semana" --esFormulario true
// NombreRuta NombreCampo select "Est. Civil Soltero=EstadoCivil.Soltero=SO,Est. Civil Casado=EstadoCivil.Casado=CA" --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo select "Est. Civil Soltero=EstadoCivil.Soltero=SO,Est. Civil Casado=EstadoCivil.Casado=CA" "Ningún estádo civil" --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo select "Est. Civil Soltero=EstadoCivil.Soltero=SO,Est. Civil Casado=EstadoCivil.Casado=CA" --esFormulario true --nombrePrefijo prefijo --esDependiente true
// NombreRuta NombreCampo select "Est. Civil Soltero=EstadoCivil.Soltero=SO,Est. Civil Casado=EstadoCivil.Casado=CA" "Ningún estádo civil" --esFormulario true --nombrePrefijo prefijo --esDependiente true

// DatosUsuario CodigoPais autocomplete --nombrePropiedad numericCode

