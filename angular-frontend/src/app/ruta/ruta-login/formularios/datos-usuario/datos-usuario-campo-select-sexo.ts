import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {Validators} from '@angular/forms';


export const DATOS_USUARIO_CAMPO_SELECT_SEXO: (

  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {

    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.sexo;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.sexo) {
    //   validators.push(Validators.required);
    // }



    return {
      tipoCampoHtml: 'select',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.sexo,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.sexo ? 6 : 12,


      validators: [
        Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(10),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'sexo',
      nombreMostrar: 'formularios.crearEditar.campoSexo.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoSexo.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoSexo.placeholderEjemplo',
      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoSexo.nombreMostrar',

      },
      select: {
        valoresSelect: [

          {
            sexo: 'M'
          },

          {
            sexo: 'F'
          },

          {
            sexo: 'O'
          },


        ],



        placeholderFiltro: 'formularios.crearEditar.campoSexo.placeholderFiltro',

        mensajeFiltroVacio: 'formularios.crearEditar.campoSexo.mensajeFiltroVacio',




        campoFiltrado: 'sexo',
        fnMostrarEnSelect: (campo: { sexo: any  }) => {


          if (campo.sexo === 'M') {
            return 'formularios.crearEditar.campoSexo.opcionM';
          }

          if (campo.sexo === 'F') {
            return 'formularios.crearEditar.campoSexo.opcionF';
          }

          if (campo.sexo === 'O') {
            return 'formularios.crearEditar.campoSexo.opcionO';
          }

          return '';
        },
        campoSeleccionado: 'sexo'
      },
      formulario: {},
      componente: claseComponente,
    };
  };
