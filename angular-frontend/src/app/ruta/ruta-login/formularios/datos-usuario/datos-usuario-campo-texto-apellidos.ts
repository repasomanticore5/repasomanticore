import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';



export const DATOS_USUARIO_CAMPO_TEXTO_APELLIDOS: (

  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {



    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.apellidos;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.apellidos) {
    //   validators.push(Validators.required);
    // }



    return {
      tipoCampoHtml: 'text',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.apellidos,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.apellidos ? 6 : 12,


      validators: [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(60),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'apellidos',
      nombreMostrar: 'formularios.crearEditar.campoApellidos.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoApellidos.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoApellidos.placeholderEjemplo',




      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoApellidos.nombreMostrar',

        minlength: 3,
        maxlength: 60,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
