import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';



export const DATOS_USUARIO_CAMPO_TEXTO_NOMBRES: (

  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {

    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.nombres;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.nombres) {
    //   validators.push(Validators.required);
    // }



    return {
      tipoCampoHtml: 'text',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.nombres,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.nombres ? 6 : 12,


      validators: [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(70),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: true, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'nombres',
      nombreMostrar: 'formularios.crearEditar.campoNombres.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoNombres.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoNombres.placeholderEjemplo',




      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoNombres.nombreMostrar',

        minlength: 3,
        maxlength: 70,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
