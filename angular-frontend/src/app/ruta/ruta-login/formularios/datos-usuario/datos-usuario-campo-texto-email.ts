import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';
import {RutaLoginComponent} from '../../ruta-login.component';



export const DATOS_USUARIO_CAMPO_TEXTO_EMAIL: (

  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {
    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.email;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.email) {
    //   validators.push(Validators.required);
    // }


    const componente = claseComponente.data.componente as RutaLoginComponent;
    const usuarioFirebase = componente.authService.objetoAuth.usuarioSeguridad?.datosUsuario;
    return {
      tipoCampoHtml: 'text',

      valorInicial: usuarioFirebase ? usuarioFirebase.email : '',

      valorActual: usuarioFirebase ? usuarioFirebase.email : '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.email,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.email ? 6 : 12,


      validators: [
        Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(10),
        // Validators.min(0),
        // Validators.max(100),
        Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: true, // Si es un campo opcional debe ser siempre 'true'

      disabled: true,
      asyncValidators: null,
      nombreCampo: 'email',
      nombreMostrar: 'formularios.crearEditar.campoEmail.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoEmail.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoEmail.placeholderEjemplo',




      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoEmail.nombreMostrar',

        // minlength: 2,
        // maxlength:10,
        // min:100,
        // max:0,
        mensajePattern: 'Ingrese un email válido',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
