import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';


export const DATOS_USUARIO_CAMPO_TEXTO_FECHA_NACIMIENTO: (
  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {

    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.fechaNacimiento;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.fechaNacimiento) {
    //   validators.push(Validators.required);
    // }


    return {
      tipoCampoHtml: 'date',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.fechaNacimiento,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.fechaNacimiento ? 6 : 12,


      validators: [
        // Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(10),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'fechaNacimiento',
      nombreMostrar: 'formularios.crearEditar.campoFechaNacimiento.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoFechaNacimiento.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoFechaNacimiento.placeholderEjemplo',


      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoFechaNacimiento.nombreMostrar',

        // minlength: 2,
        // maxlength:10,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      date: {
        selectionMode: 'single'
      },
      // inputMask: {
      //   mask: '9999-99-99',
      //   slotChar: '_',
      //   characterPattern: [],
      //   fnValidar: (campo, componente) => {
      //     // const numeros = campo.replaceAll('_', '').replaceAll('-', '');
      //     // return numeros.length >= 10;
      //     return true;
      //   }
      // },
      formulario: {},
      componente: claseComponente,
    };
  };
