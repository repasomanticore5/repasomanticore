import {CampoFormulario} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';
import {PaisInterface} from '../../../../interfaces/pais.interface';


export const DATOS_USUARIO_CAMPO_AUTOCOMPLETE_CODIGO_PAIS: (
  claseComponente: any) => CampoFormulario = (claseComponente: any) => {


  // SOLO USO SI ES FORMULARIO && Es campo del que dependen
  // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.codigoPais;
  // if (camposDependienteNoExisten) {
  //   valorCampo = undefined;
  // }


  // SOLO USO SI ES FORMULARIO && Es campo dependiente
  // const validators = [];
  // if (claseComponente.data.componente.camposRequeridos.codigoPais) {
  //   validators.push(Validators.required);
  // }


  return {
    tipoCampoHtml: 'autocomplete',

    valorInicial: '',

    valorActual: '',

    hidden: false,
    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // hidden: !claseComponente.data.componente.camposRequeridos.codigoPais,

    tamanioColumna: 6,
    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // tamanioColumna: claseComponente.data.componente.camposRequeridos.codigoPais ? 6 : 12,


    validators: [
      Validators.required,
      // Validators.minLength(2),
      // Validators.maxLength(10),
      // Validators.min(0),
      // Validators.max(100),
      // Validators.email,
      // Validators.pattern()
    ],
    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // validators,


    estaValido: true,

    disabled: false,
    asyncValidators: null,
    nombreCampo: 'codigoPais',


    nombreMostrar: 'formularios.crearEditar.campoCodigoPais.nombreMostrar',


    textoAyuda: 'formularios.crearEditar.campoCodigoPais.nombreMostrar',


    placeholderEjemplo: 'formularios.crearEditar.campoCodigoPais.nombreMostrar',


    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'formularios.crearEditar.campoCodigoPais.nombreMostrar',


    },
    autocomplete: {
      suggestions: [],
      field: 'name', // Nombre de la propiedad del objeto a visualizarse
      delay: 2000,
      inputId: 'numericCode', // Id a ser seleccionado
      emptyMessage: 'No hay registros',
      fnMostrarEnAutoComplete: (campo: PaisInterface) => {
        // Se puede concatenar los campos EJ: campo.nombreCampoRelacionUno + ' ' + campo.nombreCampoRelacionDos
        const html = `
                      <div class="row">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <img class="pais" src="${campo.flag}">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <p class="texto-pais">${campo.name}</p>
                        </div>
                      </div>
`;
        return html; // Nombre de la propiedad del objeto a visualizarse
      },
      texto: false
    },
    formulario: {},
    componente: claseComponente,
  };
};
