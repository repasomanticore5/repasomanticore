import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../constantes/nombre-modulo';

export const NOMBRE_SCOPE_NEST = NOMBRE_MODULO;

export const SCOPE_NEST = (http: HttpClient) => {
  const loaderNest = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-ejemplo-nest/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_NEST, loader: loaderNest};
};

export const LOADER_NEST = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_NEST,
  multi: true
};
