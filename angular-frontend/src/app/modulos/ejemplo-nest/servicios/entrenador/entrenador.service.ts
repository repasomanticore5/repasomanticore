import {Injectable} from '@angular/core';
import {EntrenadorModule} from './entrenador.module';
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {EntrenadorBusquedaDto} from '../../dto/entrenador-busqueda.dto';
import {HttpClient} from '@angular/common/http';
import {ServicioComunMlabs} from '@manticore-labs/ng-2021';
import {environment} from '../../../../../environments/environment';
import {NotificacionService} from '../../../../servicios/notificacion/notificacion.service';
import {ConfirmationService} from 'primeng/api';
import {TranslocoService} from '@ngneat/transloco';

@Injectable({
  providedIn: EntrenadorModule
})
export class EntrenadorService
  extends ServicioComunMlabs<EntrenadorInterface, EntrenadorBusquedaDto> {

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
    private readonly _confirmationService: ConfirmationService,
    public readonly translocoService: TranslocoService
  ) {
    super(
      'entrenador',
      _httpClient,
      EntrenadorBusquedaDto,
      'sisHabilitado',
      'id',
      environment.url,
      _notificacionService,
      _confirmationService,
      {
        crear: (clase: EntrenadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeCrear');
          return texto;
        },
        actualizar: (clase: EntrenadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeActualizar');
          return texto;
        },
        mensajeCancelacion: (clase: EntrenadorService) => {
          return {
            titulo: this.translocoService.translate('generales.modalConfirmacion.mensajeCancelarTitulo'),
            detalle: this.translocoService.translate('generales.modalConfirmacion.mensajeCancelarDescripcion')
          };
        },
        habilitar: (clase: EntrenadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeHabilitar');
          return texto;
        },
        aceptar: (clase: EntrenadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.aceptar');
          return texto;
        },
        cancelar: (clase: EntrenadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.cancelar');
          return texto;
        },
      }
    );
  }
}
