import {Injectable} from '@angular/core';
import {PokemonModule} from './pokemon.module';
import {PokemonInterface} from '../../interfaces/pokemon.interface';
import {PokemonBusquedaDto} from '../../dto/pokemon-busqueda.dto';
import {HttpClient} from '@angular/common/http';
import {ServicioComunMlabs} from '@manticore-labs/ng-2021';
import {NotificacionService} from '../../../../servicios/notificacion/notificacion.service';
import {ConfirmationService} from 'primeng/api';
import {environment} from '../../../../../environments/environment';
import {TranslocoService} from '@ngneat/transloco';

@Injectable({
  providedIn: PokemonModule
})
export class PokemonService
  extends ServicioComunMlabs<PokemonInterface, PokemonBusquedaDto> {

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
    private readonly _confirmationService: ConfirmationService,
    public readonly translocoService: TranslocoService
  ) {
    super(
      'pokemon',
      _httpClient,
      PokemonBusquedaDto,
      'sisHabilitado',
      'id',
      environment.url,
      _notificacionService,
      _confirmationService
    );
  }
}
