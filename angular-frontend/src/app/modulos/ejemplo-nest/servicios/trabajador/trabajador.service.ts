import {Injectable} from '@angular/core';
import {TrabajadorModule} from './trabajador.module';
import {TrabajadorInterface} from '../../interfaces/trabajador.interface';

import {TrabajadorBusquedaDto} from '../../dto/trabajador-busqueda.dto';
import {HttpClient} from '@angular/common/http';


import {ServicioComunMlabs} from '@manticore-labs/ng-2021';
import {NotificacionService} from '../../../../servicios/notificacion/notificacion.service';
import {ConfirmationService} from 'primeng/api';
import {environment} from '../../../../../environments/environment';
import {TranslocoService} from '@ngneat/transloco';


@Injectable({
  providedIn: TrabajadorModule
})
export class TrabajadorService

  extends ServicioComunMlabs<TrabajadorInterface, TrabajadorBusquedaDto> {
  constructor(
      private readonly _httpClient: HttpClient,
      private readonly _notificacionService: NotificacionService,
      private readonly _confirmationService: ConfirmationService,
      public readonly translocoService: TranslocoService
  ) {
    super(
        'trabajador',
        _httpClient,
        TrabajadorBusquedaDto,
        'sisHabilitado',
        'id',
        environment.url,
        _notificacionService,
        _confirmationService,
        

      {
        crear: (clase: TrabajadorService) => {
          const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeCrear');
          return texto;
        },
            actualizar: (clase: TrabajadorService) => {
        const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeActualizar');
        return texto;
      },
          mensajeCancelacion: (clase: TrabajadorService) => {
        return {
          titulo: this.translocoService.translate('generales.modalConfirmacion.mensajeCancelarTitulo'),
          detalle: this.translocoService.translate('generales.modalConfirmacion.mensajeCancelarDescripcion')
        };
      },
          habilitar: (clase: TrabajadorService) => {
        const texto = this.translocoService.translate('generales.modalConfirmacion.mensajeHabilitar');
        return texto;
      },
          aceptar: (clase: TrabajadorService) => {
        const texto = this.translocoService.translate('generales.modalConfirmacion.aceptar');
        return texto;
      },
          cancelar: (clase: TrabajadorService) => {
        const texto = this.translocoService.translate('generales.modalConfirmacion.cancelar');
        return texto;
      },
      }
        
    );

    
  }


}
