import {NgModule} from '@angular/core';
import {IMPORTS_MODULO_EJEMPLO_NEST} from './imports/imports-modulo-ejemplo-nest';
import {
  DECLARATIONS_MODULO_EJEMPLO_NEST
} from './imports/declarations-modulo-ejemplo-nest';
import {PROVIDERS_MODULO_EJEMLO_NEST} from './imports/providers-modulo-ejemplo-nest';


@NgModule({
  declarations: [
    ...DECLARATIONS_MODULO_EJEMPLO_NEST,
  ],
  imports: [
    ...IMPORTS_MODULO_EJEMPLO_NEST,
  ],
  providers: [
    ...PROVIDERS_MODULO_EJEMLO_NEST,
  ],
})
export class EjemploNestModule {
}
