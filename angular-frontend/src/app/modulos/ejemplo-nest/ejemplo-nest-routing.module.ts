import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RutaEntrenadorComponent} from './ruta/ruta-entrenador/ruta-entrenador.component';
import {RutaPokemonComponent} from './ruta/ruta-pokemon/ruta-pokemon.component';
import {ENTRENADOR_MIGAS_PAN} from './ruta/ruta-entrenador/migas-pan/entrenador-migas-pan';
import {POKEMON_MIGAS_PAN} from './ruta/ruta-pokemon/migas-pan/pokemon-migas-pan';
import {EntrenadorGuard} from './ruta/ruta-entrenador/guard/entrenador.guard';
import { TRABAJADOR_MIGAS_PAN } from './ruta/ruta-trabajador/migas-pan/trabajador-migas-pan';
import { RutaTrabajadorComponent } from './ruta/ruta-trabajador/ruta-trabajador.component';

const routes: Routes = [

  {
    path: ENTRENADOR_MIGAS_PAN(this).id,
    canActivate: [EntrenadorGuard],
    component: RutaEntrenadorComponent
  },
  {
    path: POKEMON_MIGAS_PAN(this).id,
    component: RutaPokemonComponent
  },

  {
    path: TRABAJADOR_MIGAS_PAN(this).id,
    component: RutaTrabajadorComponent
  },
  {
    path: '',
    redirectTo: ENTRENADOR_MIGAS_PAN(this).id,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjemploNestRoutingModule {
}
