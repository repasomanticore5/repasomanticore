
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {EntrenadorBusquedaDto} from '../../dto/entrenador-busqueda.dto';
import {Validators} from '@angular/forms';
import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {RutaPokemonComponent} from '../../ruta/ruta-pokemon/ruta-pokemon.component';



export const ENTRENADOR_CAMPO_TEXTO_TIPO: (

  claseComponente: ModalComponente) => CampoFormulario =
    (claseComponente: ModalComponente<RutaPokemonComponent, EntrenadorInterface, EntrenadorBusquedaDto>) => {



  const valorCampo = claseComponente
      .data
      .componente
      ._sRutaPokemonService
      .setearCampoEnFormulario(
          claseComponente,
          '_sRutaPokemonService',
          'tipo'
      );


  // SOLO USO SI ES FORMULARIO && Es campo del que dependen
  // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.tipo;
  // if (camposDependienteNoExisten) {
  //   valorCampo = undefined;
  // }


  // SOLO USO SI ES FORMULARIO && Es campo dependiente
  // const validators = [];
  // if (claseComponente.data.componente.camposRequeridos.tipo) {
  //   validators.push(Validators.required);
  // }



  return {
    tipoCampoHtml: 'text',

  valorInicial: valorCampo,

    valorActual: '',

  hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.tipo,

    tamanioColumna: 6,
    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // tamanioColumna: claseComponente.data.componente.camposRequeridos.tipo ? 6 : 12,


  validators: [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(60),
    // Validators.required,
    // Validators.minLength(2),
    // Validators.maxLength(10),
    // Validators.min(0),
    // Validators.max(100),
    // Validators.email,
    // Validators.pattern()
  ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


  estaValido: valorCampo ? true : false, // Si es un campo opcional debe ser siempre 'true'

    disabled: false,
    asyncValidators: null,
    nombreCampo: 'tipo',
    nombreMostrar: 'formularios.crearEditar.campoTipo.nombreMostrar',
    textoAyuda: 'formularios.crearEditar.campoTipo.textoAyuda',
    placeholderEjemplo: 'formularios.crearEditar.campoTipo.placeholderEjemplo',
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'formularios.crearEditar.campoTipo.nombreMostrar',
      minlength: 3,
      maxlength: 60,
      // min:100,
      // max:0,
      // mensajePattern: 'Solo letras y espacios',
    },
    formulario: {},
    componente: claseComponente,
  };
};
