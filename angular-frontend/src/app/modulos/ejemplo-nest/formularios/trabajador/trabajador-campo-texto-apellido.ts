
import {RutaTrabajadorComponent} from '../../ruta/ruta-trabajador/ruta-trabajador.component';
import {TrabajadorInterface} from '../../interfaces/trabajador.interface';
import {TrabajadorBusquedaDto} from '../../dto/trabajador-busqueda.dto';
import { ModalComponente, CampoFormulario } from '@manticore-labs/ng-2021';
import { MENSAJES_ERROR } from 'src/app/constantes/formulario/mensajes-error';
import { Validators } from '@angular/forms';



export const TRABAJADOR_CAMPO_TEXTO_APELLIDO: (
    
  claseComponente: ModalComponente) => CampoFormulario =
    (claseComponente: ModalComponente<RutaTrabajadorComponent, TrabajadorInterface, TrabajadorBusquedaDto>) => {
  


  const valorCampo = claseComponente
      .data
      .componente
      ._sRutaTrabajadorService
      .setearCampoEnFormulario(
          claseComponente,
          '_sRutaTrabajadorService',
          'apellido'
      );
  

  // SOLO USO SI ES FORMULARIO && Es campo del que dependen
  // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.apellido;
  // if (camposDependienteNoExisten) {
  //   valorCampo = undefined;
  // }


  // SOLO USO SI ES FORMULARIO && Es campo dependiente
  // const validators = [];
  // if (claseComponente.data.componente.camposRequeridos.apellido) {
  //   validators.push(Validators.required);
  // }
  


  return {
    tipoCampoHtml: 'text',
      
  valorInicial: valorCampo,
  
    valorActual: '',
    
  hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.apellido,
  
    tamanioColumna: 6,
    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // tamanioColumna: claseComponente.data.componente.camposRequeridos.apellido ? 6 : 12,
    

  validators: [
     Validators.required,
     Validators.minLength(5),
     Validators.maxLength(50),
    // Validators.min(0),
    // Validators.max(100),
    // Validators.email,
    // Validators.pattern()
  ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,
  

  estaValido: valorCampo ? true : false, // Si es un campo opcional debe ser siempre 'true'
  
    disabled: false,
    asyncValidators: null,
    nombreCampo: 'apellido',




  nombreMostrar: 'formularios.crearEditar.apellido.nombreMostrar',
  


  textoAyuda: 'formularios.crearEditar.apellido.textoAyuda',
  


  placeholderEjemplo: 'formularios.crearEditar.apellido.placeholderEjemplo',
  



    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
  
    nombreCampo: 'formularios.crearEditar.apellido.nombreMostrar',
    minlength: 5,
    maxlength: 40,
    
      // minlength: 2,
      // maxlength:10,
      // min:100,
      // max:0,
      // mensajePattern: 'Solo letras y espacios',
    },
    formulario: {},
    componente: claseComponente,
  };
};
