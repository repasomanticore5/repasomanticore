import {EntidadComunProyecto} from '../../../abstractos/entidad-comun-proyecto';
import {EntrenadorInterface} from './entrenador.interface';

export interface PokemonInterface extends EntidadComunProyecto {
  id?: number;
  entrenador?: EntrenadorInterface | number;
  nombre?: string;
  codigo?: string;
  pueblo?: string;
  // nombreCampo?: string; // Cada uno de los campos de la Entidad
  //
}
