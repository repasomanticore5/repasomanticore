import {EntidadComunProyecto} from '../../../abstractos/entidad-comun-proyecto';
export interface TrabajadorInterface extends EntidadComunProyecto{
 
  
  id?: number;
  
  // nombreCampo?: string; // Cada uno de los campos de la Entidad
  
  // nombreCampoRelacion?: CampoRelacionInterface | number;
  nombre?: string;
  apellido?: string;

}
