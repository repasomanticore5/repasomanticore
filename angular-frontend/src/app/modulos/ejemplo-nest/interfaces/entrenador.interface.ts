import {EntidadComunProyecto} from '../../../abstractos/entidad-comun-proyecto';

export interface EntrenadorInterface extends EntidadComunProyecto {
  id?: number | string;
  // nombreCampo?: string; // Cada uno de los campos de la Entidad
  nombre?: string;
  pueblo?: string;
  codigo?: string;
}
