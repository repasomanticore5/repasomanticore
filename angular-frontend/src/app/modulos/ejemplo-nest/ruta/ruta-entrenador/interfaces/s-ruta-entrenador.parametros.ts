import {RutaParametrosComun} from '@manticore-labs/ng-2021';

export interface SRutaEntrenadorParametros extends RutaParametrosComun {
    id?: string;
    // nombreCampo?: string; // Todos los campos deben ser STRING o ENUMS DE TIPO STRING
    // Normalmente es TODOS los que estén en el archivo EntrenadorBusquedaDto
    // nombreCampoRelacion?: string;
}
