import {EJEMPLO_NEST_MIGAS_PAN} from '../../../miga-pan-general/ejemplo-nest-migas-pan';
import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_ENTRENADOR_NEST} from '../transloco/loader-entrenador';

export const ENTRENADOR_MIGAS_PAN
  : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {
  return {
    id: 'gestion-entrenador',
    label: 'migaPan',
    routerLink: [
      ...EJEMPLO_NEST_MIGAS_PAN(componente).routerLink,
      'gestion-entrenador'
    ],
    scopeTransloco: NOMBRE_SCOPE_ENTRENADOR_NEST
  };
};
