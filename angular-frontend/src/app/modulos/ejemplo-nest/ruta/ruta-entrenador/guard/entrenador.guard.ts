import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {SeguridadService} from '../../../../../servicios/seguridad.service';

@Injectable()
export class EntrenadorGuard implements CanActivate {
  nombrePermiso = 'entrenadorBuscar';

  constructor(
    private readonly _seguridadService: SeguridadService,
    private readonly _router: Router,
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const tienePermiso = this._seguridadService.encontrarPermisoPorNombre(this.nombrePermiso);
    if (tienePermiso) {
      return true;
    } else {
      this._router.navigate(['/no-tiene-permisos']);
      return false;
    }
  }
}
