import {CampoFormulario} from '@manticore-labs/ng-2021';

export class CrearEntrenador {
  // Definir todos los campos dentro del FORMULARIO de creación
  // se debe igualar a undefined, OMITIR RELACIONES
  // nombreCampo?: CampoFormulario = undefined;
  nombre?: CampoFormulario = undefined;
  pueblo?: CampoFormulario = undefined;
  codigo?: CampoFormulario = undefined;
}
