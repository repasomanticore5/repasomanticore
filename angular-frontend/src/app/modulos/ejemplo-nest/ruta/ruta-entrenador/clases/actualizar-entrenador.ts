import {CampoFormulario} from '@manticore-labs/ng-2021';

export class ActualizarEntrenador {
  // Definir todos los campos dentro del FORMULARIO de edición
  // se debe igualar a undefined, OMITIR RELACIONES
  // nombreCampo?: CampoFormulario = undefined;
  nombre?: CampoFormulario = undefined;
  pueblo?: CampoFormulario = undefined;
  codigo?: CampoFormulario = undefined;
}
