import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_ENTRENADOR_NEST = NOMBRE_MODULO + 'Entrenador';

export const SCOPE_ENTRENADOR_NEST = (http: HttpClient) => {
  const loaderEntrenador = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-ejemplo-nest/entrenador/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_ENTRENADOR_NEST, loader: loaderEntrenador};
};

export const LOADER_ENTRENADOR_NEST = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_ENTRENADOR_NEST,
  multi: true
};
