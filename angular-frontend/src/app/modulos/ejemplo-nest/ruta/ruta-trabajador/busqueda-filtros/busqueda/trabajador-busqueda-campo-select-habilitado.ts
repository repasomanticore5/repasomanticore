import {CampoFormulario} from '@manticore-labs/ng-2021';
import { MENSAJES_ERROR } from 'src/app/constantes/formulario/mensajes-error';
import { ActivoInactivo } from 'src/app/enums/activo-inactivo';

export const TRABAJADOR_BUSQUEDA_CAMPO_SELECT_HABILITADO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [],
    asyncValidators: null,
    valorInicial: '',
    nombreCampo: 'sisHabilitado',


    
    nombreMostrar: 'formularios.busqueda.campoHabilitado.nombreMostrar',
    

  
    textoAyuda: 'formularios.busqueda.campoHabilitado.textoAyuda',
    

  
    placeholderEjemplo: 'formularios.busqueda.campoHabilitado.placeholderEjemplo',
    



    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
    
      nombreCampo: 'formularios.busqueda.campoHabilitado.nombreMostrar',
      
    },
    estaValido: true,
    tipoCampoHtml: 'select',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false,
    select: {
      valoresSelect: [
        {
        sisHabilitado: ActivoInactivo.Activo
        },
        {
        sisHabilitado: ActivoInactivo.Inactivo
        },
        {
        sisHabilitado: undefined
        },
      ],

      
      placeholderFiltro: 'formularios.busqueda.campoHabilitado.placeholderFiltro',
      

     
      mensajeFiltroVacio: 'formularios.busqueda.campoHabilitado.mensajeFiltroVacio',
      


      campoFiltrado: 'sisHabilitado',
      fnMostrarEnSelect: (campo: { sisHabilitado: ActivoInactivo | undefined }) => {
        if (campo.sisHabilitado === undefined) {

          
            return 'formularios.busqueda.campoHabilitado.opcionTodos';
          


        }
        if (campo.sisHabilitado === ActivoInactivo.Activo) {
          
          return 'formularios.busqueda.campoHabilitado.opcionActivo';
          
        }
        if (campo.sisHabilitado === ActivoInactivo.Inactivo) {
          
          return 'formularios.busqueda.campoHabilitado.opcionInactivo';
          
        }
        return '';
      },
      campoSeleccionado: 'sisHabilitado'
    }
  };
};
