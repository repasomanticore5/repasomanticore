import {CampoFormulario} from '@manticore-labs/ng-2021';
import { MENSAJES_ERROR } from 'src/app/constantes/formulario/mensajes-error';
import { RutaTrabajadorComponent } from '../../ruta-trabajador.component';

export const TRABAJADOR_BUSQUEDA_CAMPO_TEXTO_BUSQUEDA: (claseComponente: RutaTrabajadorComponent) => CampoFormulario = (claseComponente: RutaTrabajadorComponent) => {
  return {
    componente: claseComponente,
    validators: [],
    asyncValidators: null,
    valorInicial: undefined,
    nombreCampo: 'busqueda',

      
      nombreMostrar: 'formularios.busqueda.campoBusqueda.nombreMostrar',
      

    
      textoAyuda: 'formularios.busqueda.campoBusqueda.textoAyuda',
      

    
      placeholderEjemplo: 'formularios.busqueda.campoBusqueda.placeholderEjemplo',
      


    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
    
      nombreCampo: 'formularios.busqueda.campoBusqueda.nombreMostrar',
      
    },
    estaValido: true,
    hidden: false,
    tipoCampoHtml: 'text',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false
  };
};
