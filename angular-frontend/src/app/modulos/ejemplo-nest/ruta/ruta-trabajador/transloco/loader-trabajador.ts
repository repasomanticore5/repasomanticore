import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_TRABAJADOR = NOMBRE_MODULO + 'Trabajador';

export const SCOPE_TRABAJADOR = (http: HttpClient) => {
  //const loaderTrabajador = ['es', 'en'].reduce((acc: any, lang) => {
    //const nombreModulo =  NOMBRE_MODULO;
    const loaderTrabajador = ['es', 'en'].reduce((acc: any, lang) => {
      acc[lang] = () => http.get(`/assets/i18n/modulo-ejemplo-nest/trabajador/${lang}.json`).toPromise();
      return acc;
    }, {});

  return {scope: NOMBRE_SCOPE_TRABAJADOR, loader: loaderTrabajador};
};

export const LOADER_TRABAJADOR = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_TRABAJADOR,
  multi: true
};
