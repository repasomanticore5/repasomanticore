import {Injectable} from '@angular/core';
import {SRutaTrabajadorModule} from './s-ruta-trabajador.module';
import {SRutaTrabajadorParametros} from '../interfaces/s-ruta-trabajador.parametros';
import {TrabajadorBusquedaDto} from '../../../dto/trabajador-busqueda.dto';
import {TrabajadorInterface} from '../../../interfaces/trabajador.interface';
import {TrabajadorService} from '../../../servicios/trabajador/trabajador.service';
import {MatDialog} from '@angular/material/dialog';
import {TRABAJADOR_FORMULARIO} from '../../../formularios/trabajador/trabajador-formulario';
import {GRUPOS_FORMULARIO_BUSQUEDA} from './grupos-formulario-busqueda';
import {GRUPOS_FORMULARIO_CREAR_EDITAR} from './grupos-formulario-crear-editar';
import {DomSanitizer} from '@angular/platform-browser';
import {RutaComun} from '@manticore-labs/ng-2021';
import {CargandoService} from '../../../../../servicios/cargando/cargando.service';
import {NotificacionService} from '../../../../../servicios/notificacion/notificacion.service';
import {ActivoInactivo} from '../../../../../enums/activo-inactivo';
import {ModalCrearEditarComunComponent} from '../../../../../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';
import {TAKE} from '../../../../../constantes/numero-registros-en-tabla/take';
import {REGISTROS_POR_PAGINA} from '../../../../../constantes/numero-registros-en-tabla/registros-por-pagina';
import {MenuGeneralService} from '../../../../../servicios/menu-general/menu-general.service';
import {ArchivoPrincipalService} from '../../../../../servicios/archivos/archivo-principal.service';




@Injectable({
  providedIn: SRutaTrabajadorModule
})
export class SRutaTrabajadorService extends RutaComun<SRutaTrabajadorParametros,
  TrabajadorBusquedaDto,
  TrabajadorService,
  TrabajadorInterface> {
  transformarObjetosBusquedaDateAIsoStringBusquedaDto(arregloCamposBusquedaDate: any[]) {
    throw new Error('Method not implemented.');
  }
  constructor(
    public readonly _trabajadorService: TrabajadorService,
    public readonly _cargandoService: CargandoService,
    public readonly _notificacionService: NotificacionService,
    public matDialog: MatDialog,
    public readonly _menuGeneralService: MenuGeneralService,
    private readonly _archivoPrincipalService: ArchivoPrincipalService,
    public readonly _domSanitizer: DomSanitizer,
  ) {
    super(
      _trabajadorService,
      _cargandoService,
      _notificacionService,
      matDialog,
      TRABAJADOR_FORMULARIO,

        

        'formularios.crearEditar.formularioCrearEditar.tituloCrear',
        'formularios.crearEditar.formularioCrearEditar.descripcionCrear',
        'formularios.crearEditar.formularioCrearEditar.tituloActualizar',
        'formularios.crearEditar.formularioCrearEditar.descripcionActualizar',

        

      GRUPOS_FORMULARIO_BUSQUEDA,
      GRUPOS_FORMULARIO_CREAR_EDITAR,
      ()=> {
        return [
          // {
          //   nombreCampo: 'nombreCampo', // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Nombre Campò'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return  valorCampo ; // Valor a mostrarse del campo, se pueden hacer transformaciones
          //   },
          // },
          // {
          //   nombreCampo: 'campoOpciones',  // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Opciones'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: ActivoInactivo, nombreCampo: string) => {
          //     switch (valorCampo) { // se puede dar diferentes nombres dependiendo del valor del campo
          //       case 'opcionUno':
          //         return 'Uno';
          //       case 'opcionDos':
          //         return 'Dos';
          //     }
          //   },
          // },
        ]
      },
        ModalCrearEditarComunComponent,
        TAKE,
        REGISTROS_POR_PAGINA,
        ActivoInactivo as {Activo:any, Inactivo:any},
        _menuGeneralService,
        _archivoPrincipalService,
        _domSanitizer,
    );
    // Si se quiere que el stepper sea horizontal
    // this.vertical = false;

    // Path de propiedades para poder agrupar en la tabla

    // Ej1: {nombre:'Adrian', apellido:'Eguez', clase:'Clase uno'}
    // this.nombreAtributoAAgrupar = ['clase'];
    // En este caso solo se accede a -> registro.clase

    // Ej2: {nombre:'Adrian', apellido:'Eguez', campoRelacion:{campoNombre:'Clase'}}
    // this.nombreAtributoAAgrupar = ['campoRelacion', 'campoNombre'];
    // En este caso solo se accede a -> registro.campoRelacion.campoNombre
  }
}
