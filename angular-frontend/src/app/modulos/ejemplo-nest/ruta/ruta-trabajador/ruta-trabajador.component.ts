import {Component, OnInit} from '@angular/core';
import {SRutaTrabajadorService} from './s-ruta-trabajador/s-ruta-trabajador.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SRutaTrabajadorParametros} from './interfaces/s-ruta-trabajador.parametros';
import {TrabajadorBusquedaDto} from '../../dto/trabajador-busqueda.dto';
import {TrabajadorInterface} from '../../interfaces/trabajador.interface';
import {CrearTrabajador} from './clases/crear-trabajador';
import {ActualizarTrabajador} from './clases/actualizar-trabajador';
import {TRABAJADOR_FORMULARIO_BUSQUEDA} from './busqueda-filtros/busqueda/trabajador-formulario-busqueda';
import {MenuItem} from 'primeng/api';
import {TRABAJADOR_MIGAS_PAN} from './migas-pan/trabajador-migas-pan';
import {
  ArregloFiltroBusquedaFirestore,
  EventoCambioAutocomplete,
  EventoCambioFormulario, MigaDePanInterface, ObjetoBusquedaADto, ObjetoInternacionalizacionFormulario,
  SetearObjeto,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { NOMBRE_MODULO_ASSETS } from 'src/app/modulos/ejemplo-firebase/constantes/nombre-modulo-assets';
import { SeguridadService } from 'src/app/servicios/seguridad.service';
import { NOMBRE_SCOPE_TRABAJADOR } from './transloco/loader-trabajador';
import { ActivoInactivo } from 'src/app/enums/activo-inactivo';
import { EJEMPLO_NEST_MIGAS_PAN } from '../../miga-pan-general/ejemplo-nest-migas-pan';

@Component({
  selector: 'app-ruta-trabajador',
  templateUrl: './ruta-trabajador.component.html',
  styleUrls: ['./ruta-trabajador.component.scss']
})
export class RutaTrabajadorComponent implements OnInit {


  opciones = [
    
    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     const parametros: SRutaArchivoSecundarioParametros = {
    //       nombreIdentificador: 'trabajador',
    //       tipo: TipoArchivo.Archivo,
    //       idReferencial: id,
    //       tituloVista: this.translocoService.translate(this.objetoInternacionalizacion.nombreScopeTransloco + '.nombreRuta'),
    //     };
    //     return [
    //       ...ARCHIVO_SECUNDARIO_MIGAS_PAN(this, parametros).routerLink,
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'opcionesIrRuta.gestionarArchivos',
    //   rutaImagen: '/../comun/archivo.svg'
    //   permisoEditar: true,
    // },
    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     const parametros: SRutaArchivoSecundarioParametros = {
    //       nombreIdentificador: 'trabajador',
    //       tipo: TipoArchivo.Imagen,
    //       idReferencial: id,
    //       tituloVista: this.translocoService.translate(this.objetoInternacionalizacion.nombreScopeTransloco + '.nombreRuta'),
    //     };
    //     return [
    //       ...ARCHIVO_SECUNDARIO_MIGAS_PAN(this, parametros).routerLink,
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'opcionesIrRuta.gestionarImagenes',
    //   rutaImagen: '/../comun/imagen.svg'
    //   permisoEditar: true,
    // },
    


    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     return [
    //     ...TRABAJADOR_MIGAS_PAN(this).routerLink, // RUTA ACTUAL
    //     id,  // ids necesarios
    //     'termina-ruta' // opcional path final
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'Otra acción de ...',
    //   rutaImagen: '/trabajador/imagen-principal.svg'
  //   permisoEditar: true,
    // }
  ];

  camposFormularioBusqueda = TRABAJADOR_FORMULARIO_BUSQUEDA;
  valorBusqueda = '';
  formularioBusqueda?: TodosCamposValidados;
  botonAceptarModalDisabled = true;

  formularioCrearEditar?: TodosCamposValidados;
  sistemaLogisticoSeleccionado?: TrabajadorInterface;

  stepperActualBusqueda = 0;

  nombreServicio = '_sRutaTrabajadorService';

  // Usar cuando se necesite ocultar campos que dependan de otros:

  // ocultarFormulario = false;
  // camposRequeridos = {
  //   nombreCampoDependiendeUno: false,
  //   nombreCampoDependiendeDos: false,
  // };

  // Si existen campos AUTOGENERADOS en estas tablase se necesitarian estas variables

  // botonAceptarModalAutogeneradoDisabled = false;
  //
  // camposAutogeneradosAGuardar: CampoFormulario[] = [];
  //
  // arregloAutogenerado: { arregloDatos: CampoAutogenerado[]; grupoFormulario: GrupoFormulario[] } = {
  //   arregloDatos: [],
  //   grupoFormulario: [],
  // };

  migasPan: MigaDePanInterface[] = [];

  rutaImagenAssets = 'assets/img/' + NOMBRE_MODULO_ASSETS

  rutaImagenPrincipal = this.rutaImagenAssets + '/trabajador/imagen-principal.svg'

  
  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
        botonAtras: 'botonAtras',
        botonLimpiarFormulario: 'botonLimpiarFormulario',
        botonSiguiente: 'botonSiguiente',
        nombreScopeTransloco: NOMBRE_SCOPE_TRABAJADOR,
      }
  

  nombreRuta = 'trabajador';

  
  constructor(
    public readonly _sRutaTrabajadorService: SRutaTrabajadorService,
    private readonly _activatedRoute: ActivatedRoute,
    public readonly _router: Router,
    public readonly _seguridadService: SeguridadService,
  
    public readonly translocoService: TranslocoService,
  

  
  ) {
  }



  columnasPrimeTable = [
    // {
    //   field: 'nombreCampo',
    //   header: 'Nombre a mostrarse',
    //   posicion: 'text-center',
    //   tamanio: '60%', // tamaño en porcentaje de la columna
    //   fnMostrar: (valor: string) => {
    //   return valor;
    // },
    //     fnMostrar: (valor:TipoValor)=>{
    //       switch (valor) {
    //         case TipoValor.ValorUno:
    //           return 'Valor de uno';
    //         case TipoValor.ValorDos:
    //           return 'Valor de dos';
    //         default:
    //           return valor;
    //       }
    //     },
    // },

    // {
    //   field: '',
    //   header: 'sis_IM',
    //   posicion: 'text-center',
    //   tamanio: '15%', // tamaño en porcentaje de la columna
    //   fnMostrarImagen: (entrenador: TrabajadorInterface) => {
    //     if (entrenador.sis_IM) {
    //       return this._sRutaTrabajadorService._domSanitizer.bypassSecurityTrustResourceUrl(`data:${trabajador.sis_IM.mimetype};base64, ${trabajador.sis_IM.buffer}`);
    //     } else {
    //       return '';
    //     }
    //   }
    // },
   
    
    {
      field: 'nombre',
      header: 'nombre',
      posicion: 'text-center',
      tamanio: '15%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'apellido',
      header: 'apellido',
      posicion: 'text-center',
      tamanio: '15%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    
    {
      field: 'sisHabilitado',
    
    header: 'habilitado',
    
  posicion: 'text-left',
      tamanio: '20%'
},
  {
    field: 'id',
    
    header: 'acciones',
    
    posicion: 'text-right',
        tamanio: '20%'
  }
];

  ngOnInit(): void {
    this._obtenerParametros();
  }

  private _obtenerParametros(): void {
    this._sRutaTrabajadorService
      .obtenerYGuardarParametrosRuta(this._activatedRoute)
      .subscribe(
        (parametros: SRutaTrabajadorParametros) => {
          const busqueda = this._sRutaTrabajadorService
            .setearParametrosDeBusqueda(parametros, TrabajadorBusquedaDto);
            
            // if (parametros.nombreCampoRelacion && busqueda) {
            //   busqueda.nombreCampoRelacion = +parametros.nombreCampoRelacion;
            // }
            
            
          this._sRutaTrabajadorService
            .busquedaDto = busqueda;
          this._sRutaTrabajadorService
              .construirMigasPan(
                  this,
                  [
                    // MODULO_MIGAS_PAN,
                    EJEMPLO_NEST_MIGAS_PAN,
                    TRABAJADOR_MIGAS_PAN,
                  ],
              );
          this.buscarSuscrito();
        }
      );
  }

  buscarConFiltros(recargar?: any): void {
    

    
    if (this._sRutaTrabajadorService.busquedaDto) {
      this._sRutaTrabajadorService.busquedaDto.skip = 0;
      this._sRutaTrabajadorService.first = 0;
    }

      
      this.buscarSuscrito();
      

    
    
  }

  buscarSuscrito(recargar?: any){
    this._sRutaTrabajadorService._cargandoService.habilitarCargando();
    this._sRutaTrabajadorService
        .buscar()
        .subscribe(
            (data) => {
              this._sRutaTrabajadorService.registrosPagina = data[0].length;

              

              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaTrabajadorService.busquedaDto,
                error
              });
              this._sRutaTrabajadorService._notificacionService.anadir({

                
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                

                severidad: 'error'
              });
              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
            }
        );
  }

  // Usar para generar campos de formulario dinamicos

  // generarFormulario() {
  //   this.mostrarFormularioDePrueba = false;
  //   const arregloDatos: CampoAutogenerado[] = [];
  //   const grupoFormulario: GrupoFormulario[] = [];
  //   // crear los campos autogenerados "arregloDatos" y
  //   // AL MENOS debe existir un "grupoFormulario" que contenga a todos los campos para una visualización sencilla
  //   const respuestaAutogenerado = this._sRutaArticuloAtributoService
  //       .generarFormularioAutogenerado(
  //           arregloDatos,
  //           MENSAJES_ERROR,
  //           this,
  //           grupoFormulario
  //       )
  //   this.arregloFormulario.arregloValores = respuestaAutogenerado.arregloValores;
  //   this.arregloFormulario.grupo = respuestaAutogenerado.grupo;
  //   this.arregloFormulario.campos = respuestaAutogenerado.campos;
  //   this.mostrarFormularioDePrueba = true;
  // }

  formularioValidoBusqueda(formularioBusquedaValido: TodosCamposValidados): void {
    this.formularioBusqueda = formularioBusquedaValido;
    if (formularioBusquedaValido.valido) {
      this._sRutaTrabajadorService.setearCamposBusquedaValidos(
          formularioBusquedaValido,
          TrabajadorBusquedaDto
      );
      // Transforma objetos a valores, si no hay objetos en la busqueda
      // como un autocomplete o select por ejemplo, entonces dejar vacío.
      const arregloCamposBusquedaConObjeto: ObjetoBusquedaADto[] = [
        {
          nombreCampoEnBusqueda: 'sisHabilitado',
          nombreCampoEnDto: 'sisHabilitado'
        },
        // {
        //   nombreCampoEnBusqueda: 'campoSelectOAutocomplete',
        //   nombreCampoEnDto: 'nombreCampoSelectOAutocomplete'
        // },
      ];
      this._sRutaTrabajadorService.transformarObjetosBusquedaABusquedaDto(
        arregloCamposBusquedaConObjeto
      );
      

      
    }
  }

  cambioCampoBusqueda(eventoCambioCampoBusqueca: EventoCambioFormulario): void {
    const campoBusquedaActualizado = eventoCambioCampoBusqueca.campoFormulario;
    if (campoBusquedaActualizado) {
      this._sRutaTrabajadorService
        .setearCamposBusquedaAUndefined(
            campoBusquedaActualizado,
          TrabajadorBusquedaDto
        );
    }

  }

  formularioBusquedacambioCampoAutocomplete(evento: EventoCambioAutocomplete){
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete,
                                          registro?: TrabajadorInterface): void {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  // seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete){
  //   if(evento.campoFormulario){
      // switch (evento.campoFormulario.nombreCampo) {
      //   case 'nombreCampo': // nombres de cada uno de los campos autocomplete
      //     // this.buscarAutocompleteNombreCampo(evento); // ejecutar una búsqueda con los servicios pertinentes
      //     break;
      // }
    // }
  // }

  // buscarAutocompleteNombreCampo(evento: EventoCambioAutocomplete) {
  //   const busqueda: NombreCampoBusquedaDto = {
  //     nombreCampo: evento.query,
  //   };
  //   this._nombrCampoService
  //       .buscar(busqueda)
  //       .toPromise()
  //       .then(res => res as [NombreCampoInterface[], number])
  //       .then(data => {
  //         const arregloDatos = data[0];
  //         // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
  //         const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
  //         if (evento.campoFormulario.autocomplete) {
  //           if (Array.isArray(arregloDatos)) {
  //             evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
  //           } else {
  //             evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
  //           }
  //         }
  //         return data;
  //       });
  // }

  formularioCrearEditarValido(estaValido: TodosCamposValidados,
                              registro?: TrabajadorInterface): void {
    this._sRutaTrabajadorService.setearFormularioEnModal(estaValido);
    if (estaValido.valido) {
      this.formularioCrearEditar = estaValido;
      this.botonAceptarModalDisabled = false;
    } else {
      this.botonAceptarModalDisabled = true;
    }
  }

  formularioCrearEditarCambioCampo(eventoCambioCampo: EventoCambioFormulario,
                                   registro?: TrabajadorInterface): void {
    if (eventoCambioCampo.campoFormulario) {
        // Esta parte del codigo solo se debe de utilizar
        // si existe cambios de estructura del formulario
        // EJ: si selecciona el valor X de un campo entonces muestro tales campos
        // const formularioValido = {
        //   valido: false,
        //   camposFormulario: [eventoCambioCampo.campoFormulario],
        // };
        // this._sRutaTrabajadorService.setearFormularioEnModal(formularioValido);
        // switch (eventoCambioCampo.campoFormulario.nombreCampo) {
        //   case 'nombreCampo':
        //     if (registro) {
        //       this.actualizarValidez(
        //           eventoCambioCampo.valor,
        //           eventoCambioCampo.campoFormulario.nombreCampo,
        //           registro
        //       );
        //     } else {
        //       this.actualizarValidez(
        //           eventoCambioCampo.valor,
        //           eventoCambioCampo.campoFormulario.nombreCampo
        //       );
        //     }
        //     break;
        //   default:
        //     break;
        // }

    }
  }

//   actualizarValidez(
//       valorCampoValidez: any,
//       nombrePropiedadValidez: string,
//       registro?: any,
// ) {
//     // Los campos definidos "nombrePropiedad" cuando tengan un valor != undefined van a activar
//     // los campos que están en "nombresCamposRequeridosQueAfecta"
//     const camposAEstablecerValidezArreglo: CamposEstablecerValidez[] = [
//       {
//         nombrePropiedad: 'nombreCampo',
//         nombresCamposRequeridosQueAfecta: ['campoUno', 'campoDos']
//       },
//     // También hay el caso en donde al mostrar unos campos se oculten otros:
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoTres', 'campoCuatro'],
//         nombresCamposRequeridosAOcultar: ['campoCinco', 'campoSeis'],
//         valorPropiedad: 'valorTresCuatro',
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoCinco', 'campoSeis'],
//         nombresCamposRequeridosAOcultar: ['campoTres', 'campoCuatro'],
//         valorPropiedad: 'valorCincoSeis',
//       },
//     ];
//
//     this._sRutaTrabajadorService.establecerValidezDeFormulario<RutaTrabajadorComponent>(
//         this,
//         camposAEstablecerValidezArreglo,
//         valorCampoValidez,
//         nombrePropiedadValidez,
//         undefined,
//         registro,
//     );
//   }


  abrirModal(
      registro: TrabajadorInterface
  ){
    // Si se tienen campos dependientes se deben de
    // activarlos antes de editarlos
    // if(camposRequeridos.campoDependeUno){
    //   camposRequeridos.nombreCampoDependienteUno = true;
    // }
    // if(camposRequeridos.campoDependeDos){
    //   camposRequeridos.nombreCampoDependienteDos = true;
    // }
    const respuesta: any = this._sRutaTrabajadorService.abrirModal(this, registro);
    console.info(respuesta);
      


    // En el caso de cammpos AUTOGERENADOS se habilita esta parte del codigo

    // const respuesta = this._sRutaTrabajadorService._trabajadorService.generarFormulario(registro);
    // respuesta.arregloDatos = respuesta
    //     .arregloDatos
        // .map( // EJEMPLO DE IMPLEMENTACION:
        //     (aD) => {
        //       aD.valorInicial = registro.catValor;
        //       aD.required = true;
        //       aD.estaValido = false;
        //       aD.fnValidarInputMask = (campo, componente) => {
        //         if (campo) {
        //           return campo.length > 0
        //         } else {
        //           return true;
        //         }
        //       };
        //       return aD;
        //     }
        // );
    // const respuestaGrupoCampo = this._sRutaTrabajadorService
    //     .generarFormularioAutogenerado(
    //         respuesta.arregloDatos,
    //         MENSAJES_ERROR,
    //         this,
    //         respuesta.grupoFormulario
    //     );
    // this.abrirModalPrevisualizarTablaAutogenerado(respuestaGrupoCampo, false, registro);

  }

  // abrirModalPrevisualizarTablaAutogenerado(
  //     arregloDeDatos: {
  //   campos: (claseComponente: any) => CampoFormulario[],
  //       grupo: () => GrupoFormulario[],
  //       arregloValores: { nombre: string, campos: string[] }[]
  // },
  // mostrarTabla = false,
  //     registro?: TrabajadorInterface) {
  //   this.botonAceptarModalAutogeneradoDisabled = true;
  //   const dialogRef = this.matDialog.open(
  //       ModalFormularioAutogeneradoComponent, // Modal de autogenerado
  //       {
  //         data: {
  //           arregloFormulario: arregloDeDatos,
  //           componente: this,
  //           mostrarTabla: mostrarTabla,
  //           registro,
  //         }
  //       }
  //   );
  // }

  // cambioCampoAutogenerado(eventoCambioCampo: EventoCambioFormulario) {
  //
  // }

  // formularioValidoAutogenerado(estaValido: TodosCamposValidados, registro?: TrabajadorInterface) {
  //   if (estaValido.valido) {
  //     this.camposAutogeneradosAGuardar = estaValido
  //         .camposFormulario
  //         .filter((cF) => cF.estaValido && cF.valorActual)
  //     this.botonAceptarModalAutogeneradoDisabled = false;
  //   } else {
  //     this.botonAceptarModalAutogeneradoDisabled = true;
  //   }
  // }

  // aceptoFormularioAutogenerado(registro?: ArticuloCatalogoInterface) {
  //   if (registro) {
  //     this.editarRegistro(registro);
  //   } else {
  //     this.guardarNuevosCampos()
  //   }
  // }

  // Implementacion de guardado de campos auto generados

  // guardarNuevosCampos() {
  //
  // }

  // Implementacion de ediciion de campo autogenerado

  // editarRegistro(registro: ArticuloCatalogoInterface) {
  //
  // }

  // cambioStepperEnAutogenerado(indice: number) {
  //
  // }

  crearEditar(
    registro?: TrabajadorInterface,
  ): void {
    if (registro) {
      this.editar(registro);
    } else {
      this.crear();
    }
  }

  crear(): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaTrabajadorService.obtenerCampos<CrearTrabajador>(
        CrearTrabajador,
        this.formularioCrearEditar
      );
      // Transforma objetos a valores, si no hay objetos en el formulario
      // como un autocomplete por ejemplo, se debe dejar vacío.
      const arregloPropiedades: SetearObjeto[] = [
        // {
        //   nombreCampo: 'sisHabilitado',
        //   nombrePropiedadObjeto: 'sisHabilitado'
        // }
      ];
      camposCrear.objetoCrear = this._sRutaTrabajadorService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );


      // Setear campos extra
      // Ej:
      // camposCrear.objetoCrear.habilitado = 1;
      // Ej: Relación
      // if (this._sRutaTrabajadorService.busquedaDto) {
      //   camposCrear.objetoCrear.nombreCampoRelacion = this._sRutaTrabajadorService.busquedaDto.nombreCampoRelacion as number;
      // }
      
      camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
      
      //console.log(camposCrear.objetoCrear)

      if (this._sRutaTrabajadorService.busquedaDto) {
        this._sRutaTrabajadorService._cargandoService.habilitarCargando();
        const crear$ = this._sRutaTrabajadorService
          ._trabajadorService
          
          .crear(
              camposCrear.objetoCrear
          )
          
          
          
          .pipe(
              this._sRutaTrabajadorService.buscarDeNuevo('id')
          ) as Observable<TrabajadorInterface>;
          
        crear$
          .subscribe(
            (nuevoRegistro) => {
              nuevoRegistro.habilitado = true;
              this._sRutaTrabajadorService.arregloDatos.unshift(nuevoRegistro);
              this._sRutaTrabajadorService.arregloDatosFiltrado.unshift(nuevoRegistro);
              this._sRutaTrabajadorService.matDialog.closeAll();
              this._sRutaTrabajadorService._notificacionService.anadir({

                
                titulo: this.translocoService.translate('generales.toasters.toastExitoCrear.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoCrear.body', {nombre: nuevoRegistro.nombre}),
                

                severidad: 'success'
              });
              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
              
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaTrabajadorService.busquedaDto,
                error
              });
              this._sRutaTrabajadorService._notificacionService.anadir({
                
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                

                severidad: 'error'
              });
              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }

  editar(
    registro: TrabajadorInterface,
  ): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaTrabajadorService.obtenerCampos<ActualizarTrabajador>(
        ActualizarTrabajador,
        this.formularioCrearEditar,
        true
      );
      const arregloPropiedades: SetearObjeto[] = [];
      camposCrear.objetoCrear = this._sRutaTrabajadorService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );
      if (registro.id) {
        this._sRutaTrabajadorService._cargandoService.habilitarCargando();
        const actualizar$ = this._sRutaTrabajadorService
          ._trabajadorService
          
              .actualizar(
                  camposCrear.objetoCrear,
                  registro.id
              )
              .pipe(
                  this._sRutaTrabajadorService.buscarDeNuevo('id', registro.id)
              ) as any as Observable<TrabajadorInterface>
          
          

        actualizar$
          .subscribe(
            (registroEditado) => {
              registroEditado.habilitado = registro.habilitado;
              const indice = this._sRutaTrabajadorService.arregloDatos
                .findIndex(
                  (a: TrabajadorInterface) => a.id === registro.id
                );
              this._sRutaTrabajadorService.arregloDatos[indice] = registroEditado;
              const indiceArregloFiltrado = this._sRutaTrabajadorService.arregloDatosFiltrado
                .findIndex(
                  (a: TrabajadorInterface) => a.id === registro.id
                );
              this._sRutaTrabajadorService.arregloDatosFiltrado[indiceArregloFiltrado] = registroEditado;
              this._sRutaTrabajadorService.matDialog.closeAll();
              this._sRutaTrabajadorService._notificacionService.anadir({

                
                titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: registroEditado.nombre}),
                

                severidad: 'success'
              });
              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
              
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaTrabajadorService.busquedaDto,
                error
              });
              this._sRutaTrabajadorService._notificacionService.anadir({

                
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                

                severidad: 'error'
              });
              this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }

  // irRutaArchivos(tipo: 'imagen' | 'archivo', registro: TrabajadorInterface) {
  //   const ruta = [LLENAR_RUTA, registro.id, tipo, LLENAR_NOMBRE_IDENTIFICADOR, `Gestionar ${tipo} de ..... ${registro.nombreCampo}`];
  //   this._router.navigate(ruta);
  // }

  // abrirFormularioCrearArchivo(tipo: string, registro: TrabajadorInterface) {
  //   const formulario = ARTICULO_ARCHIVO_FORMULARIO;
  //     
  //     const rutaComunInternacionalizacion = 'formularios.crearEditar.formularioCrearEditar.';
  //     
  //     this._sRutaTrabajadorService.abrirModalArchivos(
  //       this,
  //       formulario,
  //         
  //       rutaComunInternacionalizacion + (tipo === TipoArchivo.Imagen ? 'tituloCrearImagen' : 'tituloCrearArchivo'),
  //       rutaComunInternacionalizacion + (tipo === TipoArchivo.Imagen ? 'descripcionCrearImagen' : 'descripcionCrearArchivo'),
  //         
  //       tipo,
  //       registro
  //   );
  //
  // }

  // formularioArchivoCambio(eventoCambioCampo: EventoCambioFormulario, registro: TrabajadorInterface, tipo: TipoArchivo) {
  //   // campo cambio
  // }
  //
  // formularioArchivoValido(estaValido: TodosCamposValidados, registro: TrabajadorInterface, tipo: TipoArchivo) {
  //   this.guardarFormularioValido(estaValido, registro, tipo);
  // }
  //
  // guardarFormularioValido(estaValido: TodosCamposValidados, registro: TrabajadorInterface, tipo: TipoArchivo) {
  //   if (estaValido.valido) {
  //     this._sRutaTrabajadorService.formularioArchivo = estaValido;
  //     this.botonAceptarModalDisabled = false;
  //   } else {
  //     this.botonAceptarModalDisabled = true;
  //   }
  // }

  // crearArchivo(tipo: TipoArchivo | string, registro: TrabajadorInterface) {
  //     this._sRutaTrabajadorService
  //         .crearArchivo(tipo, registro, 'trabajador')
  //         .subscribe(
  //             (nuevoRegistro) => {
  //               if (tipo === TipoArchivo.Imagen) {
  //                 this._sRutaTrabajadorService.archivoPrincipalActual = nuevoRegistro;
  //                 registro.sis_IM = nuevoRegistro;
  //               }
  //               if (tipo === TipoArchivo.Archivo) {
  //                 this._sRutaTrabajadorService.archivoPrincipalActual = nuevoRegistro;
  //                 registro.sis_AR = nuevoRegistro;
  //               }
  //               this._sRutaTrabajadorService.matDialog.closeAll();
  //               this._sRutaTrabajadorService._notificacionService.anadir({
  //                   
  //                     titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
  //                     detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: 'Trabajador'}),
  //                     severidad: 'success'
  //                 
  //
  //               });
  //               this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
  //             },
  //             (error) => {
  //               console.error({
  //                 mensaje: 'Error cargando registros',
  //                 data: this._sRutaTrabajadorService.busquedaDto,
  //                 error
  //               });
  //               this._sRutaTrabajadorService._notificacionService.anadir({
  //                   
  //                     titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
  //                     detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
  //                     severidad: 'error'
  //                 
  //               });
  //               this._sRutaTrabajadorService._cargandoService.deshabilitarCargando();
  //             }
  //         );
  // }



  // abrirModalSeleccionarGeolocalizacion() {
  //   const dataModal: DataSeleccionarGeolocalizacion = {
  //     componente: this,
  //     tituloCrear: this.translocoService.translate('generales.mapa.tituloSeleccionar'),
  //     descripcionCrear: this.translocoService.translate('generales.mapa.descripcionSeleccionar')
  //   }
  //   const respuesta: MatDialogRef<any> = this._sRutaTrabajadorService.matDialog.open(
  //       ModalSeleccionarGeolocalizacionComunComponent,
  //       {
  //         data: dataModal
  //       }
  //   );
  //
  //   if (respuesta) {
  //     respuesta
  //         .afterClosed()
  //         .subscribe(
  //             (data: {
  //               latitud: number;
  //               longitud: number;
  //             }) => {
  //               console.log(data);
  //             }
  //         )
  //   }
  // }
  //

  mostrarImagen(trabajador: TrabajadorInterface){
    if (trabajador.sis_IM) {
      return this._sRutaTrabajadorService._domSanitizer.bypassSecurityTrustResourceUrl(`data:${trabajador.sis_IM.mimetype};base64, ${trabajador.sis_IM.buffer}`);
    } else {
      return '';
    }
  }




}
