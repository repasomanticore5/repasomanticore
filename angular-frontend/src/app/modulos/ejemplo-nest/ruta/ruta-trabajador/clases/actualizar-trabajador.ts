import {CampoFormulario} from '@manticore-labs/ng-2021';
export class ActualizarTrabajador {
    // Definir todos los campos dentro del FORMULARIO de edición
    // se debe igualar a undefined, OMITIR RELACIONES
    // nombreCampo?: CampoFormulario = undefined;
    nombre?: CampoFormulario = undefined;
    apellido?: CampoFormulario = undefined;
}
