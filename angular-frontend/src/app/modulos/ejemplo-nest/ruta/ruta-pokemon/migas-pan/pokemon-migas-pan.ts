import {SRutaPokemonParametros} from '../interfaces/s-ruta-pokemon.parametros';
import {ENTRENADOR_MIGAS_PAN} from '../../ruta-entrenador/migas-pan/entrenador-migas-pan';
import {NOMBRE_SCOPE_POKEMON_NEST} from '../transloco/loader-pokemon';
import {MigaPanRuta} from '@manticore-labs/ng-2021';

export const POKEMON_MIGAS_PAN
  : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {
  let parametros: SRutaPokemonParametros = {entrenador: '0'};
  let ruta: any = {};
  if (componente) {
    if (componente.nombreServicio) {
      ruta = componente[componente.nombreServicio] as any;
      parametros = ruta.parametros as SRutaPokemonParametros;
    }
  }
  return {
    id: 'gestion-entrenador/:entrenador/gestion-pokemon',
    label: 'migaPan',
    routerLink: [
      ...ENTRENADOR_MIGAS_PAN(componente).routerLink,
      parametros.entrenador as string,
      'gestion-pokemon'
    ],
    scopeTransloco: NOMBRE_SCOPE_POKEMON_NEST
  };
};
