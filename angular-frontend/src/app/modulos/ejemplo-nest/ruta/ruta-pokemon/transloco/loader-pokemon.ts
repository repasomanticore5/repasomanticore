import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_POKEMON_NEST = NOMBRE_MODULO + 'Pokemon';

export const SCOPE_POKEMON_NEST = (http: HttpClient) => {
  const loaderPokemon = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-ejemplo-nest/pokemon/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_POKEMON_NEST, loader: loaderPokemon};
};

export const LOADER_POKEMON_NEST = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_POKEMON_NEST,
  multi: true
};
