import {CampoFormulario} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../../../constantes/formulario/mensajes-error';

export const POKEMON_BUSQUEDA_CAMPO_TEXTO_BUSQUEDA: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    componente: claseComponente,
    validators: [],
    asyncValidators: null,
    valorInicial: undefined,
    nombreCampo: 'busqueda',
    nombreMostrar: 'formularios.busqueda.campoBusqueda.nombreMostrar',
    textoAyuda: 'formularios.busqueda.campoBusqueda.textoAyuda',
    placeholderEjemplo: 'formularios.busqueda.campoBusqueda.placeholderEjemplo',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'formularios.busqueda.campoBusqueda.nombreMostrar',
    },
    estaValido: true,
    hidden: false,
    tipoCampoHtml: 'text',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false
  };
};
