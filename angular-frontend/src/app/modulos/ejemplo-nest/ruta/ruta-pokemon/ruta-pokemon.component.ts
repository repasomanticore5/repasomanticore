import {Component, OnInit} from '@angular/core';
import {SRutaPokemonService} from './s-ruta-pokemon/s-ruta-pokemon.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SRutaPokemonParametros} from './interfaces/s-ruta-pokemon.parametros';
import {PokemonBusquedaDto} from '../../dto/pokemon-busqueda.dto';
import {PokemonInterface} from '../../interfaces/pokemon.interface';
import {CrearPokemon} from './clases/crear-pokemon';
import {ActualizarPokemon} from './clases/actualizar-pokemon';
import {POKEMON_FORMULARIO_BUSQUEDA} from './busqueda-filtros/busqueda/pokemon-formulario-busqueda';
import {
    EventoCambioAutocomplete,
    EventoCambioFormulario,
    ObjetoBusquedaADto, ObjetoInternacionalizacionFormulario,
    SetearObjeto,
    TodosCamposValidados
} from '@manticore-labs/ng-2021';
import {ActivoInactivo} from '../../../../enums/activo-inactivo';
import {Observable} from 'rxjs';
import {NombreOpcionesMenu} from '../../../../enums/nombre-opciones-menu';
import {EJEMPLO_NEST_MIGAS_PAN} from '../../miga-pan-general/ejemplo-nest-migas-pan';
import {ENTRENADOR_MIGAS_PAN} from '../ruta-entrenador/migas-pan/entrenador-migas-pan';
import {PosicionOpcionesMenuNest} from '../../../../enums/posicion-opciones-menu-nest';
import {MenuItem} from 'primeng/api';
import {POKEMON_MIGAS_PAN} from './migas-pan/pokemon-migas-pan';
import {NOMBRE_MODULO_ASSETS_NEST} from '../../constantes/nombre-modulo-assets';
import {NOMBRE_SCOPE_ENTRENADOR_NEST} from '../ruta-entrenador/transloco/loader-entrenador';
import {NOMBRE_SCOPE_POKEMON_NEST} from './transloco/loader-pokemon';
import {TranslocoService} from '@ngneat/transloco';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';

@Component({
    selector: 'app-ruta-pokemon',
    templateUrl: './ruta-pokemon.component.html',
    styleUrls: ['./ruta-pokemon.component.scss']
})
export class RutaPokemonComponent implements OnInit {

    columnasPrimeTable = [
        // {
        //   field: 'nombreCampo',
        //   header: 'Nombre a mostrarse',
        //   posicion: 'text-center',
        //   tamanio: '60%', // tamaño en porcentaje de la columna
        //     fnMostrar: (valor:TipoValor)=>{
        //       switch (valor) {
        //         case TipoValor.ValorUno:
        //           return 'Valor de uno';
        //         case TipoValor.ValorDos:
        //           return 'Valor de dos';
        //         default:
        //           return valor;
        //       }
        //     }
        // },
        {
            field: 'nombre',
            header: 'nombre',
            posicion: 'text-center',
            tamanio: '60%', // tamaño en porcentaje de la columna
            fnMostrar: (valor: string) => {
                return valor;
            }
        },
        {
            field: 'sisHabilitado',
            header: 'habilitado',
            posicion: 'text-left',
            tamanio: '20%'
        },
        {
            field: 'id',
            header: 'acciones',
            posicion: 'text-right',
            tamanio: '20%'
        }
    ];

    opciones = [
        // {
        //   ruta: (id: string) => { // parametros de la ruta
        //     return ['/empieza', id, 'termina-ruta']; // ruta en arreglo
        //   },
        //   nombre: 'Otra acción de ...'
        // }
    ];

    camposFormularioBusqueda = POKEMON_FORMULARIO_BUSQUEDA;
    valorBusqueda = '';
    formularioBusqueda?: TodosCamposValidados;
    botonAceptarModalDisabled = true;

    formularioCrearEditar?: TodosCamposValidados;
    sistemaLogisticoSeleccionado?: PokemonInterface;

    stepperActualBusqueda = 0;

    nombreServicio = '_sRutaPokemonService';

    // Usar cuando se necesite ocultar campos que dependan de otros:

    // ocultarFormulario = false;
    // camposRequeridos = {
    //   nombreCampoDependiendeUno: false,
    //   nombreCampoDependiendeDos: false,
    // };

    migasPan: MigaDePanInterface[] = [];

    rutaImagenAssets = 'assets/img/' + NOMBRE_MODULO_ASSETS_NEST;

    rutaImagenPrincipal = this.rutaImagenAssets + '/pokemon/imagen-principal.svg';

    objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
        botonAtras: 'botonAtras',
        botonLimpiarFormulario: 'botonLimpiarFormulario',
        botonSiguiente: 'botonSiguiente',
        nombreScopeTransloco: NOMBRE_SCOPE_POKEMON_NEST,
    };

    constructor(
        public readonly _sRutaPokemonService: SRutaPokemonService,
        private readonly _activatedRoute: ActivatedRoute,
        public readonly _router: Router,
        public readonly translocoService: TranslocoService
    ) {
    }

    ngOnInit(): void {
        this._obtenerParametros();
    }

    private _obtenerParametros(): void {
        this._sRutaPokemonService
            .obtenerYGuardarParametrosRuta(this._activatedRoute)
            .subscribe(
                (parametros: SRutaPokemonParametros) => {
                    const busqueda = this._sRutaPokemonService
                        .setearParametrosDeBusqueda(parametros, PokemonBusquedaDto);
                    // if (parametros.nombreCampoRelacion && busqueda) {
                    //   busqueda.nombreCampoRelacion = +parametros.nombreCampoRelacion;
                    // }
                    if (parametros.entrenador && busqueda) {
                        busqueda.entrenador = +parametros.entrenador;
                    }
                    this._sRutaPokemonService
                        .busquedaDto = busqueda;
                    this._sRutaPokemonService
                        .construirMigasPan(
                            this,
                            [
                                EJEMPLO_NEST_MIGAS_PAN,
                                ENTRENADOR_MIGAS_PAN,
                                POKEMON_MIGAS_PAN,
                            ]
                        );
                    this.buscarConFiltros();
                }
            );
    }

    buscarConFiltros(): void {
        this._sRutaPokemonService._cargandoService.habilitarCargando();
        if (this._sRutaPokemonService.busquedaDto) {
            this._sRutaPokemonService.busquedaDto.skip = 0;
            this._sRutaPokemonService.first = 0;
        }
        this._sRutaPokemonService
            .buscar()
            .subscribe(
                (data) => {
                    this._sRutaPokemonService.registrosPagina = data[0].length;
                    this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                },
                (error) => {
                    console.error({
                        mensaje: 'Error cargando registros',
                        data: this._sRutaPokemonService.busquedaDto,
                        error
                    });
                    this._sRutaPokemonService._notificacionService.anadir({
                        titulo: 'Error',
                        detalle: 'Error del servidor',
                        severidad: 'error'
                    });
                    this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                }
            );
    }

    formularioValidoBusqueda(formularioBusquedaValido: TodosCamposValidados): void {
        this.formularioBusqueda = formularioBusquedaValido;
        if (formularioBusquedaValido.valido) {
            this._sRutaPokemonService.setearCamposBusquedaValidos(
                formularioBusquedaValido,
                PokemonBusquedaDto
            );
            // Transforma objetos a valores, si no hay objetos en la busqueda
            // como un autocomplete o select por ejemplo, entonces dejar vacío.
            const arregloCamposBusquedaConObjeto: ObjetoBusquedaADto[] = [
                {
                    nombreCampoEnBusqueda: 'sisHabilitado',
                    nombreCampoEnDto: 'sisHabilitado'
                },
                // {
                //   nombreCampoEnBusqueda: 'campoSelectOAutocomplete',
                //   nombreCampoEnDto: 'nombreCampoSelectOAutocomplete'
                // },
            ];
            this._sRutaPokemonService.transformarObjetosBusquedaABusquedaDto(
                arregloCamposBusquedaConObjeto
            );
        }
    }

    cambioCampoBusqueda(eventoCambioCampoBusqueca: EventoCambioFormulario): void {
        const campoBusquedaActualizado = eventoCambioCampoBusqueca.campoFormulario;
        if (campoBusquedaActualizado) {
            this._sRutaPokemonService
                .setearCamposBusquedaAUndefined(
                    campoBusquedaActualizado,
                    PokemonBusquedaDto
                );
        }

    }

    formularioBusquedacambioCampoAutocomplete(evento: EventoCambioAutocomplete) {
        // this.seleccionarBusquedaAutocompletePorEvento(evento);
    }

    formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete,
                                            registro?: PokemonInterface): void {
        // this.seleccionarBusquedaAutocompletePorEvento(evento);
    }

    // seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete){
    //   if(evento.campoFormulario){
    // switch (evento.campoFormulario.nombreCampo) {
    //   case 'nombreCampo': // nombres de cada uno de los campos autocomplete
    //     // this.buscarAutocompleteNombreCampo(evento); // ejecutar una búsqueda con los servicios pertinentes
    //     break;
    // }
    // }
    // }

    // buscarAutocompleteNombreCampo(evento: EventoCambioAutocomplete) {
    //   const busqueda: NombreCampoBusquedaDto = {
    //     nombreCampo: evento.query,
    //   };
    //   this._nombrCampoService
    //       .buscar(busqueda)
    //       .toPromise()
    //       .then(res => res as [NombreCampoInterface[], number])
    //       .then(data => {
    //         const arregloDatos = data[0];
    //         // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
    //         const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
    //         if (evento.campoFormulario.autocomplete) {
    //           if (Array.isArray(arregloDatos)) {
    //             evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
    //           } else {
    //             evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
    //           }
    //         }
    //         return data;
    //       });
    // }

    formularioCrearEditarValido(estaValido: TodosCamposValidados,
                                registro?: PokemonInterface): void {
        this._sRutaPokemonService.setearFormularioEnModal(estaValido);
        if (estaValido.valido) {
            this.formularioCrearEditar = estaValido;
            this.botonAceptarModalDisabled = false;
        } else {
            this.botonAceptarModalDisabled = true;
        }
    }

    formularioCrearEditarCambioCampo(eventoCambioCampo: EventoCambioFormulario,
                                     registro?: PokemonInterface): void {
        if (eventoCambioCampo.campoFormulario) {
            // Esta parte del codigo solo se debe de utilizar
            // si existe cambios de estructura del formulario
            // EJ: si selecciona el valor X de un campo entonces muestro tales campos
            // const formularioValido = {
            //   valido: false,
            //   camposFormulario: [eventoCambioCampo.campoFormulario],
            // };
            // this._sRutaPokemonService.setearFormularioEnModal(formularioValido);
            // switch (eventoCambioCampo.campoFormulario.nombreCampo) {
            //   case 'nombreCampo':
            //     if (registro) {
            //       this.actualizarValidez(
            //           eventoCambioCampo.valor,
            //           eventoCambioCampo.campoFormulario.nombreCampo,
            //           registro
            //       );
            //     } else {
            //       this.actualizarValidez(
            //           eventoCambioCampo.valor,
            //           eventoCambioCampo.campoFormulario.nombreCampo
            //       );
            //     }
            //     break;
            //   default:
            //     break;
            // }

        }
    }

//   actualizarValidez(
//       valorCampoValidez: any,
//       nombrePropiedadValidez: string,
//       registro?: any,
// ) {
//     // Los campos definidos "nombrePropiedad" cuando tengan un valor != undefined van a activar
//     // los campos que están en "nombresCamposRequeridosQueAfecta"
//     const camposAEstablecerValidezArreglo: CamposEstablecerValidez[] = [
//       {
//         nombrePropiedad: 'nombreCampo',
//         nombresCamposRequeridosQueAfecta: ['campoUno', 'campoDos']
//       },
//     // También hay el caso en donde al mostrar unos campos se oculten otros:
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoTres', 'campoCuatro'],
//         nombresCamposRequeridosAOcultar: ['campoCinco', 'campoSeis'],
//         valorPropiedad: 'valorTresCuatro',
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoCinco', 'campoSeis'],
//         nombresCamposRequeridosAOcultar: ['campoTres', 'campoCuatro'],
//         valorPropiedad: 'valorCincoSeis',
//       },
//     ];
//
//     this._sRutaPokemonService.establecerValidezDeFormulario<RutaPokemonComponent>(
//         this,
//         camposAEstablecerValidezArreglo,
//         valorCampoValidez,
//         nombrePropiedadValidez,
//         undefined,
//         registro,
//     );
//   }

    crearEditar(
        registro?: PokemonInterface,
    ): void {
        if (registro) {
            this.editar(registro);
        } else {
            this.crear();
        }
    }

    crear(): void {
        if (this.formularioCrearEditar) {
            const camposCrear = this._sRutaPokemonService.obtenerCampos<CrearPokemon>(
                CrearPokemon,
                this.formularioCrearEditar
            );
            // Transforma objetos a valores, si no hay objetos en el formulario
            // como un autocomplete por ejemplo, se debe dejar vacío.
            const arregloPropiedades: SetearObjeto[] = [
                // {
                //   nombreCampo: 'sisHabilitado',
                //   nombrePropiedadObjeto: 'sisHabilitado'
                // }
            ];
            camposCrear.objetoCrear = this._sRutaPokemonService.setearValoresDeObjetos(
                camposCrear.objetoCrear,
                arregloPropiedades
            );

            // Setear campos extra
            // Ej:
            // camposCrear.objetoCrear.habilitado = 1;
            // Ej: Relación
            // if (this._sRutaPokemonService.busquedaDto) {
            //   camposCrear.objetoCrear.nombreCampoRelacion = this._sRutaPokemonService.busquedaDto.nombreCampoRelacion as number;
            // }


            camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
            if (this._sRutaPokemonService.busquedaDto) {
                camposCrear.objetoCrear.entrenador = this._sRutaPokemonService.busquedaDto.entrenador as number;
            }

            if (this._sRutaPokemonService.busquedaDto) {
                this._sRutaPokemonService._cargandoService.habilitarCargando();
                const crear$ = this._sRutaPokemonService
                    ._pokemonService
                    .crear(
                        camposCrear.objetoCrear
                    )
                    .pipe(
                        this._sRutaPokemonService.buscarDeNuevo('id')
                    ) as Observable<PokemonInterface>;
                crear$
                    .subscribe(
                        (nuevoRegistro) => {
                            nuevoRegistro.habilitado = true;
                            this._sRutaPokemonService.arregloDatos.unshift(nuevoRegistro);
                            this._sRutaPokemonService.arregloDatosFiltrado.unshift(nuevoRegistro);
                            this._sRutaPokemonService.matDialog.closeAll();
                            this._sRutaPokemonService._notificacionService.anadir({
                                titulo: this.translocoService.translate('generales.toasters.toastExitoCrear.title'),
                                detalle: this.translocoService.translate('generales.toasters.toastExitoCrear.body', {nombre: nuevoRegistro.nombre}),
                                severidad: 'success'
                            });
                            this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                        },
                        (error) => {
                            console.error({
                                mensaje: 'Error cargando registros',
                                data: this._sRutaPokemonService.busquedaDto,
                                error
                            });
                            this._sRutaPokemonService._notificacionService.anadir({
                                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                                severidad: 'error'
                            });
                            this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                        }
                    );
            }
        }
    }

    editar(
        registro: PokemonInterface,
    ): void {
        if (this.formularioCrearEditar) {
            const camposCrear = this._sRutaPokemonService.obtenerCampos<ActualizarPokemon>(
                ActualizarPokemon,
                this.formularioCrearEditar,
                true
            );
            const arregloPropiedades: SetearObjeto[] = [];
            camposCrear.objetoCrear = this._sRutaPokemonService.setearValoresDeObjetos(
                camposCrear.objetoCrear,
                arregloPropiedades
            );
            if (registro.id) {
                this._sRutaPokemonService._cargandoService.habilitarCargando();
                const actualizar$ = this._sRutaPokemonService
                    ._pokemonService
                    .actualizar(
                        camposCrear.objetoCrear,
                        registro.id
                    )
                    .pipe(
                        this._sRutaPokemonService.buscarDeNuevo('id', registro.id)
                    ) as Observable<PokemonInterface>;
                actualizar$
                    .subscribe(
                        (registroEditado) => {
                            registroEditado.habilitado = registro.habilitado;
                            const indice = this._sRutaPokemonService.arregloDatos
                                .findIndex(
                                    (a: PokemonInterface) => a.id === registro.id
                                );
                            this._sRutaPokemonService.arregloDatos[indice] = registroEditado;
                            const indiceArregloFiltrado = this._sRutaPokemonService.arregloDatosFiltrado
                                .findIndex(
                                    (a: PokemonInterface) => a.id === registro.id
                                );
                            this._sRutaPokemonService.arregloDatosFiltrado[indiceArregloFiltrado] = registroEditado;
                            this._sRutaPokemonService.matDialog.closeAll();
                            this._sRutaPokemonService._notificacionService.anadir({
                                titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
                                detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: registroEditado.nombre}),
                                severidad: 'success'
                            });
                            this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                        },
                        (error) => {
                            console.error({
                                mensaje: 'Error cargando registros',
                                data: this._sRutaPokemonService.busquedaDto,
                                error
                            });
                            this._sRutaPokemonService._notificacionService.anadir({
                                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                                severidad: 'error'
                            });
                            this._sRutaPokemonService._cargandoService.deshabilitarCargando();
                        }
                    );
            }
        }
    }
}
