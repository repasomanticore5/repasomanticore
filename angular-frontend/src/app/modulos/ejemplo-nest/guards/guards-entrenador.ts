import {EntrenadorGuard} from '../ruta/ruta-entrenador/guard/entrenador.guard';
import { TrabajadorGuard } from '../ruta/ruta-trabajador/guard/trabajador.guard';

export const GUARDS_ABASTECIMIENTO = [
  EntrenadorGuard,
  TrabajadorGuard
];
