import {CommonModule} from '@angular/common';
import {TableModule} from 'primeng/table';
import {InputSwitchModule} from 'primeng/inputswitch';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SplitButtonModule} from 'primeng/splitbutton';
import {ButtonModule} from 'primeng/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MenuModule} from 'primeng/menu';
import {NotificacionModule} from '../../../servicios/notificacion/notificacion.module';
import {RippleModule} from 'primeng/ripple';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ManLabNgBootstrapModule, MlCampoFormularioModule} from '@manticore-labs/ng-2021';
import {EntrenadorModule} from '../servicios/entrenador/entrenador.module';
import {SRutaEntrenadorModule} from '../ruta/ruta-entrenador/s-ruta-entrenador/s-ruta-entrenador.module';
import {EjemploNestRoutingModule} from '../ejemplo-nest-routing.module';
import {PokemonModule} from '../servicios/pokemon/pokemon.module';
import {SRutaPokemonModule} from '../ruta/ruta-pokemon/s-ruta-pokemon/s-ruta-pokemon.module';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {TranslocoModule} from '@ngneat/transloco';
import {TituloPantallaModule} from '../../../componentes/titulo-pantalla/titulo-pantalla.module';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { TrabajadorModule } from '../servicios/trabajador/trabajador.module';
import { SRutaTrabajadorModule } from '../ruta/ruta-trabajador/s-ruta-trabajador/s-ruta-trabajador.module';


export const IMPORTS_MODULO_EJEMPLO_NEST = [
  CommonModule,
  TableModule,
  BreadcrumbModule,
  InputSwitchModule,
  FormsModule,
  ReactiveFormsModule,
  SplitButtonModule,
  ButtonModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MenuModule,
  NotificacionModule,
  MlCampoFormularioModule,
  RippleModule,
  AutoCompleteModule,
  EjemploNestRoutingModule,
  EntrenadorModule,
  SRutaEntrenadorModule,
  PokemonModule,
  SRutaPokemonModule,
  TranslocoModule,
  TituloPantallaModule,
  MatMenuModule,
  MatButtonModule,
  ManLabNgBootstrapModule,
  TrabajadorModule,
  SRutaTrabajadorModule
];
