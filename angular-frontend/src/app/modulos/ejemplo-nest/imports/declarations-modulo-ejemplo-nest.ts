import {RutaEntrenadorComponent} from '../ruta/ruta-entrenador/ruta-entrenador.component';
import {RutaPokemonComponent} from '../ruta/ruta-pokemon/ruta-pokemon.component';
import { RutaTrabajadorComponent } from '../ruta/ruta-trabajador/ruta-trabajador.component';


export const DECLARATIONS_MODULO_EJEMPLO_NEST = [
  RutaEntrenadorComponent,
  RutaPokemonComponent,
  RutaTrabajadorComponent
];
