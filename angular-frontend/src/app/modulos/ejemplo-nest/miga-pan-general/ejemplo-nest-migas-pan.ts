import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_NEST} from '../transloco/loader-nest';

export const EJEMPLO_NEST_MIGAS_PAN
    : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {

    return {
        id: 'ejemplo-nest',
        label: 'migaPan',
        routerLink: ['/ejemplo-nest'],
        scopeTransloco: NOMBRE_SCOPE_NEST
    };
};
