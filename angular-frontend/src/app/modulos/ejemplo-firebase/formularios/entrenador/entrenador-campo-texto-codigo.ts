import {RutaEntrenadorComponent} from '../../ruta/ruta-entrenador/ruta-entrenador.component';
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {EntrenadorBusquedaDto} from '../../dto/entrenador-busqueda.dto';
import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';


export const ENTRENADOR_CAMPO_TEXTO_CODIGO: (
  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente<RutaEntrenadorComponent, EntrenadorInterface, EntrenadorBusquedaDto>) => {


    const valorCampo = claseComponente
      .data
      .componente
      ._sRutaEntrenadorService
      .setearCampoEnFormulario(
        claseComponente,
        '_sRutaEntrenadorService',
        'uid'
      );


    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.nombre;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.nombre) {
    //   validators.push(Validators.required);
    // }


    return {
      tipoCampoHtml: 'text',

      valorInicial: valorCampo,

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.nombre,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.nombre ? 6 : 12,


      validators: [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(4),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: valorCampo ? true : false, // Si es un campo opcional debe ser siempre 'true'

      disabled: valorCampo ? true : false,
      asyncValidators: null,
      nombreCampo: 'codigo',
      nombreMostrar: 'formularios.crearEditar.campoCodigo.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoCodigo.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoCodigo.placeholderEjemplo',
      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {
        nombreCampo: 'formularios.crearEditar.campoCodigo.nombreMostrar',
        minlength: 2,
        maxlength: 4,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
