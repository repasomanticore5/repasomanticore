import {RutaEntrenadorComponent} from '../../ruta/ruta-entrenador/ruta-entrenador.component';
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {EntrenadorBusquedaDto} from '../../dto/entrenador-busqueda.dto';
import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';


export const ENTRENADOR_CAMPO_TEXTO_NOMBRE: (
  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente<RutaEntrenadorComponent, EntrenadorInterface, EntrenadorBusquedaDto>) => {


    const valorCampo = claseComponente
      .data
      .componente
      ._sRutaEntrenadorService
      .setearCampoEnFormulario(
        claseComponente,
        '_sRutaEntrenadorService',
        'nombre'
      );


    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.nombre;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.nombre) {
    //   validators.push(Validators.required);
    // }


    return {
      tipoCampoHtml: 'text',

      valorInicial: valorCampo,

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.nombre,

      tamanioColumna: 6,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.nombre ? 6 : 12,


      validators: [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(60),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: valorCampo ? true : false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'nombre',
      nombreMostrar: 'formularios.crearEditar.campoNombre.nombreMostrar',
      textoAyuda: 'formularios.crearEditar.campoNombre.textoAyuda',
      placeholderEjemplo: 'formularios.crearEditar.campoNombre.placeholderEjemplo',
      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {
        nombreCampo: 'formularios.crearEditar.campoNombre.nombreMostrar',
        minlength: 10,
        maxlength: 60,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
      },
      formulario: {},
      componente: claseComponente,
    };
  };
