
// export interface EntrenadorInterface extends EntidadComun{
//   id?: number;
//   // nombreCampo?: string; // Cada uno de los campos de la Entidad
//   // nombreCampoRelacion?: CampoRelacionInterface | number;
// }

import {EntidadComunProyectoFirebase} from '../../../abstractos/entidad-comun-proyecto.firebase';

export interface EntrenadorInterface extends EntidadComunProyectoFirebase {
  nombre?: string;
  pueblo?: string;
  codigo?: string;
}
