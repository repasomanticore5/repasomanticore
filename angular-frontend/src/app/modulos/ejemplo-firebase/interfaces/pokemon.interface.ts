import {EntidadComunProyectoFirebase} from '../../../abstractos/entidad-comun-proyecto.firebase';

export interface PokemonInterface extends EntidadComunProyectoFirebase {
  // nombreCampo?: string; // Cada uno de los campos de la Entidad
  // nombreCampoRelacion?: CampoRelacionInterface | number;
  nombre?: string;
  tipo?: string;
}
