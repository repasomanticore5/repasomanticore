import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_POKEMON = NOMBRE_MODULO + 'Pokemon';

export const SCOPE_POKEMON = (http: HttpClient) => {
  const loaderPokemon = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-firebase/pokemon/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_POKEMON, loader: loaderPokemon};
};

export const LOADER_POKEMON = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_POKEMON,
  multi: true
};
