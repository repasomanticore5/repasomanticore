import {MenuItem} from 'primeng/api';

import {SRutaPokemonParametros} from '../interfaces/s-ruta-pokemon.parametros';
import {ENTRENADOR_MIGAS_PAN} from '../../ruta-entrenador/migas-pan/entrenador-migas-pan';
import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_POKEMON} from '../transloco/loader-pokemon';

export let idPokemon = 'gestion-entrenador/:entrenador/gestion-pokemon';
export const POKEMON_MIGAS_PAN
  : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {
  const id = idPokemon;
  // let idEntrenador = 'gestion-entrenador';
  const label = 'Pokemon';

  // Inicializar todos los parametros de ruta con '0'
  let parametros: SRutaPokemonParametros = {
    entrenador: '0',
    // parametroRutaDos: '0',
    // parametroRutaTres: '0',
  };
  let ruta: any = {};
  if (componente) {
    if (componente.nombreServicio) {
      ruta = componente[componente.nombreServicio] as any;
      parametros = ruta.parametros as SRutaPokemonParametros;
    }
  }
  return {
    // path que va a ir en el RoutingModule
    id,
    label: 'migaPan',
    routerLink: [
      ...ENTRENADOR_MIGAS_PAN(componente).routerLink, // Se llama a la miga de pan PAPA de esta ruta.
      parametros.entrenador as string,
      'gestion-pokemon'
      // parametros.parametroRutaTres, // parametro de ruta del ultimo parametro de ruta, en este caso "parametroRutaTres"
      // 'path-final' // si existe un path al final se lo pone aqui
    ],
    scopeTransloco: NOMBRE_SCOPE_POKEMON
  };
};
