import {Component, OnInit} from '@angular/core';
import {SRutaPokemonService} from './s-ruta-pokemon/s-ruta-pokemon.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SRutaPokemonParametros} from './interfaces/s-ruta-pokemon.parametros';
import {PokemonBusquedaDto} from '../../dto/pokemon-busqueda.dto';
import {PokemonInterface} from '../../interfaces/pokemon.interface';
import {CrearPokemon} from './clases/crear-pokemon';
import {ActualizarPokemon} from './clases/actualizar-pokemon';
import {POKEMON_FORMULARIO_BUSQUEDA} from './busqueda-filtros/busqueda/pokemon-formulario-busqueda';
import {MenuItem} from 'primeng/api';
import {POKEMON_MIGAS_PAN} from './migas-pan/pokemon-migas-pan';
import {
  ArregloFiltroBusquedaFirestore, EventoCambioAutocomplete,
  EventoCambioFormulario, ObjetoBusquedaADto, ObjetoInternacionalizacionFormulario,
  SetearObjeto,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';
import {NombreOpcionesMenu} from '../../../../enums/nombre-opciones-menu';
import {Observable} from 'rxjs';
import {PosicionOpcionesMenuFirebase} from '../../../../enums/posicion-opciones-menu-firebase';
import {ActivoInactivo} from '../../../../enums/activo-inactivo';
import {TranslocoService} from '@ngneat/transloco';
import {ENTRENADOR_MIGAS_PAN} from '../ruta-entrenador/migas-pan/entrenador-migas-pan';
import {EJEMPLO_FIREBASE_MIGAS_PAN} from '../../miga-pan-general/ejemplo-firebase-migas-pan';
import {NOMBRE_MODULO_ASSETS} from '../../constantes/nombre-modulo-assets';
import {NOMBRE_SCOPE_ENTRENADOR} from '../ruta-entrenador/transloco/loader-entrenador';
import {NOMBRE_SCOPE_POKEMON} from './transloco/loader-pokemon';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';

@Component({
  selector: 'app-ruta-pokemon',
  templateUrl: './ruta-pokemon.component.html',
  styleUrls: ['./ruta-pokemon.component.scss']
})
export class RutaPokemonComponent implements OnInit {

  columnasPrimeTable = [
    // {
    //   field: 'nombreCampo',
    //   header: 'Nombre a mostrarse',
    //   posicion: 'text-center',
    //   tamanio: '60%', // tamaño en porcentaje de la columna
    //     fnMostrar: (valor:TipoValor)=>{
    //       switch (valor) {
    //         case TipoValor.ValorUno:
    //           return 'Valor de uno';
    //         case TipoValor.ValorDos:
    //           return 'Valor de dos';
    //         default:
    //           return valor;
    //       }
    //     }
    // },
    {
      field: 'nombre',
      header: 'nombre',
      posicion: 'text-center',
      tamanio: '20%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'tipo',
      header: 'tipo',
      posicion: 'text-center',
      tamanio: '20%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'sisHabilitado',
      header: 'habilitado',
      posicion: 'text-left',
      tamanio: '20%'
    },
    {
      field: 'id',
      header: 'acciones',
      posicion: 'text-right',
      tamanio: '20%'
    }
  ];

  opciones = [
    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     return [
    //     ...POKEMON_MIGAS_PAN(this).routerLink, // RUTA ACTUAL
    //     id,  // ids necesarios
    //     'termina-ruta' // opcional path final
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'Otra acción de ...'
    // }

    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     return [
    //     ...POKEMON_MIGAS_PAN(this).routerLink, // RUTA ACTUAL
    //     id,  // ids necesarios
    //     'termina-ruta' // opcional path final
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'Otra acción de ...'
    // }
  ];

  camposFormularioBusqueda = POKEMON_FORMULARIO_BUSQUEDA;
  valorBusqueda = '';
  formularioBusqueda?: TodosCamposValidados;
  botonAceptarModalDisabled = true;

  formularioCrearEditar?: TodosCamposValidados;
  sistemaLogisticoSeleccionado?: PokemonInterface;

  stepperActualBusqueda = 0;

  nombreServicio = '_sRutaPokemonService';

  // Usar cuando se necesite ocultar campos que dependan de otros:

  // ocultarFormulario = false;
  // camposRequeridos = {
  //   nombreCampoDependiendeUno: false,
  //   nombreCampoDependiendeDos: false,
  // };

  // Si existen campos AUTOGENERADOS en estas tablase se necesitarian estas variables

  // botonAceptarModalAutogeneradoDisabled = false;
  //
  // camposAutogeneradosAGuardar: CampoFormulario[] = [];
  //
  // arregloAutogenerado: { arregloDatos: CampoAutogenerado[]; grupoFormulario: GrupoFormulario[] } = {
  //   arregloDatos: [],
  //   grupoFormulario: [],
  // };

  migasPan: MigaDePanInterface[] = [];

  rutaImagenAssets = 'assets/img/' + NOMBRE_MODULO_ASSETS;
  rutaImagenPrincipal = this.rutaImagenAssets + '/pokemon/imagen-principal.svg';
  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
    botonAtras: 'botonAtras',
    botonLimpiarFormulario: 'botonLimpiarFormulario',
    botonSiguiente: 'botonSiguiente',
    nombreScopeTransloco: NOMBRE_SCOPE_POKEMON,
  };

  constructor(
    public readonly _sRutaPokemonService: SRutaPokemonService,
    private readonly _activatedRoute: ActivatedRoute,
    public readonly _router: Router,
    public readonly translocoService: TranslocoService
  ) {
  }

  ngOnInit(): void {
    this._obtenerParametros();
  }

  private _obtenerParametros(): void {
    this._sRutaPokemonService
      .obtenerYGuardarParametrosRuta(this._activatedRoute)
      .subscribe(
        (parametros: SRutaPokemonParametros) => {
          const busqueda = this._sRutaPokemonService
            .setearParametrosDeBusqueda(parametros, PokemonBusquedaDto);


          // Setear los valores de las colecciones superiores
          // Ej:
          // this._sRutaPokemonService.arregloIdColeccionesSuperiores = [
          //   parametros.nombreParametroPrimerNivel,
          //   parametros.nombreParametroSegundoNivel,
          // ];
          if (parametros.entrenador) {
            this._sRutaPokemonService.arregloIdColeccionesSuperiores = [
              parametros.entrenador
            ];
          }

          this._sRutaPokemonService
            .busquedaDto = busqueda;
          this._sRutaPokemonService
            .construirMigasPan(
              this,
              [
                // MODULO_PAPA_MIGAS_PAN, // Migas de pan anteriores "padres"
                EJEMPLO_FIREBASE_MIGAS_PAN,
                ENTRENADOR_MIGAS_PAN,
                POKEMON_MIGAS_PAN,
              ],
            );
          this.buscarConFiltros();
        }
      );
  }

  buscarConFiltros(): void {


    if (this._sRutaPokemonService.busquedaDto) {
      this._sRutaPokemonService.first = 0;
      // Setear whereFirestore al valor inicial
      this._sRutaPokemonService.whereFirestore = [];
      this._sRutaPokemonService.lastDocumentFirestore = undefined;
      this._sRutaPokemonService.yaNoHayMasRegistros = false;
      // Dependiendo de los filtros de búsqueda setear las búsquedas
      // const arregloFiltroBusquedaFirestore: ArregloFiltroBusquedaFirestore[] = [
      //   {
      //     fieldPath: 'nombrePropiedadBusquedaDto',
      //     opStr: '=='
      //   }
      // ];
      const arregloFiltroBusquedaFirestore: ArregloFiltroBusquedaFirestore[] = [
        {
          fieldPath: '__busquedaGlobal',
          opStr: 'array-contains'
        },
        {
          fieldPath: 'sisHabilitado',
          opStr: '=='
        },
        // Anadir todos las busquedas que falten
        // {
        //   fieldPath: 'campo', // nombre de campo Firestore
        //   opStr: '==' // cualquier opStr de Firestore
        // },
      ];
      this._sRutaPokemonService.establecerWhereFirestore(arregloFiltroBusquedaFirestore);
    }
    this.buscarSuscrito();

  }

  buscarSuscrito() {
    // console.log(this._sRutaPokemonService.arregloIdColeccionesSuperiores);
    this._sRutaPokemonService._cargandoService.habilitarCargando();
    this._sRutaPokemonService
      .buscar()
      .subscribe(
        (data) => {
          this._sRutaPokemonService.registrosPagina = data[0].length;
          this._sRutaPokemonService._cargandoService.deshabilitarCargando();
        },
        (error) => {
          console.error({
            mensaje: 'Error cargando registros',
            data: this._sRutaPokemonService.busquedaDto,
            error
          });
          this._sRutaPokemonService._notificacionService.anadir({
            titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
            detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
            severidad: 'error'
          });
          this._sRutaPokemonService._cargandoService.deshabilitarCargando();
        }
      );
  }

  // Usar para generar campos de formulario dinamicos

  // generarFormulario() {
  //   this.mostrarFormularioDePrueba = false;
  //   const arregloDatos: CampoAutogenerado[] = [];
  //   const grupoFormulario: GrupoFormulario[] = [];
  //   // crear los campos autogenerados "arregloDatos" y
  //   // AL MENOS debe existir un "grupoFormulario" que contenga a todos los campos para una visualización sencilla
  //   const respuestaAutogenerado = this._sRutaArticuloAtributoService
  //       .generarFormularioAutogenerado(
  //           arregloDatos,
  //           MENSAJES_ERROR,
  //           this,
  //           grupoFormulario
  //       )
  //   this.arregloFormulario.arregloValores = respuestaAutogenerado.arregloValores;
  //   this.arregloFormulario.grupo = respuestaAutogenerado.grupo;
  //   this.arregloFormulario.campos = respuestaAutogenerado.campos;
  //   this.mostrarFormularioDePrueba = true;
  // }

  formularioValidoBusqueda(formularioBusquedaValido: TodosCamposValidados): void {
    this.formularioBusqueda = formularioBusquedaValido;
    if (formularioBusquedaValido.valido) {
      this._sRutaPokemonService.setearCamposBusquedaValidos(
        formularioBusquedaValido,
        PokemonBusquedaDto
      );
      // Transforma objetos a valores, si no hay objetos en la busqueda
      // como un autocomplete o select por ejemplo, entonces dejar vacío.
      const arregloCamposBusquedaConObjeto: ObjetoBusquedaADto[] = [
        {
          nombreCampoEnBusqueda: 'sisHabilitado',
          nombreCampoEnDto: 'sisHabilitado'
        },
        // {
        //   nombreCampoEnBusqueda: 'campoSelectOAutocomplete',
        //   nombreCampoEnDto: 'nombreCampoSelectOAutocomplete'
        // },
      ];
      this._sRutaPokemonService.transformarObjetosBusquedaABusquedaDto(
        arregloCamposBusquedaConObjeto
      );
    }
  }

  cambioCampoBusqueda(eventoCambioCampoBusqueca: EventoCambioFormulario): void {
    const campoBusquedaActualizado = eventoCambioCampoBusqueca.campoFormulario;
    if (campoBusquedaActualizado) {
      this._sRutaPokemonService
        .setearCamposBusquedaAUndefined(
          campoBusquedaActualizado,
          PokemonBusquedaDto
        );
    }

  }

  formularioBusquedacambioCampoAutocomplete(evento: EventoCambioAutocomplete) {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete,
                                          registro?: PokemonInterface): void {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  // seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete){
  //   if(evento.campoFormulario){
  // switch (evento.campoFormulario.nombreCampo) {
  //   case 'nombreCampo': // nombres de cada uno de los campos autocomplete
  //     // this.buscarAutocompleteNombreCampo(evento); // ejecutar una búsqueda con los servicios pertinentes
  //     break;
  // }
  // }
  // }

  // buscarAutocompleteNombreCampo(evento: EventoCambioAutocomplete) {
  //   const busqueda: NombreCampoBusquedaDto = {
  //     nombreCampo: evento.query,
  //   };
  //   this._nombrCampoService
  //       .buscar(busqueda)
  //       .toPromise()
  //       .then(res => res as [NombreCampoInterface[], number])
  //       .then(data => {
  //         const arregloDatos = data[0];
  //         // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
  //         const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
  //         if (evento.campoFormulario.autocomplete) {
  //           if (Array.isArray(arregloDatos)) {
  //             evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
  //           } else {
  //             evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
  //           }
  //         }
  //         return data;
  //       });
  // }

  formularioCrearEditarValido(estaValido: TodosCamposValidados,
                              registro?: PokemonInterface): void {
    this._sRutaPokemonService.setearFormularioEnModal(estaValido);
    if (estaValido.valido) {
      this.formularioCrearEditar = estaValido;
      this.botonAceptarModalDisabled = false;
    } else {
      this.botonAceptarModalDisabled = true;
    }
  }

  formularioCrearEditarCambioCampo(eventoCambioCampo: EventoCambioFormulario,
                                   registro?: PokemonInterface): void {
    if (eventoCambioCampo.campoFormulario) {
      // Esta parte del codigo solo se debe de utilizar
      // si existe cambios de estructura del formulario
      // EJ: si selecciona el valor X de un campo entonces muestro tales campos
      // const formularioValido = {
      //   valido: false,
      //   camposFormulario: [eventoCambioCampo.campoFormulario],
      // };
      // this._sRutaPokemonService.setearFormularioEnModal(formularioValido);
      // switch (eventoCambioCampo.campoFormulario.nombreCampo) {
      //   case 'nombreCampo':
      //     if (registro) {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo,
      //           registro
      //       );
      //     } else {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo
      //       );
      //     }
      //     break;
      //   default:
      //     break;
      // }

    }
  }

//   actualizarValidez(
//       valorCampoValidez: any,
//       nombrePropiedadValidez: string,
//       registro?: any,
// ) {
//     // Los campos definidos "nombrePropiedad" cuando tengan un valor != undefined van a activar
//     // los campos que están en "nombresCamposRequeridosQueAfecta"
//     const camposAEstablecerValidezArreglo: CamposEstablecerValidez[] = [
//       {
//         nombrePropiedad: 'nombreCampo',
//         nombresCamposRequeridosQueAfecta: ['campoUno', 'campoDos']
//       },
//     // También hay el caso en donde al mostrar unos campos se oculten otros:
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoTres', 'campoCuatro'],
//         nombresCamposRequeridosAOcultar: ['campoCinco', 'campoSeis'],
//         valorPropiedad: 'valorTresCuatro',
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoCinco', 'campoSeis'],
//         nombresCamposRequeridosAOcultar: ['campoTres', 'campoCuatro'],
//         valorPropiedad: 'valorCincoSeis',
//       },
//     ];
//
//     this._sRutaPokemonService.establecerValidezDeFormulario<RutaPokemonComponent>(
//         this,
//         camposAEstablecerValidezArreglo,
//         valorCampoValidez,
//         nombrePropiedadValidez,
//         undefined,
//         registro,
//     );
//   }


  abrirModal(
    registro: PokemonInterface
  ) {
    // Si se tienen campos dependientes se deben de
    // activarlos antes de editarlos
    // if(camposRequeridos.campoDependeUno){
    //   camposRequeridos.nombreCampoDependienteUno = true;
    // }
    // if(camposRequeridos.campoDependeDos){
    //   camposRequeridos.nombreCampoDependienteDos = true;
    // }
    this._sRutaPokemonService.abrirModal(this, registro);

    // En el caso de cammpos AUTOGERENADOS se habilita esta parte del codigo

    // const respuesta = this._sRutaPokemonService._pokemonService.generarFormulario(registro);
    // respuesta.arregloDatos = respuesta
    //     .arregloDatos
    // .map( // EJEMPLO DE IMPLEMENTACION:
    //     (aD) => {
    //       aD.valorInicial = registro.catValor;
    //       aD.required = true;
    //       aD.estaValido = false;
    //       aD.fnValidarInputMask = (campo, componente) => {
    //         if (campo) {
    //           return campo.length > 0
    //         } else {
    //           return true;
    //         }
    //       };
    //       return aD;
    //     }
    // );
    // const respuestaGrupoCampo = this._sRutaPokemonService
    //     .generarFormularioAutogenerado(
    //         respuesta.arregloDatos,
    //         MENSAJES_ERROR,
    //         this,
    //         respuesta.grupoFormulario
    //     );
    // this.abrirModalPrevisualizarTablaAutogenerado(respuestaGrupoCampo, false, registro);

  }

  // abrirModalPrevisualizarTablaAutogenerado(
  //     arregloDeDatos: {
  //   campos: (claseComponente: any) => CampoFormulario[],
  //       grupo: () => GrupoFormulario[],
  //       arregloValores: { nombre: string, campos: string[] }[]
  // },
  // mostrarTabla = false,
  //     registro?: PokemonInterface) {
  //   this.botonAceptarModalAutogeneradoDisabled = true;
  //   const dialogRef = this.matDialog.open(
  //       ModalFormularioAutogeneradoComponent, // Modal de autogenerado
  //       {
  //         data: {
  //           arregloFormulario: arregloDeDatos,
  //           componente: this,
  //           mostrarTabla: mostrarTabla,
  //           registro,
  //         }
  //       }
  //   );
  // }

  // cambioCampoAutogenerado(eventoCambioCampo: EventoCambioFormulario) {
  //
  // }

  // formularioValidoAutogenerado(estaValido: TodosCamposValidados, registro?: PokemonInterface) {
  //   if (estaValido.valido) {
  //     this.camposAutogeneradosAGuardar = estaValido
  //         .camposFormulario
  //         .filter((cF) => cF.estaValido && cF.valorActual)
  //     this.botonAceptarModalAutogeneradoDisabled = false;
  //   } else {
  //     this.botonAceptarModalAutogeneradoDisabled = true;
  //   }
  // }

  // aceptoFormularioAutogenerado(registro?: ArticuloCatalogoInterface) {
  //   if (registro) {
  //     this.editarRegistro(registro);
  //   } else {
  //     this.guardarNuevosCampos()
  //   }
  // }

  // Implementacion de guardado de campos auto generados

  // guardarNuevosCampos() {
  //
  // }

  // Implementacion de ediciion de campo autogenerado

  // editarRegistro(registro: ArticuloCatalogoInterface) {
  //
  // }

  // cambioStepperEnAutogenerado(indice: number) {
  //
  // }

  crearEditar(
    registro?: PokemonInterface,
  ): void {
    if (registro) {
      this.editar(registro);
    } else {
      this.crear();
    }
  }

  crear(): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaPokemonService.obtenerCampos<CrearPokemon>(
        CrearPokemon,
        this.formularioCrearEditar
      );
      // Transforma objetos a valores, si no hay objetos en el formulario
      // como un autocomplete por ejemplo, se debe dejar vacío.
      const arregloPropiedades: SetearObjeto[] = [
        // {
        //   nombreCampo: 'sisHabilitado',
        //   nombrePropiedadObjeto: 'sisHabilitado'
        // }
      ];
      camposCrear.objetoCrear = this._sRutaPokemonService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );


      // Setear campos extra
      // Ej:
      // camposCrear.objetoCrear.habilitado = 1;
      // Ej: Relación
      // if (this._sRutaPokemonService.busquedaDto) {
      //   camposCrear.objetoCrear.nombreCampoRelacion = this._sRutaPokemonService.busquedaDto.nombreCampoRelacion as number;
      // }

      // Si se necesita setear el codigo dentro del objeto se puede utilizar estas lineas
      // const campos = camposCrear.objetoCrear as CrearPokemon;
      // camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
      // const codigo = campos.codigo;

      camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
      let entrenador = '';
      if (this._sRutaPokemonService.parametros) {
        if (this._sRutaPokemonService.parametros.entrenador) {
          entrenador = entrenador;
        }
      }

      if (this._sRutaPokemonService.busquedaDto) {
        this._sRutaPokemonService._cargandoService.habilitarCargando();
        const crear$ = this._sRutaPokemonService
          ._pokemonService
          .crear(
            camposCrear.objetoCrear,
            undefined,
            false,
            this._sRutaPokemonService._pokemonService.arregloCamposBusqueda,
            this._sRutaPokemonService.arregloIdColeccionesSuperiores,
            // codigo // Si se va a crear con un codigo el registro se tiene que mandar el ID
          ) as Observable<PokemonInterface>;

        crear$
          .subscribe(
            (nuevoRegistro) => {
              nuevoRegistro.habilitado = true;
              this._sRutaPokemonService.arregloDatos.unshift(nuevoRegistro);
              this._sRutaPokemonService.arregloDatosFiltrado.unshift(nuevoRegistro);
              this._sRutaPokemonService.matDialog.closeAll();
              this._sRutaPokemonService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastExitoCrear.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoCrear.body', {nombre: nuevoRegistro.nombre}),
                severidad: 'success'
              });
              this._sRutaPokemonService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaPokemonService.busquedaDto,
                error
              });
              this._sRutaPokemonService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                severidad: 'error'
              });
              this._sRutaPokemonService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }

  editar(
    registro: PokemonInterface,
  ): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaPokemonService.obtenerCampos<ActualizarPokemon>(
        ActualizarPokemon,
        this.formularioCrearEditar,
        true
      );
      const arregloPropiedades: SetearObjeto[] = [];
      camposCrear.objetoCrear = this._sRutaPokemonService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );
      if (registro.uid) {
        this._sRutaPokemonService._cargandoService.habilitarCargando();
        const actualizar$ = this._sRutaPokemonService
          ._pokemonService
          .actualizar(
            registro,
            camposCrear.objetoCrear,
            this._sRutaPokemonService.arregloIdColeccionesSuperiores
          ) as Observable<PokemonInterface>;


        actualizar$
          .subscribe(
            (registroEditado) => {
              registroEditado.habilitado = registro.habilitado;
              const indice = this._sRutaPokemonService.arregloDatos
                .findIndex(
                  (a: PokemonInterface) => a.uid === registro.uid
                );
              this._sRutaPokemonService.arregloDatos[indice] = registroEditado;
              const indiceArregloFiltrado = this._sRutaPokemonService.arregloDatosFiltrado
                .findIndex(
                  (a: PokemonInterface) => a.uid === registro.uid
                );
              this._sRutaPokemonService.arregloDatosFiltrado[indiceArregloFiltrado] = registroEditado;
              this._sRutaPokemonService.matDialog.closeAll();
              this._sRutaPokemonService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: registroEditado.nombre}),
                severidad: 'success'
              });
              this._sRutaPokemonService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaPokemonService.busquedaDto,
                error
              });
              this._sRutaPokemonService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                severidad: 'error'
              });
              this._sRutaPokemonService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }
}
