import {RutaParametrosComun} from '@manticore-labs/ng-2021';

export interface SRutaPokemonParametros extends RutaParametrosComun {
  uid?: string; // Solo para que no de errores de lint (no borrar a menos que tenga otros parametros)
  // nombreCampo?: string; // Todos los campos deben ser STRING o ENUMS DE TIPO STRING
  // Normalmente es TODOS los que estén en el archivo PokemonBusquedaDto
  // nombreCampoRelacion?: string;
  entrenador?: string;
}
