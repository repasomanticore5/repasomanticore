import {Injectable} from '@angular/core';
import {SRutaEntrenadorModule} from './s-ruta-entrenador.module';
import {SRutaEntrenadorParametros} from '../interfaces/s-ruta-entrenador.parametros';
import {EntrenadorBusquedaDto} from '../../../dto/entrenador-busqueda.dto';
import {EntrenadorInterface} from '../../../interfaces/entrenador.interface';
import {EntrenadorService} from '../../../servicios/entrenador/entrenador.service';
import {MatDialog} from '@angular/material/dialog';
import {ENTRENADOR_FORMULARIO} from '../../../formularios/entrenador/entrenador-formulario';
import {GRUPOS_FORMULARIO_BUSQUEDA} from './grupos-formulario-busqueda';
import {GRUPOS_FORMULARIO_CREAR_EDITAR} from './grupos-formulario-crear-editar';
import {RutaComun} from '@manticore-labs/ng-2021';
import {CargandoService} from '../../../../../servicios/cargando/cargando.service';
import {NotificacionService} from '../../../../../servicios/notificacion/notificacion.service';
import {ModalCrearEditarComunComponent} from '../../../../../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';
import {TAKE} from '../../../../../constantes/numero-registros-en-tabla/take';
import {REGISTROS_POR_PAGINA} from '../../../../../constantes/numero-registros-en-tabla/registros-por-pagina';
import {ActivoInactivo} from '../../../../../enums/activo-inactivo';
import {MenuGeneralService} from '../../../../../servicios/menu-general/menu-general.service';

const nombre = 'Entrenador';

@Injectable({
  providedIn: SRutaEntrenadorModule
})
export class SRutaEntrenadorService extends RutaComun<SRutaEntrenadorParametros,
  EntrenadorBusquedaDto,
  EntrenadorService,
  EntrenadorInterface> {
  constructor(
    public readonly _entrenadorService: EntrenadorService,
    public readonly _cargandoService: CargandoService,
    public readonly _notificacionService: NotificacionService,
    public matDialog: MatDialog,
    private readonly _menuGeneralService: MenuGeneralService,
  ) {
    super(
      _entrenadorService,
      _cargandoService,
      _notificacionService,
      matDialog,
      ENTRENADOR_FORMULARIO,
    'formularios.crearEditar.formularioCrearEditar.tituloCrear',
      'formularios.crearEditar.formularioCrearEditar.descripcionCrear',
      'formularios.crearEditar.formularioCrearEditar.tituloActualizar',
      'formularios.crearEditar.formularioCrearEditar.descripcionActualizar',
      GRUPOS_FORMULARIO_BUSQUEDA,
      GRUPOS_FORMULARIO_CREAR_EDITAR,
      () => {
        return [
          // {
          //   nombreCampo: 'nombreCampo', // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Nombre Campò'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return  valorCampo ; // Valor a mostrarse del campo, se pueden hacer transformaciones
          //   },
          // },
          // {
          //   nombreCampo: 'campoOpciones',  // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Opciones'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: ActivoInactivo, nombreCampo: string) => {
          //     switch (valorCampo) { // se puede dar diferentes nombres dependiendo del valor del campo
          //       case 'opcionUno':
          //         return 'Uno';
          //       case 'opcionDos':
          //         return 'Dos';
          //     }
          //   },
          // },
        ];
      },
      ModalCrearEditarComunComponent,
      TAKE,
      REGISTROS_POR_PAGINA,
      ActivoInactivo as { Activo: 1, Inactivo: 0 },
      _menuGeneralService
    );
  }
}
