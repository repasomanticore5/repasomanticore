import {CampoFormulario} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../../../../../constantes/formulario/mensajes-error';
import {TranslocoService} from '@ngneat/transloco';
import {Validators} from '@angular/forms';

export const ENTRENADOR_CAMPO_TEXTO_BUSQUEDA: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  let transloco: TranslocoService | undefined;
  if (claseComponente) {
    if (claseComponente.data) {
      if (claseComponente.data.componente) {
        if (claseComponente.data.componente.translocoService) {
          transloco = claseComponente.data.componente.translocoService;
        }
      }
    }
    if (claseComponente.translocoService) {
      transloco = claseComponente.translocoService;
    }
  }

  return {
    componente: claseComponente,
    validators: [
      // Validators.required
    ],
    asyncValidators: null,
    valorInicial: undefined,
    nombreCampo: '__busquedaGlobal',
    nombreMostrar: 'formularios.busqueda.campoBusqueda.nombreMostrar',
    textoAyuda: 'formularios.busqueda.campoBusqueda.textoAyuda',
    placeholderEjemplo: 'formularios.busqueda.campoBusqueda.placeholderEjemplo',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'formularios.busqueda.campoBusqueda.nombreMostrar',
    },
    estaValido: true,
    hidden: false,
    tipoCampoHtml: 'text',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false
  };
};
