import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_ENTRENADOR = NOMBRE_MODULO + 'Entrenador';

export const SCOPE_ENTRENADOR = (http: HttpClient) => {
  const loaderEntrenador = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-firebase/entrenador/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_ENTRENADOR, loader: loaderEntrenador};
};

export const LOADER_ENTRENADOR = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_ENTRENADOR,
  multi: true
};
