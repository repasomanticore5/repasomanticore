import {EJEMPLO_FIREBASE_MIGAS_PAN} from '../../../miga-pan-general/ejemplo-firebase-migas-pan';
import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_ENTRENADOR} from '../transloco/loader-entrenador';

export let idEntrenador = 'gestion-entrenador';
export const ENTRENADOR_MIGAS_PAN
  : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {
  const id = idEntrenador;
  return {
    id,
    label: 'migaPan',
    routerLink: [
      ...EJEMPLO_FIREBASE_MIGAS_PAN(componente).routerLink,
      'gestion-entrenador'
    ],
    scopeTransloco: NOMBRE_SCOPE_ENTRENADOR
  };
};
