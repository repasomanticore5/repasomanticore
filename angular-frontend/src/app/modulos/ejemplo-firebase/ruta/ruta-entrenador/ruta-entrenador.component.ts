import {Component, OnInit} from '@angular/core';
import {SRutaEntrenadorService} from './s-ruta-entrenador/s-ruta-entrenador.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SRutaEntrenadorParametros} from './interfaces/s-ruta-entrenador.parametros';
import {EntrenadorBusquedaDto} from '../../dto/entrenador-busqueda.dto';
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {CrearEntrenador} from './clases/crear-entrenador';
import {ActualizarEntrenador} from './clases/actualizar-entrenador';
import {ENTRENADOR_FORMULARIO_BUSQUEDA} from './busqueda-filtros/busqueda/entrenador-formulario-busqueda';
import {
  ArregloFiltroBusquedaFirestore,
  EventoCambioAutocomplete,
  EventoCambioFormulario, ObjetoBusquedaADto, ObjetoInternacionalizacionFormulario,
  SetearObjeto,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';
import {ActivoInactivo} from '../../../../enums/activo-inactivo';
import {Observable} from 'rxjs';
import {EJEMPLO_FIREBASE_MIGAS_PAN} from '../../miga-pan-general/ejemplo-firebase-migas-pan';
import {ENTRENADOR_MIGAS_PAN} from './migas-pan/entrenador-migas-pan';
import {TranslocoService} from '@ngneat/transloco';
import {NOMBRE_SCOPE_ENTRENADOR} from './transloco/loader-entrenador';
import {NOMBRE_MODULO_ASSETS} from '../../constantes/nombre-modulo-assets';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';

@Component({
  selector: 'app-ruta-entrenador',
  templateUrl: './ruta-entrenador.component.html',
  styleUrls: ['./ruta-entrenador.component.scss']
})
export class RutaEntrenadorComponent implements OnInit {

  columnasPrimeTable = [
    // {
    //   field: 'nombreCampo',
    //   header: 'Nombre a mostrarse',
    //   posicion: 'text-center',
    //   tamanio: '60%', // tamaño en porcentaje de la columna
    //     fnMostrar: (valor:TipoValor)=>{
    //       switch (valor) {
    //         case TipoValor.ValorUno:
    //           return 'Valor de uno';
    //         case TipoValor.ValorDos:
    //           return 'Valor de dos';
    //         default:
    //           return valor;
    //       }
    //     }
    // },
    {
      field: 'uid',
      header: 'codigo',
      posicion: 'text-center',
      tamanio: '20%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'nombre',
      header: 'nombre',
      posicion: 'text-center',
      tamanio: '20%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'pueblo',
      header: 'pueblo',
      posicion: 'text-center',
      tamanio: '20%', // tamaño en porcentaje de la columna
      fnMostrar: (valor: string) => {
        return valor;
      }
    },
    {
      field: 'sisHabilitado',
      header: 'habilitado',
      posicion: 'text-left',
      tamanio: '20%'
    },
    {
      field: 'id',
      header: 'acciones',
      posicion: 'text-right',
      tamanio: '20%'
    }
  ];

  opciones = [
    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     return ['/empieza', id, 'termina-ruta']; // ruta en arreglo
    //   },
    //   nombre: 'Otra acción de ...'
    // }
    {
      ruta: (id: string) => { // parametros de la ruta
        return [
          ...ENTRENADOR_MIGAS_PAN(this).routerLink, // RUTA ACTUAL
          id,  // ids necesarios
          'gestion-pokemon' // opcional path final
        ]; // ruta en arreglo
      },
      nombre: 'opcionesIrRuta.pokemon',
      rutaImagen: '/pokemon/imagen-principal.svg'
    }
  ];

  camposFormularioBusqueda = ENTRENADOR_FORMULARIO_BUSQUEDA;
  valorBusqueda = '';
  formularioBusqueda?: TodosCamposValidados;
  botonAceptarModalDisabled = true;

  formularioCrearEditar?: TodosCamposValidados;
  sistemaLogisticoSeleccionado?: EntrenadorInterface;

  stepperActualBusqueda = 0;

  nombreServicio = '_sRutaEntrenadorService';

  // Usar cuando se necesite ocultar campos que dependan de otros:

  // ocultarFormulario = false;
  // camposRequeridos = {
  //   nombreCampoDependiendeUno: false,
  //   nombreCampoDependiendeDos: false,
  // };


  migasPan: MigaDePanInterface[] = [];

  rutaImagenAssets = 'assets/img/' + NOMBRE_MODULO_ASSETS;

  rutaImagenPrincipal = this.rutaImagenAssets + '/entrenador/imagen-principal.svg';

  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
    botonAtras: 'botonAtras',
    botonLimpiarFormulario: 'botonLimpiarFormulario',
    botonSiguiente: 'botonSiguiente',
    nombreScopeTransloco: NOMBRE_SCOPE_ENTRENADOR,
  };

  constructor(
    public readonly _sRutaEntrenadorService: SRutaEntrenadorService,
    private readonly _activatedRoute: ActivatedRoute,
    public readonly _router: Router,

    public readonly translocoService: TranslocoService,

  ) {
  }

  ngOnInit(): void {
    this._obtenerParametros();
  }

  private _obtenerParametros(): void {
    this._sRutaEntrenadorService
      .obtenerYGuardarParametrosRuta(this._activatedRoute)
      .subscribe(
        (parametros: SRutaEntrenadorParametros) => {
          const busqueda = this._sRutaEntrenadorService
            .setearParametrosDeBusqueda(parametros, EntrenadorBusquedaDto);
          // Setear los campos de búsqueda para el registro
          // this._sRutaEntrenadorService.arregloCamposBusqueda = [
          //   'nombre',
          //   'pueblo'
          // ];
          // Setear los valores de las colecciones superiores
          // Ej:
          // this._sRutaEntrenadorService.arregloIdColeccionesSuperiores = [
          //   parametros.nombreParametroPrimerNivel,
          //   parametros.nombreParametroSegundoNivel,
          // ];
          this._sRutaEntrenadorService
            .busquedaDto = busqueda;

          this._sRutaEntrenadorService
            .construirMigasPan(
              this,
              [
                EJEMPLO_FIREBASE_MIGAS_PAN,
                ENTRENADOR_MIGAS_PAN,
              ],
            );
          this.buscarConFiltros();
        }
      );
  }

  buscarConFiltros(): void {
    if (this._sRutaEntrenadorService.busquedaDto) {
      this._sRutaEntrenadorService.first = 0;
      // Setear whereFirestore al valor inicial
      this._sRutaEntrenadorService.whereFirestore = [];
      this._sRutaEntrenadorService.lastDocumentFirestore = undefined;
      this._sRutaEntrenadorService.yaNoHayMasRegistros = false;
      // Dependiendo de los filtros de búsqueda setear las búsquedas
      // const arregloFiltroBusquedaFirestore: ArregloFiltroBusquedaFirestore[] = [
      //   {
      //     fieldPath: 'nombrePropiedadBusquedaDto',
      //     opStr: '=='
      //   }
      // ];
      const arregloFiltroBusquedaFirestore: ArregloFiltroBusquedaFirestore[] = [
        {
          fieldPath: '__busquedaGlobal',
          opStr: 'array-contains'
        },
        {
          fieldPath: 'sisHabilitado',
          opStr: '=='
        },
        // Anadir todos las busquedas que falten
        // {
        //   fieldPath: 'campo', // nombre de campo Firestore
        //   opStr: '==' // cualquier opStr de Firestore
        // },
      ];
      this._sRutaEntrenadorService.establecerWhereFirestore(arregloFiltroBusquedaFirestore);
    }
    this.buscarSuscrito();
  }

  buscarSuscrito() {
    this._sRutaEntrenadorService._cargandoService.habilitarCargando();
    this._sRutaEntrenadorService
      .buscar()
      .subscribe(
        (data) => {
          this._sRutaEntrenadorService.registrosPagina = data[0].length;
          this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
        },
        (error) => {
          console.error({
            mensaje: 'Error cargando registros',
            data: this._sRutaEntrenadorService.busquedaDto,
            error
          });
          this._sRutaEntrenadorService._notificacionService.anadir({
            titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
            detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
            severidad: 'error'
          });
          this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
        }
      );
  }

  // Usar para generar campos de formulario dinamicos

  // generarFormulario() {
  //   this.mostrarFormularioDePrueba = false;
  //   const arregloDatos: CampoAutogenerado[] = [];
  //   const grupoFormulario: GrupoFormulario[] = [];
  //   // crear los campos autogenerados "arregloDatos" y
  //   // AL MENOS debe existir un "grupoFormulario" que contenga a todos los campos para una visualización sencilla
  //   const respuestaAutogenerado = this._sRutaArticuloAtributoService
  //       .generarFormularioAutogenerado(
  //           arregloDatos,
  //           MENSAJES_ERROR,
  //           this,
  //           grupoFormulario
  //       )
  //   this.arregloFormulario.arregloValores = respuestaAutogenerado.arregloValores;
  //   this.arregloFormulario.grupo = respuestaAutogenerado.grupo;
  //   this.arregloFormulario.campos = respuestaAutogenerado.campos;
  //   this.mostrarFormularioDePrueba = true;
  // }

  formularioValidoBusqueda(formularioBusquedaValido: TodosCamposValidados): void {
    this.formularioBusqueda = formularioBusquedaValido;
    if (formularioBusquedaValido.valido) {
      this._sRutaEntrenadorService.setearCamposBusquedaValidos(
        formularioBusquedaValido,
        EntrenadorBusquedaDto
      );
      // Transforma objetos a valores, si no hay objetos en la busqueda
      // como un autocomplete o select por ejemplo, entonces dejar vacío.
      const arregloCamposBusquedaConObjeto: ObjetoBusquedaADto[] = [
        {
          nombreCampoEnBusqueda: 'sisHabilitado',
          nombreCampoEnDto: 'sisHabilitado'
        },
        // {
        //   nombreCampoEnBusqueda: 'campoSelectOAutocomplete',
        //   nombreCampoEnDto: 'nombreCampoSelectOAutocomplete'
        // },
      ];
      this._sRutaEntrenadorService.transformarObjetosBusquedaABusquedaDto(
        arregloCamposBusquedaConObjeto
      );
    }
  }

  cambioCampoBusqueda(eventoCambioCampoBusqueca: EventoCambioFormulario): void {
    const campoBusquedaActualizado = eventoCambioCampoBusqueca.campoFormulario;
    if (campoBusquedaActualizado) {
      this._sRutaEntrenadorService
        .setearCamposBusquedaAUndefined(
          campoBusquedaActualizado,
          EntrenadorBusquedaDto
        );
    }

  }

  formularioBusquedacambioCampoAutocomplete(evento: EventoCambioAutocomplete) {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete,
                                          registro?: EntrenadorInterface): void {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  // seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete){
  //   if(evento.campoFormulario){
  // switch (evento.campoFormulario.nombreCampo) {
  //   case 'nombreCampo': // nombres de cada uno de los campos autocomplete
  //     // this.buscarAutocompleteNombreCampo(evento); // ejecutar una búsqueda con los servicios pertinentes
  //     break;
  // }
  // }
  // }

  // buscarAutocompleteNombreCampo(evento: EventoCambioAutocomplete) {
  //   const busqueda: NombreCampoBusquedaDto = {
  //     nombreCampo: evento.query,
  //   };
  //   this._nombrCampoService
  //       .buscar(busqueda)
  //       .toPromise()
  //       .then(res => res as [NombreCampoInterface[], number])
  //       .then(data => {
  //         const arregloDatos = data[0];
  //         // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
  //         const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
  //         if (evento.campoFormulario.autocomplete) {
  //           if (Array.isArray(arregloDatos)) {
  //             evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
  //           } else {
  //             evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
  //           }
  //         }
  //         return data;
  //       });
  // }

  formularioCrearEditarValido(estaValido: TodosCamposValidados,
                              registro?: EntrenadorInterface): void {
    this._sRutaEntrenadorService.setearFormularioEnModal(estaValido);
    if (estaValido.valido) {
      this.formularioCrearEditar = estaValido;
      this.botonAceptarModalDisabled = false;
    } else {
      this.botonAceptarModalDisabled = true;
    }
  }

  formularioCrearEditarCambioCampo(eventoCambioCampo: EventoCambioFormulario,
                                   registro?: EntrenadorInterface): void {
    if (eventoCambioCampo.campoFormulario) {
      // Esta parte del codigo solo se debe de utilizar
      // si existe cambios de estructura del formulario
      // EJ: si selecciona el valor X de un campo entonces muestro tales campos
      // const formularioValido = {
      //   valido: false,
      //   camposFormulario: [eventoCambioCampo.campoFormulario],
      // };
      // this._sRutaEntrenadorService.setearFormularioEnModal(formularioValido);
      // switch (eventoCambioCampo.campoFormulario.nombreCampo) {
      //   case 'nombreCampo':
      //     if (registro) {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo,
      //           registro
      //       );
      //     } else {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo
      //       );
      //     }
      //     break;
      //   default:
      //     break;
      // }

    }
  }

//   actualizarValidez(
//       valorCampoValidez: any,
//       nombrePropiedadValidez: string,
//       registro?: any,
// ) {
//     // Los campos definidos "nombrePropiedad" cuando tengan un valor != undefined van a activar
//     // los campos que están en "nombresCamposRequeridosQueAfecta"
//     const camposAEstablecerValidezArreglo: CamposEstablecerValidez[] = [
//       {
//         nombrePropiedad: 'nombreCampo',
//         nombresCamposRequeridosQueAfecta: ['campoUno', 'campoDos']
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoTres', 'campoCuatro'],
//         nombresCamposRequeridosAOcultar: ['campoCinco', 'campoSeis'],
//         valorPropiedad: 'valorTresCuatro',
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoCinco', 'campoSeis'],
//         nombresCamposRequeridosAOcultar: ['campoTres', 'campoCuatro'],
//         valorPropiedad: 'valorCincoSeis',
//       },
//     ];
//
//     this._sRutaEntrenadorService.establecerValidezDeFormulario<RutaEntrenadorComponent>(
//         this,
//         camposAEstablecerValidezArreglo,
//         valorCampoValidez,
//         nombrePropiedadValidez,
//         undefined,
//         registro,
//     );
//   }

  abrirModal(
    registro: EntrenadorInterface
  ) {
    // Si se tienen campos dependientes se deben de
    // activarlos antes de editarlos
    // if(camposRequeridos.campoDependeUno){
    //   camposRequeridos.nombreCampoDependienteUno
    // }
    // if(camposRequeridos.campoDependeDos){
    //   camposRequeridos.nombreCampoDependienteDos
    // }
    this._sRutaEntrenadorService.abrirModal(this, registro);
  }


  crearEditar(
    registro?: EntrenadorInterface,
  ): void {
    if (registro) {
      this.editar(registro);
    } else {
      this.crear();
    }
  }

  crear(): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaEntrenadorService.obtenerCampos<CrearEntrenador>(
        CrearEntrenador,
        this.formularioCrearEditar
      );
      // Transforma objetos a valores, si no hay objetos en el formulario
      // como un autocomplete por ejemplo, se debe dejar vacío.
      const arregloPropiedades: SetearObjeto[] = [
        // {
        //   nombreCampo: 'sisHabilitado',
        //   nombrePropiedadObjeto: 'sisHabilitado'
        // }
      ];
      camposCrear.objetoCrear = this._sRutaEntrenadorService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );

      // Setear campos extra
      // Ej:
      // camposCrear.objetoCrear.habilitado = 1;
      // Ej: Relación
      // if (this._sRutaEntrenadorService.busquedaDto) {
      //   camposCrear.objetoCrear.nombreCampoRelacion = this._sRutaEntrenadorService.busquedaDto.nombreCampoRelacion as number;
      // }

      const campos = camposCrear.objetoCrear as CrearEntrenador;
      camposCrear.objetoCrear.sisHabilitado = ActivoInactivo.Activo;
      const codigo = campos.codigo;
      // delete campos['codigo'];
      if (this._sRutaEntrenadorService.busquedaDto) {
        this._sRutaEntrenadorService._cargandoService.habilitarCargando();
        const crear$ = this._sRutaEntrenadorService
          ._entrenadorService
          .crear(
            camposCrear.objetoCrear,
            codigo
          ) as Observable<EntrenadorInterface>;
        crear$
          .subscribe(
            (nuevoRegistro) => {
              nuevoRegistro.habilitado = true;
              this._sRutaEntrenadorService.arregloDatos.unshift(nuevoRegistro);
              this._sRutaEntrenadorService.arregloDatosFiltrado.unshift(nuevoRegistro);
              this._sRutaEntrenadorService.matDialog.closeAll();
              this._sRutaEntrenadorService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastExitoCrear.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoCrear.body', {nombre: nuevoRegistro.nombre}),
                severidad: 'success'
              });
              this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaEntrenadorService.busquedaDto,
                error
              });
              this._sRutaEntrenadorService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                severidad: 'error'
              });
              this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }

  editar(
    registro: EntrenadorInterface,
  ): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaEntrenadorService.obtenerCampos<ActualizarEntrenador>(
        ActualizarEntrenador,
        this.formularioCrearEditar,
        true
      );
      const arregloPropiedades: SetearObjeto[] = [
        // {
        //   nombreCampo: 'tipoAtributo',
        //   nombrePropiedadObjeto: 'idTiposAtrib',
        // }
      ];
      camposCrear.objetoCrear = this._sRutaEntrenadorService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );
      if (registro.uid) {
        this._sRutaEntrenadorService._cargandoService.habilitarCargando();
        const editar$ = this._sRutaEntrenadorService
          ._entrenadorService
          .actualizar(
            registro,
            camposCrear.objetoCrear,
            this._sRutaEntrenadorService.arregloIdColeccionesSuperiores
          ) as Observable<EntrenadorInterface>;
        editar$
          .subscribe(
            (registroEditado) => {
              registroEditado.habilitado = registro.habilitado;
              const indice = this._sRutaEntrenadorService.arregloDatos
                .findIndex(
                  (a: EntrenadorInterface) => a.uid === registro.uid
                );
              this._sRutaEntrenadorService.arregloDatos[indice] = registroEditado;
              const indiceArregloFiltrado = this._sRutaEntrenadorService.arregloDatosFiltrado
                .findIndex(
                  (a: EntrenadorInterface) => a.uid === registro.uid
                );
              this._sRutaEntrenadorService.arregloDatosFiltrado[indiceArregloFiltrado] = registroEditado;
              this._sRutaEntrenadorService.matDialog.closeAll();
              this._sRutaEntrenadorService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: registroEditado.nombre}),
                severidad: 'success'
              });
              this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaEntrenadorService.busquedaDto,
                error
              });
              this._sRutaEntrenadorService._notificacionService.anadir({
                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),
                severidad: 'error'
              });
              this._sRutaEntrenadorService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }


}
