import {EntidadComunProyectoFirebase} from '../../../../../abstractos/entidad-comun-proyecto.firebase';

export interface SRutaEntrenadorParametros extends EntidadComunProyectoFirebase {
  id?: string;
  // nombreCampo?: string; // Todos los campos deben ser STRING o ENUMS DE TIPO STRING
  // Normalmente es TODOS los que estén en el archivo EntrenadorBusquedaDto
  // nombreCampoRelacion?: string;
}
