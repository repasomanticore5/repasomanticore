import {RutaEntrenadorComponent} from '../ruta/ruta-entrenador/ruta-entrenador.component';
import {RutaPokemonComponent} from '../ruta/ruta-pokemon/ruta-pokemon.component';


export const DECLARATIONS_MODULO_EJEMPLO_FIREBASE = [
  RutaEntrenadorComponent,
  RutaPokemonComponent,
];
