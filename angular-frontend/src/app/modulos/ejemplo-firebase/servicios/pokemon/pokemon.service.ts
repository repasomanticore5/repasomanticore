import {Injectable} from '@angular/core';
import {PokemonModule} from './pokemon.module';
import {PokemonInterface} from '../../interfaces/pokemon.interface';
import {FirestoreServicioAbstractClass} from '@manticore-labs/firebase-angular';
import {AngularFirestore} from '@angular/fire/firestore';
import {FirebaseUtilService} from '../../../../servicios/firebase/firebase-util.service';
import {TAKE} from '../../../../constantes/numero-registros-en-tabla/take';

@Injectable({
  providedIn: PokemonModule
})
export class PokemonService


  extends FirestoreServicioAbstractClass<PokemonInterface> {
  constructor(
    private readonly servicioFirebase: AngularFirestore,
    private readonly firebaseUtilService: FirebaseUtilService,
  ) {
    super(
      'pokemon',
      servicioFirebase,
      firebaseUtilService,
      TAKE,
      [
        {
          collectionName: 'entrenador'
        }
      ],
      'sisHabilitado',
      ['nombre', 'tipo'], // llenar con los campos de busqueda pertinentes
    );
  }

}
