import {Injectable} from '@angular/core';
import {EntrenadorModule} from './entrenador.module';
import {EntrenadorInterface} from '../../interfaces/entrenador.interface';
import {FirestoreServicioAbstractClass} from '@manticore-labs/firebase-angular';
import {AngularFirestore} from '@angular/fire/firestore';
import {FirebaseUtilService} from '../../../../servicios/firebase/firebase-util.service';
import {TAKE} from '../../../../constantes/numero-registros-en-tabla/take';

@Injectable({
  providedIn: EntrenadorModule
})
export class EntrenadorService
  extends FirestoreServicioAbstractClass<EntrenadorInterface> {
  constructor(
    private readonly servicioFirebase: AngularFirestore,
    private readonly firebaseUtilService: FirebaseUtilService,
  ) {
    super(
      'entrenador',
      servicioFirebase,
      firebaseUtilService,
      TAKE,
      [],
      'sisHabilitado',
      ['nombre', 'pueblo', 'codigo'],
    );
  }
}
