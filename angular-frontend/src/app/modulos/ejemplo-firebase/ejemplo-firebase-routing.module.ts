import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RutaEntrenadorComponent} from './ruta/ruta-entrenador/ruta-entrenador.component';
import {ENTRENADOR_MIGAS_PAN} from './ruta/ruta-entrenador/migas-pan/entrenador-migas-pan';
import {POKEMON_MIGAS_PAN} from './ruta/ruta-pokemon/migas-pan/pokemon-migas-pan';
import {RutaPokemonComponent} from './ruta/ruta-pokemon/ruta-pokemon.component';

const routes: Routes = [

  {
    path: ENTRENADOR_MIGAS_PAN(this).id,
    component: RutaEntrenadorComponent
  },
  {
    path: POKEMON_MIGAS_PAN(this).id,
    component: RutaPokemonComponent
  },
  {
    path: '',
    redirectTo: ENTRENADOR_MIGAS_PAN(this).id,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjemploFirebaseRoutingModule {
}
