import {NgModule} from '@angular/core';
import {PROVIDERS_MODULO_EJEMLO_FIREBASE} from './imports/providers-modulo-ejemplo-nest';
import {IMPORTS_MODULO_EJEMPLO_FIREBASE} from './imports/imports-modulo-ejemplo-nest';
import {DECLARATIONS_MODULO_EJEMPLO_FIREBASE} from './imports/declarations-modulo-ejemplo-nest';


@NgModule({
  declarations: [
    ...DECLARATIONS_MODULO_EJEMPLO_FIREBASE,
  ],
  imports: [
    ...IMPORTS_MODULO_EJEMPLO_FIREBASE,
  ],
  providers: [
    ...PROVIDERS_MODULO_EJEMLO_FIREBASE,
  ],
})
export class EjemploFirebaseModule {
}
