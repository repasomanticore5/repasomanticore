import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../constantes/nombre-modulo';

export const NOMBRE_SCOPE_FIREBASE = NOMBRE_MODULO;

export const SCOPE_FIREBASE = (http: HttpClient) => {
  const loaderFirebase = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-firebase/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_FIREBASE, loader: loaderFirebase};
};

export const LOADER_FIREBASE = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_FIREBASE,
  multi: true
};
