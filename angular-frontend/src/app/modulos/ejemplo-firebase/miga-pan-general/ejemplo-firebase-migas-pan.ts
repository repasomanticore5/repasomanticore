import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_FIREBASE} from '../transloco/loader-firebase';

export const EJEMPLO_FIREBASE_MIGAS_PAN
  : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {
  const id = 'ejemplo-firebase';
  return {
    id,
    label: 'migaPan',
    routerLink: ['/ejemplo-firebase'],
    scopeTransloco: NOMBRE_SCOPE_FIREBASE
  };
};
