import {NgModule} from '@angular/core';
import {DECLARATIONS_MODULO_ARCHIVOS} from './imports/declarations-modulo-archivos';
import {IMPORTS_MODULO_ARCHIVOS} from './imports/imports-modulo-archivos';
import {PROVIDERS_MODULO_ARCHIVOS} from './imports/providers-modulo-archivos';


@NgModule({
  declarations: [
    ...DECLARATIONS_MODULO_ARCHIVOS,
  ],
  imports: [
    ...IMPORTS_MODULO_ARCHIVOS,
  ],
  providers: [
    ...PROVIDERS_MODULO_ARCHIVOS,
  ],
})
export class ArchivosRutaModule {
}
