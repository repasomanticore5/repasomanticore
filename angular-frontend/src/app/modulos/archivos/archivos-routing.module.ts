import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ARCHIVO_SECUNDARIO_MIGAS_PAN} from './ruta/ruta-archivo-secundario/migas-pan/archivo-secundario-migas-pan';
import {RutaArchivoSecundarioComponent} from './ruta/ruta-archivo-secundario/ruta-archivo-secundario.component';

const routes: Routes = [
  {
    path: ARCHIVO_SECUNDARIO_MIGAS_PAN(this).id,
    component: RutaArchivoSecundarioComponent
  },
  {
    path: '',
    redirectTo: ARCHIVO_SECUNDARIO_MIGAS_PAN(this).id,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchivosRoutingModule {
}
