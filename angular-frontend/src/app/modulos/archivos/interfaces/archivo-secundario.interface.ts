
export interface ArchivoSecundarioInterface {

  idReferencial?: number;
  buffer?: string;
  tipo?: string;
  id?: number;
  mimetype?: string;
  originalname?: string;
  size?: number;
  nombreIdentificador?: string;
  nombre?: string;
  descripcion?: string;

  [key: string]: any;
}
