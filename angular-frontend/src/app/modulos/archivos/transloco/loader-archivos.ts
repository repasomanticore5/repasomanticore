import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../constantes/nombre-modulo';

export const NOMBRE_SCOPE_ARCHIVOS = NOMBRE_MODULO;

export const SCOPE_ARCHIVOS = (http: HttpClient) => {
  const loaderArchivo = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/archivos/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_ARCHIVOS, loader: loaderArchivo};
};

export const LOADER_ARCHIVOS = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_ARCHIVOS,
  multi: true
};
