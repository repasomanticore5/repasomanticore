import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_ARCHIVO_SECUNDARIO} from '../transloco/loader-archivo-secundario';
import {SRutaArchivoSecundarioParametros} from '../interfaces/s-ruta-archivo-secundario.parametros';
import {TipoArchivo} from '../../../../../enums/tipo-archivo';
import {ARCHIVOS_MIGAS_PAN} from '../../../miga-pan-general/archivos-migas-pan';

export const ARCHIVO_SECUNDARIO_MIGAS_PAN
  : (componente: any, parametrosQuemados?: SRutaArchivoSecundarioParametros) => MigaPanRuta = (componente: any, parametrosQuemados?: SRutaArchivoSecundarioParametros) => {
  // Inicializar todos los parametros de ruta con '0'
  let parametros: SRutaArchivoSecundarioParametros = {
    nombreIdentificador: '',
    tipo: TipoArchivo.Archivo,
    idReferencial: '',
    tituloVista: '',
    // parametroRutaUno: '0',
    // parametroRutaDos: '0',
    // parametroRutaTres: '0',
  };
  let ruta: any = {};
  if (componente && !parametros) {
    if (componente.nombreServicio) {
      ruta = componente[componente.nombreServicio] as any;
      parametros = ruta.parametros as SRutaArchivoSecundarioParametros;
    }
  }
  if (parametrosQuemados) {
    parametros = parametrosQuemados;
  }
  return {
    // path que va a ir en el RoutingModule
    id: 'gestion-archivo-secundario/:idReferencial/:tipo/:nombreIdentificador/:tituloVista',
    // Nombre a visualizarse
    label: 'migaPan',

    // Aqui se debe de llenar el arreglo del path de esta ruta.
    // Se puede utilizar las migas de pan de la anterior ruta.
    routerLink: [
      ...ARCHIVOS_MIGAS_PAN(componente).routerLink,
      'gestion-archivo-secundario',
      parametros.idReferencial as string,
      parametros.tipo as string,
      parametros.nombreIdentificador as string,
      parametros.tituloVista as string,
      // ...MODULO_PAPA_MIGAS_PAN(componente).routerLink, // Se llama a la miga de pan PAPA de esta ruta.
      // parametros.parametroRutaTres as string, // parametro de ruta del ultimo parametro de ruta, en este caso "parametroRutaTres"
      // 'path-final' // si existe un path al final se lo pone aqui
    ],
    scopeTransloco: NOMBRE_SCOPE_ARCHIVO_SECUNDARIO,  // si no se quiere internacionalizar poner un string vacio Ej: ''
  };
};
