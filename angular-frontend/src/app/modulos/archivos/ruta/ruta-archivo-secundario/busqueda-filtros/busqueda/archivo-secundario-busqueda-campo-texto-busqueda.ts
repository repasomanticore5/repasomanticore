import {CampoFormulario} from '@manticore-labs/ng-2021';
import {RutaArchivoSecundarioComponent} from '../../ruta-archivo-secundario.component';
import {MENSAJES_ERROR} from '../../../../../../constantes/formulario/mensajes-error';

export const ARCHIVO_SECUNDARIO_BUSQUEDA_CAMPO_TEXTO_BUSQUEDA: (claseComponente: RutaArchivoSecundarioComponent) => CampoFormulario = (claseComponente: RutaArchivoSecundarioComponent) => {
  return {
    componente: claseComponente,
    validators: [],
    asyncValidators: null,
    valorInicial: undefined,
    nombreCampo: 'busqueda',


    nombreMostrar: 'formularios.busqueda.campoBusqueda.nombreMostrar',


    textoAyuda: 'formularios.busqueda.campoBusqueda.textoAyuda',


    placeholderEjemplo: 'formularios.busqueda.campoBusqueda.placeholderEjemplo',


    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {

      nombreCampo: 'formularios.busqueda.campoBusqueda.nombreMostrar',

    },
    estaValido: true,
    hidden: false,
    tipoCampoHtml: 'text',
    valorActual: '',
    tamanioColumna: 12,
    disabled: false
  };
};
