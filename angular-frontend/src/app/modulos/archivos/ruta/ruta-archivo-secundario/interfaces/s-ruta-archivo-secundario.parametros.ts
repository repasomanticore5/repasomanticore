import {RutaParametrosComun} from '@manticore-labs/ng-2021';
import {TipoArchivo} from '../../../../../enums/tipo-archivo';

export interface SRutaArchivoSecundarioParametros extends RutaParametrosComun {
  id?: string;
  // nombreCampo?: string; // Todos los campos deben ser STRING o ENUMS DE TIPO STRING

  // Normalmente es TODOS los que estén en el archivo ArchivoSecundarioBusquedaDto
  // nombreCampoRelacion?: string;

  // Si hay colecciones superiores en la ruta se las pone aqui
  // nombreCampoColeccionSuperior?: string;

  nombreIdentificador?: string;
  tipo?: TipoArchivo;
  idReferencial?: string;
  tituloVista?: string;
}
