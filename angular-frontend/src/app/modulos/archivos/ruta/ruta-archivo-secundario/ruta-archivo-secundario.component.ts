import {Component, OnInit} from '@angular/core';
import {SRutaArchivoSecundarioService} from './s-ruta-archivo-secundario/s-ruta-archivo-secundario.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SRutaArchivoSecundarioParametros} from './interfaces/s-ruta-archivo-secundario.parametros';
import {ArchivoSecundarioBusquedaDto} from '../../dto/archivo-secundario-busqueda.dto';
import {ArchivoSecundarioInterface} from '../../interfaces/archivo-secundario.interface';
import {CrearArchivoSecundario} from './clases/crear-archivo-secundario';
import {ActualizarArchivoSecundario} from './clases/actualizar-archivo-secundario';
import {ARCHIVO_SECUNDARIO_FORMULARIO_BUSQUEDA} from './busqueda-filtros/busqueda/archivo-secundario-formulario-busqueda';
import {MenuItem} from 'primeng/api';
import {ARCHIVO_SECUNDARIO_MIGAS_PAN} from './migas-pan/archivo-secundario-migas-pan';
import {
  ArregloFiltroBusquedaFirestore,
  EventoCambioAutocomplete,
  EventoCambioFormulario, ObjetoBusquedaADto, ObjetoInternacionalizacionFormulario,
  SetearObjeto,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';
import {TranslocoService} from '@ngneat/transloco';
import {NOMBRE_SCOPE_ARCHIVO_SECUNDARIO} from './transloco/loader-archivo-secundario';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';
import {NOMBRE_MODULO_ASSETS} from '../../../ejemplo-firebase/constantes/nombre-modulo-assets';
import {Observable} from 'rxjs';
import {ARCHIVOS_MIGAS_PAN} from '../../miga-pan-general/archivos-migas-pan';
import {NOMBRE_MODULO_ASSETS_ARCHIVOS} from '../../constantes/nombre-modulo-assets';
import {EntrenadorInterface} from '../../../ejemplo-nest/interfaces/entrenador.interface';
import {TipoArchivo} from '../../../../enums/tipo-archivo';
import {ARTICULO_ARCHIVO_FORMULARIO} from '../../../../formularios/formulario-archivos/archivo-formulario';

@Component({
  selector: 'app-ruta-archivo-secundario',
  templateUrl: './ruta-archivo-secundario.component.html',
  styleUrls: ['./ruta-archivo-secundario.component.scss']
})
export class RutaArchivoSecundarioComponent implements OnInit {

  columnasPrimeTable = [
    // {
    //   field: 'nombreCampo',
    //   header: 'Nombre a mostrarse',
    //   posicion: 'text-center',
    //   tamanio: '60%', // tamaño en porcentaje de la columna
    //   fnMostrar: (valor: string) => {
    //   return valor;
    // },
    //     fnMostrar: (valor:TipoValor)=>{
    //       switch (valor) {
    //         case TipoValor.ValorUno:
    //           return 'Valor de uno';
    //         case TipoValor.ValorDos:
    //           return 'Valor de dos';
    //         default:
    //           return valor;
    //       }
    //     },
    // },
    // {
    //   field: '',
    //   header: 'Imagen',
    //   posicion: 'text-center',
    //   tamanio: '15%', // tamaño en porcentaje de la columna
    //   fnMostrarImagen: (archivoSecundario: ArchivoSecundarioInterface) => {
    //     if (archivoSecundario.sis_imagen) {
    //       return this._sRutaArchivoSecundarioService._domSanitizer.bypassSecurityTrustResourceUrl(`data:${archivoSecundario.sis_imagen.mimetype};base64, ${archivoSecundario.sis_imagen.buffer}`);
    //     } else {
    //       return '';
    //     }
    //   },

    {
      field: '',
      header: 'sis_IM',
      posicion: 'text-center',
      tamanio: '80%', // tamaño en porcentaje de la columna
      fnMostrarImagen: (archivoSecundario: ArchivoSecundarioInterface) => {
        return this._sRutaArchivoSecundarioService._domSanitizer.bypassSecurityTrustResourceUrl(`data:${archivoSecundario.mimetype};base64, ${archivoSecundario.buffer}`);
      }
    },
    {
      field: 'sisHabilitado',
      header: 'habilitado',
      posicion: 'text-left',
      tamanio: '20%'
    },
    // {
    //   field: 'id',
    //   header: 'acciones',
    //   posicion: 'text-right',
    //   tamanio: '20%'
    // }
  ];

  opciones = [
    // {
    //   ruta: (id: string) => { // parametros de la ruta
    //     return [
    //     ...ARCHIVO_SECUNDARIO_MIGAS_PAN(this).routerLink, // RUTA ACTUAL
    //     id,  // ids necesarios
    //     'termina-ruta' // opcional path final
    //     ]; // ruta en arreglo
    //   },
    //   nombre: 'Otra acción de ...',
    //   rutaImagen: '/archivo-secundario/imagen-principal.svg'
    // }
  ];

  camposFormularioBusqueda = ARCHIVO_SECUNDARIO_FORMULARIO_BUSQUEDA;
  valorBusqueda = '';
  formularioBusqueda?: TodosCamposValidados;
  botonAceptarModalDisabled = true;

  formularioCrearEditar?: TodosCamposValidados;
  sistemaLogisticoSeleccionado?: ArchivoSecundarioInterface;

  stepperActualBusqueda = 0;

  nombreServicio = '_sRutaArchivoSecundarioService';

  // Usar cuando se necesite ocultar campos que dependan de otros:

  // ocultarFormulario = false;
  // camposRequeridos = {
  //   nombreCampoDependiendeUno: false,
  //   nombreCampoDependiendeDos: false,
  // };

  // Si existen campos AUTOGENERADOS en estas tablase se necesitarian estas variables

  // botonAceptarModalAutogeneradoDisabled = false;
  //
  // camposAutogeneradosAGuardar: CampoFormulario[] = [];
  //
  // arregloAutogenerado: { arregloDatos: CampoAutogenerado[]; grupoFormulario: GrupoFormulario[] } = {
  //   arregloDatos: [],
  //   grupoFormulario: [],
  // };

  migasPan: MigaDePanInterface[] = [];

  rutaImagenAssets = 'assets/img/' + NOMBRE_MODULO_ASSETS_ARCHIVOS;

  rutaImagenPrincipal = this.rutaImagenAssets + '/archivo-secundario/imagen-principal.svg';


  objetoInternacionalizacion: ObjetoInternacionalizacionFormulario = {
    botonAtras: 'botonAtras',
    botonLimpiarFormulario: 'botonLimpiarFormulario',
    botonSiguiente: 'botonSiguiente',
    nombreScopeTransloco: NOMBRE_SCOPE_ARCHIVO_SECUNDARIO,
  };


  constructor(
    public readonly _sRutaArchivoSecundarioService: SRutaArchivoSecundarioService,
    private readonly _activatedRoute: ActivatedRoute,
    public readonly _router: Router,
    public readonly translocoService: TranslocoService,
  ) {
  }

  ngOnInit(): void {
    this._obtenerParametros();
  }

  private _obtenerParametros(): void {
    this._sRutaArchivoSecundarioService
      .obtenerYGuardarParametrosRuta(this._activatedRoute)
      .subscribe(
        (parametros: SRutaArchivoSecundarioParametros) => {
          const busqueda = this._sRutaArchivoSecundarioService
            .setearParametrosDeBusqueda(parametros, ArchivoSecundarioBusquedaDto);

          if (parametros.tipo && busqueda) {
            busqueda.tipo = parametros.tipo as TipoArchivo;
          }
          if (parametros.nombreIdentificador && busqueda) {
            busqueda.nombreIdentificador = parametros.nombreIdentificador;
          }
          if (parametros.idReferencial && busqueda) {
            busqueda.idReferencial = +parametros.idReferencial;
          }


          this._sRutaArchivoSecundarioService
            .busquedaDto = busqueda;
          this._sRutaArchivoSecundarioService
            .construirMigasPan(
              this,
              [
                // MODULO_MIGAS_PAN,
                ARCHIVOS_MIGAS_PAN,
                ARCHIVO_SECUNDARIO_MIGAS_PAN,
              ],
            );
          this.buscarConFiltros();
        }
      );
  }

  buscarConFiltros(): void {

    if (this._sRutaArchivoSecundarioService.busquedaDto) {
      this._sRutaArchivoSecundarioService.busquedaDto.skip = 0;
      this._sRutaArchivoSecundarioService.first = 0;
    }
    this.buscarSuscrito();


  }

  buscarSuscrito() {
    this._sRutaArchivoSecundarioService._cargandoService.habilitarCargando();
    this._sRutaArchivoSecundarioService
      .buscar()
      .subscribe(
        (data) => {
          this._sRutaArchivoSecundarioService.registrosPagina = data[0].length;
          this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
        },
        (error) => {
          console.error({
            mensaje: 'Error cargando registros',
            data: this._sRutaArchivoSecundarioService.busquedaDto,
            error
          });
          this._sRutaArchivoSecundarioService._notificacionService.anadir({


            titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
            detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),


            severidad: 'error'
          });
          this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
        }
      );
  }

  // Usar para generar campos de formulario dinamicos

  // generarFormulario() {
  //   this.mostrarFormularioDePrueba = false;
  //   const arregloDatos: CampoAutogenerado[] = [];
  //   const grupoFormulario: GrupoFormulario[] = [];
  //   // crear los campos autogenerados "arregloDatos" y
  //   // AL MENOS debe existir un "grupoFormulario" que contenga a todos los campos para una visualización sencilla
  //   const respuestaAutogenerado = this._sRutaArticuloAtributoService
  //       .generarFormularioAutogenerado(
  //           arregloDatos,
  //           MENSAJES_ERROR,
  //           this,
  //           grupoFormulario
  //       )
  //   this.arregloFormulario.arregloValores = respuestaAutogenerado.arregloValores;
  //   this.arregloFormulario.grupo = respuestaAutogenerado.grupo;
  //   this.arregloFormulario.campos = respuestaAutogenerado.campos;
  //   this.mostrarFormularioDePrueba = true;
  // }

  formularioValidoBusqueda(formularioBusquedaValido: TodosCamposValidados): void {
    this.formularioBusqueda = formularioBusquedaValido;
    if (formularioBusquedaValido.valido) {
      this._sRutaArchivoSecundarioService.setearCamposBusquedaValidos(
        formularioBusquedaValido,
        ArchivoSecundarioBusquedaDto
      );
      // Transforma objetos a valores, si no hay objetos en la busqueda
      // como un autocomplete o select por ejemplo, entonces dejar vacío.
      const arregloCamposBusquedaConObjeto: ObjetoBusquedaADto[] = [
        {
          nombreCampoEnBusqueda: 'sisHabilitado',
          nombreCampoEnDto: 'sisHabilitado'
        },
        // {
        //   nombreCampoEnBusqueda: 'campoSelectOAutocomplete',
        //   nombreCampoEnDto: 'nombreCampoSelectOAutocomplete'
        // },
      ];
      this._sRutaArchivoSecundarioService.transformarObjetosBusquedaABusquedaDto(
        arregloCamposBusquedaConObjeto
      );
    }
  }

  cambioCampoBusqueda(eventoCambioCampoBusqueca: EventoCambioFormulario): void {
    const campoBusquedaActualizado = eventoCambioCampoBusqueca.campoFormulario;
    if (campoBusquedaActualizado) {
      this._sRutaArchivoSecundarioService
        .setearCamposBusquedaAUndefined(
          campoBusquedaActualizado,
          ArchivoSecundarioBusquedaDto
        );
    }

  }

  formularioBusquedacambioCampoAutocomplete(evento: EventoCambioAutocomplete) {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  formularioCrearEditarCambioAutocomplete(evento: EventoCambioAutocomplete,
                                          registro?: ArchivoSecundarioInterface): void {
    // this.seleccionarBusquedaAutocompletePorEvento(evento);
  }

  // seleccionarBusquedaAutocompletePorEvento(evento: EventoCambioAutocomplete){
  //   if(evento.campoFormulario){
  // switch (evento.campoFormulario.nombreCampo) {
  //   case 'nombreCampo': // nombres de cada uno de los campos autocomplete
  //     // this.buscarAutocompleteNombreCampo(evento); // ejecutar una búsqueda con los servicios pertinentes
  //     break;
  // }
  // }
  // }

  // buscarAutocompleteNombreCampo(evento: EventoCambioAutocomplete) {
  //   const busqueda: NombreCampoBusquedaDto = {
  //     nombreCampo: evento.query,
  //   };
  //   this._nombrCampoService
  //       .buscar(busqueda)
  //       .toPromise()
  //       .then(res => res as [NombreCampoInterface[], number])
  //       .then(data => {
  //         const arregloDatos = data[0];
  //         // SI ES NECSARIO HACER UN MAP PARA VISUALIZAR OTROS CAMPOS UTILIZAR LA SIGUIENTE LÍNEA
  //         const arregloDatos = data[0].map((a:any)=>{ a.nombreCompeto = a.nombre + ' ' + a.apellido; return a;});
  //         if (evento.campoFormulario.autocomplete) {
  //           if (Array.isArray(arregloDatos)) {
  //             evento.campoFormulario.autocomplete.suggestions = [...arregloDatos];
  //           } else {
  //             evento.campoFormulario.autocomplete.suggestions = [arregloDatos];
  //           }
  //         }
  //         return data;
  //       });
  // }

  formularioCrearEditarValido(estaValido: TodosCamposValidados,
                              registro?: ArchivoSecundarioInterface): void {
    this._sRutaArchivoSecundarioService.setearFormularioEnModal(estaValido);
    if (estaValido.valido) {
      this.formularioCrearEditar = estaValido;
      this.botonAceptarModalDisabled = false;
    } else {
      this.botonAceptarModalDisabled = true;
    }
  }

  formularioCrearEditarCambioCampo(eventoCambioCampo: EventoCambioFormulario,
                                   registro?: ArchivoSecundarioInterface): void {
    if (eventoCambioCampo.campoFormulario) {
      // Esta parte del codigo solo se debe de utilizar
      // si existe cambios de estructura del formulario
      // EJ: si selecciona el valor X de un campo entonces muestro tales campos
      // const formularioValido = {
      //   valido: false,
      //   camposFormulario: [eventoCambioCampo.campoFormulario],
      // };
      // this._sRutaArchivoSecundarioService.setearFormularioEnModal(formularioValido);
      // switch (eventoCambioCampo.campoFormulario.nombreCampo) {
      //   case 'nombreCampo':
      //     if (registro) {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo,
      //           registro
      //       );
      //     } else {
      //       this.actualizarValidez(
      //           eventoCambioCampo.valor,
      //           eventoCambioCampo.campoFormulario.nombreCampo
      //       );
      //     }
      //     break;
      //   default:
      //     break;
      // }

    }
  }

//   actualizarValidez(
//       valorCampoValidez: any,
//       nombrePropiedadValidez: string,
//       registro?: any,
// ) {
//     // Los campos definidos "nombrePropiedad" cuando tengan un valor != undefined van a activar
//     // los campos que están en "nombresCamposRequeridosQueAfecta"
//     const camposAEstablecerValidezArreglo: CamposEstablecerValidez[] = [
//       {
//         nombrePropiedad: 'nombreCampo',
//         nombresCamposRequeridosQueAfecta: ['campoUno', 'campoDos']
//       },
//     // También hay el caso en donde al mostrar unos campos se oculten otros:
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoTres', 'campoCuatro'],
//         nombresCamposRequeridosAOcultar: ['campoCinco', 'campoSeis'],
//         valorPropiedad: 'valorTresCuatro',
//       },
//       {
//         nombrePropiedad: 'nombreOtro',
//         nombresCamposRequeridosQueAfecta: ['campoCinco', 'campoSeis'],
//         nombresCamposRequeridosAOcultar: ['campoTres', 'campoCuatro'],
//         valorPropiedad: 'valorCincoSeis',
//       },
//     ];
//
//     this._sRutaArchivoSecundarioService.establecerValidezDeFormulario<RutaArchivoSecundarioComponent>(
//         this,
//         camposAEstablecerValidezArreglo,
//         valorCampoValidez,
//         nombrePropiedadValidez,
//         undefined,
//         registro,
//     );
//   }


  abrirModal() {
    // Si se tienen campos dependientes se deben de
    // activarlos antes de editarlos
    // if(camposRequeridos.campoDependeUno){
    //   camposRequeridos.nombreCampoDependienteUno = true;
    // }
    // if(camposRequeridos.campoDependeDos){
    //   camposRequeridos.nombreCampoDependienteDos = true;
    // }

    if (this._sRutaArchivoSecundarioService.parametros) {
      if (this._sRutaArchivoSecundarioService.parametros.tipo) {
        const tipo = this._sRutaArchivoSecundarioService.parametros.tipo;
        // console.log('TIPO', tipo);
        const formulario = tipo === TipoArchivo.Imagen ? ARTICULO_ARCHIVO_FORMULARIO : ARTICULO_ARCHIVO_FORMULARIO;
        const rutaComunInternacionalizacion = 'formularios.crearEditar.formularioCrearEditar.';
        this._sRutaArchivoSecundarioService.abrirModalArchivos(
          this,
          formulario,
          rutaComunInternacionalizacion + (TipoArchivo.Imagen ? 'tituloCrearImagen' : 'tituloCrearArchivo'),
          rutaComunInternacionalizacion + (TipoArchivo.Imagen ? 'descripcionCrearImagen' : 'descripcionCrearArchivo'),
          tipo,
          {}
        );
      }
    }


    // En el caso de cammpos AUTOGERENADOS se habilita esta parte del codigo

    // const respuesta = this._sRutaArchivoSecundarioService._archivoSecundarioService.generarFormulario(registro);
    // respuesta.arregloDatos = respuesta
    //     .arregloDatos
    // .map( // EJEMPLO DE IMPLEMENTACION:
    //     (aD) => {
    //       aD.valorInicial = registro.catValor;
    //       aD.required = true;
    //       aD.estaValido = false;
    //       aD.fnValidarInputMask = (campo, componente) => {
    //         if (campo) {
    //           return campo.length > 0
    //         } else {
    //           return true;
    //         }
    //       };
    //       return aD;
    //     }
    // );
    // const respuestaGrupoCampo = this._sRutaArchivoSecundarioService
    //     .generarFormularioAutogenerado(
    //         respuesta.arregloDatos,
    //         MENSAJES_ERROR,
    //         this,
    //         respuesta.grupoFormulario
    //     );
    // this.abrirModalPrevisualizarTablaAutogenerado(respuestaGrupoCampo, false, registro);

  }

  // abrirModalPrevisualizarTablaAutogenerado(
  //     arregloDeDatos: {
  //   campos: (claseComponente: any) => CampoFormulario[],
  //       grupo: () => GrupoFormulario[],
  //       arregloValores: { nombre: string, campos: string[] }[]
  // },
  // mostrarTabla = false,
  //     registro?: ArchivoSecundarioInterface) {
  //   this.botonAceptarModalAutogeneradoDisabled = true;
  //   const dialogRef = this.matDialog.open(
  //       ModalFormularioAutogeneradoComponent, // Modal de autogenerado
  //       {
  //         data: {
  //           arregloFormulario: arregloDeDatos,
  //           componente: this,
  //           mostrarTabla: mostrarTabla,
  //           registro,
  //         }
  //       }
  //   );
  // }

  // cambioCampoAutogenerado(eventoCambioCampo: EventoCambioFormulario) {
  //
  // }

  // formularioValidoAutogenerado(estaValido: TodosCamposValidados, registro?: ArchivoSecundarioInterface) {
  //   if (estaValido.valido) {
  //     this.camposAutogeneradosAGuardar = estaValido
  //         .camposFormulario
  //         .filter((cF) => cF.estaValido && cF.valorActual)
  //     this.botonAceptarModalAutogeneradoDisabled = false;
  //   } else {
  //     this.botonAceptarModalAutogeneradoDisabled = true;
  //   }
  // }

  // aceptoFormularioAutogenerado(registro?: ArticuloCatalogoInterface) {
  //   if (registro) {
  //     this.editarRegistro(registro);
  //   } else {
  //     this.guardarNuevosCampos()
  //   }
  // }

  // Implementacion de guardado de campos auto generados

  // guardarNuevosCampos() {
  //
  // }

  // Implementacion de ediciion de campo autogenerado

  // editarRegistro(registro: ArticuloCatalogoInterface) {
  //
  // }

  // cambioStepperEnAutogenerado(indice: number) {
  //
  // }

  crearEditar(
    registro?: ArchivoSecundarioInterface,
  ): void {
    if (registro) {
      this.editar(registro);
    } else {
      if (this._sRutaArchivoSecundarioService.parametros) {
        if (this._sRutaArchivoSecundarioService.parametros.tipo && this._sRutaArchivoSecundarioService.parametros.idReferencial && this._sRutaArchivoSecundarioService.parametros.nombreIdentificador) {
          this.crear(
            this._sRutaArchivoSecundarioService.parametros.tipo,
            this._sRutaArchivoSecundarioService.parametros.nombreIdentificador,
            this._sRutaArchivoSecundarioService.parametros.idReferencial
          );
        }
      }
    }
  }

  formularioArchivoCambio(eventoCambioCampo: EventoCambioFormulario, registro: EntrenadorInterface, tipo: string) {
    // campo cambio
  }

  formularioArchivoValido(estaValido: TodosCamposValidados, registro: EntrenadorInterface, tipo: string) {
    this.guardarFormularioValido(estaValido, registro, tipo);
  }

  guardarFormularioValido(estaValido: TodosCamposValidados, registro: EntrenadorInterface, tipo: string) {
    if (estaValido.valido) {
      this._sRutaArchivoSecundarioService.formularioArchivo = estaValido;
      this.botonAceptarModalDisabled = false;
    } else {
      this.botonAceptarModalDisabled = true;
    }
  }

  crearArchivo(tipo: 'imagen' | 'archivo' | string, registro: EntrenadorInterface) {
    if (this._sRutaArchivoSecundarioService.parametros) {
      if (this._sRutaArchivoSecundarioService.parametros.tipo && this._sRutaArchivoSecundarioService.parametros.idReferencial && this._sRutaArchivoSecundarioService.parametros.nombreIdentificador) {
        this.crear(
          this._sRutaArchivoSecundarioService.parametros.tipo,
          this._sRutaArchivoSecundarioService.parametros.nombreIdentificador,
          this._sRutaArchivoSecundarioService.parametros.idReferencial
        );
      }
    }
  }

  crear(tipo: TipoArchivo, nombreIdentificador: string, id: string): void {
    // console.log(this.formularioCrearEditar);
    // console.log(this._sRutaArchivoSecundarioService._archivoPrincipalService);
    if (this._sRutaArchivoSecundarioService.formularioArchivo && this._sRutaArchivoSecundarioService._archivoPrincipalService) {
      const camposCrear = this._sRutaArchivoSecundarioService.obtenerCampos<CrearArchivoSecundario>(
        CrearArchivoSecundario,
        this._sRutaArchivoSecundarioService.formularioArchivo
      );
      const archivo = {
        archivo: camposCrear.instancia.archivo?.valorActual,
        nombre: camposCrear.instancia.nombre?.valorActual,
        descripcion: camposCrear.instancia.descripcion?.valorActual,
        nombreIdentificador,
        tipo,
        id: +id,
      };

      this._sRutaArchivoSecundarioService._cargandoService.habilitarCargando();
      const obs$ = this._sRutaArchivoSecundarioService
        ._archivoPrincipalService
        .subirArchivoSecundario(
          archivo
        )
        .pipe(
          this._sRutaArchivoSecundarioService.buscarDeNuevo(
            'id',
            undefined,
            {
              nombreIdentificador: this._sRutaArchivoSecundarioService.parametros?.nombreIdentificador
            }
          )
        ) as any;
      obs$
        .subscribe(
          (nuevoRegistro: ArchivoSecundarioInterface) => {
            // console.log(nuevoRegistro);
            nuevoRegistro.habilitado = true;
            this._sRutaArchivoSecundarioService.arregloDatos.unshift(nuevoRegistro);
            this._sRutaArchivoSecundarioService.arregloDatosFiltrado.unshift(nuevoRegistro);
            this._sRutaArchivoSecundarioService.matDialog.closeAll();
            this._sRutaArchivoSecundarioService._notificacionService.anadir({


              titulo: this.translocoService.translate('generales.toasters.toastExitoCrear.title'),
              detalle: this.translocoService.translate('generales.toasters.toastExitoCrear.body', {nombre: nuevoRegistro.nombre}),


              severidad: 'success'
            });
            this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
          },
          (error: any) => {
            console.error({
              mensaje: 'Error cargando registros',
              data: this._sRutaArchivoSecundarioService.busquedaDto,
              error
            });
            this._sRutaArchivoSecundarioService._notificacionService.anadir({

              titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
              detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),


              severidad: 'error'
            });
            this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
          }
        );
    }
  }

  editar(
    registro: ArchivoSecundarioInterface,
  ): void {
    if (this.formularioCrearEditar) {
      const camposCrear = this._sRutaArchivoSecundarioService.obtenerCampos<ActualizarArchivoSecundario>(
        ActualizarArchivoSecundario,
        this.formularioCrearEditar,
        true
      );
      const arregloPropiedades: SetearObjeto[] = [];
      camposCrear.objetoCrear = this._sRutaArchivoSecundarioService.setearValoresDeObjetos(
        camposCrear.objetoCrear,
        arregloPropiedades
      );
      if (registro.id) {
        this._sRutaArchivoSecundarioService._cargandoService.habilitarCargando();
        const actualizar$ = this._sRutaArchivoSecundarioService
          ._archivoSecundarioService

          .actualizar(
            camposCrear.objetoCrear,
            registro.id
          )
          .pipe(
            this._sRutaArchivoSecundarioService.buscarDeNuevo('id', registro.id)
          ) as any as Observable<ArchivoSecundarioInterface>;


        actualizar$
          .subscribe(
            (registroEditado) => {
              registroEditado.habilitado = registro.habilitado;
              const indice = this._sRutaArchivoSecundarioService.arregloDatos
                .findIndex(
                  (a: ArchivoSecundarioInterface) => a.id === registro.id
                );
              this._sRutaArchivoSecundarioService.arregloDatos[indice] = registroEditado;
              const indiceArregloFiltrado = this._sRutaArchivoSecundarioService.arregloDatosFiltrado
                .findIndex(
                  (a: ArchivoSecundarioInterface) => a.id === registro.id
                );
              this._sRutaArchivoSecundarioService.arregloDatosFiltrado[indiceArregloFiltrado] = registroEditado;
              this._sRutaArchivoSecundarioService.matDialog.closeAll();
              this._sRutaArchivoSecundarioService._notificacionService.anadir({


                titulo: this.translocoService.translate('generales.toasters.toastExitoEditar.title'),
                detalle: this.translocoService.translate('generales.toasters.toastExitoEditar.body', {nombre: registroEditado.nombre}),


                severidad: 'success'
              });
              this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
            },
            (error) => {
              console.error({
                mensaje: 'Error cargando registros',
                data: this._sRutaArchivoSecundarioService.busquedaDto,
                error
              });
              this._sRutaArchivoSecundarioService._notificacionService.anadir({


                titulo: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.title'),
                detalle: this.translocoService.translate('generales.toasters.toastErrorConexionServidorVacio.body'),


                severidad: 'error'
              });
              this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
            }
          );
      }
    }
  }

  // irRutaArchivos(tipo: 'imagen' | 'archivo', registro: ArchivoSecundarioInterface) {
  //   const ruta = [LLENAR_RUTA, registro.id, tipo, LLENAR_NOMBRE_IDENTIFICADOR, `Gestionar ${tipo} de ..... ${registro.nombreCampo}`];
  //   this._router.navigate(ruta);
  // }

  // mostrarFormularioCrearArchivo(tipo: 'imagen' | 'archivo', registro: ArchivoSecundarioInterface) {
  //   const formulario = tipo === 'imagen' ? ALGUN_FORMULARIO : OTRO_FORMULARIO;
  //   this._sRutaArchivoSecundarioService.abrirModalArchivos(
  //       this,
  //       formulario,
  //       'Editar archivo de artículo',
  //       'Seleccione un archivo',
  //       tipo,
  //       registro
  //   );
  //
  // }

  // formularioArchivoCambio(eventoCambioCampo: EventoCambioFormulario, tipo:string) {
  //   // campo cambio
  // }
  //

  // crearArchivo(tipo: 'imagen' | 'archivo', registro: ArchivoSecundarioInterface) {
  //     this._sRutaArchivoSecundarioService
  //         .crearArchivo(tipo, registro)
  //         .subscribe(
  //             (nuevoRegistro) => {
  //               this._sRutaArchivoSecundarioService.archivoPrincipalArchivoActual = nuevoRegistro;
  //               if (tipo === 'imagen') {
  //                 registro.sis_imagen = nuevoRegistro;
  //               }
  //               if (tipo === 'archivo') {
  //                 registro.sis_archivo = nuevoRegistro;
  //               }
  //               this._sRutaArchivoSecundarioService.matDialog.closeAll();
  //               this._sRutaArchivoSecundarioService._notificacionService.anadir({
  //                 titulo: 'Éxito',
  //                 detalle: 'Actualizo el archivo',
  //                 severidad: 'success'
  //               });
  //               this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
  //             },
  //             (error) => {
  //               console.error({
  //                 mensaje: 'Error cargando registros',
  //                 data: this._sRutaArchivoSecundarioService.busquedaDto,
  //                 error
  //               });
  //               this._sRutaArchivoSecundarioService._notificacionService.anadir({
  //                 titulo: 'Error',
  //                 detalle: 'Error del Sistema',
  //                 severidad: 'error'
  //               });
  //               this._sRutaArchivoSecundarioService._cargandoService.deshabilitarCargando();
  //             }
  //         );
  // }


  atras(){
    window.history.back();
  }
}
