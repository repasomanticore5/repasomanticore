import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';
import {NOMBRE_MODULO} from '../../../constantes/nombre-modulo';

export const NOMBRE_SCOPE_ARCHIVO_SECUNDARIO = NOMBRE_MODULO + 'ArchivoSecundario';

export const SCOPE_ARCHIVO_SECUNDARIO = (http: HttpClient) => {
  const loaderArchivoSecundario = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/archivos/archivo-secundario/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_ARCHIVO_SECUNDARIO, loader: loaderArchivoSecundario};
};

export const LOADER_ARCHIVO_SECUNDARIO = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_ARCHIVO_SECUNDARIO,
  multi: true
};
