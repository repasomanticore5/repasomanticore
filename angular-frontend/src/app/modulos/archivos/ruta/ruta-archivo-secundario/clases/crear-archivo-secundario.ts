import {CampoFormulario} from '@manticore-labs/ng-2021';
export class CrearArchivoSecundario {
  // Definir todos los campos dentro del FORMULARIO de creación
  // se debe igualar a undefined, OMITIR RELACIONES
  // nombreCampo?: CampoFormulario = undefined;

  archivo?: CampoFormulario = undefined;
  nombre?: CampoFormulario = undefined;
  descripcion?: CampoFormulario = undefined;
}
