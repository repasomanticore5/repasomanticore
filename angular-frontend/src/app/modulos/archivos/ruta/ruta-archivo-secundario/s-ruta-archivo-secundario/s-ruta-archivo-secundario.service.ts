import {Injectable} from '@angular/core';
import {SRutaArchivoSecundarioModule} from './s-ruta-archivo-secundario.module';
import {SRutaArchivoSecundarioParametros} from '../interfaces/s-ruta-archivo-secundario.parametros';
import {ArchivoSecundarioBusquedaDto} from '../../../dto/archivo-secundario-busqueda.dto';
import {ArchivoSecundarioInterface} from '../../../interfaces/archivo-secundario.interface';
import {ArchivoSecundarioService} from '../../../servicios/archivo-secundario/archivo-secundario.service';
import {MatDialog} from '@angular/material/dialog';
import {ARCHIVO_SECUNDARIO_FORMULARIO} from '../../../formularios/archivo-secundario/archivo-secundario-formulario';
import {GRUPOS_FORMULARIO_BUSQUEDA} from './grupos-formulario-busqueda';
import {GRUPOS_FORMULARIO_CREAR_EDITAR} from './grupos-formulario-crear-editar';
import {MenuGeneralService} from '../../../../../servicios/menu-general/menu-general.service';
import {TAKE} from '../../../../../constantes/numero-registros-en-tabla/take';
import {CargandoService} from '../../../../../servicios/cargando/cargando.service';
import {ModalCrearEditarComunComponent} from '../../../../../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';
import {ActivoInactivo} from '../../../../../enums/activo-inactivo';
import {RutaComun} from '@manticore-labs/ng-2021';
import {NotificacionService} from '../../../../../servicios/notificacion/notificacion.service';
import {REGISTROS_POR_PAGINA} from '../../../../../constantes/numero-registros-en-tabla/registros-por-pagina';
import {ArchivoPrincipalService} from '../../../../../servicios/archivos/archivo-principal.service';
import {DomSanitizer} from '@angular/platform-browser';
import {ARTICULO_ARCHIVO_FORMULARIO} from '../../../../../formularios/formulario-archivos/archivo-formulario';


@Injectable({
  providedIn: SRutaArchivoSecundarioModule
})
export class SRutaArchivoSecundarioService extends RutaComun<SRutaArchivoSecundarioParametros,
  ArchivoSecundarioBusquedaDto,
  ArchivoSecundarioService,
  ArchivoSecundarioInterface> {
  constructor(
    public readonly _archivoSecundarioService: ArchivoSecundarioService,
    public readonly _cargandoService: CargandoService,
    public readonly _notificacionService: NotificacionService,
    public matDialog: MatDialog,
    public readonly _menuGeneralService: MenuGeneralService,
    public readonly _archivoPrincipalService: ArchivoPrincipalService,
    public readonly _domSanitizer: DomSanitizer,
  ) {
    super(
      _archivoSecundarioService,
      _cargandoService,
      _notificacionService,
      matDialog,
      ARTICULO_ARCHIVO_FORMULARIO,
      'formularios.crearEditar.formularioCrearEditar.tituloCrear',
      'formularios.crearEditar.formularioCrearEditar.descripcionCrear',
      'formularios.crearEditar.formularioCrearEditar.tituloActualizar',
      'formularios.crearEditar.formularioCrearEditar.descripcionActualizar',
      GRUPOS_FORMULARIO_BUSQUEDA,
      GRUPOS_FORMULARIO_CREAR_EDITAR,
      () => {
        return [
          // {
          //   nombreCampo: 'nombreCampo', // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Nombre Campò'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return  valorCampo ; // Valor a mostrarse del campo, se pueden hacer transformaciones
          //   },
          // },
          // {
          //   nombreCampo: 'campoOpciones',  // nombre del campo a mostrarse en la tabla
          //   nombreAMostrar: (valorCampo: any, nombreCampo: string) => {
          //     return 'Opciones'; // Nombre a mostrarse del campo
          //   },
          //   valorAMostrar: (valorCampo: ActivoInactivo, nombreCampo: string) => {
          //     switch (valorCampo) { // se puede dar diferentes nombres dependiendo del valor del campo
          //       case 'opcionUno':
          //         return 'Uno';
          //       case 'opcionDos':
          //         return 'Dos';
          //     }
          //   },
          // },
        ];
      },
      ModalCrearEditarComunComponent,
      TAKE,
      REGISTROS_POR_PAGINA,
      ActivoInactivo as { Activo: any, Inactivo: any },
      _menuGeneralService,
      _archivoPrincipalService
    );
    // Si se quiere que el stepper sea horizontal
    // this.vertical = false;

    // Path de propiedades para poder agrupar en la tabla

    // Ej1: {nombre:'Adrian', apellido:'Eguez', clase:'Clase uno'}
    // this.nombreAtributoAAgrupar = ['clase'];
    // En este caso solo se accede a -> registro.clase

    // Ej2: {nombre:'Adrian', apellido:'Eguez', campoRelacion:{campoNombre:'Clase'}}
    // this.nombreAtributoAAgrupar = ['campoRelacion', 'campoNombre'];
    // En este caso solo se accede a -> registro.campoRelacion.campoNombre
  }
}
