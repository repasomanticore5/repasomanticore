import {CommonModule} from '@angular/common';
import {TableModule} from 'primeng/table';
import {InputSwitchModule} from 'primeng/inputswitch';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SplitButtonModule} from 'primeng/splitbutton';
import {ButtonModule} from 'primeng/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MenuModule} from 'primeng/menu';
import {NotificacionModule} from '../../../servicios/notificacion/notificacion.module';
import {RippleModule} from 'primeng/ripple';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ManLabNgBootstrapModule, MlCampoFormularioModule} from '@manticore-labs/ng-2021';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {TranslocoModule} from '@ngneat/transloco';
import {TituloPantallaModule} from '../../../componentes/titulo-pantalla/titulo-pantalla.module';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {ArchivosRoutingModule} from '../archivos-routing.module';
import {SRutaArchivoSecundarioModule} from '../ruta/ruta-archivo-secundario/s-ruta-archivo-secundario/s-ruta-archivo-secundario.module';
import {ArchivoSecundarioModule} from '../servicios/archivo-secundario/archivo-secundario.module';


export const IMPORTS_MODULO_ARCHIVOS = [
  CommonModule,
  TableModule,
  BreadcrumbModule,
  InputSwitchModule,
  FormsModule,
  ReactiveFormsModule,
  SplitButtonModule,
  ButtonModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MenuModule,
  NotificacionModule,
  MlCampoFormularioModule,
  RippleModule,
  AutoCompleteModule,
  TranslocoModule,
  TituloPantallaModule,
  MatMenuModule,
  MatButtonModule,
  ManLabNgBootstrapModule,
  ArchivosRoutingModule,
  SRutaArchivoSecundarioModule,
  ArchivoSecundarioModule,
];
