import {MigaPanRuta} from '@manticore-labs/ng-2021';
import {NOMBRE_SCOPE_ARCHIVOS} from '../transloco/loader-archivos';

export const ARCHIVOS_MIGAS_PAN
    : (componente: any, items?: MigaPanRuta) => MigaPanRuta = (componente: any, items?: MigaPanRuta) => {

    return {
        id: 'archivos',
        label: 'migaPan',
        routerLink: ['/archivos'],
        scopeTransloco: NOMBRE_SCOPE_ARCHIVOS
    };
};
