import {Injectable} from '@angular/core';
import {ArchivoSecundarioModule} from './archivo-secundario.module';
import {ArchivoSecundarioInterface} from '../../interfaces/archivo-secundario.interface';

import {ArchivoSecundarioBusquedaDto} from '../../dto/archivo-secundario-busqueda.dto';
import {HttpClient} from '@angular/common/http';
import {NotificacionService} from '../../../../servicios/notificacion/notificacion.service';
import {ConfirmationService} from 'primeng/api';
import {ServicioComunMlabs} from '@manticore-labs/ng-2021';
import {environment} from '../../../../../environments/environment';
import {MENSAJES_CONFIRMACION} from '../../../../constantes/mensajes-confirmacion';
import {TranslocoService} from '@ngneat/transloco';


@Injectable({
  providedIn: ArchivoSecundarioModule
})
export class ArchivoSecundarioService

  extends ServicioComunMlabs<ArchivoSecundarioInterface, ArchivoSecundarioBusquedaDto> {
  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
    private readonly _confirmationService: ConfirmationService,
    public readonly translocoService: TranslocoService
  ) {
    super(
      'archivo-secundario',
      _httpClient,
      ArchivoSecundarioBusquedaDto,
      'sisHabilitado',
      'id',
      environment.url,
      _notificacionService,
      _confirmationService,
    );
    this.mensajesConfirmacion = MENSAJES_CONFIRMACION(this);
  }


}
