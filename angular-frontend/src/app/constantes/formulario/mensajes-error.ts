import {FormularioErrorMensaje} from '@manticore-labs/ng-2021';
import {TranslocoService} from '@ngneat/transloco';

export const MENSAJES_ERROR: (clase: any) => FormularioErrorMensaje[] =
  (claseComponente: any) => {
    return [
      {
        tipo: 'required',
        mensaje: (parametros) => {
          return 'formularios.comunes.requerido';
        }
      },
      {
        tipo: 'minlength',
        mensaje: (parametros) => {
          return 'formularios.comunes.minLength';
        }
      },
      {
        tipo: 'maxlength',
        mensaje: (parametros) => {
          return 'formularios.comunes.maxLength';
        }
      },
      {
        tipo: 'min',
        mensaje: (parametros) => {
          return 'formularios.comunes.min';
        }
      },
      {
        tipo: 'max',
        mensaje: (parametros) => {
          return 'formularios.comunes.max';
        }
      },
      {
        tipo: 'email',
        mensaje: (parametros) => {
          return 'formularios.comunes.email';
        }
      },
      {
        tipo: 'pattern',
        mensaje: (parametros) => {
          return 'formularios.comunes.pattern';
        }
      },
      {
        tipo: 'archivo',
        mensaje: (parametros) => {
          return 'formularios.comunes.tamanioMaximoEnBytes';
        }
      }
    ];
  };
