export const MENSAJES_CONFIRMACION = (claseServicio: any) => {
  if (claseServicio.translocoService) {
    return {
      crear: (clase: any) => {
        const texto = claseServicio.translocoService.translate('generales.modalConfirmacion.mensajeCrear');
        return texto;
      },
      actualizar: (clase: any) => {
        const texto = claseServicio.translocoService.translate('generales.modalConfirmacion.mensajeActualizar');
        return texto;
      },
      mensajeCancelacion: (clase: any) => {
        return {
          titulo: claseServicio.translocoService.translate('generales.modalConfirmacion.mensajeCancelarTitulo'),
          detalle: claseServicio.translocoService.translate('generales.modalConfirmacion.mensajeCancelarDescripcion')
        };
      },
      habilitar: (clase: any) => {
        const texto = claseServicio.translocoService.translate('generales.modalConfirmacion.mensajeHabilitar');
        return texto;
      },
      aceptar: (clase: any) => {
        const texto = claseServicio.translocoService.translate('generales.modalConfirmacion.aceptar');
        return texto;
      },
      cancelar: (clase: any) => {
        const texto = claseServicio.translocoService.translate('generales.modalConfirmacion.cancelar');
        return texto;
      },
    };
  } else {
    console.error({
      mensaje: 'No tiene servicio de transloco en el servicio de esta entidad',
      data: claseServicio
    });
    throw new Error('Error, no tiene servicio de transloco');
  }
};
