import {HttpClient} from '@angular/common/http';
import {TRANSLOCO_SCOPE} from '@ngneat/transloco';

export const NOMBRE_SCOPE_PRINCIPAL = 'moduloPrincipal';

export const SCOPE_PRINCIPAL = (http: HttpClient) => {
  const loaderNest = ['es', 'en'].reduce((acc: any, lang) => {
    acc[lang] = () => http.get(`/assets/i18n/modulo-principal/${lang}.json`).toPromise();
    return acc;
  }, {});

  return {scope: NOMBRE_SCOPE_PRINCIPAL, loader: loaderNest};
};

export const LOADER_PRINCIPAL = {
  provide: TRANSLOCO_SCOPE,
  deps: [HttpClient],
  useFactory: SCOPE_PRINCIPAL,
  multi: true
};
