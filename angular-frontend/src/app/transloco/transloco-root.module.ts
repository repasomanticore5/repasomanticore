import {HttpClient} from '@angular/common/http';
import {
  TRANSLOCO_LOADER,
  Translation,
  TranslocoLoader,
  TRANSLOCO_CONFIG,
  translocoConfig,
  TranslocoModule
} from '@ngneat/transloco';
import {Injectable, NgModule} from '@angular/core';
import {environment} from '../../environments/environment';
import {LOADERS} from './loaders';

@Injectable({providedIn: 'root'})
export class TranslocoHttpLoader implements TranslocoLoader {
  constructor(private http: HttpClient) {
  }

  getTranslation(lang: string) {
    return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
  }
}

@NgModule({
  exports: [TranslocoModule],
  providers: [
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        availableLangs: [
          {
            id: 'en',
            label: 'English',
          },
          {
            id: 'es',
            label: 'Español',
          },
        ],
        defaultLang: 'es',
        reRenderOnLangChange: true,
        fallbackLang: ['es', 'en'],
        failedRetries: 3,
        missingHandler: {
          allowEmpty: true, // permitir vacios
          useFallbackTranslation: true, // usar los fallback langs si hay vacios
          logMissingKey: true, // Consologear si no hay una traduccion
        },
        prodMode: environment.production,

      })
    },
    {provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader},
    ...LOADERS
  ]
})
export class TranslocoRootModule {
}
