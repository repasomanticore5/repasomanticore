import {LOADER_ENTRENADOR} from '../modulos/ejemplo-firebase/ruta/ruta-entrenador/transloco/loader-entrenador';
import {LOADER_POKEMON} from '../modulos/ejemplo-firebase/ruta/ruta-pokemon/transloco/loader-pokemon';
import {LOADER_ENTRENADOR_NEST} from '../modulos/ejemplo-nest/ruta/ruta-entrenador/transloco/loader-entrenador';
import {LOADER_POKEMON_NEST} from '../modulos/ejemplo-nest/ruta/ruta-pokemon/transloco/loader-pokemon';
import {LOADER_NEST} from '../modulos/ejemplo-nest/transloco/loader-nest';
import {LOADER_FIREBASE} from '../modulos/ejemplo-firebase/transloco/loader-firebase';
import {LOADER_ARCHIVOS} from '../modulos/archivos/transloco/loader-archivos';
import {LOADER_ARCHIVO_SECUNDARIO} from '../modulos/archivos/ruta/ruta-archivo-secundario/transloco/loader-archivo-secundario';
import {LOADER_PRINCIPAL} from './loader-principal';
import { LOADER_TRABAJADOR } from '../modulos/ejemplo-nest/ruta/ruta-trabajador/transloco/loader-trabajador';


export const LOADERS = [
  LOADER_ENTRENADOR,
  LOADER_POKEMON,
  LOADER_ENTRENADOR_NEST,
  LOADER_POKEMON_NEST,
  LOADER_NEST,
  LOADER_FIREBASE,
  LOADER_ARCHIVOS,
  LOADER_ARCHIVO_SECUNDARIO,
  LOADER_PRINCIPAL,
  LOADER_TRABAJADOR

];
