import {MigaPanRuta} from '@manticore-labs/ng-2021/lib/clases-abstractas/interfaces/miga-pan-ruta';
import {MigaDePanInterface} from '@manticore-labs/ng-2021/rutas/interfaces/miga-de-pan-interface';

export default function construirMigasPanRutaCustom(
  contexto: any,
  arreglo: ((componente: any, items?: MigaPanRuta) => MigaPanRuta)[]): MigaDePanInterface[] {
  const migasPanGeneradas = arreglo
    .map((migaPanIndividual) => {
      const migaPan = migaPanIndividual(contexto);
      if (migaPan.routerLink && migaPan.label) {
        return {
          nombre: migaPan.label,
          ruta: migaPan.routerLink,
          scopeTransloco: migaPan.scopeTransloco,
        };
      } else {
        return {
          nombre: '',
          ruta: [],
          scopeTransloco: migaPan.scopeTransloco,
        };
      }
    });
  if (contexto.migasPan) {
    contexto.migasPan = migasPanGeneradas;
  }
  return migasPanGeneradas;
}
