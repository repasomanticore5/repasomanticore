import {Component, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-item-menu',
  templateUrl: './item-menu.component.html',
  styleUrls: ['./item-menu.component.scss']
})
export class ItemMenuComponent implements OnInit {

  @Input()
  texto = '';

  @Input()
  descripcion = '';

  @Input()
  imagen = 'assets/imagenes/tower.png';

  @Input()
    // @ts-ignore
  url: string[];

  @Input()
  extras: NavigationExtras | undefined;

  constructor(private readonly _router: Router) {
  }

  ngOnInit() {
  }

  irARuta() {
    if (this.url) {
      this._router.navigate(this.url, this.extras);
    }
  }

}
