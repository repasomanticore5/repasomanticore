import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ItemMenuComponent} from './item-menu/item-menu.component';
import {CardModule} from 'primeng/card';

@NgModule({
  declarations: [ItemMenuComponent],
  imports: [
    CommonModule,
    CardModule
  ],
  exports: [ItemMenuComponent]
})
export class ItemMenuModule {
}
