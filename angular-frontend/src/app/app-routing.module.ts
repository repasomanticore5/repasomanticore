import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EJEMPLO_NEST_MIGAS_PAN} from './modulos/ejemplo-nest/miga-pan-general/ejemplo-nest-migas-pan';
import {RutaLoginComponent} from './ruta/ruta-login/ruta-login.component';
import {RutaNoEncontradoComponent} from './ruta/ruta-no-encontrado/ruta-no-encontrado.component';
import {RutaInicioComponent} from './ruta/ruta-inicio/ruta-inicio.component';
import {EJEMPLO_FIREBASE_MIGAS_PAN} from './modulos/ejemplo-firebase/miga-pan-general/ejemplo-firebase-migas-pan';
import {INICIO_MIGAS_PAN} from './miga-pan-general/inicio-migas-pan';
import {ARCHIVOS_MIGAS_PAN} from './modulos/archivos/miga-pan-general/archivos-migas-pan';
import {RutaNoTienePermisosComponent} from './ruta/ruta-no-tiene-permisos/ruta-no-tiene-permisos.component';
// import {SUBMODULO_ROLES_MIGAS_PAN} from './submodulos/submodulo-roles/miga-pan-general/submodulo-roles-migas-pan';

const routes: Routes = [
  {
    path: EJEMPLO_NEST_MIGAS_PAN(this).id,
    loadChildren: () => import('./modulos/ejemplo-nest/ejemplo-nest.module')
      .then(m => m.EjemploNestModule)
  },
  {
    path: EJEMPLO_FIREBASE_MIGAS_PAN(this).id,
    loadChildren: () => import('./modulos/ejemplo-firebase/ejemplo-firebase.module')
      .then(m => m.EjemploFirebaseModule)
  },
  {
    path: ARCHIVOS_MIGAS_PAN(this).id,
    loadChildren: () => import('./modulos/archivos/archivos-ruta.module')
      .then(m => m.ArchivosRutaModule)
  },
  // {
  //   path: SUBMODULO_ROLES_MIGAS_PAN(this).id,
  //   loadChildren: () => import('./submodulos/submodulo-roles/submodulo-roles.module')
  //     .then(m => m.SubmoduloRolesModule)
  // },
  {
    path: INICIO_MIGAS_PAN(this).id,
    component: RutaLoginComponent,
  },
  {
    path: 'inicio',
    component: RutaInicioComponent,
  },
  {
    path: 'no-encontrado',
    component: RutaNoEncontradoComponent,
  },
  {
    path: 'no-tiene-permisos',
    component: RutaNoTienePermisosComponent,
  },
  {
    path: '',
    redirectTo: INICIO_MIGAS_PAN(this).id,
    pathMatch: 'full'
  },
  {
    path: '**',
    component: RutaNoEncontradoComponent,
    pathMatch: 'full',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
