import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {DECLARATIONS_COMPONENTE_PRINCIPAL} from './imports/declarations-componte-principal';
import {IMPORTS_COMPONENTE_PRINCIPAL} from './imports/imports-componente-principal';
import {PROVIDERS_COMPONENTE_PRINCIPAL} from './imports/providers-componente-principal';
import {ENTRY_COMPONENTS} from './imports/entry-component-principal';
import { RutaNoTienePermisosComponent } from './ruta/ruta-no-tiene-permisos/ruta-no-tiene-permisos.component';

@NgModule({
  declarations: [
    ...DECLARATIONS_COMPONENTE_PRINCIPAL,
    RutaNoTienePermisosComponent,
  ],
  imports: [
    ...IMPORTS_COMPONENTE_PRINCIPAL,
  ],
  providers: [
    ...PROVIDERS_COMPONENTE_PRINCIPAL
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
