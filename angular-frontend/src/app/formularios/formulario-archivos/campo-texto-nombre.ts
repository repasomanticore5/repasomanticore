import {CampoFormulario} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';


export const CAMPO_TEXTO_NOMBRE: (
  claseComponente: any) => CampoFormulario = (claseComponente: any) => {


  // SOLO USO SI ES FORMULARIO && Es campo del que dependen
  // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.nombre;
  // if (camposDependienteNoExisten) {
  //   valorCampo = undefined;
  // }


  // SOLO USO SI ES FORMULARIO && Es campo dependiente
  // const validators = [];
  // if (claseComponente.data.componente.camposRequeridos.nombre) {
  //   validators.push(Validators.required);
  // }


  return {
    tipoCampoHtml: 'text',

    valorInicial: '',

    valorActual: '',

    hidden: false,
    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // hidden: !claseComponente.data.componente.camposRequeridos.nombre,

    tamanioColumna: 12,
    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // tamanioColumna: claseComponente.data.componente.camposRequeridos.nombre ? 6 : 12,


    validators: [
      // Validators.required,
      // Validators.minLength(2),
      // Validators.maxLength(10),
      // Validators.min(0),
      // Validators.max(100),
      // Validators.email,
      // Validators.pattern()
    ],
    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // validators,


    estaValido: true,

    disabled: false,
    asyncValidators: null,
    nombreCampo: 'nombre',


    nombreMostrar: 'formularios.crearEditar.campoArchivoNombre.nombreMostrar',


    textoAyuda: 'formularios.crearEditar.campoArchivoNombre.textoAyuda',


    placeholderEjemplo: 'formularios.crearEditar.campoArchivoNombre.placeholderEjemplo',


    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {

      nombreCampo: 'formularios.crearEditar.campoArchivoNombre.nombreMostrar',

      // minlength: 2,
      // maxlength:10,
      // min:100,
      // max:0,
      // mensajePattern: 'Solo letras y espacios',
    },
    formulario: {},
    componente: claseComponente,
  };
};
