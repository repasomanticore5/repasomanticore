import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {Validators} from '@angular/forms';
import {TipoArchivo} from '../../enums/tipo-archivo';


export const NOMBRE_RUTA_CAMPO_FILE_ARCHIVO: (
  claseComponente: ModalComponente) => CampoFormulario =
  (claseComponente: ModalComponente) => {


    // SOLO USO SI ES FORMULARIO && Es campo del que dependen
    // const camposDependienteNoExisten = !claseComponente.data.componente.camposRequeridos.archivo;
    // if (camposDependienteNoExisten) {
    //   valorCampo = undefined;
    // }


    // SOLO USO SI ES FORMULARIO && Es campo dependiente
    // const validators = [];
    // if (claseComponente.data.componente.camposRequeridos.archivo) {
    //   validators.push(Validators.required);
    // }


    const data: any = claseComponente.data;
    const accept = data.tipoArchivo === TipoArchivo.Imagen ? '.png,.jpg,.jpeg,.bmp,' : '';

    return {
      tipoCampoHtml: 'file',

      valorInicial: '',

      valorActual: '',

      hidden: false,
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // hidden: !claseComponente.data.componente.camposRequeridos.archivo,

      tamanioColumna: 12,
      // SOLO USO SI ES FORMULARIO && Es campo del que dependen
      // tamanioColumna: claseComponente.data.componente.camposRequeridos.archivo ? 6 : 12,


      validators: [
        Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(10),
        // Validators.min(0),
        // Validators.max(100),
        // Validators.email,
        // Validators.pattern()
      ],
      // SOLO USO SI ES FORMULARIO && Es campo dependiente
      // validators,


      estaValido: false, // Si es un campo opcional debe ser siempre 'true'

      disabled: false,
      asyncValidators: null,
      nombreCampo: 'archivo',


      nombreMostrar: 'formularios.crearEditar.campoArchivo.nombreMostrar',


      textoAyuda: 'formularios.crearEditar.campoArchivo.textoAyuda',


      placeholderEjemplo: 'formularios.crearEditar.campoArchivo.placeholderEjemplo',


      mensajes: MENSAJES_ERROR(claseComponente),
      parametros: {

        nombreCampo: 'formularios.crearEditar.campoArchivo.nombreMostrar',

        // minlength: 2,
        // maxlength:10,
        // min:100,
        // max:0,
        // mensajePattern: 'Solo letras y espacios',
        tamanioMaximoEnBytes: 3
      },
      file: {
        tamanioMaximoEnBytes: 3 * 1024 * 1000,
        accept,
        textoBoton: 'formularios.crearEditar.campoArchivo.textoBoton'
      },
      formulario: {},
      componente: claseComponente,
    } as CampoFormulario;
  };
