import {CampoFormulario, ModalComponente} from '@manticore-labs/ng-2021';
import {NOMBRE_RUTA_CAMPO_FILE_ARCHIVO} from './nombre-ruta-campo-file-archivo';
import {CAMPO_TEXTO_NOMBRE} from './campo-texto-nombre';
import {CAMPO_TEXTO_DESCRIPCION} from './campo-texto-descripcion';


export const ARTICULO_ARCHIVO_FORMULARIO: (claseComponente: ModalComponente) => CampoFormulario[] =
  (claseComponente: ModalComponente) => {
    return [
      // AGREGUE SUS CAMPOS AQUI
      // EJEMPLO_CAMPO_TIPO_NOMBRE(claseComponente)
      // ARTICULO_CAMPO_TEXTO_CODIGO(claseComponente),
      NOMBRE_RUTA_CAMPO_FILE_ARCHIVO(claseComponente),
      CAMPO_TEXTO_NOMBRE(claseComponente),
      CAMPO_TEXTO_DESCRIPCION(claseComponente),
    ];
  };
// yo man-lab-yo-ng:v2-campo-formulario
// Texto:
// NombreRuta NombreCampo texto
// NombreRuta NombreCampo texto --esFormulario true
// NombreRuta NombreCampo texto --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo texto --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Password:
// NombreRuta NombreCampo password
// NombreRuta NombreCampo password --esFormulario true
// NombreRuta NombreCampo password --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo password --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Busqueda:
// NombreRuta NombreCampo busqueda
// NombreRuta NombreCampo busqueda --nombrePrefijo prefijo
// Input Number:
// NombreRuta NombreCampo inputNumber
// NombreRuta NombreCampo inputNumber --esFormulario true
// NombreRuta NombreCampo inputNumber --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputNumber --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Input Mask:
// NombreRuta NombreCampo inputMask
// NombreRuta NombreCampo inputMask --esFormulario true
// NombreRuta NombreCampo inputMask --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputMask --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Input Switch:
// NombreRuta NombreCampo inputSwitch
// NombreRuta NombreCampo inputSwitch --esFormulario true
// NombreRuta NombreCampo inputSwitch --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo inputSwitch --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Autocomplete:
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo autocomplete --nombrePropiedad nombrePropiedadRelacion --esFormulario true --nombrePrefijo prefijo --esDependiente true
// Select:
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M"
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M" --esFormulario true
// NombreRuta NombreCampo select "Día Lunes=\'L\'=L,Martes=\'M\'=M" "Ningún día de la semana" --esFormulario true
// NombreRuta NombreCampo select "Estado Civil=EstadoCivil.Soltero=SO,Estado Civil=EstadoCivil.Casado=SO" --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo select "Estado Civil=EstadoCivil.Soltero=SO,Estado Civil=EstadoCivil.Casado=SO" "Ningún estádo civil" --esFormulario true --nombrePrefijo prefijo
// NombreRuta NombreCampo select "Estado Civil=EstadoCivil.Soltero=SO,Estado Civil=EstadoCivil.Casado=SO" --esFormulario true --nombrePrefijo prefijo --esDependiente true
// NombreRuta NombreCampo select "Estado Civil=EstadoCivil.Soltero=SO,Estado Civil=EstadoCivil.Casado=SO" "Ningún estádo civil" --esFormulario true --nombrePrefijo prefijo --esDependiente true



// yo man-lab-yo-ng:v2-campo-formulario ArticuloArchivo Archivo texto --esFormulario true --nombrePrefijo prefijo
// NombreRuta Archivo file --esFormulario true

