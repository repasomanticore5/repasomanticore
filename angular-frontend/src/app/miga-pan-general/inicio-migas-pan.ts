import {MenuItem} from 'primeng/api';

export const INICIO_MIGAS_PAN
  : (componente: any, items?: MenuItem) => MenuItem = (componente: any, items?: MenuItem) => {
  const id = 'inicio';
  let label = 'Inicio';
  const restoDeMigas: MenuItem[] = [];

  if (componente) {
    if (componente.translocoService) {
      const traducido = componente.translocoService.translateObject('menu');
      if (traducido) {
        if (traducido !== 'menu') {
          label = traducido[id];
        }
      }
    }
  }
  return {
    id,
    icon: 'pi pi-pw',
    expanded: true,
    label,
    routerLink: ['/inicio'],
    items: restoDeMigas.length > 0 ? restoDeMigas : undefined,
  };
};
