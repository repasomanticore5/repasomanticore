import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../servicios/auth/auth.service';

@Injectable()
export class AutorizacionInterceptor implements HttpInterceptor {

  constructor(private readonly _authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this._authService.objetoAuth.jwt) {
      const authReq = req.clone({
        headers: req.headers.set('authorization', 'Bearer: ' + this._authService.objetoAuth.jwt),
      });
      return next.handle(authReq);
    } else {
      const authReq = req.clone({});
      return next.handle(authReq);
    }
  }

}
