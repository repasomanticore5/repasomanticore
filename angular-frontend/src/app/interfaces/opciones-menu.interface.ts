import {NavigationExtras} from '@angular/router';

export interface OpcionesMenuInterface {
  texto: string;
  descripcion: string;
  imagen: string;
  url: string[];
  extras?: NavigationExtras | undefined;
}
