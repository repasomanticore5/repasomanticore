export interface PaisInterface {
  flag: string;
  name: string;
  numericCode: string;
}
