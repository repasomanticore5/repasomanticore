export enum ActivoInactivo {
  Activo = 1,
  Inactivo = 0,
}

export interface ActivoInactivoInterface {
  Activo: 1;
  Inactivo: 0;
}
