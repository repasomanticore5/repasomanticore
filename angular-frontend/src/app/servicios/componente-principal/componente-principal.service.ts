import {Injectable} from '@angular/core';
import {ComponentePrincipalModule} from './componente-principal.module';

@Injectable({
  providedIn: ComponentePrincipalModule
})
export class ComponentePrincipalService {

  constructor() {
  }
}
