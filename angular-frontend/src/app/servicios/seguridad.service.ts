import {Injectable} from '@angular/core';
import {AuthService} from './auth/auth.service';

@Injectable()
export class SeguridadService {
  constructor(
    public readonly _authService: AuthService,
  ) {
  }

  encontrarPermisoPorNombre(nombrePermiso: string) {
    if (this._authService.objetoAuth.usuarioSeguridad) {
      return this._authService.objetoAuth.usuarioSeguridad.permisos
        .some(
          (nombre: string) => {
            return nombre === nombrePermiso;
          }
        );
    } else {
      return false;
    }
  }
}
