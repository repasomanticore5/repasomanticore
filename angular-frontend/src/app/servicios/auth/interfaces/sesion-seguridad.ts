export interface SesionSeguridad {
    datosUsuario: {
      nombres: string;
      apellidos: string;
      email: string;
      fechaNacimiento: string;
      identificacionPais: string;
      codigoPais: string;
      sisCreado: string;
      sisModificado: string;
      sisHabilitado: number;
      id: number;
      sexo: string;
      telefono: string;
      uid: string;
    };
    permisos: string[];
}
