import firebase from 'firebase';
import User = firebase.User;
export interface UsuarioFirebaseCallbackLoginInterface {
  additionalUserInfo: {
    isNewUser: boolean;
    providerId: string;
    profile: {
      email: string;
      granted_scopes: string;
      id: string;
      locale: string;
      name: string;
      verified_email: boolean;
    }
  };
  credential: {
    a: null
    accessToken: string;
    idToken: string;
    providerId: string;
    signInMethod: string;
  };
  operationType: string;
  user: User;

  [key: string]: any;
}
