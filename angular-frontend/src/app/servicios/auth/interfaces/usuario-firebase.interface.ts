export interface UsuarioFirebaseInterface {
  apiKey?: string;
  appName?: string;
  authDomain?: string;
  createdAt: string;
  displayName?: string;
  email: string;
  emailVerified: boolean;
  isAnonymous?: boolean;
  lastLoginAt: string;
  phoneNumber?: any;
  photoURL?: any;
  providerData?: {
    displayName?: string;
    email?: string;
    phoneNumber?: null
    photoURL?: null
    providerId?: string;
    uid?: string;
  }[];
  redirectEventId?: any;
  stsTokenManager?: {
    accessToken?: string;
    apiKey?: string;
    expirationTime?: number;
    refreshToken?: string;
  };
  tenantId?: any;
  uid: string;
}
