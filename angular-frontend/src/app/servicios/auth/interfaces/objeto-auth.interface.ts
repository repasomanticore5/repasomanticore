import {UsuarioFirebaseCallbackLoginInterface} from './usuario-firebase-callback-login.interface';
import firebase from 'firebase';
import User = firebase.User;
import {UsuarioFirebaseInterface} from './usuario-firebase.interface';
import {DatosUsuarioInterface} from '../../../ruta/ruta-login/interfaces/datos-usuario.interface';
import {SesionSeguridad} from './sesion-seguridad';

export interface ObjetoAuthInterface {
  estaLogeado: boolean;
  // usuario: {
  //   usuarioFirebaseRaw?: User;
  //   usuarioFirebase?: UsuarioFirebaseInterface;
  //   usuario?: DatosUsuarioInterface;
  //   usuarioCallbackLogin?: UsuarioFirebaseCallbackLoginInterface,
  // };
  usuarioSeguridad?: SesionSeguridad;
  jwt?: string;
}
