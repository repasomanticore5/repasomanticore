import {SesionSeguridad} from './sesion-seguridad';

export interface RespuestaAutenticarSeguridad {
    mensaje: string;
    autenticado: boolean;
    usuario?: SesionSeguridad;
    usuarioConPermisos?: boolean;
    jwt?: string;
}
