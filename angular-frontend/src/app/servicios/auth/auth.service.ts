import {Injectable} from '@angular/core';
import firebase from 'firebase';
import User = firebase.User;
import {ObjetoAuthInterface} from './interfaces/objeto-auth.interface';
import {UsuarioFirebaseInterface} from './interfaces/usuario-firebase.interface';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {RegistroInterface} from '../../ruta/ruta-login/interfaces/registro.interface';
import {RespuestaAutenticarSeguridad} from './interfaces/respuesta-autenticar-seguridad';
import {SesionSeguridad} from './interfaces/sesion-seguridad';
import Dexie from 'dexie';
import {MenuGeneralService} from '../menu-general/menu-general.service';
import {of} from 'rxjs';

declare var usuarioIndexHtml: SesionSeguridad | any;

@Injectable()
export class AuthService {
  objetoAuth: ObjetoAuthInterface = {
    estaLogeado: false,
    // usuario: {
    //   usuario: undefined,
    //   usuarioFirebase: undefined,
    //   usuarioFirebaseRaw: undefined,
    //   usuarioCallbackLogin: undefined,
    // },
  };

  constructor(
    public _httpClient: HttpClient,
    private readonly _menuGeneralService: MenuGeneralService,
  ) {
  }

  logearse(usuarioFirebaseRaw: User, usuario?: any) {
    this.objetoAuth.estaLogeado = true;
    // this.objetoAuth.usuario.usuarioFirebaseRaw = usuarioFirebaseRaw;
    // this.objetoAuth.usuario.usuarioFirebase = usuarioFirebaseRaw.toJSON() as any as UsuarioFirebaseInterface;
    // this.objetoAuth.usuario.usuario = usuario;
  }

  desLogearse() {
    this.objetoAuth.estaLogeado = false;
    // this.objetoAuth.usuario.usuarioFirebaseRaw = undefined;
    // this.objetoAuth.usuario.usuarioFirebase = undefined;
    // this.objetoAuth.usuario.usuario = undefined;
  }

  buscarDatoUsuario(uid: string) {
    return this._httpClient
      .get(environment.url + '/datos-usuario?uid=' + uid)
      .pipe(
        map(
          (data) => data as [RegistroInterface[], number]
        )
      );
  }

  crearNuevoUsuario(usuario: RegistroInterface) {
    return this._httpClient
      .post(environment.url + '/datos-usuario', usuario)
      .pipe(
        map(
          (data) => data as RegistroInterface
        )
      );
  }

  autenticar(token: string) {
    let headers = new HttpHeaders();
    headers = headers.set('idtoken', token);
    if (environment.production) {
      return this._httpClient
        .post(environment.url + '/seguridad/autenticar', {}, {headers})
        .pipe(
          map(
            (a) => a as RespuestaAutenticarSeguridad
          )
        );
    } else {
      return of({
        mensaje: 'Logeado',
        autenticado: true,
        usuario: usuarioIndexHtml,
        jwt: usuarioIndexHtml.jwt,
        usuarioConPermisos: true,
      } as RespuestaAutenticarSeguridad);
    }
  }

  async guardarDatosDeSesion(usuario: SesionSeguridad, jwt: string) {
    this.objetoAuth.estaLogeado = true;
    this.objetoAuth.usuarioSeguridad = usuario;
    this.objetoAuth.jwt = jwt;
    const db = new Dexie('mlabs_seguridad') as any;
    db.version(1)
      .stores({
        token: 'id,usuario'
      });
    const tokenGuardado = await db.token.put({id: 1, usuario: JSON.stringify(usuario)});
    // const indice = usuario.permisos.findIndex((a) => a === 'entrenadorEditarHabilitado');
    // delete usuario.permisos[indice];
    // console.log('YA ESTA', this.objetoAuth);
    // console.log(' usuario.permisos',  usuario.permisos);
    this._menuGeneralService.revisionPermisos(this._menuGeneralService.itemsDeMenu, usuario.permisos);
  }

}
