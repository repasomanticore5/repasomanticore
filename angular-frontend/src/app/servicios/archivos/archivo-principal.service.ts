import {Injectable} from '@angular/core';
import {NotificacionService} from '../notificacion/notificacion.service';
import {ConfirmationService} from 'primeng/api';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {map, mergeMap} from 'rxjs/operators';
import {ArchivoInterface} from '@manticore-labs/ng-2021/lib/clases-abstractas/interfaces/archivo.interface';
import {CrearArchivoDto} from '@manticore-labs/ng-2021/lib/clases-abstractas/interfaces/crear-archivo.dto';

declare var usuario: string;

@Injectable()
export class ArchivoPrincipalService {

  url = environment.url;
  confirmarCrear = true;
  confirmarActualizar = true;
  confirmarHabilitar = true;

  constructor(
    public readonly notificacionService: NotificacionService,
    public readonly confirmationService: ConfirmationService,
    private readonly _httpClient: HttpClient,
  ) {
  }

  subirArchivoPrincipal(registro: CrearArchivoDto): Observable<ArchivoInterface> {

    // formulario
    const formData: FormData = new FormData();
    formData.append('archivo', registro.archivo, registro.archivo.name);
    formData.append('id', registro.id.toString());
    formData.append('tipo', registro.tipo);
    if (registro.nombre) {
      formData.append('nombre', registro.nombre as string);
    }
    if (registro.descripcion) {
      formData.append('descripcion', registro.descripcion as string);
    }
    formData.append('nombreIdentificador', registro.nombreIdentificador);

    // cabeceras
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};


    if (this.confirmarCrear && this.notificacionService && this.confirmationService) {
      const promesa = new Promise<boolean>(
        (resolve, reject) => {
          if (this.confirmationService) {
            this.confirmationService.confirm({
              message: '¿Está seguro de crear?',
              accept: () => {
                resolve(true);
              },
              reject: () => {
                resolve(false);
              }
            });
          }
        }
      );
      return fromPromise(promesa)
        .pipe(
          mergeMap(
            (respuesta) => {
              if (respuesta) {
                return this._httpClient
                  .post(this.url + '/archivo-secundario/subir-archivo-principal',
                    formData,
                    options
                  )
                  .pipe(
                    map(r => r as ArchivoInterface)
                  );
              } else {
                if (this.notificacionService) {
                  this.notificacionService
                    .anadir({
                      severidad: 'warn',
                      titulo: 'No acepta usuario',
                      detalle: 'El usuario no acepto el mensaje de confirmación'
                    });
                }
                return throwError({codigo: 'CANCELADO', mensaje: 'no acepta acción'});
              }
            }
          )
        );
    } else {
      return this._httpClient
        .post(
          this.url + '/archivo-secundario/subir-archivo-principal',
          formData,
          options
        )
        .pipe(
          map(r => r as ArchivoInterface)
        );
    }
  }


  subirArchivoSecundario(registro: CrearArchivoDto): Observable<ArchivoInterface> {

    // formulario
    const formData: FormData = new FormData();
    formData.append('archivo', registro.archivo, registro.archivo.name);
    formData.append('id', registro.id.toString());
    formData.append('tipo', registro.tipo);
    formData.append('nombre', registro.nombre as string);
    formData.append('descripcion', registro.descripcion as string);
    formData.append('nombreIdentificador', registro.nombreIdentificador);

    // cabeceras
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};


    if (this.confirmarCrear && this.notificacionService && this.confirmationService) {
      const promesa = new Promise<boolean>(
        (resolve, reject) => {
          if (this.confirmationService) {
            this.confirmationService.confirm({
              message: '¿Está seguro de crear?',
              accept: () => {
                resolve(true);
              },
              reject: () => {
                resolve(false);
              }
            });
          }
        }
      );
      return fromPromise(promesa)
        .pipe(
          mergeMap(
            (respuesta) => {
              if (respuesta) {
                return this._httpClient
                  .post(this.url + '/archivo-secundario/subir-archivo-secundario',
                    formData,
                    options
                  )
                  .pipe(
                    map(r => r as ArchivoInterface)
                  );
              } else {
                if (this.notificacionService) {
                  this.notificacionService
                    .anadir({
                      severidad: 'warn',
                      titulo: 'No acepta usuario',
                      detalle: 'El usuario no acepto el mensaje de confirmación'
                    });
                }
                return throwError({codigo: 'CANCELADO', mensaje: 'no acepta acción'});
              }
            }
          )
        );
    } else {
      return this._httpClient
        .post(
          this.url + '/archivo-secundario/subir-archivo-secundario',
          formData,
          options
        )
        .pipe(
          map(r => r as ArchivoInterface)
        );
    }
  }


  // subirArchivoPrincipal(registro: CrearArchivoPrincipalDto): Observable<ArchivoInterface> {
  //
  //   // formulario
  //   const formData: FormData = new FormData();
  //   formData.append('archivo', registro.archivo, registro.archivo.name);
  //   formData.append('id', registro.id.toString());
  //   formData.append('tipo', registro.tipo);
  //   formData.append('nombreIdentificador', registro.nombreIdentificador);
  //
  //   // cabeceras
  //   const headers = new HttpHeaders();
  //   headers.append('Content-Type', 'multipart/form-data');
  //   headers.append('Accept', 'application/json');
  //   const options = {headers};
  //
  //
  //   if (this.confirmarCrear && this.notificacionService && this.confirmationService) {
  //     const promesa = new Promise<boolean>(
  //       (resolve, reject) => {
  //         if (this.confirmationService) {
  //           this.confirmationService.confirm({
  //             message: '¿Está seguro de crear?',
  //             accept: () => {
  //               resolve(true);
  //             },
  //             reject: () => {
  //               resolve(false);
  //             }
  //           });
  //         }
  //       }
  //     );
  //     return fromPromise(promesa)
  //       .pipe(
  //         mergeMap(
  //           (respuesta) => {
  //             if (respuesta) {
  //               return this._httpClient
  //                 .post(this.url + '/seguridad/archivo-secundario/subir-archivo-principal',
  //                   formData,
  //                   options
  //                 )
  //                 .pipe(
  //                   map(r => r as ArchivoInterface)
  //                 );
  //             } else {
  //               if (this.notificacionService) {
  //                 this.notificacionService
  //                   .anadir({
  //                     severidad: 'warn',
  //                     titulo: 'No acepta usuario',
  //                     detalle: 'El usuario no acepto el mensaje de confirmación'
  //                   });
  //               }
  //               return throwError({codigo: 'CANCELADO', mensaje: 'no acepta acción'});
  //             }
  //           }
  //         )
  //       );
  //   } else {
  //     return this._httpClient
  //       .post(
  //         this.url + '/seguridad/archivo-secundario/subir-archivo-principal',
  //         formData,
  //         options
  //       )
  //       .pipe(
  //         map(r => r as ArchivoInterface)
  //       );
  //   }
  // }
}
