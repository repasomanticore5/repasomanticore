export interface Notificacion {
  severidad?: 'success' | 'info' | 'warn' | 'error';
  titulo?: string;
  detalle?: string;
}
