import {Injectable} from '@angular/core';
import {NotificacionModule} from './notificacion.module';
import {MessageService} from 'primeng/api';
import {Notificacion} from './interfaces/notificacion';

@Injectable({
  providedIn: NotificacionModule
})
export class NotificacionService {

  constructor(
    private readonly _messageService: MessageService
  ) {
  }

  anadir(notificacion: Notificacion) {
    this._messageService
      .add(
        {
          severity: notificacion.severidad,
          summary: notificacion.titulo,
          detail: notificacion.detalle
        }
      );
  }
}
