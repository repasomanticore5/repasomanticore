import {EventEmitter, Injectable} from '@angular/core';
import {CargandoServiceInterface} from '@manticore-labs/ng-2021';

@Injectable({
  providedIn: 'root'
})
export class CargandoService implements CargandoServiceInterface{
  [key: string]: any;

  constructor() {
  }

  cambioBloqueado: EventEmitter<boolean> = new EventEmitter<boolean>();

  estaBloqueado = false;

  activo = false;

  bloquear(): void {
  }

  desbloquear(): void {
  }

  habilitarCargando() {
    this.activo = true;
  }

  deshabilitarCargando() {
    this.activo = false;
  }
}
