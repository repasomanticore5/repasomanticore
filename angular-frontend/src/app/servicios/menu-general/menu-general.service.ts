import {EventEmitter, Injectable} from '@angular/core';
import {EJEMPLO_FIREBASE_MIGAS_PAN} from '../../modulos/ejemplo-firebase/miga-pan-general/ejemplo-firebase-migas-pan';
import {TranslocoService} from '@ngneat/transloco';
import {ItemMenuTraducido} from '@manticore-labs/ng-2021';
import {INICIO_MIGAS_PAN} from '../../miga-pan-general/inicio-migas-pan';
import {MenuItem} from 'primeng/api';
import {ENTRENADOR_MIGAS_PAN} from '../../modulos/ejemplo-nest/ruta/ruta-entrenador/migas-pan/entrenador-migas-pan';
import {environment} from '../../../environments/environment';

@Injectable()
export class MenuGeneralService {
  aparecer = true;

  itemsDeMenu: {
    inicio: MenuItem[],
    ejemploNest: MenuItem[],
  } = {
    inicio: [
      {
        label: 'menu.inicio',
        icon: 'pi pi-pw',
        id: '', // permisos
        visible: true,
        routerLink: INICIO_MIGAS_PAN(this).routerLink,
      }
    ],
    ejemploNest: [
      {
        label: 'menu.ejemploNest.moduloEntrenador',
        icon: 'pi pi-pw',
        id: 'entrenadorBuscar', // permisos
        visible: false,
        items: [
          {
            label: 'menu.ejemploNest.entrenador',
            icon: 'pi pi-pw',
            routerLink: ENTRENADOR_MIGAS_PAN(this).routerLink,
            id: 'entrenadorBuscar,', // permisos
          },
        ]
      }
    ]
  };

  itemsDeMenuFiltrado: {
    inicio: MenuItem[],
    ejemploNest: MenuItem[],
  } = {
    inicio: [
      {
        label: 'menu.inicio',
        icon: 'pi pi-pw',
        id: '', // permisos
        visible: true,
        routerLink: INICIO_MIGAS_PAN(this).routerLink,
      }
    ],
    ejemploNest: []
  };

  cambioMenu: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private _translocoService: TranslocoService,
  ) {
    if (environment.production) {
      setTimeout(
        () => {
          this.revisionPermisos(this.itemsDeMenu, []);
        },
        1000
      );
    }

  }

  revisionPermisos(
    menu: {
      inicio: MenuItem[],
      ejemploNest: MenuItem[],
    },
    permisos: string[]
  ) {
    this.itemsDeMenuFiltrado = menu;
    this.setearVisibilidadMenu(menu.inicio, permisos);
    this.setearVisibilidadMenu(menu.ejemploNest, permisos);
    this.cambioMenu.emit(true);

    // this.itemsDeMenu.servicioLogistico = [
    //   SERVICIO_LOGISTICO_MIGAS_PAN(this),
    //   CLASE_MIGAS_PAN(this),
    //   CLASE_BIEN_MIGAS_PAN(this),
    //   TIPO_MEDIDA_MIGAS_PAN(this),
    //   TIPO_ATRIBUTO_MIGAS_PAN(this),
    //   CATALOGO_ATRIBUTO_MIGAS_PAN(this),
    // ];
  }

  setearVisibilidadMenu(items: MenuItem[], permisos: string[]) {
    if (items) {
      if (items.length > 0) {
        items = items.map(
          (item) => {
            if (item.label) {
              try {
                item.label = this._translocoService.translate(item.label);
              } catch (error) {
                console.error({
                  error,
                  mensaje: 'Error traduciendo label'
                });
              }
            }
            if (item.id) {
              const permisosEnItem = item.id.split(',');
              item.visible = permisos.some(
                (permiso) => {
                  const tienePermiso = permisosEnItem.some((permisoItem) => permisoItem === permiso);
                  return tienePermiso;
                }
              );
            }
            if (item.items) {
              if (item.items.length > 0) {
                this.setearVisibilidadMenu(item.items, permisos);
              }
            }
            return item;
          }
        );
      }
    }
  }
}
