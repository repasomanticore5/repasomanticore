import {AngularFirestore} from '@angular/fire/firestore';
import {FirebaseUtil} from '@manticore-labs/firebase-angular';
import {Injectable} from '@angular/core';

@Injectable()
export class FirebaseUtilService extends FirebaseUtil {

  constructor(private readonly _angularFirestore: AngularFirestore) {
    super(_angularFirestore);
    const a: any = _angularFirestore.firestore.app as any;
    const firebase = a.firebase_.firestore;
    this.FieldValue = firebase.FieldValue;
    this.Timestamp = firebase.Timestamp;
  }

}

