import {Component, OnInit} from '@angular/core';
import {NotificacionService} from './servicios/notificacion/notificacion.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CargandoService as CargandoServiceLocal} from './servicios/cargando/cargando.service';
import {MenuGeneralService} from './servicios/menu-general/menu-general.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';

import firebase from 'firebase/app';
import {AuthProvider} from 'ngx-auth-firebaseui';
import {environment} from '../environments/environment';
import {AuthService} from './servicios/auth/auth.service';
import {MenuItem} from 'primeng/api';
import {TranslocoService} from '@ngneat/transloco';
import {flatMap, mergeMap} from 'rxjs/operators';
import {SesionSeguridad} from './servicios/auth/interfaces/sesion-seguridad';
import {of} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'angular-frontend';
  showFiller = false;
  nombreAplicativo = environment.aplicativo.nombreAplicativo;
  versionAplicativo = environment.aplicativo.versionAplicativo;
  menuInicio: MenuItem[] = [{label: 'Inicio', routerLink: ['/inicio']}];


  constructor(
    public readonly cargandoService: CargandoServiceLocal,
    public readonly _notificacionService: NotificacionService,
    private readonly _httpClient: HttpClient,
    private readonly _router: Router,
    public dialog: MatDialog,
    public readonly menuGeneralService: MenuGeneralService,
    public readonly firestore: AngularFirestore,
    public angularFireAuth: AngularFireAuth,
    public readonly authService: AuthService,
    private _translocoService: TranslocoService,
  ) {

    this.establecerLenguajesDeInternacionalizacion(this._translocoService);
  }

  traducir() {
    alert(this._translocoService.translate('nombre'));
  }

  ngOnInit(): void {
    // this.menuGeneralService.itemsDeMenu
    //   .pokemonFirebase
    //   .subscribe(
    //     (a:any)=>{
    //       ////console.log('VALIENDO VERGA EN EL COMPONENTE PRINCIPAL', a)
    //     }
    //   )

    this.cambioMenu();
    this.escucharCambiosDeIdioma();
    this.escucharEstadoAutenticacionUsuario();
  }

  escucharEstadoAutenticacionUsuario() {
    this.angularFireAuth
      .authState
      .subscribe(
        (usuario) => {
          if (environment.production) {
            if (usuario) {
              usuario
                .getIdToken()
                .then(
                  (token) => {
                    this.autenticar(token);
                  }
                )
                .catch(
                  (error) => {
                    console.error({
                      mensaje: 'Error conectandose al servidor para identificar la sesion',
                      error
                    });
                  }
                );
            } else {
              this.authService.desLogearse();
            }
          } else {
            this.autenticar('1');
          }
        }
      );
  }

  autenticar(token: string) {
    this.authService
      .autenticar(token)
      .pipe(
        mergeMap(
          async (respuesta) => {
            if (respuesta.usuario && respuesta.jwt) {
              this.authService
                .guardarDatosDeSesion(respuesta.usuario as SesionSeguridad, respuesta.jwt)
                .then()
                .catch(
                  (error) => {
                    console.error({
                      mensaje: 'Error guardando sesion',
                      error
                    });
                  }
                );
            }else{
              console.error({
                mensaje: 'Error guardando datos de sesion',
              });
            }
            return respuesta;
          }
        )
      )
      .subscribe(
        (respuesta) => {
          console.info({mensaje: 'Logeado correctamente', data: {usuario: respuesta}});
        },
        (error) => {
          console.error({
            mensaje: 'Error guardando sesion',
            error
          });
        }
      );
  }

  escucharCambiosDeIdioma() {
    this._translocoService
      .selectTranslate('nombre')
      .subscribe(
        (data) => {
          console.info('Cambio idioma a: ', data);
        }
      );
  }

  establecerLenguajesDeInternacionalizacion(_translocoService: TranslocoService) {
    _translocoService.setAvailableLangs(
      [
        {
          id: 'en',
          label: 'English'
        },
        {
          id: 'es',
          label: 'Español'
        }
      ]
    );
  }

  cambiarIdioma(idioma: 'es' | 'en') {
    this._translocoService.setActiveLang(idioma);
    if (this.authService.objetoAuth.usuarioSeguridad) {
      this.menuGeneralService.revisionPermisos(this.menuGeneralService.itemsDeMenu, this.authService.objetoAuth.usuarioSeguridad.permisos);
    }
    // this.menuGeneralService.recargarMenu();
  }

  login() {
  }

  logout() {
  }

  printUser(event: any) {
    // //console.log(event);
  }

  printError(event: any) {
    console.error(event);
  }

  cambioMenu() {
    this.menuGeneralService
      .cambioMenu
      .subscribe(
        () => {
          this.menuGeneralService.aparecer = false;
          setTimeout(
            () => {
              this.menuGeneralService.aparecer = true;
            },
            1
          );
        }
      );
  }
}
