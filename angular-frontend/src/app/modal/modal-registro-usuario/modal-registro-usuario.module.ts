import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {MlCampoFormularioModule} from '@manticore-labs/ng-2021';
import {TranslocoModule} from '@ngneat/transloco';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {ModalRegistroUsuarioComponent} from './modal-registro-usuario.component';

@NgModule({
  imports: [
    MlCampoFormularioModule,
    CommonModule,
    MatDialogModule,
    ButtonModule,
    RippleModule,
    TranslocoModule,
    MatButtonModule,
    MatIconModule,
  ],
  declarations: [
    ModalRegistroUsuarioComponent
  ],
  entryComponents: [
    ModalRegistroUsuarioComponent
  ],
  exports: [
    ModalRegistroUsuarioComponent
  ],
})
export class ModalRegistroUsuarioModule {
}
