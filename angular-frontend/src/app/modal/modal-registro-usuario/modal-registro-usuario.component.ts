import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {
  EventoCambioAutocomplete,
  EventoCambioFormulario,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';

@Component({
  selector: 'app-modal-registro-usuario',
  templateUrl: './modal-registro-usuario.component.html',
  styleUrls: ['./modal-registro-usuario.component.scss']
})
export class ModalRegistroUsuarioComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: any
      // {
      // componente: any,
      // registro?: any,
      // camposFormulario: any,
      // servicio: ServicioComun,
      // cargandoService: CargandoService,
      // notificacionService: NotificacionService,
      // tituloCrear: string,
      // descripcionCrear: string,
      // tituloActualizar: string,
      // descripcionActualizar: string,
      // gruposFormularioCrearEditar: () => GrupoFormulario[],
      // rutaServicio: RutaComun,
      // stepperActual: number,
      // tipoArchivo?: 'imagen' | 'archivo',
    // }
  ) {
  }

  ngOnInit(): void {
  }

  cambioAutocomplete(evento: EventoCambioAutocomplete) {
    this.data.componente.formularioCrearEditarCambioAutocomplete(evento);
  }



  formularioValido(estaValido: TodosCamposValidados) {
    this.data.componente.formularioCrearEditarValido(estaValido, this.data.registro);
    // if (this.data.tipoArchivo) {
    //   this.data.componente.formularioArchivoValido(estaValido, this.data.registro, this.data.tipoArchivo);
    // } else {
    //   this.data.componente.formularioCrearEditarValido(estaValido, this.data.registro);
    // }
  }

  cambioCampo(eventoCambioCampo: EventoCambioFormulario) {
    // if (this.data.tipoArchivo) {
    //   this.data.componente.formularioArchivoCambio(eventoCambioCampo, this.data.registro, this.data.tipoArchivo);
    // } else {
    //   if (eventoCambioCampo.campoFormulario) {
    //     if (this.data.componente.nombreServicio) {
    //       this.data.componente[this.data.componente.nombreServicio]
    //         .camposSeleccionadosFormulario[eventoCambioCampo.campoFormulario.nombreCampo] = eventoCambioCampo.valor;
    //     }
    //     this.data.componente.formularioCrearEditarCambioCampo(eventoCambioCampo, this.data.registro);
    //   }
    // }

  }

  guardarEditar() {
    // if (this.data.tipoArchivo){
    //   this.data.componente.crearArchivo(this.data.tipoArchivo, this.data.registro);
    // }else{
      this.data.componente.crearEditar();
    // }
  }

  cambioStepperEnModal(indice: number) {
    // this.data.rutaServicio.cambioStepperEnModal(indice);
  }

}
