import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CargandoService} from '../../servicios/cargando/cargando.service';
import {NotificacionService} from '../../servicios/notificacion/notificacion.service';
import {
  EventoCambioAutocomplete,
  EventoCambioFormulario,
  GrupoFormulario, RutaComun,
  ServicioComun,
  TodosCamposValidados
} from '@manticore-labs/ng-2021';

@Component({
  selector: 'app-modal-crear-editar-comun',
  templateUrl: './modal-crear-editar-comun.component.html',
  styleUrls: ['./modal-crear-editar-comun.component.scss']
})
export class ModalCrearEditarComunComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      componente: any,
      registro?: any,
      camposFormulario: any,
      servicio: ServicioComun,
      cargandoService: CargandoService,
      notificacionService: NotificacionService,
      tituloCrear: string,
      descripcionCrear: string,
      tituloActualizar: string,
      descripcionActualizar: string,
      gruposFormularioCrearEditar: () => GrupoFormulario[],
      rutaServicio: RutaComun,
      stepperActual: number,
      tipoArchivo?: 'imagen' | 'archivo',
    }
  ) {
  }

  ngOnInit(): void {
  }

  cambioAutocomplete(evento: EventoCambioAutocomplete) {
    this.data.componente.formularioCrearEditarCambioAutocomplete(evento, this.data.registro);
  }

  formularioValido(estaValido: TodosCamposValidados) {
    if (this.data.tipoArchivo) {
      this.data.componente.formularioArchivoValido(estaValido, this.data.registro, this.data.tipoArchivo);
    } else {
      this.data.componente.formularioCrearEditarValido(estaValido, this.data.registro);
    }
  }

  cambioCampo(eventoCambioCampo: EventoCambioFormulario) {
    if (this.data.tipoArchivo) {
      this.data.componente.formularioArchivoCambio(eventoCambioCampo, this.data.registro, this.data.tipoArchivo);
    } else {
      if (eventoCambioCampo.campoFormulario) {
        if (this.data.componente.nombreServicio) {
          this.data.componente[this.data.componente.nombreServicio]
            .camposSeleccionadosFormulario[eventoCambioCampo.campoFormulario.nombreCampo] = eventoCambioCampo.valor;
        }
        this.data.componente.formularioCrearEditarCambioCampo(eventoCambioCampo, this.data.registro);
      }
    }

  }

  guardarEditar() {
    if (this.data.tipoArchivo){
      this.data.componente.crearArchivo(this.data.tipoArchivo, this.data.registro);
    }else{
      this.data.componente.crearEditar(this.data.registro);
    }
  }

  cambioStepperEnModal(indice: number) {
    this.data.rutaServicio.cambioStepperEnModal(indice);
  }

}
