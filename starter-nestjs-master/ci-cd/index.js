require('dotenv').config()
const fs = require('fs');
const pathEnvironment = process.env.MLAB_PATH_ENVIRONMENT ? process.env.MLAB_PATH_ENVIRONMENT : '../catalogo/src/environment/config.ts';
try {
    let contenidoArchivo = fs.readFileSync('./config.ts', 'utf-8');
    const variables = [
        {
            nombre: '$DP_CREAR$',
            valor: process.env.MLAB_DP_CREAR ? process.env.MLAB_DP_CREAR : false,
        },
        {
            nombre: '$DP_SER_LOG$',
            valor: process.env.MLAB_DP_SER_LOG ? process.env.MLAB_DP_SER_LOG : false,
        },
        {
            nombre: '$DP_CONJ$',
            valor: process.env.MLAB_DP_CONJ ? process.env.MLAB_DP_CONJ : false,
        },
        {
            nombre: '$DP_SUBCONJ$',
            valor: process.env.MLAB_DP_SUBCONJ ? process.env.MLAB_DP_SUBCONJ : false,
        },
        {
            nombre: '$CATEG$',
            valor: process.env.MLAB_DP_CATEG ? process.env.MLAB_DP_CATEG : false,
        },
        {
            nombre: '$TIP_ATRI$',
            valor: process.env.MLAB_DP_TIP_ATRI ? process.env.MLAB_DP_TIP_ATRI : false,
        },
        {
            nombre: '$CLAS_BIEN$',
            valor: process.env.MLAB_DP_CLAS_BIEN ? process.env.MLAB_DP_CLAS_BIEN : false,
        },
        {
            nombre: '$CAT_ATRIB$',
            valor: process.env.MLAB_DP_CAT_ATRIB ? process.env.MLAB_DP_CAT_ATRIB : false,
        },
        {
            nombre: '$ATRIB$',
            valor: process.env.MLAB_DP_ATRIB ? process.env.MLAB_DP_ATRIB : false,
        },
        {
            nombre: '$CLAS$',
            valor: process.env.MLAB_DP_CLAS ? process.env.MLAB_DP_CLAS : false,
        },
        {
            nombre: '$TIP_MED$',
            valor: process.env.MLAB_DP_TIP_MED ? process.env.MLAB_DP_TIP_MED : false,
        },
        {
            nombre: '$MED$',
            valor: process.env.MLAB_DP_MED ? process.env.MLAB_DP_MED : false,
        },
        {
            nombre: '$DB_SID$',
            valor: process.env.MLAB_DB_SID ? process.env.MLAB_DB_SID : 'SISLOGE',
        },
        {
            nombre: '$DB_HOST$',
            valor: process.env.MLAB_DB_HOST ? process.env.MLAB_DB_HOST : '75.119.132.248',
        },
        {
            nombre: '$DB_PORT$',
            valor: process.env.MLAB_DB_PORT ? process.env.MLAB_DB_PORT : 1521,
        },
        {
            nombre: '$DB_TYPE$',
            valor: process.env.MLAB_DB_TYPE ? process.env.MLAB_DB_TYPE : 'oracle',
        },
        {
            nombre: '$DB_SCHEMA$',
            valor: process.env.MLAB_DB_SCHEMA ? process.env.MLAB_DB_SCHEMA : 'SISLOGDESAR',
        },
        {
            nombre: '$DB_USERNAME$',
            valor: process.env.MLAB_DB_USERNAME ? process.env.MLAB_DB_USERNAME : 'SISLOGDESAR',
        },
        {
            nombre: '$DB_PASSWORD$',
            valor: process.env.MLAB_DB_PASSWORD ? process.env.MLAB_DB_PASSWORD : 'SISLOGDESAR',
        },
        {
            nombre: '$DB_SYNCRHONIZE$',
            valor: process.env.MLAB_DB_SYNCRHONIZE ? process.env.MLAB_DB_SYNCRHONIZE : false,
        },
        {
            nombre: '$DB_DROPSCHEMA$',
            valor: process.env.MLAB_DB_DROPSCHEMA ? process.env.MLAB_DB_DROPSCHEMA : false,
        },
        {
            nombre: '$DB_CACHE$',
            valor: process.env.MLAB_DB_CACHE ? process.env.MLAB_DB_CACHE : true,
        },
        {
            nombre: '$DB_DEBUG$',
            valor: process.env.MLAB_DB_DEBUG ? process.env.MLAB_DB_DEBUG : true,
        },
        {
            nombre: '$DB_MAX_QET$',
            valor: process.env.MLAB_DB_MAX_QET ? process.env.MLAB_DB_MAX_QET : 1000000,
        },
        {
            nombre: '$DB_BUFFER_ENTRIES$',
            valor: process.env.MLAB_DB_BUFFER_ENTRIES ? process.env.MLAB_DB_BUFFER_ENTRIES : 1000000,
        },
        {
            nombre: '$PORT$',
            valor: process.env.MLAB_PORT ? process.env.MLAB_PORT : 3000,
        },
        {
            nombre: '$SES_SECRET$',
            valor: process.env.MLAB_SES_SECRET ? process.env.MLAB_SES_SECRET : 'session_cookie_secret',
        },
        {
            nombre: '$RED_PORT$',
            valor: process.env.MLAB_RED_PORT ? process.env.MLAB_RED_PORT : 6379,
        },
        {
            nombre: '$RED_HOST$',
            valor: process.env.MLAB_RED_HOST ? process.env.MLAB_RED_HOST : '161.97.152.203',
        },
        {
            nombre: '$SEG_ACTIVAR$',
            valor: process.env.MLAB_SEG_ACTIVAR ? process.env.MLAB_SEG_ACTIVAR : false,
        },
        {
            nombre: '$SEG_DB_SCHEMA$',
            valor: process.env.MLAB_SEG_DB_SCHEMA ? process.env.MLAB_SEG_DB_SCHEMA : 'SEGURIDADESDESAR',
        },
        {
            nombre: '$AUD_DB_SCHEMA$',
            valor: process.env.MLAB_AUD_DB_SCHEMA ? process.env.MLAB_AUD_DB_SCHEMA : 'AUDITORIADESAR',
        },
    ];
    variables.forEach(
        (variable) => {
            contenidoArchivo = contenidoArchivo.replace(variable.nombre, variable.valor);
        }
    )
    fs.writeFileSync(pathEnvironment, contenidoArchivo, 'utf-8');
    console.info({
        mensaje: 'Se reemplazo el archivo',
        contenidoArchivo,
    });
} catch (error) {
    throw new Error('Error leyendo archivo o escribiendo');
}
