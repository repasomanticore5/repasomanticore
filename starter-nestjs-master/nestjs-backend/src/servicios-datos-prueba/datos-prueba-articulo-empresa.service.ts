import { Injectable } from '@nestjs/common';
import { DATOS_PRUEBA } from './datos-prueba';
import { CONFIG } from '../environment/config';
import { ARTICULO_EMPRESA_DATOS_PRUEBA } from '../submodulo-articulo-empresa/articulo-empresa/constantes/articulo-empresa.datos-prueba';
import { ArticuloEmpresaService } from '../submodulo-articulo-empresa/articulo-empresa/articulo-empresa.service';
import { ArticuloEmpresaEntity } from '../submodulo-articulo-empresa/articulo-empresa/articulo-empresa.entity';
import { PrecioService } from '../submodulo-articulo-empresa/precio/precio.service';
import { CostoCompraService } from '../submodulo-articulo-empresa/costo-compra/costo-compra.service';
import { PRECIO_DATOS_PRUEBA } from '../submodulo-articulo-empresa/precio/constantes/precio.datos-prueba';
import { PrecioEntity } from '../submodulo-articulo-empresa/precio/precio.entity';
import { COSTO_COMPRA_DATOS_PRUEBA } from '../submodulo-articulo-empresa/costo-compra/constantes/costo-compra.datos-prueba';
import { CostoCompraEntity } from '../submodulo-articulo-empresa/costo-compra/costo-compra.entity';
import { DatosPruebaEmpresaService } from './datos-prueba-empresa.service';

@Injectable()
export class DatosPruebaArticuloEmpresaService {
  constructor(
    private readonly _articuloEmpresaService: ArticuloEmpresaService,
    private readonly _precioService: PrecioService,
    private readonly _costoCompraService: CostoCompraService,
    private readonly _datosPruebaEmpresaService: DatosPruebaEmpresaService,
  ) {}

  async crearArticulosEmpresa() {
    DATOS_PRUEBA.articuloEmpresa = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const articulo of DATOS_PRUEBA.articulo) {
        for (const empresaArticulo of ARTICULO_EMPRESA_DATOS_PRUEBA) {
          const respuesta = ((await this._articuloEmpresaService.crear(
            empresaArticulo(empresa.id, articulo.id),
          )) as any) as ArticuloEmpresaEntity;
          DATOS_PRUEBA.articuloEmpresa.push(respuesta);
        }
      }
    }
    if (CONFIG.datosPrueba.precio) {
      await this.crearPrecio();
    }
    if (CONFIG.datosPrueba.costoCompra) {
      await this.crearCostoCompra();
    }
    if (CONFIG.datosPrueba.servicioEstablecimiento) {
      await this._datosPruebaEmpresaService.crearServiciosEstablecimientos();
    }
  }

  async crearPrecio() {
    DATOS_PRUEBA.precio = [];
    for (const articuloEmpresa of DATOS_PRUEBA.articuloEmpresa) {
      for (const precio of PRECIO_DATOS_PRUEBA) {
        const respuesta = ((await this._precioService.crear(
          precio(articuloEmpresa.id),
        )) as any) as PrecioEntity;
        DATOS_PRUEBA.precio.push(respuesta);
      }
    }
  }

  async crearCostoCompra() {
    DATOS_PRUEBA.costoCompra = [];
    for (const articuloEmpresa of DATOS_PRUEBA.articuloEmpresa) {
      for (const costoCompra of COSTO_COMPRA_DATOS_PRUEBA) {
        const respuesta = ((await this._costoCompraService.crear(
          costoCompra(articuloEmpresa.id),
        )) as any) as CostoCompraEntity;
        DATOS_PRUEBA.costoCompra.push(respuesta);
      }
    }
  }
}
