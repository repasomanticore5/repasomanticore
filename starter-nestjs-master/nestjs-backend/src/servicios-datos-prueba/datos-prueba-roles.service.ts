import { DATOS_PRUEBA } from './datos-prueba';
import { DatosUsuarioService } from '../submodulo-roles/datos-usuario/datos-usuario.service';
import { DATOS_USUARIO_DATOS_PRUEBA } from '../submodulo-roles/datos-usuario/constantes/datos-usuario.datos-prueba';
import { DatosUsuarioEntity } from '../submodulo-roles/datos-usuario/datos-usuario.entity';
import { Injectable } from '@nestjs/common';
import { PermisoRolService } from '../submodulo-roles/permiso-rol/permiso-rol.service';
import { NombrePermisoService } from '../submodulo-roles/nombre-permiso/nombre-permiso.service';
import { RolService } from '../submodulo-roles/rol/rol.service';
import { RolPorUsuarioService } from '../submodulo-roles/rol-por-usuario/rol-por-usuario.service';
import { DispositivosMovilesService } from '../submodulo-roles/dispositivos-moviles/dispositivos-moviles.service';
import { MODULO_DATOS_PRUEBA } from '../submodulo-roles/modulo/constantes/modulo.datos-prueba';
import { ModuloService } from '../submodulo-roles/modulo/modulo.service';
import { ModuloEntity } from '../submodulo-roles/modulo/modulo.entity';
import { CONFIG } from '../environment/config';
import { ROL_DATOS_PRUEBA } from '../submodulo-roles/rol/constantes/rol.datos-prueba';
import { RolEntity } from '../submodulo-roles/rol/rol.entity';
import e from 'express';
import { NOMBRE_PERMISO_DATOS_PRUEBA } from '../submodulo-roles/nombre-permiso/constantes/nombre-permiso.datos-prueba';
import { NombrePermisoEntity } from '../submodulo-roles/nombre-permiso/nombre-permiso.entity';
import { PERMISO_ROL_DATOS_PRUEBA } from '../submodulo-roles/permiso-rol/constantes/permiso-rol.datos-prueba';
import { PermisoRolEntity } from '../submodulo-roles/permiso-rol/permiso-rol.entity';
import { ROL_POR_USUARIO_DATOS_PRUEBA } from '../submodulo-roles/rol-por-usuario/constantes/rol-por-usuario.datos-prueba';
import { RolPorUsuarioEntity } from '../submodulo-roles/rol-por-usuario/rol-por-usuario.entity';
import { DISPOSITIVOS_MOVILES_DATOS_PRUEBA } from '../submodulo-roles/dispositivos-moviles/constantes/dispositivos-moviles.datos-prueba';
import { DispositivosMovilesEntity } from '../submodulo-roles/dispositivos-moviles/dispositivos-moviles.entity';

@Injectable()
export class DatosPruebaRolesService {
  constructor(
    private readonly _datosUsuarioService: DatosUsuarioService,
    private readonly _moduloService: ModuloService,
    private readonly _permisoRolService: PermisoRolService,
    private readonly _nombrePermisoService: NombrePermisoService,
    private readonly _rolService: RolService,
    private readonly _rolPorUsuarioService: RolPorUsuarioService,
    private readonly _dispositivosMovilesService: DispositivosMovilesService,
  ) {}

  async crearDatosPruebaSubmoduloRoles() {
    if (CONFIG.datosPrueba.datosUsuario) {
      await this.crearDatosUsuario();
    }
    if (CONFIG.datosPrueba.modulo) {
      await this.crearModulos();
    }
  }
  async crearDatosUsuario() {
    DATOS_PRUEBA.datosUsuario = [];
    for (const datosUsuario of DATOS_USUARIO_DATOS_PRUEBA) {
      const respuesta = ((await this._datosUsuarioService.crear(
        datosUsuario(),
      )) as any) as DatosUsuarioEntity;
      DATOS_PRUEBA.datosUsuario.push(respuesta);
    }
    if (CONFIG.datosPrueba.dispMovilesUsuario) {
      await this.crearDispositivosMoviles();
    }
  }

  async crearModulos() {
    DATOS_PRUEBA.modulo = [];
    for (const modulo of MODULO_DATOS_PRUEBA) {
      const respuesta = ((await this._moduloService.crear(
        modulo(),
      )) as any) as ModuloEntity;
      DATOS_PRUEBA.modulo.push(respuesta);
    }
  }

  async crearRoles() {
    DATOS_PRUEBA.rol = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const rol of ROL_DATOS_PRUEBA) {
        const respuesta = ((await this._rolService.crear(
          rol(empresa.id),
        )) as any) as RolEntity;
        DATOS_PRUEBA.rol.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.nombrePermiso) {
      await this.crearNombresPermiso();
    }
  }

  async crearNombresPermiso() {
    DATOS_PRUEBA.nombrePermiso = [];
    for (const modulo of DATOS_PRUEBA.modulo) {
      for (const nombrePermiso of NOMBRE_PERMISO_DATOS_PRUEBA) {
        const respuesta = ((await this._nombrePermisoService.crear(
          nombrePermiso(modulo.id),
        )) as any) as NombrePermisoEntity;
        DATOS_PRUEBA.nombrePermiso.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.permisoRol) {
      await this.crearPermisoRol();
    }
  }

  async crearPermisoRol() {
    DATOS_PRUEBA.permisoRol = [];
    for (const rol of DATOS_PRUEBA.rol) {
      for (const nombrePermiso of DATOS_PRUEBA.nombrePermiso) {
        for (const permisoRol of PERMISO_ROL_DATOS_PRUEBA) {
          const respuesta = ((await this._permisoRolService.crear(
            permisoRol(rol.id, nombrePermiso.id),
          )) as any) as PermisoRolEntity;
          DATOS_PRUEBA.permisoRol.push(respuesta);
        }
      }
    }
    if (CONFIG.datosPrueba.rolPorUsuario) {
      await this.crearRolPorUsuario();
    }
  }

  async crearRolPorUsuario() {
    DATOS_PRUEBA.rolPorUsuario = [];
    for (const rol of DATOS_PRUEBA.rol) {
      for (const datosUsuario of DATOS_PRUEBA.datosUsuario) {
        for (const rolPorUsuario of ROL_POR_USUARIO_DATOS_PRUEBA) {
          const respuesta = ((await this._rolPorUsuarioService.crear(
            rolPorUsuario(datosUsuario.id, rol.id),
          )) as any) as RolPorUsuarioEntity;
          DATOS_PRUEBA.rolPorUsuario.push(respuesta);
        }
      }
    }
  }

  async crearDispositivosMoviles() {
    DATOS_PRUEBA.dispMovilesUsuario = [];
    for (const datosUsuario of DATOS_PRUEBA.datosUsuario) {
      for (const dispositivoMovil of DISPOSITIVOS_MOVILES_DATOS_PRUEBA) {
        const respuesta = ((await this._dispositivosMovilesService.crear(
          dispositivoMovil(datosUsuario.id),
        )) as any) as DispositivosMovilesEntity;
        DATOS_PRUEBA.dispMovilesUsuario.push(respuesta);
      }
    }
  }
}
