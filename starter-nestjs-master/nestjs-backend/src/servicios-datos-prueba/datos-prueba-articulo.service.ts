import { Injectable } from '@nestjs/common';
import { DATOS_PRUEBA } from './datos-prueba';
import { GrupoService } from '../submodulo-articulo/grupo/grupo.service';
import { SubgrupoService } from '../submodulo-articulo/subgrupo/subgrupo.service';
import { ArticuloService } from '../submodulo-articulo/articulo/articulo.service';
import { DetalleAdicionalArticuloService } from '../submodulo-articulo/detalle-adicional-articulo/detalle-adicional-articulo.service';
import { BusquedaArticuloTipoService } from '../submodulo-articulo/busqueda-articulo-tipo/busqueda-articulo-tipo.service';
import { BusquedaArticuloValorService } from '../submodulo-articulo/busqueda-articulo-valor/busqueda-articulo-valor.service';
import { BusquedaPorArticuloService } from '../submodulo-articulo/busqueda-por-articulo/busqueda-por-articulo.service';
import { EtiquetaArticuloService } from '../submodulo-articulo/etiqueta-articulo/etiqueta-articulo.service';
import { EtiquetaPorArticuloService } from '../submodulo-articulo/etiqueta-por-articulo/etiqueta-por-articulo.service';
import { BusquedaXContactoEmpresaService } from '../submodulo-articulo/busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.service';
import { GRUPO_DATOS_PRUEBA } from '../submodulo-articulo/grupo/constantes/grupo.datos-prueba';
import { GrupoEntity } from '../submodulo-articulo/grupo/grupo.entity';
import { BUSQUEDA_ARTICULO_TIPO_DATOS_PRUEBA } from '../submodulo-articulo/busqueda-articulo-tipo/constantes/busqueda-articulo-tipo.datos-prueba';
import { BusquedaArticuloTipoEntity } from '../submodulo-articulo/busqueda-articulo-tipo/busqueda-articulo-tipo.entity';
import { BUSQUEDA_ARTICULO_VALOR_DATOS_PRUEBA } from '../submodulo-articulo/busqueda-articulo-valor/constantes/busqueda-articulo-valor.datos-prueba';
import { BusquedaArticuloValorEntity } from '../submodulo-articulo/busqueda-articulo-valor/busqueda-articulo-valor.entity';
import { ETIQUETA_ARTICULO_DATOS_PRUEBA } from '../submodulo-articulo/etiqueta-articulo/constantes/etiqueta-articulo.datos-prueba';
import { EtiquetaArticuloEntity } from '../submodulo-articulo/etiqueta-articulo/etiqueta-articulo.entity';
import { ETIQUETA_POR_ARTICULO_DATOS_PRUEBA } from '../submodulo-articulo/etiqueta-por-articulo/constantes/etiqueta-por-articulo.datos-prueba';
import { EtiquetaPorArticuloEntity } from '../submodulo-articulo/etiqueta-por-articulo/etiqueta-por-articulo.entity';
import { SUBGRUPO_DATOS_PRUEBA } from '../submodulo-articulo/subgrupo/constantes/subgrupo.datos-prueba';
import { SubgrupoEntity } from '../submodulo-articulo/subgrupo/subgrupo.entity';
import { ARTICULO_DATOS_PRUEBA } from '../submodulo-articulo/articulo/constantes/articulo.datos-prueba';
import { ArticuloEntity } from '../submodulo-articulo/articulo/articulo.entity';
import { DETALLE_ADICIONAL_ARTICULO_DATOS_PRUEBA } from '../submodulo-articulo/detalle-adicional-articulo/constantes/detalle-adicional-articulo.datos-prueba';
import { DetalleAdicionalArticuloEntity } from '../submodulo-articulo/detalle-adicional-articulo/detalle-adicional-articulo.entity';
import { BUSQUEDA_POR_ARTICULO_DATOS_PRUEBA } from '../submodulo-articulo/busqueda-por-articulo/constantes/busqueda-por-articulo.datos-prueba';
import { BusquedaPorArticuloEntity } from '../submodulo-articulo/busqueda-por-articulo/busqueda-por-articulo.entity';
import { BUSQUEDA_X_CONTACTO_EMPRESA_DATOS_PRUEBA } from '../submodulo-articulo/busqueda-x-contacto-empresa/constantes/busqueda-x-contacto-empresa.datos-prueba';
import { BusquedaXContactoEmpresaEntity } from '../submodulo-articulo/busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.entity';
import { CONFIG } from '../environment/config';

@Injectable()
export class DatosPruebaArticuloService {
  constructor(
    private readonly _grupoService: GrupoService,
    private readonly _subgrupoService: SubgrupoService,
    private readonly _articuloService: ArticuloService,
    private readonly _detalleAdicionalArticuloService: DetalleAdicionalArticuloService,
    private readonly _busquedaArticuloTipoService: BusquedaArticuloTipoService,
    private readonly _busquedaArticuloValorService: BusquedaArticuloValorService,
    private readonly _busquedaPorArticuloService: BusquedaPorArticuloService,
    private readonly _etiquetaArticuloService: EtiquetaArticuloService,
    private readonly _etiquetaPorArticuloService: EtiquetaPorArticuloService,
    private readonly _busquedaXContactoEmpresaService: BusquedaXContactoEmpresaService,
  ) {}

  async crearDatosPruebaSubmoduloArticulo() {
    if (CONFIG.datosPrueba.etiquetaArticulo) {
      await this.crearEtiquetasArticulo();
    }
    if (CONFIG.datosPrueba.busquedaArticuloTipo) {
      await this.crearBusquedasArticuloTipo();
    }
    if (CONFIG.datosPrueba.grupo) {
      await this.crearGrupos();
    }
  }

  async crearGrupos() {
    DATOS_PRUEBA.grupo = [];
    for (const grupo of GRUPO_DATOS_PRUEBA) {
      const respuesta = ((await this._grupoService.crear(
        grupo(),
      )) as any) as GrupoEntity;
      DATOS_PRUEBA.grupo.push(respuesta);
    }
    if (CONFIG.datosPrueba.subgrupo) {
      await this.crearSubgrupos();
    }
  }

  async crearBusquedasArticuloTipo() {
    DATOS_PRUEBA.busquedaArticuloTipo = [];
    for (const busquedaArticuloTipo of BUSQUEDA_ARTICULO_TIPO_DATOS_PRUEBA) {
      const respuesta = ((await this._busquedaArticuloTipoService.crear(
        busquedaArticuloTipo(),
      )) as any) as BusquedaArticuloTipoEntity;
      DATOS_PRUEBA.busquedaArticuloTipo.push(respuesta);
    }
    if (CONFIG.datosPrueba.busquedaArticuloValor) {
      await this.crearBusquedasArticuloValor();
    }
  }

  async crearBusquedasArticuloValor() {
    DATOS_PRUEBA.busquedaArticuloValor = [];
    for (const busquedaArticuloTipo of DATOS_PRUEBA.busquedaArticuloTipo) {
      for (const busquedaArticuloValor of BUSQUEDA_ARTICULO_VALOR_DATOS_PRUEBA) {
        const respuesta = ((await this._busquedaArticuloValorService.crear(
          busquedaArticuloValor(busquedaArticuloTipo.id),
        )) as any) as BusquedaArticuloValorEntity;
        DATOS_PRUEBA.busquedaArticuloValor.push(respuesta);
      }
    }
  }

  async crearEtiquetasArticulo() {
    DATOS_PRUEBA.etiquetaArticulo = [];
    for (const etiquetaArticulo of ETIQUETA_ARTICULO_DATOS_PRUEBA) {
      const respuesta = ((await this._etiquetaArticuloService.crear(
        etiquetaArticulo(),
      )) as any) as EtiquetaArticuloEntity;
      DATOS_PRUEBA.etiquetaArticulo.push(respuesta);
    }
  }

  async crearEtiquetasPorArticulo() {
    DATOS_PRUEBA.etiquetaPorArticulo = [];
    for (const etiquetaArticulo of DATOS_PRUEBA.etiquetaArticulo) {
      for (const articulo of DATOS_PRUEBA.articulo) {
        for (const etiquetaPorArticulo of ETIQUETA_POR_ARTICULO_DATOS_PRUEBA) {
          const respuesta = ((await this._etiquetaPorArticuloService.crear(
            etiquetaPorArticulo(etiquetaArticulo.id, articulo.id),
          )) as any) as EtiquetaPorArticuloEntity;
          DATOS_PRUEBA.etiquetaPorArticulo.push(respuesta);
        }
      }
    }
  }

  async crearSubgrupos() {
    DATOS_PRUEBA.subgrupo = [];
    for (const grupo of DATOS_PRUEBA.grupo) {
      for (const subgrupo of SUBGRUPO_DATOS_PRUEBA) {
        const respuesta = ((await this._subgrupoService.crear(
          subgrupo(grupo.id),
        )) as any) as SubgrupoEntity;
        DATOS_PRUEBA.subgrupo.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.articulo) {
      await this.crearArticulos();
    }
  }

  async crearArticulos() {
    DATOS_PRUEBA.articulo = [];
    for (const subgrupo of DATOS_PRUEBA.subgrupo) {
      for (const articulo of ARTICULO_DATOS_PRUEBA) {
        const respuesta = ((await this._articuloService.crear(
          articulo(subgrupo.id),
        )) as any) as ArticuloEntity;
        DATOS_PRUEBA.articulo.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.detalleAdicionalArticulo) {
      await this.crearDetalleAdicionalArticulos();
    }
    if (CONFIG.datosPrueba.etiquetaPorArticulo) {
      await this.crearEtiquetasPorArticulo();
    }
    if (CONFIG.datosPrueba.busquedaPorArticulo) {
      await this.crearBusquedasPorArticulo();
    }
  }

  async crearDetalleAdicionalArticulos() {
    DATOS_PRUEBA.detalleAdicionalArticulo = [];
    for (const articulo of DATOS_PRUEBA.articulo) {
      for (const detalle of DETALLE_ADICIONAL_ARTICULO_DATOS_PRUEBA) {
        const respuesta = ((await this._detalleAdicionalArticuloService.crear(
          detalle(articulo.id),
        )) as any) as DetalleAdicionalArticuloEntity;
        DATOS_PRUEBA.detalleAdicionalArticulo.push(respuesta);
      }
    }
  }

  async crearBusquedasPorArticulo() {
    DATOS_PRUEBA.busquedaPorArticulo = [];
    for (const busquedaArticuloValor of DATOS_PRUEBA.busquedaArticuloValor) {
      for (const articulo of DATOS_PRUEBA.articulo) {
        for (const busquedaPorArticulo of BUSQUEDA_POR_ARTICULO_DATOS_PRUEBA) {
          const respuesta = ((await this._busquedaPorArticuloService.crear(
            busquedaPorArticulo(busquedaArticuloValor.id, articulo.id),
          )) as any) as BusquedaPorArticuloEntity;
          DATOS_PRUEBA.busquedaPorArticulo.push(respuesta);
        }
      }
    }
  }

  async crearBusquedasXContactoEmpresa() {
    DATOS_PRUEBA.busquedaXContactoEmpresa = [];
    for (const busquedaArticuloTipo of DATOS_PRUEBA.busquedaArticuloTipo) {
      for (const contactoEmpresa of DATOS_PRUEBA.contactoEmpresa) {
        for (const busquedaXContactoEmpresa of BUSQUEDA_X_CONTACTO_EMPRESA_DATOS_PRUEBA) {
          const respuesta = ((await this._busquedaXContactoEmpresaService.crear(
            busquedaXContactoEmpresa(
              busquedaArticuloTipo.id,
              contactoEmpresa.id,
            ),
          )) as any) as BusquedaXContactoEmpresaEntity;
          DATOS_PRUEBA.busquedaXContactoEmpresa.push(respuesta);
        }
      }
    }
  }
}
