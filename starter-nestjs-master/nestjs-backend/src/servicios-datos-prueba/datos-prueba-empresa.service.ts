import { Injectable } from '@nestjs/common';
import { ContactoEmpresaService } from '../submodulo-empresa/contacto-empresa/contacto-empresa.service';
import { CONTACTO_EMPRESA_DATOS_PRUEBA } from '../submodulo-empresa/contacto-empresa/constantes/contacto-empresa.datos-prueba';
import { ContactoEmpresaEntity } from '../submodulo-empresa/contacto-empresa/contacto-empresa.entity';
import { AppService } from '../app.service';
import { DATOS_PRUEBA } from './datos-prueba';
import { EmpresaService } from '../submodulo-empresa/empresa/empresa.service';
import { DireccionService } from '../submodulo-empresa/direccion/direccion.service';
import { EdificioService } from '../submodulo-empresa/edificio/edificio.service';
import { EstablecimientoService } from '../submodulo-empresa/establecimiento/establecimiento.service';
import { PisoService } from '../submodulo-empresa/piso/piso.service';
import { AreaPisoService } from '../submodulo-empresa/area-piso/area-piso.service';
import { TipoCargoService } from '../submodulo-empresa/tipo-cargo/tipo-cargo.service';
import { TipoBodegaService } from '../submodulo-empresa/tipo-bodega/tipo-bodega.service';
import { TipoIndustriaService } from '../submodulo-empresa/tipo-industria/tipo-industria.service';
import { EmpresaIndustriaService } from '../submodulo-empresa/empresa-industria/empresa-industria.service';
import { CodigoPaisService } from '../submodulo-empresa/codigo-pais/codigo-pais.service';
import { HorarioService } from '../submodulo-empresa/horario/horario.service';
import { ServicioEstablecimientoService } from '../submodulo-empresa/servicio-establecimiento/servicio-establecimiento.service';
import { HorarioServicioService } from '../submodulo-empresa/horario-servicio/horario-servicio.service';
import { TIPO_INDUSTRIA_DATOS_PRUEBA } from '../submodulo-empresa/tipo-industria/constantes/tipo-industria.datos-prueba';
import { TipoIndustriaEntity } from '../submodulo-empresa/tipo-industria/tipo-industria.entity';
import { CODIGO_PAIS_DATOS_PRUEBA } from '../submodulo-empresa/codigo-pais/constantes/codigo-pais.datos-prueba';
import { CodigoPaisEntity } from '../submodulo-empresa/codigo-pais/codigo-pais.entity';
import { EMPRESA_DATOS_PRUEBA } from '../submodulo-empresa/empresa/constantes/empresa.datos-prueba';
import { EmpresaEntity } from '../submodulo-empresa/empresa/empresa.entity';
import { CONFIG } from '../environment/config';
import { EMPRESA_INDUSTRIA_DATOS_PRUEBA } from '../submodulo-empresa/empresa-industria/constantes/empresa-industria.datos-prueba';
import { EmpresaIndustriaEntity } from '../submodulo-empresa/empresa-industria/empresa-industria.entity';
import { HORARIO_DATOS_PRUEBA } from '../submodulo-empresa/horario/constantes/horario.datos-prueba';
import { HorarioEntity } from '../submodulo-empresa/horario/horario.entity';
import { EDIFICIO_DATOS_PRUEBA } from '../submodulo-empresa/edificio/constantes/edificio.datos-prueba';
import { EdificioEntity } from '../submodulo-empresa/edificio/edificio.entity';
import { ESTABLECIMIENTO_DATOS_PRUEBA } from '../submodulo-empresa/establecimiento/constantes/establecimiento.datos-prueba';
import { EstablecimientoEntity } from '../submodulo-empresa/establecimiento/establecimiento.entity';
import { DIRECCION_DATOS_PRUEBA } from '../submodulo-empresa/direccion/constantes/direccion.datos-prueba';
import { DireccionEntity } from '../submodulo-empresa/direccion/direccion.entity';
import { PISO_DATOS_PRUEBA } from '../submodulo-empresa/piso/constantes/piso.datos-prueba';
import { PisoEntity } from '../submodulo-empresa/piso/piso.entity';
import { TIPO_CARGO_DATOS_PRUEBA } from '../submodulo-empresa/tipo-cargo/constantes/tipo-cargo.datos-prueba';
import { TipoCargoEntity } from '../submodulo-empresa/tipo-cargo/tipo-cargo.entity';
import { AREA_PISO_DATOS_PRUEBA } from '../submodulo-empresa/area-piso/constantes/area-piso.datos-prueba';
import { AreaPisoEntity } from '../submodulo-empresa/area-piso/area-piso.entity';
import { TIPO_BODEGA_DATOS_PRUEBA } from '../submodulo-empresa/tipo-bodega/constantes/tipo-bodega.datos-prueba';
import { TipoBodegaEntity } from '../submodulo-empresa/tipo-bodega/tipo-bodega.entity';
import { SERVICIO_ESTABLECIMIENTO_DATOS_PRUEBA } from '../submodulo-empresa/servicio-establecimiento/constantes/servicio-establecimiento.datos-prueba';
import { ServicioEstablecimientoEntity } from '../submodulo-empresa/servicio-establecimiento/servicio-establecimiento.entity';
import { HORARIO_SERVICIO_DATOS_PRUEBA } from '../submodulo-empresa/horario-servicio/constantes/horario-servicio.datos-prueba';
import { HorarioServicioEntity } from '../submodulo-empresa/horario-servicio/horario-servicio.entity';
import { DatosPruebaArticuloEmpresaService } from './datos-prueba-articulo-empresa.service';
import { DatosPruebaArticuloService } from './datos-prueba-articulo.service';
import { DatosContactoService } from '../submodulo-empresa/datos-contacto/datos-contacto.service';
import { BodegaService } from '../submodulo-empresa/bodega/bodega.service';
import { DATOS_CONTACTO_DATOS_PRUEBA } from '../submodulo-empresa/datos-contacto/constantes/datos-contacto.datos-prueba';
import { DatosContactoEntity } from '../submodulo-empresa/datos-contacto/datos-contacto.entity';
import { BODEGA_DATOS_PRUEBA } from '../submodulo-empresa/bodega/constantes/bodega.datos-prueba';
import { obtenerEnteroRandom } from '../constantes/retornar-entero-random';
import { BodegaEntity } from '../submodulo-empresa/bodega/bodega.entity';
import { BodegaTipoService } from '../submodulo-empresa/bodega-tipo/bodega-tipo.service';
import { BODEGA_TIPO_DATOS_PRUEBA } from '../submodulo-empresa/bodega-tipo/constantes/bodega-tipo.datos-prueba';
import { BodegaTipoEntity } from '../submodulo-empresa/bodega-tipo/bodega-tipo.entity';
import { CaracteristicaBodegaService } from '../submodulo-empresa/caracteristica-bodega/caracteristica-bodega.service';
import { CARACTERISTICA_BODEGA_DATOS_PRUEBA } from '../submodulo-empresa/caracteristica-bodega/constantes/caracteristica-bodega.datos-prueba';
import { CaracteristicaBodegaEntity } from '../submodulo-empresa/caracteristica-bodega/caracteristica-bodega.entity';
import { CaracteristicaTipoBodegaService } from '../submodulo-empresa/caracteristica-tipo-bodega/caracteristica-tipo-bodega.service';
import { CARACTERISTICA_TIPO_BODEGA_DATOS_PRUEBA } from '../submodulo-empresa/caracteristica-tipo-bodega/constantes/caracteristica-tipo-bodega.datos-prueba';
import { CaracteristicaTipoBodegaEntity } from '../submodulo-empresa/caracteristica-tipo-bodega/caracteristica-tipo-bodega.entity';
import { CaracteristicaBodegaTipoEntity } from '../submodulo-empresa/caracteristica-bodega-tipo/caracteristica-bodega-tipo.entity';
import { CaracteristicaBodegaTipoService } from '../submodulo-empresa/caracteristica-bodega-tipo/caracteristica-bodega-tipo.service';
import { CARACTERISTICA_BODEGA_TIPO_DATOS_PRUEBA } from '../submodulo-empresa/caracteristica-bodega-tipo/constantes/caracteristica-bodega-tipo.datos-prueba';
import { ContactoHorarioServicioService } from '../submodulo-empresa/contacto-horario-servicio/contacto-horario-servicio.service';
import { CONTACTO_HORARIO_SERVICIO_DATOS_PRUEBA } from '../submodulo-empresa/contacto-horario-servicio/constantes/contacto-horario-servicio.datos-prueba';
import { ContactoHorarioServicioEntity } from '../submodulo-empresa/contacto-horario-servicio/contacto-horario-servicio.entity';
import { DepartamentoEmpresaService } from '../submodulo-empresa/departamento-empresa/departamento-empresa.service';
import { DEPARTAMENTO_EMPRESA_DATOS_PRUEBA } from '../submodulo-empresa/departamento-empresa/constantes/departamento-empresa.datos-prueba';
import { DepartamentoEmpresaEntity } from '../submodulo-empresa/departamento-empresa/departamento-empresa.entity';
import { DepartamentoTrabajadorService } from '../submodulo-empresa/departamento-trabajador/departamento-trabajador.service';
import { DepartamentoTrabajadorEntity } from '../submodulo-empresa/departamento-trabajador/departamento-trabajador.entity';
import { DEPARTAMENTO_TRABAJADOR_DATOS_PRUEBA } from '../submodulo-empresa/departamento-trabajador/constantes/departamento-trabajador.datos-prueba';
import { FORMULA_SUFIJO_DATOS_PRUEBA } from '../submodulo-empresa/formula-sufijo/constantes/formula-sufijo.datos-prueba';
import { FormulaPrefijoService } from '../submodulo-empresa/formula-prefijo/formula-prefijo.service';
import { FormulaPrefijoEntity } from '../submodulo-empresa/formula-prefijo/formula-prefijo.entity';
import { FORMULA_PREFIJO_DATOS_PRUEBA } from '../submodulo-empresa/formula-prefijo/constantes/formula-prefijo.datos-prueba';
import { FormulaSufijoService } from '../submodulo-empresa/formula-sufijo/formula-sufijo.service';
import { FormulaSufijoEntity } from '../submodulo-empresa/formula-sufijo/formula-sufijo.entity';
import { FormulaArtService } from '../submodulo-empresa/formula-art/formula-art.service';
import { FORMULA_ART_DATOS_PRUEBA } from '../submodulo-empresa/formula-art/constantes/formula-art.datos-prueba';
import { INGREDIENTE_FORMULA_ART_DATOS_PRUEBA } from '../submodulo-empresa/ingrediente-formula-art/constantes/ingrediente-formula-art.datos-prueba';
import { IngredienteFormulaArtService } from '../submodulo-empresa/ingrediente-formula-art/ingrediente-formula-art.service';
import { IngredienteFormulaArtEntity } from '../submodulo-empresa/ingrediente-formula-art/ingrediente-formula-art.entity';
import { FormulaArtEntity } from '../submodulo-empresa/formula-art/formula-art.entity';
import { DatosPruebaRolesService } from './datos-prueba-roles.service';
import { AREA_TRABAJADOR_DATOS_PRUEBA } from '../submodulo-empresa/area-trabajador/constantes/area-trabajador.datos-prueba';
import { AreaTrabajadorService } from '../submodulo-empresa/area-trabajador/area-trabajador.service';
import { AreaTrabajadorEntity } from '../submodulo-empresa/area-trabajador/area-trabajador.entity';
import { ARTICULO_EMPRESA_DATOS_PRUEBA } from '../submodulo-articulo-empresa/articulo-empresa/constantes/articulo-empresa.datos-prueba';

@Injectable()
export class DatosPruebaEmpresaService {
  constructor(
    private readonly _empresaService: EmpresaService,
    private readonly _direccionService: DireccionService,
    private readonly _edificioService: EdificioService,
    private readonly _establecimientoService: EstablecimientoService,
    private readonly _pisoService: PisoService,
    private readonly _areaPiso: AreaPisoService,
    private readonly _contactoEmpresaService: ContactoEmpresaService,
    private readonly _tipoCargoService: TipoCargoService,
    private readonly _tipoBodegaService: TipoBodegaService,
    private readonly _tipoIndustriaService: TipoIndustriaService,
    private readonly _empresaIndustriaService: EmpresaIndustriaService,
    private readonly _codigoPaisService: CodigoPaisService,
    private readonly _horarioService: HorarioService,
    private readonly _servicioEstablecimientoService: ServicioEstablecimientoService,
    private readonly _horarioServicioService: HorarioServicioService,
    private readonly _contactoHorarioServicioService: ContactoHorarioServicioService,
    private readonly _datosPruebaArticuloService: DatosPruebaArticuloService,
    private readonly _datosContactoService: DatosContactoService,
    private readonly _bodegaService: BodegaService,
    private readonly _bodegaTipoService: BodegaTipoService,
    private readonly _caracteristicaBodega: CaracteristicaBodegaService,
    private readonly _caracteristicaTipoBodegaService: CaracteristicaTipoBodegaService,
    private readonly _caracteristicaBodegaTipoService: CaracteristicaBodegaTipoService,
    private readonly _departamentoEmpresaService: DepartamentoEmpresaService,
    private readonly _departamentoTrabajadorService: DepartamentoTrabajadorService,
    private readonly _formulaPrefijoService: FormulaPrefijoService,
    private readonly _formulaSufijoService: FormulaSufijoService,
    private readonly _formulaArService: FormulaArtService,
    private readonly _ingredienteFormulaArtService: IngredienteFormulaArtService,
    private readonly _datosPruebaRolesService: DatosPruebaRolesService,
    private readonly _areaTrabajadorService: AreaTrabajadorService,
  ) {}

  async crearDatosPruebaSubmoduloEmpresa() {
    if (CONFIG.datosPrueba.tipoIndustria) {
      await this.crearTipoIndustria();
    }
    if (CONFIG.datosPrueba.codigoPais) {
      await this.crearCodigoPais();
    }
    if (CONFIG.datosPrueba.empresa) {
      await this.crearEmpresas();
    }
  }

  async crearContactosEmpresa() {
    DATOS_PRUEBA.contactoEmpresa = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const tipoCargo of DATOS_PRUEBA.tipoCargo) {
        if (tipoCargo.empresa === empresa.id) {
          for (const [
            index,
            contactoEmpresa,
          ] of CONTACTO_EMPRESA_DATOS_PRUEBA.entries()) {
            const datosUsuario = DATOS_PRUEBA.datosUsuario[index];
            const respuesta = ((await this._contactoEmpresaService.crear(
              contactoEmpresa(empresa.id, tipoCargo.id, datosUsuario.id),
            )) as any) as ContactoEmpresaEntity;
            DATOS_PRUEBA.contactoEmpresa.push(respuesta);
          }
        }
      }
    }
    if (CONFIG.datosPrueba.busquedaXContactoEmpresa) {
      await this._datosPruebaArticuloService.crearBusquedasXContactoEmpresa();
    }
    if (CONFIG.datosPrueba.datosContacto) {
      await this.crearDatosContacto();
    }
  }

  async crearTipoIndustria() {
    DATOS_PRUEBA.tipoIndustria = [];
    for (const tipoIndustria of TIPO_INDUSTRIA_DATOS_PRUEBA) {
      const respuesta = ((await this._tipoIndustriaService.crear(
        tipoIndustria(),
      )) as any) as TipoIndustriaEntity;
      DATOS_PRUEBA.tipoIndustria.push(respuesta);
    }
  }

  async crearCodigoPais() {
    DATOS_PRUEBA.codigoPais = [];
    for (const codigoPais of CODIGO_PAIS_DATOS_PRUEBA) {
      const respuesta = ((await this._codigoPaisService.crear(
        codigoPais(),
      )) as any) as CodigoPaisEntity;
      DATOS_PRUEBA.codigoPais.push(respuesta);
    }
  }

  async crearEmpresas() {
    DATOS_PRUEBA.empresa = [];
    for (const datoPrueba of EMPRESA_DATOS_PRUEBA) {
      const respuesta = ((await this._empresaService.crear(
        datoPrueba(),
      )) as any) as EmpresaEntity;
      DATOS_PRUEBA.empresa.push(respuesta);
    }
    if (CONFIG.datosPrueba.horario) {
      await this.crearHorarios();
    }
    if (CONFIG.datosPrueba.tipoCargo) {
      await this.crearTipoCargo();
    }
    if (CONFIG.datosPrueba.tipoBodega) {
      await this.crearTipoBodega();
    }
    if (CONFIG.datosPrueba.edificio) {
      await this.crearEdificiosDatosPrueba();
    }
    if (CONFIG.datosPrueba.caracteristicaBodega) {
      await this.crearCaracteristicasBodega();
    }
    if (CONFIG.datosPrueba.empresaIndustria) {
      await this.crearEmpresaIndustria();
    }
    // if (CONFIG.datosPrueba.articuloEmpresa) {
    //   await this._datosPruebaArticuloEmpresaService.crearArticulosEmpresa();
    // }
    if (CONFIG.datosPrueba.departamentoEmpresa) {
      await this.crearDepartamentoEmpresa();
    }
    if (CONFIG.datosPrueba.formulaPrefijo) {
      await this.crearFormulaPrefijo();
    }
    if (CONFIG.datosPrueba.formulaSufijo) {
      await this.crearFormulaSufijo();
    }
    if (CONFIG.datosPrueba.rol) {
      await this._datosPruebaRolesService.crearRoles();
    }
  }

  async crearEmpresaIndustria() {
    DATOS_PRUEBA.empresaIndustria = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const tipoIndustria of DATOS_PRUEBA.tipoIndustria) {
        for (const empresaIndustria of EMPRESA_INDUSTRIA_DATOS_PRUEBA) {
          const respuesta = ((await this._empresaIndustriaService.crear(
            empresaIndustria(empresa.id, tipoIndustria.id),
          )) as any) as EmpresaIndustriaEntity;
          DATOS_PRUEBA.empresaIndustria.push(respuesta);
        }
      }
    }
  }

  async crearHorarios() {
    DATOS_PRUEBA.horario = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const horario of HORARIO_DATOS_PRUEBA) {
        const respuesta = ((await this._horarioService.crear(
          horario(empresa.id),
        )) as any) as HorarioEntity;
        DATOS_PRUEBA.horario.push(respuesta);
      }
    }
  }

  async crearEdificiosDatosPrueba() {
    DATOS_PRUEBA.edificio = [];
    const seCreanDatosDireccion = CONFIG.datosPrueba.direccion;
    seCreanDatosDireccion ? (DATOS_PRUEBA.direccion = []) : true;
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const edificio of EDIFICIO_DATOS_PRUEBA) {
        const respuestaEdificio = ((await this._edificioService.crear(
          edificio(empresa.id),
        )) as any) as EdificioEntity;
        DATOS_PRUEBA.edificio.push(respuestaEdificio);
      }
    }
    if (CONFIG.datosPrueba.direccion) {
      await this.crearDirecciones();
    }
    if (CONFIG.datosPrueba.establecimiento) {
      await this.crearEstablecimientos();
    }
    if (CONFIG.datosPrueba.piso) {
      await this.crearPisos();
    }
    if (CONFIG.datosPrueba.bodega) {
      if (DATOS_PRUEBA.contactoEmpresa) {
        await this.crearBodegas();
      } else {
        console.error('aun no existen contacto empresa');
      }
    }
  }

  async crearEstablecimientos() {
    DATOS_PRUEBA.establecimiento = [];
    for (const edificio of DATOS_PRUEBA.edificio) {
      for (const establecimiento of ESTABLECIMIENTO_DATOS_PRUEBA) {
        const respuesta = ((await this._establecimientoService.crear(
          establecimiento(edificio.id),
        )) as any) as EstablecimientoEntity;
        DATOS_PRUEBA.establecimiento.push(respuesta);
      }
    }
    // if (CONFIG.datosPrueba.servicioEstablecimiento) {
    //   await this.crearServiciosEstablecimientos();
    // }
  }

  private async crearDirecciones() {
    DATOS_PRUEBA.establecimiento = [];
    for (const edificio of DATOS_PRUEBA.edificio) {
      for (const dirección of DIRECCION_DATOS_PRUEBA) {
        const respuesta = ((await this._direccionService.crear(
          dirección(edificio.id),
        )) as any) as DireccionEntity;
        DATOS_PRUEBA.direccion.push(respuesta);
      }
    }
  }

  private async crearPisos() {
    DATOS_PRUEBA.piso = [];
    for (const edificio of DATOS_PRUEBA.edificio) {
      for (const piso of PISO_DATOS_PRUEBA) {
        const respuesta = ((await this._pisoService.crear(
          piso(edificio.id),
        )) as any) as PisoEntity;
        DATOS_PRUEBA.piso.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.areaPiso) {
      await this.crearAreasPiso();
    }
  }

  private async crearTipoCargo() {
    DATOS_PRUEBA.tipoCargo = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const tipoCargo of TIPO_CARGO_DATOS_PRUEBA) {
        const respuesta = ((await this._tipoCargoService.crear(
          tipoCargo(empresa.id),
        )) as any) as TipoCargoEntity;
        DATOS_PRUEBA.tipoCargo.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.contactoEmpresa) {
      await this.crearContactosEmpresa();
    }
  }

  private async crearDepartamentoTrabajador() {
    DATOS_PRUEBA.departamentoTrabajador = [];

    for (const departamentoEmpresa of DATOS_PRUEBA.departamentoEmpresa) {
      for (const contactoEmpresa of DATOS_PRUEBA.contactoEmpresa) {
        if (departamentoEmpresa.empresa === contactoEmpresa.empresa) {
          for (const departamentoTrabajador of DEPARTAMENTO_TRABAJADOR_DATOS_PRUEBA) {
            const respuesta = ((await this._departamentoTrabajadorService.crear(
              departamentoTrabajador(
                contactoEmpresa.id,
                departamentoEmpresa.id,
              ),
            )) as any) as DepartamentoTrabajadorEntity;
            DATOS_PRUEBA.departamentoTrabajador.push(respuesta);
          }
        }
      }
    }
  }

  private async crearDepartamentoEmpresa() {
    DATOS_PRUEBA.departamentoEmpresa = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const departamentoEmpresa of DEPARTAMENTO_EMPRESA_DATOS_PRUEBA) {
        const respuesta = ((await this._departamentoEmpresaService.crear(
          departamentoEmpresa(empresa.id),
        )) as any) as DepartamentoEmpresaEntity;
        DATOS_PRUEBA.departamentoEmpresa.push(respuesta);
      }
    }

    if (CONFIG.datosPrueba.departamentoTrabajador) {
      await this.crearDepartamentoTrabajador();
    }
  }

  private async crearAreasPiso() {
    DATOS_PRUEBA.areaPiso = [];
    for (const piso of DATOS_PRUEBA.piso) {
      for (const areaPiso of AREA_PISO_DATOS_PRUEBA) {
        const respuesta = ((await this._areaPiso.crear(
          areaPiso(piso.id),
        )) as any) as AreaPisoEntity;
        DATOS_PRUEBA.areaPiso.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.areaTrabajador) {
      await this.crearAreaTrabajador();
    }
  }

  private async crearAreaTrabajador() {
    DATOS_PRUEBA.areaTrabajador = [];
    for (const areaPiso of DATOS_PRUEBA.areaPiso) {
      for (const contactoEmpresa of DATOS_PRUEBA.contactoEmpresa) {
        for (const areaTrabajador of AREA_TRABAJADOR_DATOS_PRUEBA) {
          const respuesta = ((await this._areaTrabajadorService.crear(
            areaTrabajador(areaPiso.id, contactoEmpresa.id),
          )) as any) as AreaTrabajadorEntity;
          DATOS_PRUEBA.areaTrabajador.push(respuesta);
        }
      }
    }
  }

  private async crearTipoBodega() {
    DATOS_PRUEBA.tipoBodega = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const tipoBodega of TIPO_BODEGA_DATOS_PRUEBA) {
        const respuesta = ((await this._tipoBodegaService.crear(
          tipoBodega(empresa.id),
        )) as any) as TipoBodegaEntity;
        DATOS_PRUEBA.tipoBodega.push(respuesta);
      }
    }
  }

  async crearServiciosEstablecimientos() {
    DATOS_PRUEBA.servicioEstablecimiento = [];
    for (const establecimiento of DATOS_PRUEBA.establecimiento) {
      for (const articuloEmpresa of DATOS_PRUEBA.articuloEmpresa) {
        for (const servicioEstablecimiento of SERVICIO_ESTABLECIMIENTO_DATOS_PRUEBA) {
          const respuesta = ((await this._servicioEstablecimientoService.crear(
            servicioEstablecimiento(establecimiento.id, articuloEmpresa.id),
          )) as any) as ServicioEstablecimientoEntity;
          DATOS_PRUEBA.servicioEstablecimiento.push(respuesta);
        }
      }
    }
    if (CONFIG.datosPrueba.horarioServicio) {
      await this.crearHorarioServicios();
    }
  }

  async crearHorarioServicios() {
    DATOS_PRUEBA.horarioServicio = [];
    for (const horario of DATOS_PRUEBA.horario) {
      for (const servicioEstablecimiento of DATOS_PRUEBA.servicioEstablecimiento) {
        for (const horarioServicio of HORARIO_SERVICIO_DATOS_PRUEBA) {
          const respuesta = ((await this._horarioServicioService.crear(
            horarioServicio(horario.id, servicioEstablecimiento.id),
          )) as any) as HorarioServicioEntity;
          DATOS_PRUEBA.horarioServicio.push(respuesta);
        }
      }
    }
    if (CONFIG.datosPrueba.contactoHorarioServicio) {
      await this.crearContactoHorarioServio();
    }
  }

  async crearDatosContacto() {
    DATOS_PRUEBA.datosContacto = [];
    for (const contactoEmpresa of DATOS_PRUEBA.contactoEmpresa) {
      for (const [
        index,
        datosContacto,
      ] of DATOS_CONTACTO_DATOS_PRUEBA.entries()) {
        if (contactoEmpresa.datosUsuario === index + 1) {
          const respuesta = ((await this._datosContactoService.crear(
            datosContacto(contactoEmpresa.id),
          )) as any) as DatosContactoEntity;
          DATOS_PRUEBA.datosContacto.push(respuesta);
        }
      }
    }
  }

  async crearBodegas() {
    DATOS_PRUEBA.bodega = [];
    for (const [index, bodega] of BODEGA_DATOS_PRUEBA.entries()) {
      const idContactoEmpresa = obtenerEnteroRandom(
        1,
        DATOS_PRUEBA.contactoEmpresa.length,
      );
      const respuesta = ((await this._bodegaService.crear(
        bodega(DATOS_PRUEBA.edificio[index].id, idContactoEmpresa),
      )) as any) as BodegaEntity;
      DATOS_PRUEBA.bodega.push(respuesta);
    }
    if (CONFIG.datosPrueba.bodegaTipo) {
      await this.crearBodegasTipo();
    }
  }

  private async crearBodegasTipo() {
    DATOS_PRUEBA.bodegaTipo = [];
    for (const [indice, bodega] of DATOS_PRUEBA.bodega.entries()) {
      const respuesta = ((await this._bodegaTipoService.crear(
        BODEGA_TIPO_DATOS_PRUEBA[0](
          bodega.id,
          DATOS_PRUEBA.tipoBodega[indice].id,
        ),
      )) as any) as BodegaTipoEntity;
      DATOS_PRUEBA.bodegaTipo.push(respuesta);
    }
  }

  private async crearCaracteristicasBodega() {
    DATOS_PRUEBA.caracteristicaBodega = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const caracteristicaBodega of CARACTERISTICA_BODEGA_DATOS_PRUEBA) {
        const respuesta = ((await this._caracteristicaBodega.crear(
          caracteristicaBodega(empresa.id),
        )) as any) as CaracteristicaBodegaEntity;
        DATOS_PRUEBA.caracteristicaBodega.push(respuesta);
      }
    }
    if (CONFIG.datosPrueba.caracteristicaTipoBodega) {
      await this.crearCaracteristicaTipoBodega();
    }
  }

  private async crearCaracteristicaTipoBodega() {
    DATOS_PRUEBA.caracteristicaTipoBodega = [];
    for (const [
      indice,
      caracteristicaBodega,
    ] of DATOS_PRUEBA.caracteristicaBodega.entries()) {
      const respuesta = ((await this._caracteristicaTipoBodegaService.crear(
        CARACTERISTICA_TIPO_BODEGA_DATOS_PRUEBA[0](
          caracteristicaBodega.id,
          DATOS_PRUEBA.tipoBodega[indice].id,
        ),
      )) as any) as CaracteristicaTipoBodegaEntity;
      DATOS_PRUEBA.caracteristicaTipoBodega.push(respuesta);
    }
    if (CONFIG.datosPrueba.caracteristicaBodegaTipo) {
      await this.crearCaracteristicaBodegaTipo();
    }
  }

  private async crearCaracteristicaBodegaTipo() {
    DATOS_PRUEBA.caracteristicaBodegaTipo = [];
    for (const [
      indice,
      caracteristicaTipoBodega,
    ] of DATOS_PRUEBA.caracteristicaTipoBodega.entries()) {
      const respuesta = ((await this._caracteristicaBodegaTipoService.crear(
        CARACTERISTICA_BODEGA_TIPO_DATOS_PRUEBA[0](
          caracteristicaTipoBodega.id,
          DATOS_PRUEBA.bodegaTipo[indice].id,
        ),
      )) as any) as CaracteristicaBodegaTipoEntity;
      DATOS_PRUEBA.caracteristicaBodegaTipo.push(respuesta);
    }
  }

  private async crearContactoHorarioServio() {
    DATOS_PRUEBA.contactoHorarioServicio = [];
    for (const horarioServicio of DATOS_PRUEBA.horarioServicio) {
      for (const contactoEmpresa of DATOS_PRUEBA.contactoEmpresa) {
        const respuesta = ((await this._contactoHorarioServicioService.crear(
          CONTACTO_HORARIO_SERVICIO_DATOS_PRUEBA[0](
            horarioServicio.id,
            contactoEmpresa.id,
          ),
        )) as any) as ContactoHorarioServicioEntity;
        DATOS_PRUEBA.contactoHorarioServicio.push(respuesta);
      }
    }
  }

  private async crearFormulaPrefijo() {
    DATOS_PRUEBA.formulaPrefijo = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const formulaPrefijo of FORMULA_PREFIJO_DATOS_PRUEBA) {
        const respuesta = ((await this._formulaPrefijoService.crear(
          formulaPrefijo(empresa.id),
        )) as any) as FormulaPrefijoEntity;
        DATOS_PRUEBA.formulaPrefijo.push(respuesta);
      }
    }
  }

  private async crearFormulaSufijo() {
    DATOS_PRUEBA.formulaSufijo = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const formulaSufijo of FORMULA_SUFIJO_DATOS_PRUEBA) {
        const respuesta = ((await this._formulaSufijoService.crear(
          formulaSufijo(empresa.id),
        )) as any) as FormulaSufijoEntity;
        DATOS_PRUEBA.formulaSufijo.push(respuesta);
      }
    }

    if (CONFIG.datosPrueba.formulaArt) {
      await this.crearFomulaArt();
    }
  }

  private async crearFomulaArt() {
    DATOS_PRUEBA.formulaArt = [];
    for (const empresa of DATOS_PRUEBA.empresa) {
      for (const formulaSufijo of DATOS_PRUEBA.formulaSufijo) {
        for (const formulaPrefijo of DATOS_PRUEBA.formulaPrefijo) {
          if (
            formulaPrefijo.empresa === empresa.id &&
            formulaSufijo.empresa === empresa.id
          ) {
            for (const formulaArt of FORMULA_ART_DATOS_PRUEBA) {
              const respuesta = ((await this._formulaArService.crear(
                formulaArt(formulaPrefijo.id, formulaSufijo.id, empresa.id),
              )) as any) as FormulaArtEntity;
              DATOS_PRUEBA.formulaArt.push(respuesta);
            }
          }
        }
      }
    }
    if (CONFIG.datosPrueba.ingredienteFormulaArt) {
      await this.crearIngredienteFormulaArt();
    }
  }

  private async crearIngredienteFormulaArt() {
    DATOS_PRUEBA.ingredienteFormulaArt = [];
    for (const formulaArt of DATOS_PRUEBA.formulaArt) {
      for (const ingredienteFormulaArt of INGREDIENTE_FORMULA_ART_DATOS_PRUEBA) {
        const respuesta = ((await this._ingredienteFormulaArtService.crear(
          ingredienteFormulaArt(formulaArt.id),
        )) as any) as IngredienteFormulaArtEntity;
        DATOS_PRUEBA.ingredienteFormulaArt.push(respuesta);
      }
    }
  }
}
