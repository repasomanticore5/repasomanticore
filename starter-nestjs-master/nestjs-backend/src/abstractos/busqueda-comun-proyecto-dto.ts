import { BusquedaComunDto } from '@manticore-labs/nest-2021';
import { IsIn, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { ActivoInactivo } from '../enums/activo-inactivo';

export abstract class BusquedaComunProyectoDto extends BusquedaComunDto {
  @IsOptional()
  @IsIn([ActivoInactivo.Activo, ActivoInactivo.Inactivo])
  @Expose()
  sisHabilitado?: ActivoInactivo;
}
