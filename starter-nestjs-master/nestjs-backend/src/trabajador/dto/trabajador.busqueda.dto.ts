import {IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import { BusquedaComunProyectoDto } from 'src/abstractos/busqueda-comun-proyecto-dto';

export class TrabajadorBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
