import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import { HabilitadoDtoComun } from 'src/abstractos/habilitado-dto-comun';

export class TrabajadorCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;

       @IsNotEmpty()
    @IsString()
    @MinLength(5)
    @MaxLength(50)
    @Expose()
    nombre: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(5)
    @MaxLength(50)
    @Expose()
    apellido: string;
}
