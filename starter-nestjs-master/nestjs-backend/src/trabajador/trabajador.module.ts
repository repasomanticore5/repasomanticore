import { Module } from '@nestjs/common';
import { TrabajadorController } from './trabajador.controller';
import { TRABAJADOR_IMPORTS } from './constantes/trabajador.imports';
import { TRABAJADOR_PROVIDERS } from './constantes/trabajador.providers';

@Module({
  imports: [...TRABAJADOR_IMPORTS],
  providers: [...TRABAJADOR_PROVIDERS],
  exports: [...TRABAJADOR_PROVIDERS],
  controllers: [TrabajadorController],
})
export class TrabajadorModule {}
