import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditoriaModule } from 'src/auditoria/auditoria.module';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';
import { SeguridadModule } from 'src/seguridad/seguridad.module';
import { TrabajadorEntity } from '../trabajador.entity';

export const TRABAJADOR_IMPORTS = [
  TypeOrmModule.forFeature([TrabajadorEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
