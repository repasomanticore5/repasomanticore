import { ActivoInactivo } from 'src/enums/activo-inactivo';
import { TrabajadorCrearDto } from '../dto/trabajador.crear.dto';

export const TRABAJADOR_DATOS_PRUEBA: (() => TrabajadorCrearDto)[] = [
  () => {
    const dato = new TrabajadorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TrabajadorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TrabajadorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
