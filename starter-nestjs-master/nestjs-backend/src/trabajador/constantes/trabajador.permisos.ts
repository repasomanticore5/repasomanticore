import { ObjetoSeguridadPermisos } from "@manticore-labs/nest-2021";

export const PERMISOS_TRABAJADOR: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'trabajador';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/trabajador/, // /trabajador
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/trabajador\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /trabajador?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/trabajador\/\d+\/modificar-habilitado/, // /trabajador/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/trabajador\/\d+/, // /trabajador/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
