import { EntidadComunProyecto } from 'src/abstractos/entidad-comun-proyecto';
import { PREFIJO_BASE } from 'src/constantes/prefijo-base';
import { TareaEntity } from 'src/tarea/tarea.entity';
import {
    Column,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TRABAJADOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class TrabajadorEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_TRABAJADOR',
        unsigned: true,
    })
    id: number;

    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

     @Column({
         name: 'nombre',
         type: 'varchar',
         length: '60',
         nullable: false,
     })
     nombre: string;

     @Column({
        name: 'apellido',
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    apellido: string;

  

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];

    //Padre

    @OneToMany(() => TareaEntity, 
    (r) => r.trabajador)
    tareas: TareaEntity[];

}
