import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {ArticuloEmpresaEntity} from '../articulo-empresa/articulo-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'PRECIO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'valor', 'esPrincipal'])
export class PrecioEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_PRECIO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'valor',
        type: 'decimal',
        nullable: false,
        scale: 4,
        precision: 10,
    })
    valor: number;

    @Column({
        name: 'es_principal',
        type: 'tinyint',
        nullable: false,
        default: 0,
    })
    esPrincipal?: 0 | 1 = 0;

    @ManyToOne(() => ArticuloEmpresaEntity,
        (articuloEmpresa) => articuloEmpresa.preciosAE,
        {nullable: false})
    articuloEmpresaP: ArticuloEmpresaEntity;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
