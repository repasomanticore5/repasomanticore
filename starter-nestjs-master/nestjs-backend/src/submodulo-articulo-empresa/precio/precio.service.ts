import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { PrecioEntity } from './precio.entity';
import { PrecioBusquedaDto } from './dto/precio.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';
import { AuditoriaService } from '../../auditoria/auditoria.service';

@Injectable()
export class PrecioService extends ServicioComun<
  PrecioEntity,
  PrecioBusquedaDto
> {
  constructor(
    @InjectRepository(PrecioEntity, NOMBRE_CADENA_CONEXION)
    public precioEntityRepository: Repository<PrecioEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      precioEntityRepository,
      _connection,
      PrecioEntity,
      'id',
      _auditoriaService,
      // Por defecto NO se transforma todos los campos a mayúsculas.
      // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
      // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
      (objetoATransformar, metodo) => objetoATransformar,
    );
  }
}
