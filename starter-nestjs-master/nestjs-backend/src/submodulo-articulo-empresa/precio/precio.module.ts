import { Module } from '@nestjs/common';
import { PrecioController } from './precio.controller';
import { PRECIO_IMPORTS } from './constantes/precio.imports';
import { PRECIO_PROVIDERS } from './constantes/precio.providers';

@Module({
  imports: [...PRECIO_IMPORTS],
  providers: [...PRECIO_PROVIDERS],
  exports: [...PRECIO_PROVIDERS],
  controllers: [PrecioController],
})
export class PrecioModule {}
