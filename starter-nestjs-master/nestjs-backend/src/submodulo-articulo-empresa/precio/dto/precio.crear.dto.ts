import {IsIn, IsInt, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';
import {ArticuloEmpresaEntity} from "../../articulo-empresa/articulo-empresa.entity";

export class PrecioCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsNotEmpty()
  @IsNumberString()
  @Expose()
  valor: string;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  esPrincipal: 0 | 1;

  @IsOptional()
  @IsInt()
  articuloEmpresaP: number;

}
