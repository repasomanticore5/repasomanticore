import { IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class PrecioBusquedaDto extends BusquedaComunProyectoDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  @IsOptional()
  @Expose()
  esPrincipal: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  articuloEmpresaP: string;
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
