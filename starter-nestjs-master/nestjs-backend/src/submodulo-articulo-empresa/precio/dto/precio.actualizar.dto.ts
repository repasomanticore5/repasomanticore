import {IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString} from 'class-validator';
import { Expose } from 'class-transformer';

export class PrecioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  valor?: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  esPrincipal?: 0 | 1;
}
