import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_PRECIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'precio';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/precio/, // /precio
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/precio\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /precio?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/precio\/\d+\/modificar-habilitado/, // /precio/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/precio\/\d+/, // /precio/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
