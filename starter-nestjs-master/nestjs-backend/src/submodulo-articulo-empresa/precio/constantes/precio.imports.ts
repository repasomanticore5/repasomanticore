import {TypeOrmModule} from '@nestjs/typeorm';
import {PrecioEntity} from '../precio.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const PRECIO_IMPORTS = [
    TypeOrmModule.forFeature([PrecioEntity], NOMBRE_CADENA_CONEXION),
    SeguridadModule,
    AuditoriaModule,
];
