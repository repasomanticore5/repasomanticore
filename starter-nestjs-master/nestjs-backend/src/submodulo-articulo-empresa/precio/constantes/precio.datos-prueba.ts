import { PrecioCrearDto } from '../dto/precio.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const PRECIO_DATOS_PRUEBA: ((
  idArticuloEmpresa: number,
) => PrecioCrearDto)[] = [
  (idArticuloEmpresa: number) => {
    const dato = new PrecioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.valor = '10.00';
    dato.esPrincipal = 1;
    dato.articuloEmpresaP = idArticuloEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
