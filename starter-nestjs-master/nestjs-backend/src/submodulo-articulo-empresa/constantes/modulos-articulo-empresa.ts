import { ArticuloEmpresaModule } from '../articulo-empresa/articulo-empresa.module';
import { PrecioModule } from '../precio/precio.module';
import { CostoCompraModule } from '../costo-compra/costo-compra.module';

export const MODULOS_ARTICULO_EMPRESA = [
  ArticuloEmpresaModule,
  PrecioModule,
  CostoCompraModule,
];
