import { ArticuloEmpresaEntity } from '../articulo-empresa/articulo-empresa.entity';
import { PrecioEntity } from '../precio/precio.entity';
import { CostoCompraEntity } from '../costo-compra/costo-compra.entity';

export interface DatosPruebaArticuloEmpresaInterface {
  articuloEmpresa: ArticuloEmpresaEntity[];
  precio: PrecioEntity[];
  costoCompra: CostoCompraEntity[];
}
