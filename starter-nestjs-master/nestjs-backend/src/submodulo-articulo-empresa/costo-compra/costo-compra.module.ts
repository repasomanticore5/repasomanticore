import { Module } from '@nestjs/common';
import { CostoCompraController } from './costo-compra.controller';
import { COSTO_COMPRA_IMPORTS } from './constantes/costo-compra.imports';
import { COSTO_COMPRA_PROVIDERS } from './constantes/costo-compra.providers';

@Module({
  imports: [...COSTO_COMPRA_IMPORTS],
  providers: [...COSTO_COMPRA_PROVIDERS],
  exports: [...COSTO_COMPRA_PROVIDERS],
  controllers: [CostoCompraController],
})
export class CostoCompraModule {}
