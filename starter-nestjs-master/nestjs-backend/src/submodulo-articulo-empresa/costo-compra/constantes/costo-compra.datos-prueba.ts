import { CostoCompraCrearDto } from '../dto/costo-compra.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const COSTO_COMPRA_DATOS_PRUEBA: ((
  idArticuloEmpresa: number,
) => CostoCompraCrearDto)[] = [
  (idArticuloEmpresa: number) => {
    const dato = new CostoCompraCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.articuloEmpresaCC = idArticuloEmpresa;
    dato.costoFinal = 12;
    dato.costoInicial = 10;
    dato.cantidadCompraFinal = 10;
    dato.cantidadCompraInicial = 10;
    return dato;
  },
];
