import {ObjetoSeguridadPermisos} from "@manticore-labs/nest-2021";

export const PERMISOS_COSTO_COMPRA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'costo-compra';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/costo-compra/, // /costo-compra
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/costo-compra\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /costo-compra?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/costo-compra\/\d+\/modificar-habilitado/, // /costo-compra/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/costo-compra\/\d+/, // /costo-compra/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
