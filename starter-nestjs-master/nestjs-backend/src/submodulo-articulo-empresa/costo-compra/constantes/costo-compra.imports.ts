import { TypeOrmModule } from '@nestjs/typeorm';
import { CostoCompraEntity } from '../costo-compra.entity';
import {NOMBRE_CADENA_CONEXION} from "../../../constantes/nombre-cadena-conexion";
import {SeguridadModule} from "../../../seguridad/seguridad.module";
import {AuditoriaModule} from "../../../auditoria/auditoria.module";

export const COSTO_COMPRA_IMPORTS = [
  TypeOrmModule.forFeature([CostoCompraEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
