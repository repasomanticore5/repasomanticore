import {IsInt, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from "../../../abstractos/habilitado-dto-comun";

export class CostoCompraCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;


    @IsNotEmpty()
    @IsNumber()
    @Expose()
    costoInicial: number;

    @IsNotEmpty()
    @IsNumber()
    @Expose()
    costoFinal: number;

    @IsNotEmpty()
    @IsNumber()
    @Expose()
    cantidadCompraInicial: number;

    @IsNotEmpty()
    @IsNumber()
    @Expose()
    cantidadCompraFinal: number;

    @IsOptional()
    @IsInt()
    articuloEmpresaCC: number;

}


