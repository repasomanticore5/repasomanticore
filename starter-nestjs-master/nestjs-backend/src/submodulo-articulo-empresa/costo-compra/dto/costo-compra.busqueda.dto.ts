import { IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class CostoCompraBusquedaDto extends BusquedaComunProyectoDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  articuloEmpresaCC: string;
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
