import {IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class CostoCompraActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR


    @IsOptional()
    @IsNumber()
    @Expose()
    costoInicial: number;

    @IsOptional()
    @IsNumber()
    @Expose()
    costoFinal: number;

    @IsOptional()
    @IsNumber()
    @Expose()
    cantidadCompraInicial: number;

    @IsOptional()
    @IsNumber()
    @Expose()
    cantidadCompraFinal: number;
}
