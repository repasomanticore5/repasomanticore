import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {ArticuloEmpresaEntity} from "../articulo-empresa/articulo-empresa.entity";

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'COSTO_COMPRA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class CostoCompraEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_COSTO_COMPRA',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'costo_inicial',
        type: 'decimal',
        scale: 4,
        precision: 10,
        nullable: false,
    })
    costoInicial: number;


    @Column({
        name: 'costo_final',
        type: 'decimal',
        scale: 4,
        precision: 10,
        nullable: false,
    })
    costoFinal: number;

    @Column({
        name: 'cantidad_compra_inicial',
        type: 'int',
        nullable: false,
    })
    cantidadCompraInicial: number;

    @Column({
        name: 'cantidad_compra_final',
        type: 'int',
        nullable: false,
    })
    cantidadCompraFinal: number;


    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

    @ManyToOne(() => ArticuloEmpresaEntity,
        (articuloEmpresa) => articuloEmpresa.costosComprasAE,
        {nullable: false})
    articuloEmpresaCC: ArticuloEmpresaEntity;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
