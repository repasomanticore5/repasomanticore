import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class ArticuloEmpresaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  esInsumo?: 0 | 1;

  @IsOptional()
  @IsNumber()
  @Expose()
  esCompra?: 0 | 1;

  @IsOptional()
  @IsNumber()
  @Expose()
  esVenta?: 0 | 1;
}
