import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';
import {ArticuloEmpresaCrearDto} from "./articulo-empresa.crear.dto";
import {ActivoInactivo} from "../../../enums/activo-inactivo";

export class ArticuloEmpresaHabilitadoDto extends HabilitadoDtoComun {

}
