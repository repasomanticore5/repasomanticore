import { IsInt, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class ArticuloEmpresaCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  esInsumo?: 0 | 1;

  @IsOptional()
  @IsNumber()
  @Expose()
  esCompra?: 0 | 1;

  @IsOptional()
  @IsNumber()
  @Expose()
  esVenta?: 0 | 1;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  articuloAE: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  empresaAE: number;
}
