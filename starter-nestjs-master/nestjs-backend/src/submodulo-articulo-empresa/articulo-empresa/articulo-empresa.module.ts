import { Module } from '@nestjs/common';
import { ArticuloEmpresaController } from './articulo-empresa.controller';
import { ARTICULO_EMPRESA_IMPORTS } from './constantes/articulo-empresa.imports';
import { ARTICULO_EMPRESA_PROVIDERS } from './constantes/articulo-empresa.providers';

@Module({
  imports: [...ARTICULO_EMPRESA_IMPORTS],
  providers: [...ARTICULO_EMPRESA_PROVIDERS],
  exports: [...ARTICULO_EMPRESA_PROVIDERS],
  controllers: [ArticuloEmpresaController],
})
export class ArticuloEmpresaModule {}
