import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_ARTICULO_EMPRESA: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'articulo-empresa';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/articulo-empresa/, // /articulo-empresa
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/articulo-empresa\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /articulo-empresa?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/articulo-empresa\/\d+\/modificar-habilitado/, // /articulo-empresa/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/articulo-empresa\/\d+/, // /articulo-empresa/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
