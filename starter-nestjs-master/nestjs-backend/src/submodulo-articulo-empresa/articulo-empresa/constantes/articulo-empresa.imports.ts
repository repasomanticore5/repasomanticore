import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticuloEmpresaEntity } from '../articulo-empresa.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { SeguridadModule } from '../../../seguridad/seguridad.module';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';

export const ARTICULO_EMPRESA_IMPORTS = [
  TypeOrmModule.forFeature([ArticuloEmpresaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
