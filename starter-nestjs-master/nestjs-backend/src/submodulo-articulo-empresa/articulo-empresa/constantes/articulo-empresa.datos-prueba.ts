import { ArticuloEmpresaCrearDto } from '../dto/articulo-empresa.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ARTICULO_EMPRESA_DATOS_PRUEBA: ((
  idEmpresa: number,
  idArticulo: number,
) => ArticuloEmpresaCrearDto)[] = [
  (idEmpresa: number, idArticulo: number) => {
    const dato = new ArticuloEmpresaCrearDto();
    dato.esInsumo = 1;
    dato.articuloAE = idArticulo;
    dato.empresaAE = idEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
