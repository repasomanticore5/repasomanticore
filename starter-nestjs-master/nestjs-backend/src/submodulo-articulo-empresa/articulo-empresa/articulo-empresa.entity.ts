import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ArticuloEntity } from '../../submodulo-articulo/articulo/articulo.entity';
import { CostoCompraEntity } from '../costo-compra/costo-compra.entity';
import { PrecioEntity } from '../precio/precio.entity';
import { EmpresaEntity } from '../../submodulo-empresa/empresa/empresa.entity';
import { ServicioEstablecimientoEntity } from '../../submodulo-empresa/servicio-establecimiento/servicio-establecimiento.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ARTICULO_EMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'articuloAE'])
export class ArticuloEmpresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_ARTICULO_EMPRESA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'es_insumo',
    type: 'tinyint',
    nullable: true,
    default: 0,
  })
  esInsumo?: 0 | 1 = 0;

  @Column({
    name: 'es_compra',
    type: 'tinyint',
    nullable: true,
    default: 0,
  })
  esCompra?: 0 | 1 = 0;

  @Column({
    name: 'es_venta',
    type: 'tinyint',
    nullable: true,
    default: 0,
  })
  esVenta?: 0 | 1 = 0;

  @ManyToOne(() => ArticuloEntity, (articulo) => articulo.articulosEmpresaA, {
    nullable: false,
  })
  articuloAE: ArticuloEntity;

  @ManyToOne(() => EmpresaEntity, (empresa) => empresa.articulosEmpresaE, {
    nullable: false,
  })
  empresaAE: EmpresaEntity;

  @OneToMany(
    () => CostoCompraEntity,
    (costoCompra) => costoCompra.articuloEmpresaCC,
  )
  costosComprasAE: CostoCompraEntity[];

  @OneToMany(() => PrecioEntity, (precio) => precio.articuloEmpresaP)
  preciosAE: PrecioEntity[];

  @OneToMany(() => ServicioEstablecimientoEntity, (r) => r.articuloEmpresa)
  serviciosEstablecimiento: ServicioEstablecimientoEntity[];
}
