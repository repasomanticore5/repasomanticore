import {EntrenadorEntity} from '../../modulo-prueba/entrenador/entrenador.entity';
import {PokemonEntity} from '../../modulo-prueba/pokemon/pokemon.entity';
import {ENTITIES_EMPRESA} from '../../submodulo-empresa/constantes/entities-empresa';
import {ENTITIES_ARTICULO} from '../../submodulo-articulo/constantes/entities-articulo';
import {ENTITIES_ARTICULO_EMPRESA} from '../../submodulo-articulo-empresa/constantes/entities-articulo-empresa';
import {ENTITIES_ROLES} from '../../submodulo-roles/constantes/entities-roles';
import {ENTITIES_FACTURACION_ELECTRONICA} from '../../submodulo-facturacion-electronica/constantes/facturacion-electronica-empresa';
import { TareaEntity } from 'src/tarea/tarea.entity';
import { TrabajadorEntity } from 'src/trabajador/trabajador.entity';

export const ENTITIES_BASE_RELACIONAL = [
    EntrenadorEntity,
    PokemonEntity,
    TareaEntity,
    TrabajadorEntity,
   
    ...ENTITIES_EMPRESA,
    ...ENTITIES_ARTICULO,
    ...ENTITIES_ARTICULO_EMPRESA,
    ...ENTITIES_ROLES,
    ...ENTITIES_FACTURACION_ELECTRONICA,
];
