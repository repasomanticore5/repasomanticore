import {AppService} from '../app.service';
import {SeguridadGuard} from '../guards/seguridad/seguridad.guard';
import {SERVICIOS_DATOS_PRUEBA} from './servicios-datos-prueba';

export const PROVIDERS = [
    AppService,
    SeguridadGuard,
    ...SERVICIOS_DATOS_PRUEBA
];
