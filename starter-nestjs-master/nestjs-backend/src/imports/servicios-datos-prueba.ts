import { DatosPruebaEmpresaService } from '../servicios-datos-prueba/datos-prueba-empresa.service';
import { DatosPruebaArticuloService } from '../servicios-datos-prueba/datos-prueba-articulo.service';
import { DatosPruebaArticuloEmpresaService } from '../servicios-datos-prueba/datos-prueba-articulo-empresa.service';
import { DatosPruebaRolesService } from '../servicios-datos-prueba/datos-prueba-roles.service';

export const SERVICIOS_DATOS_PRUEBA = [
  DatosPruebaEmpresaService,
  DatosPruebaArticuloService,
  DatosPruebaArticuloEmpresaService,
  DatosPruebaRolesService,
];
