import {CONEXION_BASE_RELACIONAL} from '../base-datos/base-relacional/conexion-base-relacional';
import {CONEXION_BASE_AUDITORIA} from '../base-datos/base-auditoria/conexion-base-auditoria';
import {CONEXION_BASE_SEGURIDAD} from '../base-datos/base-seguridad/conexion-base-seguridad';
import {CONEXION_BASE_NO_RELACIONAL} from '../base-datos/base-no-relacional/conexion-base-no-relacional';
import {EntrenadorModule} from '../modulo-prueba/entrenador/entrenador.module';
import {PokemonModule} from '../modulo-prueba/pokemon/pokemon.module';
import {FrontendModule} from '../modulo-frontend/frontend.module';
import {ServeStaticModule} from '@nestjs/serve-static';
import {join} from 'path';
import {MODULOS_EMPRESA} from '../submodulo-empresa/constantes/modulos-empresa';
import {MODULOS_ARTICULO} from '../submodulo-articulo/constantes/modulos-articulo';
import {MODULOS_ARTICULO_EMPRESA} from '../submodulo-articulo-empresa/constantes/modulos-articulo-empresa';
import {MODULOS_ROLES} from '../submodulo-roles/constantes/modulos-roles';
import {MODULOS_FACTURACION_ELECTRONICA} from '../submodulo-facturacion-electronica/constantes/modulos-facturacion-electronica';
import { TareaModule } from 'src/tarea/tarea.module';
import { TrabajadorModule } from 'src/trabajador/trabajador.module';

export const IMPORTS = [
    // FirestoreModule, // Si se desea trabajar con el modulo de firebase des-comentar esto
    CONEXION_BASE_RELACIONAL,
    CONEXION_BASE_AUDITORIA,
    CONEXION_BASE_SEGURIDAD,
    CONEXION_BASE_NO_RELACIONAL,
    // ServeStaticModule.forRoot({
    //     rootPath: join(__dirname, '..', 'publico'),
    // }),
    EntrenadorModule,
    PokemonModule,
    FrontendModule,
    TareaModule,
    TrabajadorModule,
    ...MODULOS_EMPRESA,
    ...MODULOS_ARTICULO,
    ...MODULOS_ARTICULO_EMPRESA,
    ...MODULOS_ROLES,
    ...MODULOS_FACTURACION_ELECTRONICA,
];
