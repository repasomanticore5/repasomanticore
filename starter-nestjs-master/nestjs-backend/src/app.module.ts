import { Module } from '@nestjs/common';
import { CONTROLLERS } from './imports/controllers';
import { PROVIDERS } from './imports/providers';
import { IMPORTS } from './imports/imports';
import { DatosPrueba } from './interfaces/datos-prueba';
import { EntrenadorService } from './modulo-prueba/entrenador/entrenador.service';
import { CONFIG } from './environment/config';
import { ENTRENADOR_DATOS_PRUEBA } from './modulo-prueba/entrenador/constantes/entrenador.datos-prueba';
import { EntrenadorEntity } from './modulo-prueba/entrenador/entrenador.entity';
import { POKEMON_DATOS_PRUEBA } from './modulo-prueba/pokemon/constantes/pokemon.datos-prueba';
import { PokemonService } from './modulo-prueba/pokemon/pokemon.service';
import { PokemonEntity } from './modulo-prueba/pokemon/pokemon.entity';
import { DatosPruebaEmpresaService } from './servicios-datos-prueba/datos-prueba-empresa.service';
import { DATOS_PRUEBA } from './servicios-datos-prueba/datos-prueba';
import { DatosPruebaArticuloService } from './servicios-datos-prueba/datos-prueba-articulo.service';
import { DatosPruebaRolesService } from './servicios-datos-prueba/datos-prueba-roles.service';
import { DatosPruebaArticuloEmpresaService } from './servicios-datos-prueba/datos-prueba-articulo-empresa.service';

@Module({
  imports: [...IMPORTS],
  controllers: [...CONTROLLERS],
  providers: [...PROVIDERS],
})
export class AppModule {
  datosPrueba: DatosPrueba = DATOS_PRUEBA;

  constructor(
    private readonly _entrenadorService: EntrenadorService,
    private readonly _pokemonService: PokemonService,
    private readonly _datosPruebaArticuloService: DatosPruebaArticuloService,
    private readonly _datosPruebaEmpresaService: DatosPruebaEmpresaService,
    private readonly _datosPruebaRolesService: DatosPruebaRolesService,
    private readonly _datosPruebaArticuloEmpresaService: DatosPruebaArticuloEmpresaService,

  ) {
    if (CONFIG.datosPrueba.crear) {
      this.crearDatosPrueba()
        .then(() => {
          console.info({
            mensaje: 'Termino de crear datos',
          });
          console.info({
            mensaje:
              'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
          });
        })
        .catch((error) => {
          console.info({
            mensaje:
              'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
          });
          console.error({
            mensaje: 'Hubo errores.',
            error,
          });
        });
    } else {
      console.info({
        mensaje:
          'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
      });
    }
  }

  async crearDatosPrueba() {
    if (CONFIG.datosPrueba.entrenador) {
      await this.crearEntrenador11();
    }
    if (CONFIG.datosPrueba.submoduloRoles) {
      await this._datosPruebaRolesService.crearDatosPruebaSubmoduloRoles();
    }
    if (CONFIG.datosPrueba.submoduloArticulo) {
      await this._datosPruebaArticuloService.crearDatosPruebaSubmoduloArticulo();
    }
    if (CONFIG.datosPrueba.submoduloEmpresa) {
      await this._datosPruebaEmpresaService.crearDatosPruebaSubmoduloEmpresa();
    }
    if (CONFIG.datosPrueba.articuloEmpresa) {
      await this._datosPruebaArticuloEmpresaService.crearArticulosEmpresa();
    }
  }

  async crearEntrenador11() {
    this.datosPrueba.entrenador = [];
    for (const datoPrueba of ENTRENADOR_DATOS_PRUEBA) {
      const respuesta = ((await this._entrenadorService.crear(
        datoPrueba(),
      )) as any) as EntrenadorEntity;
      this.datosPrueba.entrenador.push(respuesta);
    }
    if (CONFIG.datosPrueba.pokemon) {
      await this.crearPokemon21();
    }
  }

  async crearPokemon21() {
    this.datosPrueba.pokemon = [];
    for (const entrenador of this.datosPrueba.entrenador) {
      for (const datoPrueba of POKEMON_DATOS_PRUEBA) {
        const respuesta = ((await this._pokemonService.crear(
          datoPrueba(entrenador.id))) as any) as PokemonEntity;
        this.datosPrueba.pokemon.push(respuesta);
      }
    }
  }
}
