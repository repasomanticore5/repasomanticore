import {TypeOrmModule} from '@nestjs/typeorm';
import {PokemonEntity} from '../pokemon.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const POKEMON_IMPORTS = [
    TypeOrmModule.forFeature([PokemonEntity], NOMBRE_CADENA_CONEXION),
    SeguridadModule,
    AuditoriaModule,
];
