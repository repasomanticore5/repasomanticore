import {PokemonCrearDto} from '../dto/pokemon.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const POKEMON_DATOS_PRUEBA: ((entrenador: number) => PokemonCrearDto)[] = [
    (idEntrenador: number) => {
        const dato = new PokemonCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.nombre = 'Bulbasaur';
        dato.tipo = 'Planta / Veneno';
        dato.codigo = '001';
        dato.sisHabilitado = ActivoInactivo.Activo;
        dato.entrenador = idEntrenador;
        return dato;
    },
    (idEntrenador: number) => {
        const dato = new PokemonCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.nombre = 'Charmander';
        dato.tipo = 'Fuego';
        dato.codigo = '004';
        dato.sisHabilitado = ActivoInactivo.Activo;
        dato.entrenador = idEntrenador;
        return dato;
    },
    (idEntrenador: number) => {
        const dato = new PokemonCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.nombre = 'Squirtle';
        dato.tipo = 'Agua';
        dato.codigo = '007';
        dato.sisHabilitado = ActivoInactivo.Activo;
        dato.entrenador = idEntrenador;
        return dato;
    },
];
