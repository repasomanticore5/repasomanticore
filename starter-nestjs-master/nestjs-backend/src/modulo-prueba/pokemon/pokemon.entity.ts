import {
    Column,
    Entity,
    Index, ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {EntrenadorEntity} from '../entrenador/entrenador.entity';

const PREFIJO_TABLA = ''; // EJ: PREFIJO_
const nombreCampoNombre = PREFIJO_TABLA + 'NOMBRE';
const nombreCampoTipo = PREFIJO_TABLA + 'TIPO';
const nombreCampoCodigo = PREFIJO_TABLA + 'CODIGO';

@Entity(PREFIJO_BASE + 'POKEMON')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
@Index(['nombre', 'tipo', 'codigo'])
export class PokemonEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_POKEMON',
        unsigned: true,
    })
    id: number;

    @Column({
        name: nombreCampoNombre,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    nombre: string;

    @Column({
        name: nombreCampoTipo,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    tipo: string;

    @Column({
        name: nombreCampoCodigo,
        type: 'char',
        length: '3',
        nullable: false,
    })
    codigo: string;

    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    @ManyToOne(() => EntrenadorEntity, (r) => r.pokemons, { nullable: false })
    entrenador: EntrenadorEntity;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
