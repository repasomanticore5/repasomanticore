import {IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class PokemonBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
    @IsOptional()
    @IsNumberString()
    @Expose()
    entrenador: string;
}
