import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {EntrenadorService} from './entrenador.service';
import {EntrenadorHabilitadoDto} from './dto/entrenador.habilitado.dto';
import {ENTRENADOR_OCC} from './constantes/entrenador.occ';
import {EntrenadorCrearDto} from './dto/entrenador.crear.dto';
import {EntrenadorActualizarDto} from './dto/entrenador.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {EntrenadorEntity} from './entrenador.entity';
import {EntrenadorBusquedaDto} from './dto/entrenador.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {ControladorComun} from '@manticore-labs/nest-2021'
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'entrenador';

@Controller(nombreControlador)
export class EntrenadorController extends ControladorComun {
    constructor(
        private readonly _entrenadorService: EntrenadorService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _entrenadorService,
            _seguridadService,
            _auditoriaService,
            ENTRENADOR_OCC(
                EntrenadorHabilitadoDto,
                EntrenadorCrearDto,
                EntrenadorActualizarDto,
                EntrenadorBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: EntrenadorBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._entrenadorService.entrenadorEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<EntrenadorEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._entrenadorService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._entrenadorService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<EntrenadorEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }

                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({pueblo: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({codigo: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                consultaWhere = consulta.where as FindConditions<EntrenadorEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                // consulta = this._entrenadorService.convertirEnMayusculas(consulta);/
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            qb.leftJoinAndSelect(aliasTabla + '.pokemons', 'pokemons');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }
}
