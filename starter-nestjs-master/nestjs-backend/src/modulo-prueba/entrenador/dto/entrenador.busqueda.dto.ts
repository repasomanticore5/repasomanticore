import {IsNumberString, IsOptional, IsString} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class EntrenadorBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
