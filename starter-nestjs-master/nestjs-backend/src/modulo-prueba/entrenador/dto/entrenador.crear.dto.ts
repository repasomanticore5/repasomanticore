import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class EntrenadorCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    pueblo: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(3)
    @Expose()
    codigo: string;
}
