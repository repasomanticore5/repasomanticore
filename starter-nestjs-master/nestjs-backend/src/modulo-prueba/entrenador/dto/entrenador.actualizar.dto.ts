import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';

export class EntrenadorActualizarDto {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    pueblo: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(3)
    @Expose()
    codigo: string;
}
