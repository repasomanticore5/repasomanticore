import {
    Column,
    Entity,
    Index, OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {PokemonEntity} from '../pokemon/pokemon.entity';

const PREFIJO_TABLA = ''; // EJ: PREFIJO_
const nombreCampoNombre = PREFIJO_TABLA + 'NOMBRE';
const nombreCampoPueblo = PREFIJO_TABLA + 'PUEBLO';
const nombreCampoCodigo = PREFIJO_TABLA + 'CODIGO';

@Entity(PREFIJO_BASE + 'ENTRENADOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
@Index(['nombre', 'pueblo', 'codigo'])
export class EntrenadorEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_ENTRENADOR',
        unsigned: true,
    })
    id: number;

    @Column({
        name: nombreCampoNombre,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    nombre: string;

    @Column({
        name: nombreCampoPueblo,
        type: 'varchar',
        length: '60',
        nullable: false,
    })
    pueblo: string;

    @Column({
        name: nombreCampoCodigo,
        type: 'char',
        length: '3',
        nullable: false,
    })
    codigo: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];

    @OneToMany(() => PokemonEntity, (r) => r.entrenador)
    pokemons: PokemonEntity[];
}
