import {TypeOrmModule} from '@nestjs/typeorm';
import {EntrenadorEntity} from '../entrenador.entity';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';

export const ENTRENADOR_IMPORTS = [
    TypeOrmModule.forFeature([EntrenadorEntity], NOMBRE_CADENA_CONEXION),
    SeguridadModule,
    AuditoriaModule,
];
