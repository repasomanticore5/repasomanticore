import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_ENTRENADOR: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
    const nombreEntidad = 'entrenador';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/entrenador/, // /entrenador
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/entrenador\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /entrenador?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/entrenador\/\d+\/modificar-habilitado/, // /entrenador/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/entrenador\/\d+/, // /entrenador/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
