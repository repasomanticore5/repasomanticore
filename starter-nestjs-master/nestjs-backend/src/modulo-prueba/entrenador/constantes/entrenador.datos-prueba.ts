import {EntrenadorCrearDto} from '../dto/entrenador.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const ENTRENADOR_DATOS_PRUEBA: (() => EntrenadorCrearDto)[] = [
    () => {
        const dato = new EntrenadorCrearDto();
        // LLENAR DATOS DE PRUEBA
        dato.nombre = 'Ash Ketchup';
        dato.pueblo = 'Paleta';
        dato.codigo = '001';
        dato.sisHabilitado = ActivoInactivo.Activo;
        return dato;
    },
    () => {
        const dato = new EntrenadorCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.nombre = 'Cristian Lara';
        dato.pueblo = 'Quito';
        dato.codigo = '002';
        dato.sisHabilitado = ActivoInactivo.Activo;
        return dato;
    },
];
