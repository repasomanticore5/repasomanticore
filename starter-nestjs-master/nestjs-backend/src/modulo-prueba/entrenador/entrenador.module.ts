import { Module } from '@nestjs/common';
import { EntrenadorController } from './entrenador.controller';
import { ENTRENADOR_IMPORTS } from './constantes/entrenador.imports';
import { ENTRENADOR_PROVIDERS } from './constantes/entrenador.providers';

@Module({
  imports: [...ENTRENADOR_IMPORTS],
  providers: [...ENTRENADOR_PROVIDERS],
  exports: [...ENTRENADOR_PROVIDERS],
  controllers: [EntrenadorController],
})
export class EntrenadorModule {}
