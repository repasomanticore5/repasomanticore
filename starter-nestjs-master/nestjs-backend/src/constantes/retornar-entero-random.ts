export function obtenerEnteroRandom(min: number, max: number) { // min esta incluido, max esta excluido
  return Math.floor(Math.random() * (max - min + 1)) + min;
}