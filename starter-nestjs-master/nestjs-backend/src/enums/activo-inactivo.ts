export enum ActivoInactivo {
    Activo = 'A',
    Inactivo = 'I',
}
