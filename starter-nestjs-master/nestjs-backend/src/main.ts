import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {RedisIoAdapter} from './redis-io-adapter/redis-io-adapter';
import {CONFIG} from './environment/config';
import {json} from 'body-parser';
import * as passport from 'passport';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as RateLimit from 'express-rate-limit';
import * as RateLimitRedisStore from 'rate-limit-redis';
import * as Auth0Strategy from 'passport-auth0';

import {INestApplication, ValidationPipe} from '@nestjs/common';

const session = require('express-session');
const redis = require('redis');

const RedisStore = require('connect-redis')(session);
const redisClient = redis.createClient({
    port: CONFIG.redis.REDIS_PORT,
    host: CONFIG.redis.REDIS_HOST,

});


async function bootstrap() {
    const app: INestApplication | any = await NestFactory.create(AppModule);
    app.useWebSocketAdapter(new RedisIoAdapter(app) as any);
    app.enableCors();
    app.useStaticAssets(__dirname + '/../publico');
    app.set('view engine', 'ejs');

    app.use(json({limit: '50mb'}));
    const strategy = new Auth0Strategy(
        {
            domain: `${CONFIG.auth0.AUTH0_CUENTA}.auth0.com`,
            clientID: CONFIG.auth0.AUTH0_CLIENT_ID,
            clientSecret: CONFIG.auth0.AUTH0_SECRET,
            callbackURL: CONFIG.urls.callbackUrl(),
        },
        (accessToken, refreshToken, extraParams, profile, done) => {
            return done(null, profile);
        },
    );
    if (CONFIG.compresion.COMPRESION_HABILITADO) {
        const checkForHTML = req => {
            const url = req.url.split('.');
            const extension = url[url.length - 1];
            if (['/'].indexOf(extension) > -1) {
                return true; // compress only .html files sent from server
            }
            return false;
        };
        app.use(compression({filter: checkForHTML}));
    }
    app.use(
        session({
            store: new RedisStore({
                client: redisClient,
                port: CONFIG.redis.REDIS_PORT,
                host: CONFIG.redis.REDIS_HOST,
            }),
            secret: CONFIG.session.SESSION_PASSWORD,
            resave: false,
            saveUninitialized: true,
        }),
    );
    app.use(passport.initialize());
    app.use(passport.session());
    passport.serializeUser((user, done) => {
        done(null, user);
    });
    passport.deserializeUser((user, done) => {
        done(null, user);
    });
    passport.use(strategy);
    if (CONFIG.helmet.HELMET_HABILITADO) {
        app.use(helmet());
    }
    if (CONFIG.rateLimit.RATE_LIMIT_HABILITADO) {
        const limiter = new RateLimit({
            store: new RateLimitRedisStore({redisClient}),
            ...CONFIG.rateLimit,
        });
        app.use(limiter);
    }
    app.useGlobalPipes(new ValidationPipe(
        {
            disableErrorMessages: CONFIG.production,
        },
    ));
    console.log({mensaje: 'App levantada en ' + CONFIG.PORT});
    await app.listen(CONFIG.PORT);
}

bootstrap();
