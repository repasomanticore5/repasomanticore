import { EntidadComunProyecto } from 'src/abstractos/entidad-comun-proyecto';
import { PREFIJO_BASE } from 'src/constantes/prefijo-base';
import { TrabajadorEntity } from 'src/trabajador/trabajador.entity';
import {
    Column,
    Entity,
    Index,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TAREA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class TareaEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_TAREA',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'descripcion',
        type: 'varchar',
        length: '50',
        nullable: false,
    })
    descripcion: string;

    @Column({
        name: 'comentario',
        type: 'varchar',
        length: '50',
        nullable: false,
    })
    comentario: string;


    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];

    //hijo

    @ManyToOne(() => TrabajadorEntity, 
    (r) => r.tareas, 
    { nullable: false })
    trabajador: TrabajadorEntity;

    
}
