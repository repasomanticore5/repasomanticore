import { Module } from '@nestjs/common';
import { TareaController } from './tarea.controller';
import { TAREA_IMPORTS } from './constantes/tarea.imports';
import { TAREA_PROVIDERS } from './constantes/tarea.providers';

@Module({
  imports: [...TAREA_IMPORTS],
  providers: [...TAREA_PROVIDERS],
  exports: [...TAREA_PROVIDERS],
  controllers: [TareaController],
})
export class TareaModule {}
