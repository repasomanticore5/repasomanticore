import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {TareaService} from './tarea.service';
import {TareaHabilitadoDto} from './dto/tarea.habilitado.dto';
import {TAREA_OCC} from './constantes/tarea.occ';
import {TareaCrearDto} from './dto/tarea.crear.dto';
import {TareaActualizarDto} from './dto/tarea.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {TareaEntity} from './tarea.entity';
import {TareaBusquedaDto} from './dto/tarea.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { SeguridadService } from 'src/seguridad/seguridad.service';
import { AuditoriaService } from 'src/auditoria/auditoria.service';
import { NUMERO_REGISTROS_DEFECTO } from 'src/constantes/numero-registros-defecto';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from 'src/constantes/numero-maximo-registros-consultarse';
import { SeguridadGuard } from 'src/guards/seguridad/seguridad.guard';

const nombreControlador = 'tarea';

@Controller(nombreControlador)
export class TareaController extends ControladorComun {
    constructor(
        private readonly _tareaService: TareaService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _tareaService,
            _seguridadService,
            _auditoriaService,
            TAREA_OCC(
                TareaHabilitadoDto,
                TareaCrearDto,
                TareaActualizarDto,
                TareaBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: TareaBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._tareaService.tareaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<TareaEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._tareaService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._tareaService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<TareaEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({descripcion: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<TareaEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                consulta.where = this.setearFiltro(
                    'trabajador',
                    consultaWhere,
                    parametrosConsulta.trabajador,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._tareaService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);
            
            // RELACIONES CON PAPAS o HIJOS
            
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');
            qb.leftJoinAndSelect(aliasTabla + '.trabajador', 'trabajador');
            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            
            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();

            // Activar para archivos estas lineas

            // const consultaGetManyAndCount = await qb
            //     .skip(consulta.skip)
            //     .take(consulta.take)
            //     .getManyAndCount();
            // return this._tareaService.recuperarImagenesArchivoSecundario(
            //     consultaGetManyAndCount,
            //     this._archivoPrincipalService,
            //     'tarea',
            //     ['AR', 'IM'],
            // );
        }
    }
}
