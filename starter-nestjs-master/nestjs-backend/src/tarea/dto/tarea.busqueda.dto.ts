import {IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import { BusquedaComunProyectoDto } from 'src/abstractos/busqueda-comun-proyecto-dto';

export class TareaBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    trabajador: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
