import {IsNotEmpty, IsNumber, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import { HabilitadoDtoComun } from 'src/abstractos/habilitado-dto-comun';

export class TareaCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    // @IsNotEmpty()
    // @IsString()
    // @MinLength(10)
    // @MaxLength(60)
    // @Expose()
    // prefijoCampoEjemplo: string;

     @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    @Expose()
    descripcion: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    @Expose()
    comentario: string;

    @IsNotEmpty()
    @IsNumber()
    @Expose()
    trabajador: number;

   
}
