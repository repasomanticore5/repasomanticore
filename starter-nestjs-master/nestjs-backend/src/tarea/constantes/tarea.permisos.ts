import { ObjetoSeguridadPermisos } from "@manticore-labs/nest-2021";

export const PERMISOS_TAREA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'tarea';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tarea/, // /tarea
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tarea\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tarea?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tarea\/\d+\/modificar-habilitado/, // /tarea/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tarea\/\d+/, // /tarea/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
