import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditoriaModule } from 'src/auditoria/auditoria.module';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';
import { SeguridadModule } from 'src/seguridad/seguridad.module';
import { TareaEntity } from '../tarea.entity';

export const TAREA_IMPORTS = [
  TypeOrmModule.forFeature([TareaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
