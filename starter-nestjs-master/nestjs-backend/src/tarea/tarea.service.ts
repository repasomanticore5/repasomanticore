import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TareaEntity } from './tarea.entity';
import {TareaBusquedaDto} from './dto/tarea.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { NOMBRE_CADENA_CONEXION } from 'src/constantes/nombre-cadena-conexion';
import { AuditoriaService } from 'src/auditoria/auditoria.service';

@Injectable()
export class TareaService extends ServicioComun<TareaEntity, TareaBusquedaDto> {
  constructor(
    @InjectRepository(TareaEntity, NOMBRE_CADENA_CONEXION)
    public tareaEntityRepository: Repository<TareaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        tareaEntityRepository,
      _connection,
        TareaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
