import { Module } from '@nestjs/common';
import { ClienteController } from './cliente.controller';
import { CLIENTE_IMPORTS } from './constantes/cliente.imports';
import { CLIENTE_PROVIDERS } from './constantes/cliente.providers';

@Module({
  imports: [...CLIENTE_IMPORTS],
  providers: [...CLIENTE_PROVIDERS],
  exports: [...CLIENTE_PROVIDERS],
  controllers: [ClienteController],
})
export class ClienteModule {}
