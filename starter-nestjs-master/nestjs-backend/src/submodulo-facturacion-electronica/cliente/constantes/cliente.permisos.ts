import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_CLIENTE: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'cliente';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/cliente/, // /cliente
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/cliente\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /cliente?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/cliente\/\d+\/modificar-habilitado/, // /cliente/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/cliente\/\d+/, // /cliente/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
