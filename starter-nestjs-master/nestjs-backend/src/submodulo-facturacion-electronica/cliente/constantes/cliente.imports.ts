import { TypeOrmModule } from '@nestjs/typeorm';
import { ClienteEntity } from '../cliente.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const CLIENTE_IMPORTS = [
  TypeOrmModule.forFeature([ClienteEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
