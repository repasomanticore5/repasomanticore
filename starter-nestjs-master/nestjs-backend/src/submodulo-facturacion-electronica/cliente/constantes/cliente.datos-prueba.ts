import { ClienteCrearDto } from '../dto/cliente.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const CLIENTE_DATOS_PRUEBA: (() => ClienteCrearDto)[] = [
  () => {
    const dato = new ClienteCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new ClienteCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new ClienteCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
