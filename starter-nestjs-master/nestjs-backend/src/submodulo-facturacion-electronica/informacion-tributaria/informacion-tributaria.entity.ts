import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {TipoIdentificacionEntity} from "../tipo-identificacion/tipo-identificacion.entity";

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'INFORMACION_TRIBUTARIA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class InformacionTributariaEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_INFORMACION_TRIBUTARIA',
        unsigned: true,
    })
    id: number;

    // @Column({
    //     name: nombreCampoEjemplo,
    //     type: 'varchar',
    //     length: '60',
    //     nullable: false,
    // })
    // prefijoCampoEjemplo: string;

    @Index()
    @Column({
        name: 'razon_social',
        type: 'varchar',
        length: 255,
        nullable: false,
    })
    razonSocial: string;

    @Index()
    @Column({
        name: 'identificacion',
        type: 'varchar',
        length: 50,
        nullable: false,
    })
    identificacion: string;

    @Column({
        name: 'direccion',
        type: 'varchar',
        length: 200,
        nullable: false,
    })
    direccion: string;

    @Column({
        name: 'telefono',
        type: 'varchar',
        length: 10,
        nullable: false,
    })
    telefono: string;

    @Column({
        name: 'correo',
        type: 'varchar',
        length: 60,
        nullable: true,
    })
    correo: string = null;

    @Column({
        name: 'tipo_contribuyente',
        type: 'varchar',
        length: 2,
        nullable: false,
        default: 'PN',
        comment: 'PN: persona natural, SO: sociendades, ES: especial',
    })
    tipoContribuyente: 'PN' | 'SO' | 'SE' = 'PN';

    @Column({
        name: 'contribuyente_especial',
        type: 'varchar',
        length: 5,
        nullable: true,
    })
    contribuyenteEspecial: string = null;

    @Column({
        name: 'obligado_contabilidad',
        type: 'tinyint',
        nullable: false,
        default: 0,
        comment: '1: si, 0: no',
    })
    obligadoContabilidad: 0 | 1 = 0;

    @Column({
        name: 'es_transportista',
        type: 'tinyint',
        nullable: false,
        default: 0,
        comment: '1: si, 0: no',
    })
    esTransportista: 0 | 1 = 0;

    @ManyToOne(
        () => TipoIdentificacionEntity,
        (r) => r.informacionesTributarias,
        {nullable: false}
    )
    tipoIdentificacion: TipoIdentificacionEntity | number;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
