import { InformacionTributariaCrearDto } from '../dto/informacion-tributaria.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const INFORMACION_TRIBUTARIA_DATOS_PRUEBA: (() => InformacionTributariaCrearDto)[] = [
  () => {
    const dato = new InformacionTributariaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new InformacionTributariaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new InformacionTributariaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
