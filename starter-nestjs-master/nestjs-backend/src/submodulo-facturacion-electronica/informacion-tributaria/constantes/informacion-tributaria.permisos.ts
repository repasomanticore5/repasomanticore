import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_INFORMACION_TRIBUTARIA: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'informacion-tributaria';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/informacion-tributaria/, // /informacion-tributaria
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/informacion-tributaria\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /informacion-tributaria?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/informacion-tributaria\/\d+\/modificar-habilitado/, // /informacion-tributaria/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/informacion-tributaria\/\d+/, // /informacion-tributaria/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
