import { TypeOrmModule } from '@nestjs/typeorm';
import { InformacionTributariaEntity } from '../informacion-tributaria.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const INFORMACION_TRIBUTARIA_IMPORTS = [
  TypeOrmModule.forFeature(
    [InformacionTributariaEntity],
    NOMBRE_CADENA_CONEXION,
  ),
  SeguridadModule,
  AuditoriaModule,
];
