import { Module } from '@nestjs/common';
import { InformacionTributariaController } from './informacion-tributaria.controller';
import { INFORMACION_TRIBUTARIA_IMPORTS } from './constantes/informacion-tributaria.imports';
import { INFORMACION_TRIBUTARIA_PROVIDERS } from './constantes/informacion-tributaria.providers';

@Module({
  imports: [...INFORMACION_TRIBUTARIA_IMPORTS],
  providers: [...INFORMACION_TRIBUTARIA_PROVIDERS],
  exports: [...INFORMACION_TRIBUTARIA_PROVIDERS],
  controllers: [InformacionTributariaController],
})
export class InformacionTributariaModule {}
