import { IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class VehiculoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  @Expose()
  marca: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(8)
  @Expose()
  placa: string;
}
