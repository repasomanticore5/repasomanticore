import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class VehiculoCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  @Expose()
  marca: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(8)
  @Expose()
  placa: string;
}
