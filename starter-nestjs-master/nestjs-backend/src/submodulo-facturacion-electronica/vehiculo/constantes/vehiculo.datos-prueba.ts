import { VehiculoCrearDto } from '../dto/vehiculo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const VEHICULO_DATOS_PRUEBA: (() => VehiculoCrearDto)[] = [
  () => {
    const dato = new VehiculoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.marca = 'Suzuki';
    dato.placa = 'ISO954A';
    return dato;
  },
  () => {
    const dato = new VehiculoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.marca = 'Toyota';
    dato.placa = 'ISO954B';
    return dato;
  },
  () => {
    const dato = new VehiculoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.marca = 'Kia';
    dato.placa = 'ISO954B';
    return dato;
  },
];
