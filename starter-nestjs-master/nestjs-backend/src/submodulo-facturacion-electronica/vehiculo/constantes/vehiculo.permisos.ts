import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_VEHICULO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'vehiculo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/vehiculo/, // /vehiculo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/vehiculo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /vehiculo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/vehiculo\/\d+\/modificar-habilitado/, // /vehiculo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/vehiculo\/\d+/, // /vehiculo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
