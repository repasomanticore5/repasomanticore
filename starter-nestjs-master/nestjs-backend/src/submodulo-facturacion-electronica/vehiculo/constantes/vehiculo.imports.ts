import { TypeOrmModule } from '@nestjs/typeorm';
import { VehiculoEntity } from '../vehiculo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const VEHICULO_IMPORTS = [
  TypeOrmModule.forFeature([VehiculoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
