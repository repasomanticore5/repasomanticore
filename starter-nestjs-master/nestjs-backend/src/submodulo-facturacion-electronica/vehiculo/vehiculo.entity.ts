import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'VEHICULO')
@Index(['marca', 'placa', 'sisHabilitado', 'sisCreado', 'sisModificado'])
export class VehiculoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_VEHICULO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'marca',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  marca: string;

  @Column({
    name: 'placa',
    type: 'varchar',
    length: 8,
    nullable: false,
  })
  placa: string;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
