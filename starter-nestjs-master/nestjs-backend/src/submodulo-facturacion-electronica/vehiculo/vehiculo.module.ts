import { Module } from '@nestjs/common';
import { VehiculoController } from './vehiculo.controller';
import { VEHICULO_IMPORTS } from './constantes/vehiculo.imports';
import { VEHICULO_PROVIDERS } from './constantes/vehiculo.providers';

@Module({
  imports: [...VEHICULO_IMPORTS],
  providers: [...VEHICULO_PROVIDERS],
  exports: [...VEHICULO_PROVIDERS],
  controllers: [VehiculoController],
})
export class VehiculoModule {}
