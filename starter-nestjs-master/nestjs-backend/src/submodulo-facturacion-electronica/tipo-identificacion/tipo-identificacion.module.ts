import { Module } from '@nestjs/common';
import { TipoIdentificacionController } from './tipo-identificacion.controller';
import { TIPO_IDENTIFICACION_IMPORTS } from './constantes/tipo-identificacion.imports';
import { TIPO_IDENTIFICACION_PROVIDERS } from './constantes/tipo-identificacion.providers';

@Module({
  imports: [...TIPO_IDENTIFICACION_IMPORTS],
  providers: [...TIPO_IDENTIFICACION_PROVIDERS],
  exports: [...TIPO_IDENTIFICACION_PROVIDERS],
  controllers: [TipoIdentificacionController],
})
export class TipoIdentificacionModule {}
