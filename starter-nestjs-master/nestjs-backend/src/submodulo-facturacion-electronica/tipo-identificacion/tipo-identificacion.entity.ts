import {
    Column,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {InformacionTributariaEntity} from "../informacion-tributaria/informacion-tributaria.entity";

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_IDENTIFICACION')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class TipoIdentificacionEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_TIPO_IDENTIFICACION',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'nombre',
        type: 'varchar',
        length: '50',
        nullable: false,
    })
    nombre: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    @OneToMany(
        () => InformacionTributariaEntity,
        (r) => r.tipoIdentificacion,
    )
    informacionesTributarias: InformacionTributariaEntity[];
}
