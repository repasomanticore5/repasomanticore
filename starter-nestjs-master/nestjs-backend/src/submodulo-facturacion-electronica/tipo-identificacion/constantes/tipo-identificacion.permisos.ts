import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_IDENTIFICACION: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'tipo-identificacion';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tipo-identificacion/, // /tipo-identificacion
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tipo-identificacion\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-identificacion?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-identificacion\/\d+\/modificar-habilitado/, // /tipo-identificacion/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-identificacion\/\d+/, // /tipo-identificacion/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
