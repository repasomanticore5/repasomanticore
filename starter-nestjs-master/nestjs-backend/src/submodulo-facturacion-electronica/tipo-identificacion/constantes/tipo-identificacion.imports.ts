import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoIdentificacionEntity } from '../tipo-identificacion.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const TIPO_IDENTIFICACION_IMPORTS = [
  TypeOrmModule.forFeature([TipoIdentificacionEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
