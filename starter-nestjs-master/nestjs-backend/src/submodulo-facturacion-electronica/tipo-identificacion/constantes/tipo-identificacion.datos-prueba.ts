import { TipoIdentificacionCrearDto } from '../dto/tipo-identificacion.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TIPO_IDENTIFICACION_DATOS_PRUEBA: (() => TipoIdentificacionCrearDto)[] = [
  () => {
    const dato = new TipoIdentificacionCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'Cedula';
    return dato;
  },
  () => {
    const dato = new TipoIdentificacionCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'RUC';
    return dato;
  },
  () => {
    const dato = new TipoIdentificacionCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'Pasaporte';
    return dato;
  },
];
