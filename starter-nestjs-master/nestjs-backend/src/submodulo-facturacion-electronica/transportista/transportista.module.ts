import { Module } from '@nestjs/common';
import { TransportistaController } from './transportista.controller';
import { TRANSPORTISTA_IMPORTS } from './constantes/transportista.imports';
import { TRANSPORTISTA_PROVIDERS } from './constantes/transportista.providers';

@Module({
  imports: [...TRANSPORTISTA_IMPORTS],
  providers: [...TRANSPORTISTA_PROVIDERS],
  exports: [...TRANSPORTISTA_PROVIDERS],
  controllers: [TransportistaController],
})
export class TransportistaModule {}
