import { TypeOrmModule } from '@nestjs/typeorm';
import { TransportistaEntity } from '../transportista.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const TRANSPORTISTA_IMPORTS = [
  TypeOrmModule.forFeature([TransportistaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
