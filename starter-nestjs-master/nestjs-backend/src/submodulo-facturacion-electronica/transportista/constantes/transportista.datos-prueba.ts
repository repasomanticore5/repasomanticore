import { TransportistaCrearDto } from '../dto/transportista.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TRANSPORTISTA_DATOS_PRUEBA: (() => TransportistaCrearDto)[] = [
  () => {
    const dato = new TransportistaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TransportistaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TransportistaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
