import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_TRANSPORTISTA: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'transportista';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/transportista/, // /transportista
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/transportista\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /transportista?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/transportista\/\d+\/modificar-habilitado/, // /transportista/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/transportista\/\d+/, // /transportista/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
