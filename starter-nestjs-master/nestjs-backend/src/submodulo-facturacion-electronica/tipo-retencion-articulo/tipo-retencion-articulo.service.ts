import {Injectable} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {TipoRetencionArticuloEntity} from './tipo-retencion-articulo.entity';
import {TipoRetencionArticuloBusquedaDto} from './dto/tipo-retencion-articulo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class TipoRetencionArticuloService extends ServicioComun<TipoRetencionArticuloEntity, TipoRetencionArticuloBusquedaDto> {
    constructor(
        @InjectRepository(TipoRetencionArticuloEntity, NOMBRE_CADENA_CONEXION)
        public tipoRetencionArticuloEntityRepository: Repository<TipoRetencionArticuloEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            tipoRetencionArticuloEntityRepository,
            _connection,
            TipoRetencionArticuloEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar
        );
    }
}
