import {TypeOrmModule} from '@nestjs/typeorm';
import {TipoRetencionArticuloEntity} from '../tipo-retencion-articulo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const TIPO_RETENCION_ARTICULO_IMPORTS = [
    TypeOrmModule.forFeature([TipoRetencionArticuloEntity], NOMBRE_CADENA_CONEXION),
    SeguridadModule,
    AuditoriaModule,
];
