import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_RETENCION_ARTICULO: () => { permisos: ObjetoSeguridadPermisos[], nombreEntidad: string } = () => {
    const nombreEntidad = 'tipo-retencion-articulo';
    const permisos: ObjetoSeguridadPermisos[] = [
        {
            nombrePermiso: nombreEntidad + 'Crear',
            metodoHTTP: 'POST',
            urlExpressionRegular: /\/tipo-retencion-articulo/, // /tipo-retencion-articulo
        },
        {
            nombrePermiso: nombreEntidad + 'Buscar',
            metodoHTTP: 'GET',
            urlExpressionRegular: /\/tipo-retencion-articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-retencion-articulo?asdas=asdasd
        },
        {
            nombrePermiso: nombreEntidad + 'EditarHabilitado',
            metodoHTTP: 'PUT',
            urlExpressionRegular: /\/tipo-retencion-articulo\/\d+\/modificar-habilitado/, // /tipo-retencion-articulo/1/modificar-habilitado
        },
        {
            nombrePermiso: nombreEntidad + 'Editar',
            metodoHTTP: 'PUT',
            urlExpressionRegular: /\/tipo-retencion-articulo\/\d+/, // /tipo-retencion-articulo/1
        },
    ];
    return {
        permisos,
        nombreEntidad
    };
};
