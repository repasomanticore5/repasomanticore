import {TipoRetencionArticuloCrearDto} from '../dto/tipo-retencion-articulo.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const TIPO_RETENCION_ARTICULO_DATOS_PRUEBA: (() => TipoRetencionArticuloCrearDto)[] = [
    () => {
        const dato = new TipoRetencionArticuloCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.sisHabilitado = ActivoInactivo.Activo;
        return dato;
    },
    () => {
        const dato = new TipoRetencionArticuloCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.sisHabilitado = ActivoInactivo.Activo;
        return dato;
    },
    () => {
        const dato = new TipoRetencionArticuloCrearDto();
        // LLENAR DATOS DE PRUEBA
        // dato.nombreCampo = 'Valor campo';
        dato.sisHabilitado = ActivoInactivo.Activo;
        return dato;
    },
];
