import {Module} from '@nestjs/common';
import {TipoRetencionArticuloController} from './tipo-retencion-articulo.controller';
import {TIPO_RETENCION_ARTICULO_IMPORTS} from './constantes/tipo-retencion-articulo.imports';
import {TIPO_RETENCION_ARTICULO_PROVIDERS} from './constantes/tipo-retencion-articulo.providers';

@Module({
    imports: [...TIPO_RETENCION_ARTICULO_IMPORTS],
    providers: [...TIPO_RETENCION_ARTICULO_PROVIDERS],
    exports: [...TIPO_RETENCION_ARTICULO_PROVIDERS],
    controllers: [TipoRetencionArticuloController],
})
export class TipoRetencionArticuloModule {
}
