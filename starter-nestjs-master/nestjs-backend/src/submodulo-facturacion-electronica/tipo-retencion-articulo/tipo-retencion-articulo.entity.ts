import {
    Column,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_RETENCION_ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class TipoRetencionArticuloEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_TIPO_RETENCION_ARTICULO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'nombre',
        type: 'varchar',
        length: 10,
        nullable: false,
    })
    nombre: string;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
