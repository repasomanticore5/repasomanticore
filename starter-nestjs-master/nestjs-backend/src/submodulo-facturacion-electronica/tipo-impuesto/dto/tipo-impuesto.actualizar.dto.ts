import { IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class TipoImpuestoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigoSri: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(10)
  @Expose()
  siglas: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;
}
