import {
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class TipoImpuestoBusquedaDto extends BusquedaComunProyectoDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsString()
  @Expose()
  codigoSri: string;

  @IsOptional()
  @IsString()
  @Expose()
  codigo: string;

  @IsOptional()
  @IsString()
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @Expose()
  siglas: string;
}
