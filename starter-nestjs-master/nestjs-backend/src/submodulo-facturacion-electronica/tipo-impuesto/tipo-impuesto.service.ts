import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TipoImpuestoEntity } from './tipo-impuesto.entity';
import {TipoImpuestoBusquedaDto} from './dto/tipo-impuesto.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class TipoImpuestoService extends ServicioComun<TipoImpuestoEntity, TipoImpuestoBusquedaDto> {
  constructor(
    @InjectRepository(TipoImpuestoEntity, NOMBRE_CADENA_CONEXION)
    public tipoImpuestoEntityRepository: Repository<TipoImpuestoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        tipoImpuestoEntityRepository,
      _connection,
        TipoImpuestoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
