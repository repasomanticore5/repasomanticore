import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_IMPUESTO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'tipo-impuesto';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tipo-impuesto/, // /tipo-impuesto
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tipo-impuesto\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-impuesto?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-impuesto\/\d+\/modificar-habilitado/, // /tipo-impuesto/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-impuesto\/\d+/, // /tipo-impuesto/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
