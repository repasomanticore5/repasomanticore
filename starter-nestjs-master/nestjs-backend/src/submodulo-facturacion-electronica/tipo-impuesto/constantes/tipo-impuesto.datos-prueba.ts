import { TipoImpuestoCrearDto } from '../dto/tipo-impuesto.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TIPO_IMPUESTO_DATOS_PRUEBA: (() => TipoImpuestoCrearDto)[] = [
  () => {
    const dato = new TipoImpuestoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.codigoSri = '2';
    dato.codigo = '2';
    dato.nombre = 'Impuesto al valor agregado';
    dato.siglas = 'IVA';
    return dato;
  },
  () => {
    const dato = new TipoImpuestoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.codigoSri = '3';
    dato.codigo = '3';
    dato.nombre = 'Impuesto a los consumos especiales';
    dato.siglas = 'ICE';
    return dato;
  },
  () => {
    const dato = new TipoImpuestoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.codigoSri = '5';
    dato.codigo = '5';
    dato.nombre = 'Impuesto redimibile a las botellas plásticas no retornables';
    dato.siglas = 'IRBPNR';
    return dato;
  },
];
