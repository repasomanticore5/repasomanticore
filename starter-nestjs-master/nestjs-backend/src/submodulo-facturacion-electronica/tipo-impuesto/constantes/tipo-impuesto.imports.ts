import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoImpuestoEntity } from '../tipo-impuesto.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const TIPO_IMPUESTO_IMPORTS = [
  TypeOrmModule.forFeature([TipoImpuestoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
