import { Module } from '@nestjs/common';
import { TipoImpuestoController } from './tipo-impuesto.controller';
import { TIPO_IMPUESTO_IMPORTS } from './constantes/tipo-impuesto.imports';
import { TIPO_IMPUESTO_PROVIDERS } from './constantes/tipo-impuesto.providers';

@Module({
  imports: [...TIPO_IMPUESTO_IMPORTS],
  providers: [...TIPO_IMPUESTO_PROVIDERS],
  exports: [...TIPO_IMPUESTO_PROVIDERS],
  controllers: [TipoImpuestoController],
})
export class TipoImpuestoModule {}
