import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_IMPUESTO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class TipoImpuestoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_TIPO_IMPUESTO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'codigo_sri',
    type: 'varchar',
    length: '120',
    nullable: false,
  })
  codigoSri: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: '120',
    nullable: true,
  })
  codigo: string;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: '100',
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'siglas',
    type: 'varchar',
    length: '10',
    nullable: false,
  })
  siglas: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: '10',
    nullable: true,
  })
  descripcion: string;

  // @Column({
  //     name: nombreCampoEjemplo,
  //     type: 'varchar',
  //     length: '60',
  //     nullable: false,
  // })
  // prefijoCampoEjemplo: string;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
