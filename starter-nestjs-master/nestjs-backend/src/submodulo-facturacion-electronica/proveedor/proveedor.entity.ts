import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'PROVEEDOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ProveedorEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_PROVEEDOR',
    unsigned: true,
  })
  id: number;

  // @Column({
  //     name: nombreCampoEjemplo,
  //     type: 'varchar',
  //     length: '60',
  //     nullable: false,
  // })
  // prefijoCampoEjemplo: string;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
