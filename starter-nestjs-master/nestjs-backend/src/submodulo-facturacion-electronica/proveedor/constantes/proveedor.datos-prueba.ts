import { ProveedorCrearDto } from '../dto/proveedor.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const PROVEEDOR_DATOS_PRUEBA: (() => ProveedorCrearDto)[] = [
  () => {
    const dato = new ProveedorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new ProveedorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new ProveedorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
