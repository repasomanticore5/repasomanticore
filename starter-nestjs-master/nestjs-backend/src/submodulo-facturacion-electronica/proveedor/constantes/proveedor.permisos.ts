import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_PROVEEDOR: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'proveedor';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/proveedor/, // /proveedor
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/proveedor\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /proveedor?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/proveedor\/\d+\/modificar-habilitado/, // /proveedor/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/proveedor\/\d+/, // /proveedor/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
