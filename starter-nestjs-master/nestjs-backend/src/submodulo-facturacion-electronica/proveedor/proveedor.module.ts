import { Module } from '@nestjs/common';
import { ProveedorController } from './proveedor.controller';
import { PROVEEDOR_IMPORTS } from './constantes/proveedor.imports';
import { PROVEEDOR_PROVIDERS } from './constantes/proveedor.providers';

@Module({
  imports: [...PROVEEDOR_IMPORTS],
  providers: [...PROVEEDOR_PROVIDERS],
  exports: [...PROVEEDOR_PROVIDERS],
  controllers: [ProveedorController],
})
export class ProveedorModule {}
