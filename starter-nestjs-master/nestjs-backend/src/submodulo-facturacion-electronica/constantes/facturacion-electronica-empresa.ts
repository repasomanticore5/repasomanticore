import {VehiculoEntity} from '../vehiculo/vehiculo.entity';
import {TipoIdentificacionEntity} from '../tipo-identificacion/tipo-identificacion.entity';
import {TipoImpuestoEntity} from '../tipo-impuesto/tipo-impuesto.entity';
import {InformacionTributariaEntity} from '../informacion-tributaria/informacion-tributaria.entity';
import {TransportistaEntity} from '../transportista/transportista.entity';
import {ClienteEntity} from '../cliente/cliente.entity';
import {ProveedorEntity} from '../proveedor/proveedor.entity';
import {TipoRetencionArticuloEntity} from '../tipo-retencion-articulo/tipo-retencion-articulo.entity';

export const ENTITIES_FACTURACION_ELECTRONICA = [
    VehiculoEntity,
    TipoIdentificacionEntity,
    TipoImpuestoEntity,
    InformacionTributariaEntity,
    TransportistaEntity,
    ClienteEntity,
    ProveedorEntity,
    TipoRetencionArticuloEntity,
];
