import {VehiculoEntity} from '../vehiculo/vehiculo.entity';
import {TipoRetencionArticuloEntity} from '../tipo-retencion-articulo/tipo-retencion-articulo.entity';
import {TipoIdentificacionEntity} from '../tipo-identificacion/tipo-identificacion.entity';

export interface DatosPruebaFacturacionElectronicaInterface {
    vehiculo?: VehiculoEntity[];
    tipoRetencionArticulo?: TipoRetencionArticuloEntity[];
    tipoIdentificacion?: TipoIdentificacionEntity[];
}
