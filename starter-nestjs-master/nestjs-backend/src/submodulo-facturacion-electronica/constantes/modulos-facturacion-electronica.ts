import {VehiculoModule} from '../vehiculo/vehiculo.module';
import {TipoIdentificacionModule} from '../tipo-identificacion/tipo-identificacion.module';
import {TipoImpuestoModule} from '../tipo-impuesto/tipo-impuesto.module';
import {InformacionTributariaModule} from '../informacion-tributaria/informacion-tributaria.module';
import {TransportistaModule} from '../transportista/transportista.module';
import {ClienteModule} from '../cliente/cliente.module';
import {ProveedorModule} from '../proveedor/proveedor.module';
import {TipoRetencionArticuloModule} from '../tipo-retencion-articulo/tipo-retencion-articulo.module';

export const MODULOS_FACTURACION_ELECTRONICA = [
    VehiculoModule,
    TipoIdentificacionModule,
    TipoImpuestoModule,
    InformacionTributariaModule,
    TransportistaModule,
    ClienteModule,
    ProveedorModule,
    TipoRetencionArticuloModule,
];
