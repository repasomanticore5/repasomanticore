import * as jwksRsa from 'jwks-rsa';
import {CONFIG_DATOS_PRUEBA_EMPERSA} from '../submodulo-empresa/constantes/config-datos-prueba-empresa';
import {CONFIG_DATOS_PRUEBA_ARTICULO} from '../submodulo-articulo/constantes/config-datos-prueba-articulo';
import { CONFIG_DATOS_PRUEBA_ROLES } from '../submodulo-roles/constantes/config-datos-prueba-roles';
import { CONFIG_DATOS_PRUEBA_ARTICULO_EMPERSA } from '../submodulo-articulo-empresa/constantes/config-datos-prueba-articulo-empresa';
import {CONFIG_DATOS_PRUEBA_FACTURACION_ELECTRONICA} from '../submodulo-facturacion-electronica/constantes/config-datos-prueba-facturacion-electronica';

export const CONFIG = {
  versionAplicativoBackend: '1.0.0',
  versionAplicativoFrontend: '1.0.0',
  production: false,
  PORT: 3000,
  NUMERO_MAXIMO_REGISTROS_CONSULTARSE: 30,
  NUMERO_TOTAL_REGISTROS: 10,
  datosPrueba: {
    crear: true,
    entrenador: true,
    pokemon: true,
    tarea:true,
    trabajador:true,
    ...CONFIG_DATOS_PRUEBA_EMPERSA,
    ...CONFIG_DATOS_PRUEBA_ARTICULO,
    ...CONFIG_DATOS_PRUEBA_ROLES,
    ...CONFIG_DATOS_PRUEBA_ARTICULO_EMPERSA,
    ...CONFIG_DATOS_PRUEBA_FACTURACION_ELECTRONICA,
  },
  bdd_relacional: {
    DB_NOMBRE_CONEXION: 'default',
    DB_ID: '', // Solo para Oracle
    DB_HOST: 'localhost',
    DB_PORT: 30501,
    DB_TYPE: 'mysql',
    DB_DATABASE: 'development', // Schema en Oracle
    DB_USERNAME: 'development',
    DB_PASSWORD: 'development',
    DB_SYNCRHONIZE: true,
    DB_DROPSCHEMA: true,
    DB_CACHE: true,
    DB_DEBUG: true,
    DB_MAX_QET: 1000000,
    DB_BUFFER_ENTRIES: 1000000,
    DB_RETRY_DELAY: 40000,
    DB_RETRY_ATTEMPTS: 3,
    DB_CONNECTION_TIMEOUT: 40000,
    DB_KEEP_CONNECTION_ALIVE: true,
    DB_SSL: false,
    DB_CHARSET: 'utf8mb4',
    DB_TIMEZONE: 'local',
    DB_MIGRATIONS_RUN: false,
  },
  seguridad: {
    SEGURIDAD_HABILITADO: false,
    DB_NOMBRE_CONEXION: 'seguridad',
    DB_DATABASE: 'development',
    DB_USERNAME: 'development',
    DB_PASSWORD: 'development',
  },
  auditoria: {
    AUDITORIA_HABILITADO: false,
    DB_NOMBRE_CONEXION: 'auditoria',
    DB_DATABASE: 'development',
    DB_USERNAME: 'development',
    DB_PASSWORD: 'development',
  },
  redis: {
    REDIS_PORT: 6379,
    REDIS_HOST: '161.97.152.203',
    REDIS_DATABASE: 0,
    REDIS_PASSWORD: undefined,
  },
  mongodb: {
    MONGO_NOMBRE_CONEXION: 'conexion_mongo',
    MONGO_TYPE: 'mongodb',
    MONGO_USE_NEW_URL_PARSER: true,
    MONGO_DROP_SCHEMA: true,
    MONGO_URL: `mongodb://usuario:12345678@127.0.0.1:30503/desarrollo?authSource=admin&useUnifiedTopology=true`,
    // MONGO_URL: `mongodb://usuario:password@localhost:30503/base_de_Datos?authSource=admin`,
    MONGO_USERNAME: 'adrianeguez',
    MONGO_PASSWORD: '12345678',
    MONGO_DATABASE: 'desarrollo',
    MONGO_HOST: 'localhost',
    MONGO_PORT: 30503,
  },
  session: {
    SESSION_PASSWORD: 'session_cookie_secret',
  },
  urls: {
    URL_PROTOCOLO: 'http',
    URL_IP: 'localhost',
    URL_PUERTO_ESCUCHA: 3000,
    URL_PUERTO_ESCUCHA_WEBSOCKETS: 3000,
    URL_SEGMENTO: '/',
    url() {
      return `${this.URL_PROTOCOLO}://${this.URL_IP}:${this.URL_PUERTO_ESCUCHA}${this.URL_SEGMENTO}`;
    },
    callbackUrl() {
      return this.url() + 'callback';
    },
    logoutUrl() {
      return this.url() + 'logout';
    },
    frontEndUrl() {
      return this.url() + 'front-app';
    },
    frontEndSegmento() {
      return this.url() + 'front-app';
    },
    urlWebSockets() {
      return `${this.URL_PROTOCOLO}://${this.URL_IP}:${this.URL_PUERTO_ESCUCHA_WEBSOCKETS}${this.URL_SEGMENTO}`;
    },
  },
  auth0: {
    AUTH0_CUENTA: 'prueba-submodulo-fear',
    AUTH0_CLIENT_ID: 'CpoH35FdTicK6UQfLU9ZBQ4AObeWZuuE',
    AUTH0_SECRET:
      'vgfZPxRycxpxDZxI9d2jzO4i3CbctcXWAbkooDsrvzcg34swYachQkF55ylwuF77',
    AUTH0_CLIENT_ID_REFRESH_TOKEN: '971tc3Ij27pb4O0R6rkykhTOFsMoreLU',
    AUTH0_SECRET_REFRESH_TOKEN:
      'FTFrFFSfOxPzKhCLOkBPAmQRvMZT5CkrVoFaadr31f3IFUIzZzoh4ibSDzCbpcFl',
    opcionesJWT: () => {
      if (this) {
        const contexto = this as any;
        return {
          secret: jwksRsa.expressJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 1000,
            jwksUri: `https://${contexto.CONFIG.auth0.AUTH0_CUENTA}.auth0.com/.well-known/jwks.json`,
          }),
          issuer: `https://${contexto.CONFIG.auth0.AUTH0_CUENTA}.auth0.com/`,
          algorithms: ['RS256'],
        };
      } else {
        return undefined;
      }
    },
  },
  auth0PasswordLogin: {
    client_id: '29TLH7m7DXzsvPR77jP2S5epjFWuLTAv',
    client_secret:
      '_To2VtezcD9yJ6s3j8bXiOxdI6X6nvu3CUE5Fpsz0ARToBwUVBJgx0_0RGu6MfLX',
  },
  oneSignal: {
    api_key: '',
    app_id: '',
  },
  paymentez: {
    client_app_code: '',
    client_app_key: '',
    server_app_code: '',
    server_app_key: '',
    url: '',
  },
  googleCloud: {
    url: 'https://storage.googleapis.com',
    bucket: '/archivos-prueba-manticore',
  },
  compresion: {
    COMPRESION_HABILITADO: false,
  },
  helmet: {
    HELMET_HABILITADO: false,
  },
  rateLimit: {
    RATE_LIMIT_HABILITADO: false,
    windowMs: 2 * 60 * 1000, // 2 minutes
    max: 10000, // limit each IP to 10000 requests per windowMs
    message:
      'Lo sentimos, demasiadas peticiones por el momento. Intentalo más tarde.',
  },
  firebase: {
    LLAVE: '',
  },
};
