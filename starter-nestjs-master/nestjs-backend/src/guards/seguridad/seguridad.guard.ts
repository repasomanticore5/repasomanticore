import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';

@Injectable()
export class SeguridadGuard implements CanActivate {
    // constructor(private readonly _seguridadService: SeguridadService) {}
    //
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request: any = context.switchToHttp().getRequest();
        return true  //this._seguridadService.validarPorRol(request);
    }
}
