import {Controller, Get, Req, Res, Session} from '@nestjs/common';

@Controller()
export class FrontendController {
    @Get('front-app')
    getHello(
        @Res() response,
        @Session() session
    ) {
        response.render('index', {
            usuario: session.user   ,
        })
    }
}
