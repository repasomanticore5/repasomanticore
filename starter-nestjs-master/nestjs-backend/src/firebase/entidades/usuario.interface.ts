import {UsuarioAbstractoProyectoInterface} from './abstractos/usuario-abstracto-proyecto.interface';
import {FieldValue, Timestamp} from '@firebase/firestore-types';

export interface UsuarioInterface extends UsuarioAbstractoProyectoInterface {
    createdAt?: Timestamp | number | FieldValue;
    updatedAt?: Timestamp | number | FieldValue;
}
