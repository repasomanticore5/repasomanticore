import {Injectable} from '@nestjs/common';
import {FirebaseService} from '../firebase.service';
import {auth} from 'firebase-admin/lib/auth';
import UserRecord = auth.UserRecord;
import {UsuarioInterface} from '../entidades/usuario.interface';
import {UsuarioFirebaseService} from '../../usuario-firebase/usuario-firebase.service';
import {FirestoreAuthAbstractService} from '@manticore-labs/firebase-nest';

@Injectable()
export class FirestoreAuthService extends FirestoreAuthAbstractService<UsuarioInterface> {
    constructor(
        private readonly _firebaseService: FirebaseService,
        private readonly _firestoreUsuarioService: UsuarioFirebaseService,
    ) {
        super(_firebaseService.firebaseAdmin as any);
    }

    crearUseryUsuario(
        usuario: UsuarioInterface,
        roles: string[] = [],
        camposExtra?: any,
    ): Promise<{ usuarioNuevo: UserRecord, usuarioEntidad: UsuarioInterface }> {
        let usuarioNuevo;
        let usuarioEntidad;
        return new Promise(
            (resolve, reject) => {
                this.createUser(usuario)
                    .then(
                        (data) => {
                            usuarioNuevo = data;
                            const uid = data.uid;
                            usuarioEntidad = {
                                roles,
                                uid,
                            };
                            return this._firestoreUsuarioService
                                .crear(
                                    {
                                        roles,
                                        usersUid: uid,
                                        ...camposExtra
                                    },
                                    usuario.email,
                                ) as Promise<UsuarioInterface>;
                        },
                    )
                    .then(
                        () => resolve({usuarioNuevo, usuarioEntidad}),
                    )
                    .catch(
                        (error) => reject(error),
                    );
            },
        );
    }
}
