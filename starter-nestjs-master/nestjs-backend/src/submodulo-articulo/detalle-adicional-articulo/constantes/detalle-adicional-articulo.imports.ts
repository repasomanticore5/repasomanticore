import { TypeOrmModule } from '@nestjs/typeorm';
import { DetalleAdicionalArticuloEntity } from '../detalle-adicional-articulo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const DETALLE_ADICIONAL_ARTICULO_IMPORTS = [
  TypeOrmModule.forFeature(
    [DetalleAdicionalArticuloEntity],
    NOMBRE_CADENA_CONEXION,
  ),
  SeguridadModule,
  AuditoriaModule,
];
