import { DetalleAdicionalArticuloCrearDto } from '../dto/detalle-adicional-articulo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const DETALLE_ADICIONAL_ARTICULO_DATOS_PRUEBA: ((
  idArticulo: number,
) => DetalleAdicionalArticuloCrearDto)[] = [
  (idArticulo: number) => {
    const dato = new DetalleAdicionalArticuloCrearDto();
    dato.articuloDAA = idArticulo;
    dato.nombre = 'detalle adicional';
    dato.valor = 'informatica';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.articuloDAA = idArticulo;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
