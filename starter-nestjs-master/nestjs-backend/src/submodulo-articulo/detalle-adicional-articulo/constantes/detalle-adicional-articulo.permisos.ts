import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_DETALLE_ADICIONAL_ARTICULO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'detalle-adicional-articulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/detalle-adicional-articulo/, // /detalle-adicional-articulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/detalle-adicional-articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /detalle-adicional-articulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/detalle-adicional-articulo\/\d+\/modificar-habilitado/, // /detalle-adicional-articulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/detalle-adicional-articulo\/\d+/, // /detalle-adicional-articulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
