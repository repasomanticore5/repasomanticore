import { Module } from '@nestjs/common';
import { DetalleAdicionalArticuloController } from './detalle-adicional-articulo.controller';
import { DETALLE_ADICIONAL_ARTICULO_IMPORTS } from './constantes/detalle-adicional-articulo.imports';
import { DETALLE_ADICIONAL_ARTICULO_PROVIDERS } from './constantes/detalle-adicional-articulo.providers';

@Module({
  imports: [...DETALLE_ADICIONAL_ARTICULO_IMPORTS],
  providers: [...DETALLE_ADICIONAL_ARTICULO_PROVIDERS],
  exports: [...DETALLE_ADICIONAL_ARTICULO_PROVIDERS],
  controllers: [DetalleAdicionalArticuloController],
})
export class DetalleAdicionalArticuloModule {}
