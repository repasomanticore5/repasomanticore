import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ArticuloEntity } from '../articulo/articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DETALLE_ADICIONAL_ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class DetalleAdicionalArticuloEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_DETALLE_ADICIONAL_ARTICULO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'valor',
    type: 'varchar',
    length: 60,
    nullable: false,
  })
  valor: string;


  @ManyToOne(
    () => ArticuloEntity,
    (articulo) => articulo.detallesAdicionalArticuloA,
    { nullable: false },
  )
  articuloDAA: ArticuloEntity | number;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
