import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { DetalleAdicionalArticuloEntity } from './detalle-adicional-articulo.entity';
import { DetalleAdicionalArticuloBusquedaDto } from './dto/detalle-adicional-articulo.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class DetalleAdicionalArticuloService extends ServicioComun<
  DetalleAdicionalArticuloEntity,
  DetalleAdicionalArticuloBusquedaDto
> {
  constructor(
    @InjectRepository(DetalleAdicionalArticuloEntity, NOMBRE_CADENA_CONEXION)
    public detalleAdicionalArticuloEntityRepository: Repository<DetalleAdicionalArticuloEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      detalleAdicionalArticuloEntityRepository,
      _connection,
      DetalleAdicionalArticuloEntity,
      'id',
      _auditoriaService,
      // Por defecto NO se transforma todos los campos a mayúsculas.
      // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
      // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
      (objetoATransformar, metodo) => objetoATransformar,
    );
  }
}
