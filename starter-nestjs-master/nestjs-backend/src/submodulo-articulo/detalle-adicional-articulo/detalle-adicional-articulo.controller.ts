import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {DetalleAdicionalArticuloService} from './detalle-adicional-articulo.service';
import {DetalleAdicionalArticuloHabilitadoDto} from './dto/detalle-adicional-articulo.habilitado.dto';
import {DETALLE_ADICIONAL_ARTICULO_OCC} from './constantes/detalle-adicional-articulo.occ';
import {DetalleAdicionalArticuloCrearDto} from './dto/detalle-adicional-articulo.crear.dto';
import {DetalleAdicionalArticuloActualizarDto} from './dto/detalle-adicional-articulo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {DetalleAdicionalArticuloEntity} from './detalle-adicional-articulo.entity';
import {DetalleAdicionalArticuloBusquedaDto} from './dto/detalle-adicional-articulo.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'detalle-adicional-articulo';

@Controller(nombreControlador)
export class DetalleAdicionalArticuloController extends ControladorComun {
    constructor(
        private readonly _detalleAdicionalArticuloService: DetalleAdicionalArticuloService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _detalleAdicionalArticuloService,
            _seguridadService,
            _auditoriaService,
            DETALLE_ADICIONAL_ARTICULO_OCC(
                DetalleAdicionalArticuloHabilitadoDto,
                DetalleAdicionalArticuloCrearDto,
                DetalleAdicionalArticuloActualizarDto,
                DetalleAdicionalArticuloBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: DetalleAdicionalArticuloBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            // QUERY BUILDER https://typeorm.io/#/select-query-builder
            const qb = this._detalleAdicionalArticuloService.detalleAdicionalArticuloEntityRepository
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<DetalleAdicionalArticuloEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._detalleAdicionalArticuloService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._detalleAdicionalArticuloService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<DetalleAdicionalArticuloEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({valor: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                consultaWhere = consulta.where as FindConditions<DetalleAdicionalArticuloEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                consulta.where = this.setearFiltro(
                    'articuloDAA',
                    consultaWhere,
                    parametrosConsulta.articuloDAA,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._detalleAdicionalArticuloService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            if (parametrosConsulta.findById) {
                qb.andWhere(aliasTabla + '.id = :id', {
                    id: parametrosConsulta.findById,
                });
            }
            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }
}
