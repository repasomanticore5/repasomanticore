import {IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class DetalleAdicionalArticuloActualizarDto {

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(100)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(60)
    @Expose()
    valor: string;

    @IsOptional()
    @IsNumber()
    @Expose()
    articuloDAA: number;
}
