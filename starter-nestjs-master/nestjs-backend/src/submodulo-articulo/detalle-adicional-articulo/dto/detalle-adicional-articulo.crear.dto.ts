import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class DetalleAdicionalArticuloCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(60)
  @Expose()
  valor: string;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  articuloDAA: number;
}
