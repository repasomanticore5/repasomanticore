import {
    Column,
    Entity,
    Index,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {SubgrupoEntity} from '../subgrupo/subgrupo.entity';
import {DetalleAdicionalArticuloEntity} from '../detalle-adicional-articulo/detalle-adicional-articulo.entity';
import {BusquedaPorArticuloEntity} from '../busqueda-por-articulo/busqueda-por-articulo.entity';
import {ArticuloEmpresaEntity} from '../../submodulo-articulo-empresa/articulo-empresa/articulo-empresa.entity';
import {EtiquetaPorArticuloEntity} from '../etiqueta-por-articulo/etiqueta-por-articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ArticuloEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_ARTICULO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'nombre',
        type: 'varchar',
        length: '100',
        nullable: false,
    })
    nombre: string;

    @Column({
        name: 'descripcion',
        type: 'varchar',
        length: '255',
        nullable: true,
    })
    descripcion?: string;

    @Column({
        name: 'codigo',
        type: 'varchar',
        length: '120',
        nullable: false,
    })
    codigo: string;

    @Column({
        name: 'codigo_auxiliar',
        type: 'varchar',
        length: '120',
        nullable: true,
    })
    codigoAuxiliar?: string;

    @Column({
        name: 'marca',
        type: 'varchar',
        length: '120',
        nullable: true,
    })
    marca?: string;

    @Column({
        name: 'modelo',
        type: 'varchar',
        length: '120',
        nullable: true,
    })
    modelo?: string;

    @Column({
        name: 'anio_fabrica',
        type: 'int',
        nullable: true,
    })
    anioFabrica?: number;

    @Column({
        name: 'fabricante',
        type: 'varchar',
        length: '120',
        nullable: true,
    })
    fabricante?: string;

    @Column({
        name: 'es_servicio',
        type: 'tinyint',
        nullable: true,
        default: 0,
    })
    esServicio?: 0 | 1;

    @Column({
        name: 'dimension_ancho',
        type: 'decimal',
        scale: 8,
        precision: 10,
        nullable: true,
    })
    dimensionAncho?: number;

    @Column({
        name: 'dim_largo',
        type: 'decimal',
        scale: 8,
        precision: 10,
        nullable: true,
    })
    dimensionLargo?: number;

    @Column({
        name: 'dimension_profundidad',
        type: 'decimal',
        scale: 8,
        precision: 10,
        nullable: true,
    })
    dimensionProfunidad?: number;

    @Column({
        name: 'peso',
        type: 'decimal',
        scale: 8,
        precision: 10,
        nullable: true,
    })
    peso?: number;

    @Column({
        name: 'vida_util',
        type: 'decimal',
        scale: 8,
        precision: 10,
        nullable: true,
    })
    vidaUtil?: number;

    // no se que van de aqui en adelante el lucid dice autocomplete
    // @Column({
    //   name: 'dimension_ancho_medida',
    //   type: 'varchar',
    //   length: '100',
    //   nullable: true,
    // })
    // dimensionAnchoMedida?: string;
    //
    // @Column({
    //   name: 'dimension_largo_medida',
    //   type: 'varchar',
    //   length: '100',
    //   nullable: true,
    // })
    // dimensionLargoMedida?: string;
    //
    // @Column({
    //   name: 'dimension_profundidad_medida',
    //   type: 'varchar',
    //   length: '100',
    //   nullable: true,
    // })
    // dimensionProfundidadMedida?: string;
    //
    // @Column({
    //   name: 'peso_medida',
    //   type: 'varchar',
    //   length: '100',
    //   nullable: true,
    // })
    // pesoMedida?: string;
    //
    // @Column({
    //   name: 'vida_util_medida',
    //   type: 'varchar',
    //   length: '100',
    //   nullable: true,
    // })
    // vidaUtilMedida?: string;

    @ManyToOne(() => SubgrupoEntity, (subgrupo) => subgrupo.articulosS, {
        nullable: false,
    })
    subgrupoA: SubgrupoEntity | number;

    @OneToMany(
        () => DetalleAdicionalArticuloEntity,
        (detalleAdicionalArticulo) => detalleAdicionalArticulo.articuloDAA,
    )
    detallesAdicionalArticuloA: DetalleAdicionalArticuloEntity[];

    @OneToMany(
        () => BusquedaPorArticuloEntity,
        (busquedaPorArticulo) => busquedaPorArticulo.articuloBPA,
    )
    busquedasPorArticuloA: BusquedaPorArticuloEntity[];

    @OneToMany(
        () => ArticuloEmpresaEntity,
        (articuloEmpresa) => articuloEmpresa.articuloAE,
    )
    articulosEmpresaA: ArticuloEmpresaEntity[];

    @OneToMany(
        () => EtiquetaPorArticuloEntity,
        (etiquetaPorArticulo) => etiquetaPorArticulo.articuloEPA,
    )
    etiquetasPorArticuloA: EtiquetaPorArticuloEntity[];
}
