import { Module } from '@nestjs/common';
import { ArticuloController } from './articulo.controller';
import { ARTICULO_IMPORTS } from './constantes/articulo.imports';
import { ARTICULO_PROVIDERS } from './constantes/articulo.providers';

@Module({
  imports: [...ARTICULO_IMPORTS],
  providers: [...ARTICULO_PROVIDERS],
  exports: [...ARTICULO_PROVIDERS],
  controllers: [ArticuloController],
})
export class ArticuloModule {}
