import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class ArticuloCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsNumber()
  @Expose()
  subgrupoA: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigo: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigoAuxiliar: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  marca: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  modelo: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  anioFabrica: number;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  fabricante: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  dimensionAncho: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  dimensionLargo: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  dimensionProfundidad: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  peso: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  vidaUtil: number;

  // faltan dtos de los autocompletes.
}
