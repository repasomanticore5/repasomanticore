import { ArticuloCrearDto } from '../dto/articulo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ARTICULO_DATOS_PRUEBA: ((
  idSubgrupo: number,
) => ArticuloCrearDto)[] = [
  (idSubgrupo: number) => {
    const dato = new ArticuloCrearDto();
    dato.nombre = 'Intel Core i9-10900K 3.7 GHz';
    dato.codigo = 'ART001' + idSubgrupo;
    dato.subgrupoA = idSubgrupo;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idSubgrupo: number) => {
    const dato = new ArticuloCrearDto();
    dato.nombre = 'IAMD Ryzen 9 3950X 3.5 GHz';
    dato.codigo = 'ART002' + idSubgrupo;
    dato.subgrupoA = idSubgrupo;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
