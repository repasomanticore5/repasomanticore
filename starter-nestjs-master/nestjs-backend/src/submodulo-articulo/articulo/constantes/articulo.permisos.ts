import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_ARTICULO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'articulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/articulo/, // /articulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /articulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/articulo\/\d+\/modificar-habilitado/, // /articulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/articulo\/\d+/, // /articulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
