import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticuloEntity } from '../articulo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { SeguridadModule } from '../../../seguridad/seguridad.module';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';

export const ARTICULO_IMPORTS = [
  TypeOrmModule.forFeature([ArticuloEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
