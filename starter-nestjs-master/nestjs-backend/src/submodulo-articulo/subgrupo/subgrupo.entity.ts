import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { GrupoEntity } from '../grupo/grupo.entity';
import { ArticuloEntity } from '../articulo/articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'SUBGRUPO')
@Index([
  'sisHabilitado',
  'sisCreado',
  'sisModificado',
  'codigo',
  'codigoAuxiliar',
  'nombre',
])
export class SubgrupoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_SUBGRUPO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: 120,
    nullable: false,
    unique: true,
  })
  codigo: string;

  @Column({
    name: 'codigo_auxiliar',
    type: 'varchar',
    length: 120,
    nullable: true,
    unique: true,
  })
  codigoAuxiliar: string;

  @ManyToOne(() => GrupoEntity, (grupo) => grupo.subgruposG, {
    nullable: false,
  })
  grupoS: GrupoEntity | number;

  @OneToMany(() => ArticuloEntity, (articulo) => articulo.subgrupoA)
  articulosS: ArticuloEntity[];
}
