import { SubgrupoCrearDto } from '../dto/subgrupo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const SUBGRUPO_DATOS_PRUEBA: ((
  idGrupo: number,
) => SubgrupoCrearDto)[] = [
  (idGrupo: number) => {
    const dato = new SubgrupoCrearDto();
    dato.nombre = 'informatica';
    dato.codigo = 'SGR001';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.grupoS = idGrupo;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
