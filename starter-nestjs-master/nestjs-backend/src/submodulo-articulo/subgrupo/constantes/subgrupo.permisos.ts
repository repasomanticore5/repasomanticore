import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_SUBGRUPO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'subgrupo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/subgrupo/, // /subgrupo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/subgrupo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /subgrupo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/subgrupo\/\d+\/modificar-habilitado/, // /subgrupo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/subgrupo\/\d+/, // /subgrupo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
