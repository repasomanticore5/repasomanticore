import { TypeOrmModule } from '@nestjs/typeorm';
import { SubgrupoEntity } from '../subgrupo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { SeguridadModule } from '../../../seguridad/seguridad.module';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';

export const SUBGRUPO_IMPORTS = [
  TypeOrmModule.forFeature([SubgrupoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
