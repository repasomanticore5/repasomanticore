import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class SubgrupoActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(100)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(120)
    @Expose()
    codigo: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(120)
    @Expose()
    codigoAuxiliar: string;

    @IsOptional()
    @IsNumber()
    @Expose()
    grupoS: number;

}
