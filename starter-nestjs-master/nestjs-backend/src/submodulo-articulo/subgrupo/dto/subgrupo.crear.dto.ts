import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class SubgrupoCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigo: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(120)
  @Expose()
  codigoAuxiliar: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  grupoS: number;
}
