import { Module } from '@nestjs/common';
import { SubgrupoController } from './subgrupo.controller';
import { SUBGRUPO_IMPORTS } from './constantes/subgrupo.imports';
import { SUBGRUPO_PROVIDERS } from './constantes/subgrupo.providers';

@Module({
  imports: [...SUBGRUPO_IMPORTS],
  providers: [...SUBGRUPO_PROVIDERS],
  exports: [...SUBGRUPO_PROVIDERS],
  controllers: [SubgrupoController],
})
export class SubgrupoModule {}
