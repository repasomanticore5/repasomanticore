import { ArticuloModule } from '../articulo/articulo.module';
import { GrupoModule } from '../grupo/grupo.module';
import { SubgrupoModule } from '../subgrupo/subgrupo.module';
import { BusquedaArticuloTipoModule } from '../busqueda-articulo-tipo/busqueda-articulo-tipo.module';
import { BusquedaArticuloValorModule } from '../busqueda-articulo-valor/busqueda-articulo-valor.module';
import { BusquedaPorArticuloModule } from '../busqueda-por-articulo/busqueda-por-articulo.module';
import { EtiquetaArticuloModule } from '../etiqueta-articulo/etiqueta-articulo.module';
import { EtiquetaPorArticuloModule } from '../etiqueta-por-articulo/etiqueta-por-articulo.module';
import { DetalleAdicionalArticuloModule } from '../detalle-adicional-articulo/detalle-adicional-articulo.module';
import { BusquedaXContactoEmpresaModule } from '../busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.module';

export const MODULOS_ARTICULO = [
  ArticuloModule,
  GrupoModule,
  SubgrupoModule,
  BusquedaArticuloTipoModule,
  BusquedaArticuloValorModule,
  BusquedaPorArticuloModule,
  EtiquetaArticuloModule,
  EtiquetaPorArticuloModule,
  DetalleAdicionalArticuloModule,
  BusquedaXContactoEmpresaModule,
];
