import { ArticuloEntity } from '../articulo/articulo.entity';
import { GrupoEntity } from '../grupo/grupo.entity';
import { SubgrupoEntity } from '../subgrupo/subgrupo.entity';
import { BusquedaArticuloTipoEntity } from '../busqueda-articulo-tipo/busqueda-articulo-tipo.entity';
import { BusquedaArticuloValorEntity } from '../busqueda-articulo-valor/busqueda-articulo-valor.entity';
import { BusquedaPorArticuloEntity } from '../busqueda-por-articulo/busqueda-por-articulo.entity';
import { EtiquetaArticuloEntity } from '../etiqueta-articulo/etiqueta-articulo.entity';
import { EtiquetaPorArticuloEntity } from '../etiqueta-por-articulo/etiqueta-por-articulo.entity';
import { DetalleAdicionalArticuloEntity } from '../detalle-adicional-articulo/detalle-adicional-articulo.entity';
import { BusquedaXContactoEmpresaEntity } from '../busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.entity';

export const ENTITIES_ARTICULO = [
  ArticuloEntity,
  GrupoEntity,
  SubgrupoEntity,
  BusquedaArticuloTipoEntity,
  BusquedaArticuloValorEntity,
  BusquedaPorArticuloEntity,
  EtiquetaArticuloEntity,
  EtiquetaPorArticuloEntity,
  DetalleAdicionalArticuloEntity,
  BusquedaXContactoEmpresaEntity,
];
