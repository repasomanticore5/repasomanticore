export const CONFIG_DATOS_PRUEBA_ARTICULO = {
  submoduloArticulo: true,
  grupo: true,
  subgrupo: true,
  articulo: true,
  detalleAdicionalArticulo: true,
  busquedaArticuloTipo: true,
  busquedaArticuloValor: true,
  busquedaPorArticulo: true,
  etiquetaArticulo: true,
  etiquetaPorArticulo: true,
  busquedaXContactoEmpresa: true,
};
