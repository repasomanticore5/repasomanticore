import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class BusquedaArticuloTipoActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(100)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;
}
