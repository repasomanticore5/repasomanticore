import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class BusquedaArticuloTipoCrearDto extends HabilitadoDtoComun {
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(100)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;
}
