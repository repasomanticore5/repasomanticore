import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_BUSQUEDA_ARTICULO_TIPO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'busqueda-articulo-tipo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/busqueda-articulo-tipo/, // /busqueda-articulo-tipo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/busqueda-articulo-tipo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /busqueda-articulo-tipo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-articulo-tipo\/\d+\/modificar-habilitado/, // /busqueda-articulo-tipo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-articulo-tipo\/\d+/, // /busqueda-articulo-tipo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
