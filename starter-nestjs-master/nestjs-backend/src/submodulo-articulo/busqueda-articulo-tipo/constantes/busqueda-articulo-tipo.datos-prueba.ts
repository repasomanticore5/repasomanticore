import { BusquedaArticuloTipoCrearDto } from '../dto/busqueda-articulo-tipo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const BUSQUEDA_ARTICULO_TIPO_DATOS_PRUEBA: (() => BusquedaArticuloTipoCrearDto)[] = [
  () => {
    const dato = new BusquedaArticuloTipoCrearDto();
    dato.nombre = 'velocidad';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
