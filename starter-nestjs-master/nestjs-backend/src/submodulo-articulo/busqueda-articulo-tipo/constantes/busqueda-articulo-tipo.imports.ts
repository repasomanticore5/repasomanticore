import { TypeOrmModule } from '@nestjs/typeorm';
import { BusquedaArticuloTipoEntity } from '../busqueda-articulo-tipo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const BUSQUEDA_ARTICULO_TIPO_IMPORTS = [
  TypeOrmModule.forFeature(
    [BusquedaArticuloTipoEntity],
    NOMBRE_CADENA_CONEXION,
  ),
  SeguridadModule,
  AuditoriaModule,
];
