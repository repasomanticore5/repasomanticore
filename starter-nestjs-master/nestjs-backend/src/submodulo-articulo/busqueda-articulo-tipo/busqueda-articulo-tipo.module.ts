import { Module } from '@nestjs/common';
import { BusquedaArticuloTipoController } from './busqueda-articulo-tipo.controller';
import { BUSQUEDA_ARTICULO_TIPO_IMPORTS } from './constantes/busqueda-articulo-tipo.imports';
import { BUSQUEDA_ARTICULO_TIPO_PROVIDERS } from './constantes/busqueda-articulo-tipo.providers';

@Module({
  imports: [...BUSQUEDA_ARTICULO_TIPO_IMPORTS],
  providers: [...BUSQUEDA_ARTICULO_TIPO_PROVIDERS],
  exports: [...BUSQUEDA_ARTICULO_TIPO_PROVIDERS],
  controllers: [BusquedaArticuloTipoController],
})
export class BusquedaArticuloTipoModule {}
