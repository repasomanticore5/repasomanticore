import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { BusquedaArticuloValorEntity } from '../busqueda-articulo-valor/busqueda-articulo-valor.entity';
import { BusquedaXContactoEmpresaEntity } from '../busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BUSQUEDA_ARTICULO_TIPO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BusquedaArticuloTipoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_BUSQUEDA_ARTICULO_TIPO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string;


  @OneToMany(
      () => BusquedaArticuloValorEntity,
      (busquedaArticuloValor) => busquedaArticuloValor.busquedaArticuloTipoBAV,
  )
  busquedasArticuloValorBAT: BusquedaArticuloValorEntity[];

  @OneToMany(
      () => BusquedaXContactoEmpresaEntity,
      (busquedaXContactoEmpresa) =>
          busquedaXContactoEmpresa.busquedaArticuloTipoBXCE,
  )
  busquedasXContactoEmpresaBAT: BusquedaXContactoEmpresaEntity[];
}
