import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get,
    HttpCode, InternalServerErrorException, Param, Post,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {EtiquetaPorArticuloService} from './etiqueta-por-articulo.service';
import {EtiquetaPorArticuloHabilitadoDto} from './dto/etiqueta-por-articulo.habilitado.dto';
import {ETIQUETA_POR_ARTICULO_OCC} from './constantes/etiqueta-por-articulo.occ';
import {EtiquetaPorArticuloCrearDto} from './dto/etiqueta-por-articulo.crear.dto';
import {EtiquetaPorArticuloActualizarDto} from './dto/etiqueta-por-articulo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {EtiquetaPorArticuloEntity} from './etiqueta-por-articulo.entity';
import {EtiquetaPorArticuloBusquedaDto} from './dto/etiqueta-por-articulo.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'etiqueta-por-articulo';

@Controller(nombreControlador)
export class EtiquetaPorArticuloController extends ControladorComun {
    constructor(
        private readonly _etiquetaPorArticuloService: EtiquetaPorArticuloService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _etiquetaPorArticuloService,
            _seguridadService,
            _auditoriaService,
            ETIQUETA_POR_ARTICULO_OCC(
                EtiquetaPorArticuloHabilitadoDto,
                EtiquetaPorArticuloCrearDto,
                EtiquetaPorArticuloActualizarDto,
                EtiquetaPorArticuloBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: EtiquetaPorArticuloBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._etiquetaPorArticuloService.etiquetaPorArticuloEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<EtiquetaPorArticuloEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._etiquetaPorArticuloService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._etiquetaPorArticuloService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<EtiquetaPorArticuloEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push(
                //         {
                //             etiquetaArticuloEPA: {
                //                 nombre: Like('%' + parametrosConsulta.busqueda + '%')
                //             }
                //         }
                //     );
                //     consulta.where = consultaWhere;
                // }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<EtiquetaPorArticuloEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                consulta.where = this.setearFiltro(
                    'articuloEPA',
                    consultaWhere,
                    parametrosConsulta.articuloEPA,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._etiquetaPorArticuloService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            qb.leftJoinAndSelect(aliasTabla + '.etiquetaArticuloEPA', 'etiquetaArticulo');
            // qb.leftJoinAndSelect(aliasTabla + '.articuloEPA', 'articulo');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            if (parametrosConsulta.findById) {
                qb.andWhere(aliasTabla + '.id = :id', {
                    id: parametrosConsulta.findById,
                });
            }
            //     consultaWhere.push(
            //         {
            //             etiquetaArticuloEPA: {
            //                 nombre: Like('%' + parametrosConsulta.busqueda + '%')
            //             }
            //         }
            //     );

            if (parametrosConsulta.busqueda) {
                qb.andWhere('etiquetaArticulo.nombre LIKE :nombreA', {
                    nombreA: `%${parametrosConsulta.busqueda}%`,
                })
            }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }

    @Post('save-many/etiquetas-por-articulo')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async saveManyEtiquetasPorArticulo(
        @Body('etiquetaPorArticulo') etiquetaPorArticulo: EtiquetaPorArticuloCrearDto[],
    ) {
        if (etiquetaPorArticulo.length <= 0) {
            console.error({
                mensaje: 'Error al guardar varias etiquetas por articulo',
                data: etiquetaPorArticulo,
            });
            throw new BadRequestException({
                mensaje: 'Error al guardar varias etiquetas por articulo',
            });
        }

        try {
            return await this._etiquetaPorArticuloService
                .saveManyEtiquetasPorArticulo(etiquetaPorArticulo);
        } catch (e) {
            console.error({
                mensaje: 'Error al guardar varias etiquetas por articulo',
                errror: e,
                data: etiquetaPorArticulo,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al guardar varias etiquetas por articulo',
            });
        }
    }

    @Delete(':idEtiquetaPorArticulo')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async eliminarEtiquetaPorArticulo(
        @Param('idEtiquetaPorArticulo') id: string,
    ) {
        if (!id) {
            console.error({
                mensaje: 'Error al eliminar etiqueta por articulo',
                data: id,
            });
            throw new BadRequestException({
                mensaje: 'Error al eliminar etiqueta por articulo',
            });
        }

        try {
            return await this._etiquetaPorArticuloService
                .eliminarEtiquetaPorArticulo(+id);
        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar etiqueta por articulo',
                errror: e,
                data: id,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar etiqueta por articulo',
            });
        }
    }
}
