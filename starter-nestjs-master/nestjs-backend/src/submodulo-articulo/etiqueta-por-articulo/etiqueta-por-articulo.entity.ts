import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EtiquetaArticuloEntity } from '../etiqueta-articulo/etiqueta-articulo.entity';
import {ArticuloEntity} from '../articulo/articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ETIQUETA_POR_ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class EtiquetaPorArticuloEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_ETIQUETA_POR_ARTICULO',
    unsigned: true,
  })
  id: number;


  @ManyToOne(
      () => EtiquetaArticuloEntity,
      (etiquetaArticulo) => etiquetaArticulo.etiquetasPorArticuloEA,
      { nullable: false },
  )
  etiquetaArticuloEPA: EtiquetaArticuloEntity | number;

  @ManyToOne(
      () => ArticuloEntity,
      (articulo) => articulo.etiquetasPorArticuloA,
      { nullable: false },
  )
  articuloEPA: ArticuloEntity | number;

}
