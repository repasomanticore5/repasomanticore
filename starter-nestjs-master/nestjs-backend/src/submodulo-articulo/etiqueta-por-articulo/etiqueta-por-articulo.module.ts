import { Module } from '@nestjs/common';
import { EtiquetaPorArticuloController } from './etiqueta-por-articulo.controller';
import { ETIQUETA_POR_ARTICULO_IMPORTS } from './constantes/etiqueta-por-articulo.imports';
import { ETIQUETA_POR_ARTICULO_PROVIDERS } from './constantes/etiqueta-por-articulo.providers';

@Module({
  imports: [...ETIQUETA_POR_ARTICULO_IMPORTS],
  providers: [...ETIQUETA_POR_ARTICULO_PROVIDERS],
  exports: [...ETIQUETA_POR_ARTICULO_PROVIDERS],
  controllers: [EtiquetaPorArticuloController],
})
export class EtiquetaPorArticuloModule {}
