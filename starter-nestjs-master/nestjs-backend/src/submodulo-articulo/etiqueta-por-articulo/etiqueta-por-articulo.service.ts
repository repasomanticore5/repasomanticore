import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {EtiquetaPorArticuloEntity} from './etiqueta-por-articulo.entity';
import {EtiquetaPorArticuloBusquedaDto} from './dto/etiqueta-por-articulo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {EtiquetaPorArticuloCrearDto} from './dto/etiqueta-por-articulo.crear.dto';

@Injectable()
export class EtiquetaPorArticuloService extends ServicioComun<EtiquetaPorArticuloEntity,
    EtiquetaPorArticuloBusquedaDto> {
    constructor(
        @InjectRepository(EtiquetaPorArticuloEntity, NOMBRE_CADENA_CONEXION)
        public etiquetaPorArticuloEntityRepository: Repository<EtiquetaPorArticuloEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            etiquetaPorArticuloEntityRepository,
            _connection,
            EtiquetaPorArticuloEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar,
        );
    }

    async saveManyEtiquetasPorArticulo(etiquetaPorArticulo: EtiquetaPorArticuloCrearDto[]): Promise<EtiquetaPorArticuloEntity[]> {
        try {

            const respuestaGuardar = await this.etiquetaPorArticuloEntityRepository
                .save(etiquetaPorArticulo);
            const busqueda = respuestaGuardar.map(
                async (resp) => {
                    const etiquetaPorArticuloCreada = await this.etiquetaPorArticuloEntityRepository
                        .createQueryBuilder('etiquetaPorArticulo')
                        .leftJoinAndSelect(
                            'etiquetaPorArticulo.etiquetaArticuloEPA',
                            'etiquetaArticulo',
                            'etiquetaPorArticulo.etiquetaArticuloEPA = etiquetaArticulo.id',
                        )
                        .where('etiquetaPorArticulo.id = :idA', {idA: resp.id})
                        .getOne();
                    return {
                        ...etiquetaPorArticuloCreada,
                        habilitado: true,
                    }
                }
            );
            return await Promise.all(busqueda);
        } catch (e) {
            console.error({
                mensaje: 'Error al guardar varias etiquetas por articulo',
                errror: e,
                data: etiquetaPorArticulo,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al guardar varias etiquetas por articulo',
            });
        }
    }

    async eliminarEtiquetaPorArticulo(id: number): Promise<{ mensaje: string }> {
        try {
            await this.etiquetaPorArticuloEntityRepository
                .createQueryBuilder('etiquetaPorArticulo')
                .delete()
                .where('id = :idA', {idA: id})
                .execute();
            return {
                mensaje: 'Ok',
            }
        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar etiqueta por articulo',
                errror: e,
                data: id,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar etiqueta por articulo',
            });
        }
    }
}
