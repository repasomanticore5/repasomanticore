import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_ETIQUETA_POR_ARTICULO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'etiqueta-por-articulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/etiqueta-por-articulo/, // /etiqueta-por-articulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/etiqueta-por-articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /etiqueta-por-articulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/etiqueta-por-articulo\/\d+\/modificar-habilitado/, // /etiqueta-por-articulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/etiqueta-por-articulo\/\d+/, // /etiqueta-por-articulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
