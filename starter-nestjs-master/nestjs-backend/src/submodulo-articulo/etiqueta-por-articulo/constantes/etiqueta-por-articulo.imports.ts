import { TypeOrmModule } from '@nestjs/typeorm';
import { EtiquetaPorArticuloEntity } from '../etiqueta-por-articulo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const ETIQUETA_POR_ARTICULO_IMPORTS = [
  TypeOrmModule.forFeature([EtiquetaPorArticuloEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
