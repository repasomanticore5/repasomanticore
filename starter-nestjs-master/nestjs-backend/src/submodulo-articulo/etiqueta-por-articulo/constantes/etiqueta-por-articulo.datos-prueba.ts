import { EtiquetaPorArticuloCrearDto } from '../dto/etiqueta-por-articulo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ETIQUETA_POR_ARTICULO_DATOS_PRUEBA: ((
  idEtiquetaArticulo: number,
  idArticulo: number,
) => EtiquetaPorArticuloCrearDto)[] = [
  (idEtiquetaArticulo: number, idArticulo: number) => {
    const dato = new EtiquetaPorArticuloCrearDto();
    dato.etiquetaArticuloEPA = idEtiquetaArticulo;
    dato.articuloEPA = idArticulo;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
