import {IsInt, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class EtiquetaPorArticuloCrearDto extends HabilitadoDtoComun {

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  articuloEPA: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  etiquetaArticuloEPA: number;
}
