import {IsInt, IsNotEmpty, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class EtiquetaPorArticuloActualizarDto {

    @IsOptional()
    @IsInt()
    @Expose()
    etiquetaArticuloEPA?: number;

    @IsOptional()
    @IsInt()
    @Expose()
    articuloEPA?: number;
}
