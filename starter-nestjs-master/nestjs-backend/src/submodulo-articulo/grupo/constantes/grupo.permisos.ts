import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_GRUPO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'grupo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/grupo/, // /grupo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/grupo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /grupo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/grupo\/\d+\/modificar-habilitado/, // /grupo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/grupo\/\d+/, // /grupo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
