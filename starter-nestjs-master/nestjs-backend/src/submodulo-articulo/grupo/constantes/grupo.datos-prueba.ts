import { GrupoCrearDto } from '../dto/grupo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const GRUPO_DATOS_PRUEBA: (() => GrupoCrearDto)[] = [
  () => {
    const dato = new GrupoCrearDto();
    dato.nombre = 'tecnologia';
    dato.codigo = 'GRP001';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
