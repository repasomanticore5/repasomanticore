import { Module } from '@nestjs/common';
import { GrupoController } from './grupo.controller';
import { GRUPO_IMPORTS } from './constantes/grupo.imports';
import { GRUPO_PROVIDERS } from './constantes/grupo.providers';

@Module({
  imports: [...GRUPO_IMPORTS],
  providers: [...GRUPO_PROVIDERS],
  exports: [...GRUPO_PROVIDERS],
  controllers: [GrupoController],
})
export class GrupoModule {}
