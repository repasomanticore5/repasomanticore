import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { SubgrupoEntity } from '../subgrupo/subgrupo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'GRUPO')
@Index([
  'sisHabilitado',
  'sisCreado',
  'sisModificado',
  'codigo',
  'codigoAuxiliar',
  'nombre',
])
export class GrupoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_GRUPO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: 120,
    nullable: false,
    unique: true,
  })
  codigo: string;

  @Column({
    name: 'codigo_auxiliar',
    type: 'varchar',
    length: 120,
    nullable: true,
    unique: true,
  })
  codigoAuxiliar: string;

  @OneToMany(() => SubgrupoEntity, (subgrupo) => subgrupo.grupoS)
  subgruposG: SubgrupoEntity[];
}
