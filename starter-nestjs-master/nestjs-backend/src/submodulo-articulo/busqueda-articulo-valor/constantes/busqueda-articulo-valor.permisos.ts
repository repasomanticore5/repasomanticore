import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_BUSQUEDA_ARTICULO_VALOR: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'busqueda-articulo-valor';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/busqueda-articulo-valor/, // /busqueda-articulo-valor
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/busqueda-articulo-valor\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /busqueda-articulo-valor?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-articulo-valor\/\d+\/modificar-habilitado/, // /busqueda-articulo-valor/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-articulo-valor\/\d+/, // /busqueda-articulo-valor/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
