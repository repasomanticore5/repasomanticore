import { BusquedaArticuloValorCrearDto } from '../dto/busqueda-articulo-valor.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const BUSQUEDA_ARTICULO_VALOR_DATOS_PRUEBA: ((
  idBusquedaArticuloTipo: number,
) => BusquedaArticuloValorCrearDto)[] = [
  (idBusquedaArticuloTipo: number) => {
    const dato = new BusquedaArticuloValorCrearDto();
    dato.valor = '3.7 GHz';
    dato.busquedaArticuloTipoBAV = idBusquedaArticuloTipo;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
