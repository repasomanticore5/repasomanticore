import { TypeOrmModule } from '@nestjs/typeorm';
import { BusquedaArticuloValorEntity } from '../busqueda-articulo-valor.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const BUSQUEDA_ARTICULO_VALOR_IMPORTS = [
  TypeOrmModule.forFeature(
    [BusquedaArticuloValorEntity],
    NOMBRE_CADENA_CONEXION,
  ),
  SeguridadModule,
  AuditoriaModule,
];
