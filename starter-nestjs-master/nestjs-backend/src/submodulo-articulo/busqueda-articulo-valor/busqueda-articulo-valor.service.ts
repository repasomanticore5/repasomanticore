import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { BusquedaArticuloValorEntity } from './busqueda-articulo-valor.entity';
import { BusquedaArticuloValorBusquedaDto } from './dto/busqueda-articulo-valor.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class BusquedaArticuloValorService extends ServicioComun<
  BusquedaArticuloValorEntity,
  BusquedaArticuloValorBusquedaDto
> {
  constructor(
    @InjectRepository(BusquedaArticuloValorEntity, NOMBRE_CADENA_CONEXION)
    public busquedaArticuloValorEntityRepository: Repository<BusquedaArticuloValorEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      busquedaArticuloValorEntityRepository,
      _connection,
      BusquedaArticuloValorEntity,
      'id',
      _auditoriaService,
      // Por defecto NO se transforma todos los campos a mayúsculas.
      // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
      // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
      (objetoATransformar, metodo) => objetoATransformar,
    );
  }
}
