import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { BusquedaArticuloTipoEntity } from '../busqueda-articulo-tipo/busqueda-articulo-tipo.entity';
import { BusquedaPorArticuloEntity } from '../busqueda-por-articulo/busqueda-por-articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BUSQUEDA_ARTICULO_VALOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BusquedaArticuloValorEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_BUSQUEDA_ARTICULO_VALOR',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'valor',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  valor: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string;

  @ManyToOne(
    () => BusquedaArticuloTipoEntity,
    (busquedaArticuloTipo) => busquedaArticuloTipo.busquedasArticuloValorBAT,
    { nullable: false },
  )
  busquedaArticuloTipoBAV: BusquedaArticuloTipoEntity | number;

  @OneToMany(
    () => BusquedaPorArticuloEntity,
    (busquedaPorArticulo) => busquedaPorArticulo.busquedaArticuloValorBPA,
  )
  busquedasPorArticuloBAV: BusquedaPorArticuloEntity[];
}
