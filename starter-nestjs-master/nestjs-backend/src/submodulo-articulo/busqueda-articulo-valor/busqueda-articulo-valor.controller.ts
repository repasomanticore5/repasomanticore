import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {BusquedaArticuloValorService} from './busqueda-articulo-valor.service';
import {BusquedaArticuloValorHabilitadoDto} from './dto/busqueda-articulo-valor.habilitado.dto';
import {BUSQUEDA_ARTICULO_VALOR_OCC} from './constantes/busqueda-articulo-valor.occ';
import {BusquedaArticuloValorCrearDto} from './dto/busqueda-articulo-valor.crear.dto';
import {BusquedaArticuloValorActualizarDto} from './dto/busqueda-articulo-valor.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {BusquedaArticuloValorEntity} from './busqueda-articulo-valor.entity';
import {BusquedaArticuloValorBusquedaDto} from './dto/busqueda-articulo-valor.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'busqueda-articulo-valor';

@Controller(nombreControlador)
export class BusquedaArticuloValorController extends ControladorComun {
    constructor(
        private readonly _busquedaArticuloValorService: BusquedaArticuloValorService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _busquedaArticuloValorService,
            _seguridadService,
            _auditoriaService,
            BUSQUEDA_ARTICULO_VALOR_OCC(
                BusquedaArticuloValorHabilitadoDto,
                BusquedaArticuloValorCrearDto,
                BusquedaArticuloValorActualizarDto,
                BusquedaArticuloValorBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: BusquedaArticuloValorBusquedaDto,
        @Req() request,
    ) {
        console.log(parametrosConsulta);
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            // QUERY BUILDER https://typeorm.io/#/select-query-builder
            const qb = this._busquedaArticuloValorService.busquedaArticuloValorEntityRepository
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<BusquedaArticuloValorEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._busquedaArticuloValorService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._busquedaArticuloValorService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<BusquedaArticuloValorEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({
                        valor: Like('%' + parametrosConsulta.busqueda + '%'),
                    });
                    consulta.where = consultaWhere;
                }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<BusquedaArticuloValorEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                consulta.where = this.setearFiltro(
                    'busquedaArticuloTipoBAV',
                    consultaWhere,
                    parametrosConsulta.busquedaArticuloTipoBAV,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._busquedaArticuloValorService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            qb.leftJoinAndSelect(aliasTabla + '.busquedaArticuloTipoBAV', 'busquedaArticuloTipo');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            if (parametrosConsulta.idsBusquedaArtValor) {
                const {ids} = JSON.parse(parametrosConsulta.idsBusquedaArtValor);
                if (ids.length > 0) {
                    qb.andWhere(aliasTabla + `.id NOT IN (${ids})`)
                }
            }

            if (parametrosConsulta.findById) {
                qb.andWhere(aliasTabla + '.id = :id', {
                    id: parametrosConsulta.findById,
                });
            }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }
}
