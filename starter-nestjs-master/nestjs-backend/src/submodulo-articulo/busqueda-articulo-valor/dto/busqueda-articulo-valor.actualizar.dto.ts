import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class BusquedaArticuloValorActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(100)
    @Expose()
    valor: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;

    @IsOptional()
    @IsNumber()
    @Expose()
    busquedaArticuloTipoBAV: number;
}
