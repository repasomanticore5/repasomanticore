import {IsNumberString, IsOptional, IsString} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class BusquedaArticuloValorBusquedaDto extends BusquedaComunProyectoDto {
    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    findById: string;

    @IsOptional()
    @IsString()
    @Expose()
    busquedaArticuloTipoBAV: string;

    @IsOptional()
    @IsString()
    @Expose()
    idsBusquedaArtValor: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
