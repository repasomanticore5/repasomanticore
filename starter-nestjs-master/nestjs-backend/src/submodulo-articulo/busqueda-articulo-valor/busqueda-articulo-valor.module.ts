import { Module } from '@nestjs/common';
import { BusquedaArticuloValorController } from './busqueda-articulo-valor.controller';
import { BUSQUEDA_ARTICULO_VALOR_IMPORTS } from './constantes/busqueda-articulo-valor.imports';
import { BUSQUEDA_ARTICULO_VALOR_PROVIDERS } from './constantes/busqueda-articulo-valor.providers';

@Module({
  imports: [...BUSQUEDA_ARTICULO_VALOR_IMPORTS],
  providers: [...BUSQUEDA_ARTICULO_VALOR_PROVIDERS],
  exports: [...BUSQUEDA_ARTICULO_VALOR_PROVIDERS],
  controllers: [BusquedaArticuloValorController],
})
export class BusquedaArticuloValorModule {}
