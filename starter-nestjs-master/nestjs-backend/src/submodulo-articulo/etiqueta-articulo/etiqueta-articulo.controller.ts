import {
    BadGatewayException,
    Controller,
    Get,
    HttpCode, InternalServerErrorException,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {EtiquetaArticuloService} from './etiqueta-articulo.service';
import {EtiquetaArticuloHabilitadoDto} from './dto/etiqueta-articulo.habilitado.dto';
import {ETIQUETA_ARTICULO_OCC} from './constantes/etiqueta-articulo.occ';
import {EtiquetaArticuloCrearDto} from './dto/etiqueta-articulo.crear.dto';
import {EtiquetaArticuloActualizarDto} from './dto/etiqueta-articulo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {EtiquetaArticuloEntity} from './etiqueta-articulo.entity';
import {EtiquetaArticuloBusquedaDto} from './dto/etiqueta-articulo.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'etiqueta-articulo';

@Controller(nombreControlador)
export class EtiquetaArticuloController extends ControladorComun {
    constructor(
        private readonly _etiquetaArticuloService: EtiquetaArticuloService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _etiquetaArticuloService,
            _seguridadService,
            _auditoriaService,
            ETIQUETA_ARTICULO_OCC(
                EtiquetaArticuloHabilitadoDto,
                EtiquetaArticuloCrearDto,
                EtiquetaArticuloActualizarDto,
                EtiquetaArticuloBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: EtiquetaArticuloBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._etiquetaArticuloService.etiquetaArticuloEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<EtiquetaArticuloEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._etiquetaArticuloService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._etiquetaArticuloService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<EtiquetaArticuloEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({
                        nombre: Like('%' + parametrosConsulta.busqueda + '%'),
                    });
                    consulta.where = consultaWhere;
                }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<EtiquetaArticuloEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._etiquetaArticuloService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }

    @Get('busqueda-etiqueta-articulo/disponible')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtenerEtiquetaArticuloDisponible(
        @Query('criterioBusqueda') criterioBusqueda: string,
    ) {
        if (!criterioBusqueda) {
            console.error({
                mensaje: 'No se ha enviado criterios de busqueda',
            });
            throw new BadGatewayException({
                mensaje: 'No se ha enviado criterios de busqueda',
            });
        }

        const {
            where: {
                idsEtiquetaArticulo
            }
        } = JSON.parse(criterioBusqueda);

        try {
            return await this._etiquetaArticuloService
                .obtenerEtiquetaArticuloDisponible(idsEtiquetaArticulo)
        } catch (e) {
            console.error({
                mensaje: 'Error al buscar etiquetas articulo disponibles',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al buscar etiquetas articulo disponibles',
            });
        }
    }
}
