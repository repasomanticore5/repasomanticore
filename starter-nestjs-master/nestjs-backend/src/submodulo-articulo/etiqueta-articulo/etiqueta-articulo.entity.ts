import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EtiquetaPorArticuloEntity } from '../etiqueta-por-articulo/etiqueta-por-articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ETIQUETA_ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class EtiquetaArticuloEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_ETIQUETA_ARTICULO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string;

  @OneToMany(
    () => EtiquetaPorArticuloEntity,
    (etiquetaPorArticulo) => etiquetaPorArticulo.etiquetaArticuloEPA,
  )
  etiquetasPorArticuloEA: EtiquetaPorArticuloEntity[];
}
