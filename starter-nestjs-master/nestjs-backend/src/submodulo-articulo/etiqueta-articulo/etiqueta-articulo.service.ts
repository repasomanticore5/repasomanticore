import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {EtiquetaArticuloEntity} from './etiqueta-articulo.entity';
import {EtiquetaArticuloBusquedaDto} from './dto/etiqueta-articulo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class EtiquetaArticuloService extends ServicioComun<EtiquetaArticuloEntity,
    EtiquetaArticuloBusquedaDto> {
    constructor(
        @InjectRepository(EtiquetaArticuloEntity, NOMBRE_CADENA_CONEXION)
        public etiquetaArticuloEntityRepository: Repository<EtiquetaArticuloEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            etiquetaArticuloEntityRepository,
            _connection,
            EtiquetaArticuloEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar,
        );
    }

    async obtenerEtiquetaArticuloDisponible(idsEtiquetaArticulo: number[]): Promise<[EtiquetaArticuloEntity[], number]> {
        try {
            const query = this.etiquetaArticuloEntityRepository
                .createQueryBuilder('etiquetaArticulo')
                .where('etiquetaArticulo.sisHabilitado = "A" ')

            if (idsEtiquetaArticulo.length > 0) {
                query.andWhere(`etiquetaArticulo.id NOT IN (${idsEtiquetaArticulo})`)
            }

            return query.getManyAndCount()
        } catch (e) {
            console.error({
                mensaje: 'Error al buscar etiquetas articulo disponibles',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al buscar etiquetas articulo disponibles',
            });
        }
    }
}
