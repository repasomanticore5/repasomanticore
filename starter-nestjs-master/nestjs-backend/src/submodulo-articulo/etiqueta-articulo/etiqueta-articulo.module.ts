import { Module } from '@nestjs/common';
import { EtiquetaArticuloController } from './etiqueta-articulo.controller';
import { ETIQUETA_ARTICULO_IMPORTS } from './constantes/etiqueta-articulo.imports';
import { ETIQUETA_ARTICULO_PROVIDERS } from './constantes/etiqueta-articulo.providers';

@Module({
  imports: [...ETIQUETA_ARTICULO_IMPORTS],
  providers: [...ETIQUETA_ARTICULO_PROVIDERS],
  exports: [...ETIQUETA_ARTICULO_PROVIDERS],
  controllers: [EtiquetaArticuloController],
})
export class EtiquetaArticuloModule {}
