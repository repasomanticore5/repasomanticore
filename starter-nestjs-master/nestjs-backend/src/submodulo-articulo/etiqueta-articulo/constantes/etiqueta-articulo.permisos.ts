import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_ETIQUETA_ARTICULO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'etiqueta-articulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/etiqueta-articulo/, // /etiqueta-articulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/etiqueta-articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /etiqueta-articulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/etiqueta-articulo\/\d+\/modificar-habilitado/, // /etiqueta-articulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/etiqueta-articulo\/\d+/, // /etiqueta-articulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
