import { EtiquetaArticuloCrearDto } from '../dto/etiqueta-articulo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ETIQUETA_ARTICULO_DATOS_PRUEBA: (() => EtiquetaArticuloCrearDto)[] = [
  () => {
    const dato = new EtiquetaArticuloCrearDto();
    dato.nombre = 'tecnología';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
