import { TypeOrmModule } from '@nestjs/typeorm';
import { EtiquetaArticuloEntity } from '../etiqueta-articulo.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const ETIQUETA_ARTICULO_IMPORTS = [
  TypeOrmModule.forFeature([EtiquetaArticuloEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
