import {
    Column,
    Entity,
    Index,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {BusquedaArticuloValorEntity} from '../busqueda-articulo-valor/busqueda-articulo-valor.entity';
import {ArticuloEntity} from '../articulo/articulo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BUSQUEDA_POR_ARTICULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BusquedaPorArticuloEntity extends EntidadComunProyecto {

    @PrimaryGeneratedColumn({
        name: 'ID_BUSQUEDA_POR_ARTICULO',
        unsigned: true,
    })
    id: number;

    @ManyToOne(
        () => BusquedaArticuloValorEntity,
        (busquedaArticuloValor) => busquedaArticuloValor.busquedasPorArticuloBAV,
        {nullable: false},
    )
    busquedaArticuloValorBPA: BusquedaArticuloValorEntity | number;

    @ManyToOne(
        () => ArticuloEntity,
        (articulo) => articulo.busquedasPorArticuloA,
        {nullable: false},
    )
    articuloBPA: ArticuloEntity | number;

}
