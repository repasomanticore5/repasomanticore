import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get,
    HttpCode, InternalServerErrorException, Param, Post,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {BusquedaPorArticuloService} from './busqueda-por-articulo.service';
import {BusquedaPorArticuloHabilitadoDto} from './dto/busqueda-por-articulo.habilitado.dto';
import {BUSQUEDA_POR_ARTICULO_OCC} from './constantes/busqueda-por-articulo.occ';
import {BusquedaPorArticuloCrearDto} from './dto/busqueda-por-articulo.crear.dto';
import {BusquedaPorArticuloActualizarDto} from './dto/busqueda-por-articulo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {BusquedaPorArticuloEntity} from './busqueda-por-articulo.entity';
import {BusquedaPorArticuloBusquedaDto} from './dto/busqueda-por-articulo.busqueda.dto';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'busqueda-por-articulo';

@Controller(nombreControlador)
export class BusquedaPorArticuloController extends ControladorComun {
    constructor(
        private readonly _busquedaPorArticuloService: BusquedaPorArticuloService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _busquedaPorArticuloService,
            _seguridadService,
            _auditoriaService,
            BUSQUEDA_POR_ARTICULO_OCC(
                BusquedaPorArticuloHabilitadoDto,
                BusquedaPorArticuloCrearDto,
                BusquedaPorArticuloActualizarDto,
                BusquedaPorArticuloBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: BusquedaPorArticuloBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._busquedaPorArticuloService.busquedaPorArticuloEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<BusquedaPorArticuloEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._busquedaPorArticuloService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._busquedaPorArticuloService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<BusquedaPorArticuloEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({
                        busquedaArticuloValorBPA: {
                            valor: Like('%' + parametrosConsulta.busqueda + '%')
                        }
                    });
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({
                        busquedaArticuloValorBPA: {
                            busquedaArticuloTipoBAV: {
                                nombre: Like('%' + parametrosConsulta.busqueda + '%')
                            }
                        }
                    });
                    consulta.where = consultaWhere;
                }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<BusquedaPorArticuloEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                // consulta.where = this.setearFiltro(
                //     'busquedaArticuloValorBPA',
                //     consultaWhere,
                //     parametrosConsulta.busquedaArticuloValorBPA,
                // );
                consulta.where = this.setearFiltro(
                    'articuloBPA',
                    consultaWhere,
                    parametrosConsulta.articuloBPA,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._busquedaPorArticuloService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            qb.leftJoinAndSelect(aliasTabla + '.busquedaArticuloValorBPA', 'busquedaArticuloValor');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            qb.leftJoinAndSelect('busquedaArticuloValor.busquedaArticuloTipoBAV', 'busquedaArticuloTipo');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            if (parametrosConsulta.idBusquedaRegistrados) {
                qb.andWhere(aliasTabla + '.articuloBPA = :idA', {
                    idA: +parametrosConsulta.idBusquedaRegistrados,
                });
            }
            if (parametrosConsulta.findById) {
                qb.andWhere(aliasTabla + '.id = :id', {
                    id: parametrosConsulta.findById,
                });
            }

            if (parametrosConsulta.busqueda) {
                qb.andWhere('busquedaArticuloTipo.nombre LIKE :nombreA', {
                    nombreA: `%${parametrosConsulta.busqueda}%`,
                });

                qb.orWhere('busquedaArticuloValor.valor LIKE :nombreA', {
                    nombreA: `%${parametrosConsulta.busqueda}%`,
                });
            }

            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }

    @Post('save-many/busqueda-por-articulo')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async saveManyBusquedasPorArticulo(
        @Body('arregloBusquedasPorArticulo') busquedasPorArticulo: BusquedaPorArticuloCrearDto[],
    ) {
        if (busquedasPorArticulo.length < 1) {
            console.error({
                mensaje: 'Error del usuario, no envia parametros',
            });
            throw new BadRequestException({
                mensaje: 'Error del usuario, no envia parametros',
            });
        }

        try {
            return await this._busquedaPorArticuloService
                .saveManyBusquedasPorArticulo(busquedasPorArticulo);
        } catch (e) {
            console.error({
                mensaje: 'Error guardando many busquedas por articulo',
                error: e,
                data: busquedasPorArticulo,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error guardando many busquedas por articulo',
            });
        }
    }

    @Delete(':id')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async eliminar(
        @Param('id') idBusquedaPorArticulo: string,
    ) {
        try {
            return await this._busquedaPorArticuloService
                .eliminar(+idBusquedaPorArticulo)
        } catch (e) {
            console.error({
                mensaje: 'Error eliminando busqueda por articulo',
                error: e,
                data: {idBusquedaPorArticulo},
            });
            throw new InternalServerErrorException({
                mensaje: 'Error eliminando busqueda por articulo',
            });
        }
    }
}
