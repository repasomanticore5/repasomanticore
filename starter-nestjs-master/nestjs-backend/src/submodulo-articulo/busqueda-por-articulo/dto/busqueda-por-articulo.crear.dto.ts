import { IsNotEmpty, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class BusquedaPorArticuloCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsNumber()
  @Expose()
  busquedaArticuloValorBPA: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  articuloBPA: number;
}
