import {IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class BusquedaPorArticuloBusquedaDto extends BusquedaComunProyectoDto {
    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
    @IsOptional()
    @IsNumberString()
    @Expose()
    busquedaArticuloValorBPA: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    articuloBPA: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    idBusquedaRegistrados: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    findById: string;

}
