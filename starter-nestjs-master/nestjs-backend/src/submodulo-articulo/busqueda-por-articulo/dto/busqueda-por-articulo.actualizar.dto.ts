import {IsInt, IsNotEmpty, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class BusquedaPorArticuloActualizarDto {
    @IsOptional()
    @IsInt()
    @Expose()
    busquedaArticuloValorBPA?: number;

    @IsOptional()
    @IsInt()
    @Expose()
    articuloBPA?: number;
}
