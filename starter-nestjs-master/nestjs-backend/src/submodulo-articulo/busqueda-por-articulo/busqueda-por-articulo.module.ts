import { Module } from '@nestjs/common';
import { BusquedaPorArticuloController } from './busqueda-por-articulo.controller';
import { BUSQUEDA_POR_ARTICULO_IMPORTS } from './constantes/busqueda-por-articulo.imports';
import { BUSQUEDA_POR_ARTICULO_PROVIDERS } from './constantes/busqueda-por-articulo.providers';

@Module({
  imports: [...BUSQUEDA_POR_ARTICULO_IMPORTS],
  providers: [...BUSQUEDA_POR_ARTICULO_PROVIDERS],
  exports: [...BUSQUEDA_POR_ARTICULO_PROVIDERS],
  controllers: [BusquedaPorArticuloController],
})
export class BusquedaPorArticuloModule {}
