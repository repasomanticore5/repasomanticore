import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {BusquedaPorArticuloEntity} from './busqueda-por-articulo.entity';
import {BusquedaPorArticuloBusquedaDto} from './dto/busqueda-por-articulo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {BusquedaPorArticuloCrearDto} from './dto/busqueda-por-articulo.crear.dto';

@Injectable()
export class BusquedaPorArticuloService extends ServicioComun<BusquedaPorArticuloEntity,
    BusquedaPorArticuloBusquedaDto> {
    constructor(
        @InjectRepository(BusquedaPorArticuloEntity, NOMBRE_CADENA_CONEXION)
        public busquedaPorArticuloEntityRepository: Repository<BusquedaPorArticuloEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            busquedaPorArticuloEntityRepository,
            _connection,
            BusquedaPorArticuloEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar,
        );
    }

    async saveManyBusquedasPorArticulo(busquedasPorArticulo: BusquedaPorArticuloCrearDto[]): Promise<BusquedaPorArticuloEntity[]> {
        try {
            const respuestaCrear = await this.busquedaPorArticuloEntityRepository
                .save(busquedasPorArticulo);
            const creadosConRelaciones = respuestaCrear
                .map(
                    async (busquedaPorArticulo) => {
                        const busquedaPorAticuloCreada = await this.busquedaPorArticuloEntityRepository
                            .createQueryBuilder('busquedaPorArticulo')
                            .innerJoinAndSelect(
                                'busquedaPorArticulo.busquedaArticuloValorBPA',
                                'busquedaArticuloValor',
                                'busquedaPorArticulo.busquedaArticuloValorBPA = busquedaArticuloValor.id'
                            )
                            .innerJoinAndSelect(
                                'busquedaArticuloValor.busquedaArticuloTipoBAV',
                                'busquedaArticuloTipo',
                                'busquedaArticuloValor.busquedaArticuloTipoBAV = busquedaArticuloTipo.id',
                            )
                            .where('busquedaPorArticulo.id = :idA', {idA: busquedaPorArticulo.id})
                            .getOne();
                        return {
                            ...busquedaPorAticuloCreada,
                            habilitado: true,
                        }
                    }
                );
            return Promise.all(creadosConRelaciones);
        } catch (e) {
            console.error({
                mensaje: 'Error guardando many busquedas por articulo',
                error: e,
                data: busquedasPorArticulo,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error guardando many busquedas por articulo',
            });
        }
    }

    async eliminar(id: number): Promise<{ mensaje: string }> {
        try {
            const eliminar = await this.busquedaPorArticuloEntityRepository
                .createQueryBuilder('busquedaPorArticulo')
                .delete()
                .where('id = :idB', {idB: id})
                .execute();

            return {
                mensaje: 'Ok',
            }
        } catch (e) {
            console.error({
                mensaje: 'Error eliminando busqueda por articulo',
                error: e,
                data: {id},
            });
            throw new InternalServerErrorException({
                mensaje: 'Error eliminando busqueda por articulo',
            });
        }
    }
}
