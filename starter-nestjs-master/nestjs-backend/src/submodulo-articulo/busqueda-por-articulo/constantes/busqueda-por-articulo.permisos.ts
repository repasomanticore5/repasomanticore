import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_BUSQUEDA_POR_ARTICULO: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'busqueda-por-articulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/busqueda-por-articulo/, // /busqueda-por-articulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/busqueda-por-articulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /busqueda-por-articulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-por-articulo\/\d+\/modificar-habilitado/, // /busqueda-por-articulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-por-articulo\/\d+/, // /busqueda-por-articulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
