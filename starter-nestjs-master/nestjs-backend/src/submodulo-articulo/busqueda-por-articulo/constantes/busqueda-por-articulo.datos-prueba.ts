import { BusquedaPorArticuloCrearDto } from '../dto/busqueda-por-articulo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const BUSQUEDA_POR_ARTICULO_DATOS_PRUEBA: ((
  idBusquedaArticuloValor: number,
  idArticulo: number,
) => BusquedaPorArticuloCrearDto)[] = [
  (idBusquedaArticuloValor: number, idArticulo: number) => {
    const dato = new BusquedaPorArticuloCrearDto();
    dato.busquedaArticuloValorBPA = idBusquedaArticuloValor;
    dato.articuloBPA = idArticulo;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
