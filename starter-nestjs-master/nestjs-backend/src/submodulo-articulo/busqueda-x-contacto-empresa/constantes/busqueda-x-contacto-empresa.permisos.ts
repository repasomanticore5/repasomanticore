import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_BUSQUEDA_X_CONTACTO_EMPRESA: () => {
  permisos: ObjetoSeguridadPermisos[];
  nombreEntidad: string;
} = () => {
  const nombreEntidad = 'busqueda-x-contacto-empresa';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/busqueda-x-contacto-empresa/, // /busqueda-x-contacto-empresa
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/busqueda-x-contacto-empresa\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /busqueda-x-contacto-empresa?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-x-contacto-empresa\/\d+\/modificar-habilitado/, // /busqueda-x-contacto-empresa/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/busqueda-x-contacto-empresa\/\d+/, // /busqueda-x-contacto-empresa/1
    },
  ];
  return {
    permisos,
    nombreEntidad,
  };
};
