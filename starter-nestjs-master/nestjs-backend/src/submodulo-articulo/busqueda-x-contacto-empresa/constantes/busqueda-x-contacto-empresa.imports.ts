import { TypeOrmModule } from '@nestjs/typeorm';
import { BusquedaXContactoEmpresaEntity } from '../busqueda-x-contacto-empresa.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const BUSQUEDA_X_CONTACTO_EMPRESA_IMPORTS = [
  TypeOrmModule.forFeature(
    [BusquedaXContactoEmpresaEntity],
    NOMBRE_CADENA_CONEXION,
  ),
  SeguridadModule,
  AuditoriaModule,
];
