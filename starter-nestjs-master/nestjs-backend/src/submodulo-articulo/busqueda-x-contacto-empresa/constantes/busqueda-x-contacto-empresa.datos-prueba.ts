import { BusquedaXContactoEmpresaCrearDto } from '../dto/busqueda-x-contacto-empresa.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const BUSQUEDA_X_CONTACTO_EMPRESA_DATOS_PRUEBA: ((
  idBusquedaArticuloTipo: number,
  idContactoEmpresa: number,
) => BusquedaXContactoEmpresaCrearDto)[] = [
  (idBusquedaArticuloTipo: number, idContactoEmpresa: number) => {
    const dato = new BusquedaXContactoEmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.busquedaArticuloTipoBXCE = idBusquedaArticuloTipo;
    dato.contactoEmpresaBXCE = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
