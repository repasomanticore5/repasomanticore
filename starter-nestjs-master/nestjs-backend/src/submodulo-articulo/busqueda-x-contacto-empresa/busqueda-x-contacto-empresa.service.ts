import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { BusquedaXContactoEmpresaEntity } from './busqueda-x-contacto-empresa.entity';
import { BusquedaXContactoEmpresaBusquedaDto } from './dto/busqueda-x-contacto-empresa.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class BusquedaXContactoEmpresaService extends ServicioComun<
  BusquedaXContactoEmpresaEntity,
  BusquedaXContactoEmpresaBusquedaDto
> {
  constructor(
    @InjectRepository(BusquedaXContactoEmpresaEntity, NOMBRE_CADENA_CONEXION)
    public busquedaXContactoEmpresaEntityRepository: Repository<BusquedaXContactoEmpresaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      busquedaXContactoEmpresaEntityRepository,
      _connection,
      BusquedaXContactoEmpresaEntity,
      'id',
      _auditoriaService,
      // Por defecto NO se transforma todos los campos a mayúsculas.
      // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
      // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
      (objetoATransformar, metodo) => objetoATransformar,
    );
  }
}
