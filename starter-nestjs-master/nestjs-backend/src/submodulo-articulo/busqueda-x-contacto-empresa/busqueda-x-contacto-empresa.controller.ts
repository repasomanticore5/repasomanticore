import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { BusquedaXContactoEmpresaService } from './busqueda-x-contacto-empresa.service';
import { BusquedaXContactoEmpresaHabilitadoDto } from './dto/busqueda-x-contacto-empresa.habilitado.dto';
import { BUSQUEDA_X_CONTACTO_EMPRESA_OCC } from './constantes/busqueda-x-contacto-empresa.occ';
import { BusquedaXContactoEmpresaCrearDto } from './dto/busqueda-x-contacto-empresa.crear.dto';
import { BusquedaXContactoEmpresaActualizarDto } from './dto/busqueda-x-contacto-empresa.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { BusquedaXContactoEmpresaEntity } from './busqueda-x-contacto-empresa.entity';
import { BusquedaXContactoEmpresaBusquedaDto } from './dto/busqueda-x-contacto-empresa.busqueda.dto';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'busqueda-x-contacto-empresa';

@Controller(nombreControlador)
export class BusquedaXContactoEmpresaController extends ControladorComun {
  constructor(
    private readonly _busquedaXContactoEmpresaService: BusquedaXContactoEmpresaService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _busquedaXContactoEmpresaService,
      _seguridadService,
      _auditoriaService,
      BUSQUEDA_X_CONTACTO_EMPRESA_OCC(
        BusquedaXContactoEmpresaHabilitadoDto,
        BusquedaXContactoEmpresaCrearDto,
        BusquedaXContactoEmpresaActualizarDto,
        BusquedaXContactoEmpresaBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: BusquedaXContactoEmpresaBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._busquedaXContactoEmpresaService.busquedaXContactoEmpresaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<BusquedaXContactoEmpresaEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._busquedaXContactoEmpresaService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._busquedaXContactoEmpresaService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<BusquedaXContactoEmpresaEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<BusquedaXContactoEmpresaEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );

        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._busquedaXContactoEmpresaService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
