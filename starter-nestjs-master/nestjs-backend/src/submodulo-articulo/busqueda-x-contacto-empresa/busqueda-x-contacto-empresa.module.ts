import { Module } from '@nestjs/common';
import { BusquedaXContactoEmpresaController } from './busqueda-x-contacto-empresa.controller';
import { BUSQUEDA_X_CONTACTO_EMPRESA_IMPORTS } from './constantes/busqueda-x-contacto-empresa.imports';
import { BUSQUEDA_X_CONTACTO_EMPRESA_PROVIDERS } from './constantes/busqueda-x-contacto-empresa.providers';

@Module({
  imports: [...BUSQUEDA_X_CONTACTO_EMPRESA_IMPORTS],
  providers: [...BUSQUEDA_X_CONTACTO_EMPRESA_PROVIDERS],
  exports: [...BUSQUEDA_X_CONTACTO_EMPRESA_PROVIDERS],
  controllers: [BusquedaXContactoEmpresaController],
})
export class BusquedaXContactoEmpresaModule {}
