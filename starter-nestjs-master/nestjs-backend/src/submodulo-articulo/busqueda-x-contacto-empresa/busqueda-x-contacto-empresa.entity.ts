import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { BusquedaArticuloTipoEntity } from '../busqueda-articulo-tipo/busqueda-articulo-tipo.entity';
import { ContactoEmpresaEntity } from '../../submodulo-empresa/contacto-empresa/contacto-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BUSQUEDA_X_CONTACTO_EMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BusquedaXContactoEmpresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_BUSQUEDA_X_CONTACTO_EMPRESA',
    unsigned: true,
  })
  id: number;

  // @Column({
  //     name: nombreCampoEjemplo,
  //     type: 'varchar',
  //     length: '60',
  //     nullable: false,
  // })
  // prefijoCampoEjemplo: string;

  @ManyToOne(
    () => BusquedaArticuloTipoEntity,
    (busquedaArticuloTipo) => busquedaArticuloTipo.busquedasXContactoEmpresaBAT,
    { nullable: false },
  )
  busquedaArticuloTipoBXCE: BusquedaArticuloTipoEntity | number;

  @ManyToOne(
    () => ContactoEmpresaEntity,
    (contactoEmpresa) => contactoEmpresa.busquedaXContactoEmpresaCE,
    { nullable: false },
  )
  contactoEmpresaBXCE: ContactoEmpresaEntity | number;
  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
