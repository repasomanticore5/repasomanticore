import { IsNotEmpty, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class BusquedaXContactoEmpresaCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsNumber()
  @Expose()
  busquedaArticuloTipoBXCE: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  contactoEmpresaBXCE: number;
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
}
