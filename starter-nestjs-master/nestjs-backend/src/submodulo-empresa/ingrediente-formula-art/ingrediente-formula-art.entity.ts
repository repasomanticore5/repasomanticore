import {
  Column,
  Entity,
  Index, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { FormulaArtEntity } from '../formula-art/formula-art.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'INGREDIENTE_FORMULA_ART')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class IngredienteFormulaArtEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_INGREDIENTE_FORMULA_ART',
    unsigned: true,
  })
  id: number;

  @Index()
  @Column({
    name: 'cantidad',
    type: 'decimal',
    scale: 4,
    precision: 10,
    nullable: false,
  })
  cantidad: number;

  @Column({
    name: 'perdida',
    type: 'decimal',
    scale: 4,
    precision: 10,
    nullable: false,
  })
  perdida: number;

  @ManyToOne(
    () => FormulaArtEntity,
    (r) => r.ingredientesFormulaArt,
    { nullable: true },
  )
  formulaArt: FormulaArtEntity | number;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
