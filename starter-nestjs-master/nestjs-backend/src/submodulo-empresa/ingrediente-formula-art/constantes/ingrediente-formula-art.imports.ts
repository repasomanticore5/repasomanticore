import { TypeOrmModule } from '@nestjs/typeorm';
import { IngredienteFormulaArtEntity } from '../ingrediente-formula-art.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const INGREDIENTE_FORMULA_ART_IMPORTS = [
  TypeOrmModule.forFeature([IngredienteFormulaArtEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
