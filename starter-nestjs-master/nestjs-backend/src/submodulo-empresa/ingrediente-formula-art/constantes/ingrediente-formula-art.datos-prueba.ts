import { IngredienteFormulaArtCrearDto } from '../dto/ingrediente-formula-art.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const INGREDIENTE_FORMULA_ART_DATOS_PRUEBA: ((
  idFormularArt: number,
) => IngredienteFormulaArtCrearDto)[] = [
  (idFormularArt: number) => {
    const dato = new IngredienteFormulaArtCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.cantidad = 10.5;
    dato.perdida = 0.5;
    dato.formulaArt = idFormularArt;
    return dato;
  },
];
