import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_INGREDIENTE_FORMULA_ART: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'ingrediente-formula-art';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/ingrediente-formula-art/, // /ingrediente-formula-art
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/ingrediente-formula-art\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /ingrediente-formula-art?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/ingrediente-formula-art\/\d+\/modificar-habilitado/, // /ingrediente-formula-art/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/ingrediente-formula-art\/\d+/, // /ingrediente-formula-art/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
