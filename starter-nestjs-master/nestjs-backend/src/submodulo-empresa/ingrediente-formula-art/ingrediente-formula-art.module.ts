import { Module } from '@nestjs/common';
import { IngredienteFormulaArtController } from './ingrediente-formula-art.controller';
import { INGREDIENTE_FORMULA_ART_IMPORTS } from './constantes/ingrediente-formula-art.imports';
import { INGREDIENTE_FORMULA_ART_PROVIDERS } from './constantes/ingrediente-formula-art.providers';

@Module({
  imports: [...INGREDIENTE_FORMULA_ART_IMPORTS],
  providers: [...INGREDIENTE_FORMULA_ART_PROVIDERS],
  exports: [...INGREDIENTE_FORMULA_ART_PROVIDERS],
  controllers: [IngredienteFormulaArtController],
})
export class IngredienteFormulaArtModule {}
