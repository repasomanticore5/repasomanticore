import { IsInt, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class IngredienteFormulaArtCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  cantidad: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  perdida: number;

  @IsOptional()
  @IsInt()
  @Expose()
  formulaArt: number;
}
