import { IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class IngredienteFormulaArtActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  cantidad: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  perdida: number;

}
