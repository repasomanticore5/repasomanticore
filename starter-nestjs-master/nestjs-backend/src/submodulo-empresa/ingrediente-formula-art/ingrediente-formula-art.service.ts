import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { IngredienteFormulaArtEntity } from './ingrediente-formula-art.entity';
import {IngredienteFormulaArtBusquedaDto} from './dto/ingrediente-formula-art.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class IngredienteFormulaArtService extends ServicioComun<IngredienteFormulaArtEntity, IngredienteFormulaArtBusquedaDto> {
  constructor(
    @InjectRepository(IngredienteFormulaArtEntity, NOMBRE_CADENA_CONEXION)
    public ingredienteFormulaArtEntityRepository: Repository<IngredienteFormulaArtEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        ingredienteFormulaArtEntityRepository,
      _connection,
        IngredienteFormulaArtEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
