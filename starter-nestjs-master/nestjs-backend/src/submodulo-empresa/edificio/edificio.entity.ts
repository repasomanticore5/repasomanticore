import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { BodegaEntity } from '../bodega/bodega.entity';
import { EstablecimientoEntity } from '../establecimiento/establecimiento.entity';
import { PisoEntity } from '../piso/piso.entity';
import { DireccionEntity } from '../direccion/direccion.entity';
import { EmpresaEntity } from '../empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'EDIFICIO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'esMatriz', 'nombre'])
export class EdificioEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_EDIFICIO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'es_matriz',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esMatriz: 0 | 1 = 0;

  @Column({
    name: 'telefono',
    type: 'varchar',
    length: 10,
    nullable: true,
  })
  telefono: string = null;

  @Column({
    name: 'whatsapp',
    type: 'varchar',
    length: 12,
    nullable: true,
  })
  whatsapp: string = null;

  @Column({
    name: 'nombre_responsable',
    type: 'varchar',
    length: 70,
    nullable: true,
  })
  nombreResponsable: string = null;

  @Column({
    name: 'extension',
    type: 'varchar',
    length: 5,
    nullable: true,
  })
  extension: string = null;

  @ManyToOne(() => EmpresaEntity, (r) => r.edificios, { nullable: true })
  empresa: EmpresaEntity | number;

  @OneToOne(() => DireccionEntity, (r) => r.edificio )
  direccion: DireccionEntity | number;

  @OneToMany(() => BodegaEntity, (r) => r.edificio)
  bodegas: BodegaEntity[];

  @OneToMany(() => EstablecimientoEntity, (r) => r.edificio)
  establecimientos: EstablecimientoEntity[];

  @OneToMany(() => PisoEntity, (r) => r.edificio)
  pisos: PisoEntity[];
}
