import {IsIn, IsInt, IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class EdificioBusquedaDto extends BusquedaComunProyectoDto {
    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    empresa: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    findById: string;

    @IsOptional()
    @Expose()
    esMatriz: number;
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
