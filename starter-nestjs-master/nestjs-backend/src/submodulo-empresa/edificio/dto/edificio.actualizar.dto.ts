import {IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class EdificioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsIn([0, 1])
    @Expose()
    esMatriz: 0 | 1;

    @IsOptional()
    @IsString()
    @MinLength(9)
    @MaxLength(10)
    @Expose()
    telefono: string;

    @IsOptional()
    @IsString()
    @MaxLength(12)
    @Expose()
    whatsapp: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(70)
    @Expose()
    nombreResponsable: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(5)
    @Expose()
    extension: string;

    @IsOptional()
    @IsInt()
    @Expose()
    empresa: number;

    @IsOptional()
    @IsInt()
    @Expose()
    direccion: number;
}
