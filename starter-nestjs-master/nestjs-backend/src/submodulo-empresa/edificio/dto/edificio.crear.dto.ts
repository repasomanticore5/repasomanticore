import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class EdificioCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsIn([0, 1])
  @Expose()
  esMatriz: 0 | 1;

  @IsOptional()
  @IsString()
  @MinLength(9)
  @MaxLength(10)
  @Expose()
  telefono: string;

  @IsOptional()
  @IsString()
  @MaxLength(12)
  @Expose()
  whatsapp: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(70)
  @Expose()
  nombreResponsable: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(5)
  @Expose()
  extension: string;

  @IsOptional()
  @IsInt()
  @Expose()
  empresa: number;

  // @IsOptional()
  // @IsInt()
  // @Expose()
  // direccion: number;
}
