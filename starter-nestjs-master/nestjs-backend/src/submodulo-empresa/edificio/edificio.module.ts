import { Module } from '@nestjs/common';
import { EdificioController } from './edificio.controller';
import { EDIFICIO_IMPORTS } from './constantes/edificio.imports';
import { EDIFICIO_PROVIDERS } from './constantes/edificio.providers';

@Module({
  imports: [...EDIFICIO_IMPORTS],
  providers: [...EDIFICIO_PROVIDERS],
  exports: [...EDIFICIO_PROVIDERS],
  controllers: [EdificioController],
})
export class EdificioModule {}
