import { EdificioCrearDto } from '../dto/edificio.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const EDIFICIO_DATOS_PRUEBA: ((
  idEmpresa: number,
) => EdificioCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new EdificioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Edificio 1';
    dato.esMatriz = 1;
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.telefono = '0992998263';
    dato.whatsapp = '583996569523';
    dato.nombreResponsable = 'Alfonso';
    dato.extension = '12';
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new EdificioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Edificio 2';
    dato.esMatriz = 0;
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new EdificioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Edificio 3';
    dato.esMatriz = 0;
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    return dato;
  },
];
