import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_EDIFICIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'edificio';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/edificio/, // /edificio
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/edificio\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /edificio?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/edificio\/\d+\/modificar-habilitado/, // /edificio/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/edificio\/\d+/, // /edificio/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
