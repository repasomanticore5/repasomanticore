import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ContactoEmpresaService } from './contacto-empresa.service';
import { ContactoEmpresaHabilitadoDto } from './dto/contacto-empresa.habilitado.dto';
import { CONTACTO_EMPRESA_OCC } from './constantes/contacto-empresa.occ';
import { ContactoEmpresaCrearDto } from './dto/contacto-empresa.crear.dto';
import { ContactoEmpresaActualizarDto } from './dto/contacto-empresa.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { ContactoEmpresaEntity } from './contacto-empresa.entity';
import { ContactoEmpresaBusquedaDto } from './dto/contacto-empresa.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'contacto-empresa';

@Controller(nombreControlador)
export class ContactoEmpresaController extends ControladorComun {
  constructor(
    private readonly _contactoEmpresaService: ContactoEmpresaService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _contactoEmpresaService,
      _seguridadService,
      _auditoriaService,
      CONTACTO_EMPRESA_OCC(
        ContactoEmpresaHabilitadoDto,
        ContactoEmpresaCrearDto,
        ContactoEmpresaActualizarDto,
        ContactoEmpresaBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: ContactoEmpresaBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._contactoEmpresaService.contactoEmpresaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<ContactoEmpresaEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._contactoEmpresaService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._contactoEmpresaService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<ContactoEmpresaEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<ContactoEmpresaEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'empresa',
          consultaWhere,
          parametrosConsulta.empresa,
        );
        consulta.where = this.setearFiltro(
          'tipoCargo',
          consultaWhere,
          parametrosConsulta.tipoCargo,
        );
        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._contactoEmpresaService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS
      qb.leftJoinAndSelect(aliasTabla + '.datosUsuario', 'datosUsuario');
      qb.leftJoinAndSelect(aliasTabla + '.tipoCargo', 'tipoCargo');

      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      if (parametrosConsulta.busqueda) {
        qb.andWhere(
          'datosUsuario.nombres LIKE :busqueda OR ' +
            'datosUsuario.apellidos LIKE :busqueda OR ' +
            'datosUsuario.identificacionPais LIKE :busqueda',
          {
            busqueda: `%${parametrosConsulta.busqueda.trim()}%`,
          },
        );
      }

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
