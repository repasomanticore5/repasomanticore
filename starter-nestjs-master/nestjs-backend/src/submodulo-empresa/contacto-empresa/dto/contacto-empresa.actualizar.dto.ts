import {IsInt, IsNotEmpty, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class ContactoEmpresaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsInt()
    @Expose()
    empresa: number;

    @IsOptional()
    @IsInt()
    @Expose()
    tipoCargo: number;

    @IsOptional()
    @IsInt()
    @Expose()
    datosUsuario: number;
}
