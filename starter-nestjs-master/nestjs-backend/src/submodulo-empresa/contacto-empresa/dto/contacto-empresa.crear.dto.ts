import {IsInt, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class ContactoEmpresaCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsOptional()
    @IsInt()
    @Expose()
    empresa: number;

    @IsOptional()
    @IsInt()
    @Expose()
    tipoCargo: number;

    @IsOptional()
    @IsInt()
    @Expose()
    datosUsuario: number;
}
