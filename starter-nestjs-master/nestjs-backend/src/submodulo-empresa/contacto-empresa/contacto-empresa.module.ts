import { Module } from '@nestjs/common';
import { ContactoEmpresaController } from './contacto-empresa.controller';
import { CONTACTO_EMPRESA_IMPORTS } from './constantes/contacto-empresa.imports';
import { CONTACTO_EMPRESA_PROVIDERS } from './constantes/contacto-empresa.providers';

@Module({
  imports: [...CONTACTO_EMPRESA_IMPORTS],
  providers: [...CONTACTO_EMPRESA_PROVIDERS],
  exports: [...CONTACTO_EMPRESA_PROVIDERS],
  controllers: [ContactoEmpresaController],
})
export class ContactoEmpresaModule {}
