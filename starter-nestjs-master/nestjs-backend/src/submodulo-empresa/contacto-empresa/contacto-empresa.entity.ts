import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { AreaTrabajadorEntity } from '../area-trabajador/area-trabajador.entity';
import { BodegaEntity } from '../bodega/bodega.entity';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { DatosContactoEntity } from '../datos-contacto/datos-contacto.entity';
import { TipoCargoEntity } from '../tipo-cargo/tipo-cargo.entity';
import { ContactoHorarioServicioEntity } from '../contacto-horario-servicio/contacto-horario-servicio.entity';
import { DepartamentoTrabajadorEntity } from '../departamento-trabajador/departamento-trabajador.entity';
import { BusquedaXContactoEmpresaEntity } from '../../submodulo-articulo/busqueda-x-contacto-empresa/busqueda-x-contacto-empresa.entity';
import { DatosUsuarioEntity } from '../../submodulo-roles/datos-usuario/datos-usuario.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CONTACTO_EMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ContactoEmpresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_CONTACTO_EMPRESA',
    unsigned: true,
  })
  id: number;

  @ManyToOne(() => EmpresaEntity, (r) => r.contactosEmpresa, { nullable: true })
  empresa: EmpresaEntity | number;

  @ManyToOne(() => DatosUsuarioEntity, (r) => r.contactosEmpresa, {
    nullable: true,
  })
  datosUsuario: DatosUsuarioEntity | number;

  @ManyToOne(() => TipoCargoEntity, (r) => r.contactosEmpresa, {
    nullable: true,
  })
  tipoCargo: TipoCargoEntity | number;

  @OneToMany((type) => AreaTrabajadorEntity, (r) => r.contactoEmpresa)
  areasTrabajador: AreaTrabajadorEntity[];

  @OneToMany((type) => BodegaEntity, (r) => r.contactoEmpresa)
  bodegas: BodegaEntity[];

  @OneToMany((type) => DatosContactoEntity, (r) => r.contactoEmpresa)
  datosContacto: BodegaEntity[];

  @OneToMany((type) => ContactoHorarioServicioEntity, (r) => r.contactoEmpresa)
  contactosHorariosServicio: ContactoHorarioServicioEntity[];

  @OneToMany((type) => DepartamentoTrabajadorEntity, (r) => r.contactoEmpresa)
  departamentosTrabajador: DepartamentoTrabajadorEntity[];

  @OneToMany(
    (type) => BusquedaXContactoEmpresaEntity,
    (contactoEmpresa) => contactoEmpresa.busquedaArticuloTipoBXCE,
  )
  busquedaXContactoEmpresaCE: BusquedaXContactoEmpresaEntity[];
}
