import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactoEmpresaEntity } from '../contacto-empresa.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const CONTACTO_EMPRESA_IMPORTS = [
  TypeOrmModule.forFeature([ContactoEmpresaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
