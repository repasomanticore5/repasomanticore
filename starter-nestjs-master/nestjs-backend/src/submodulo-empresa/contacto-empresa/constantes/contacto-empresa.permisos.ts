import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CONTACTO_EMPRESA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'contacto-empresa';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/contacto-empresa/, // /contacto-empresa
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/contacto-empresa\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /contacto-empresa?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/contacto-empresa\/\d+\/modificar-habilitado/, // /contacto-empresa/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/contacto-empresa\/\d+/, // /contacto-empresa/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
