import { ContactoEmpresaCrearDto } from '../dto/contacto-empresa.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const CONTACTO_EMPRESA_DATOS_PRUEBA: (
    (idEmpresa: number, idTipoCargo: number, idDatosUsuario?: number) => ContactoEmpresaCrearDto)[] = [
  (idEmpresa: number, idTipoCargo: number, idDatosUsuario?: number) => {
    const dato = new ContactoEmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    dato.tipoCargo = idTipoCargo;
    dato.datosUsuario = idDatosUsuario;
    return dato;
  },
  (idEmpresa: number, idTipoCargo: number, idDatosUsuario?: number) => {
    const dato = new ContactoEmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    dato.tipoCargo = idTipoCargo;
    dato.datosUsuario = idDatosUsuario;
    return dato;
  },
];
