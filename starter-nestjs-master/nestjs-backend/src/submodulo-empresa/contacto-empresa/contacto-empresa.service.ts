import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ContactoEmpresaEntity } from './contacto-empresa.entity';
import {ContactoEmpresaBusquedaDto} from './dto/contacto-empresa.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class ContactoEmpresaService extends ServicioComun<ContactoEmpresaEntity, ContactoEmpresaBusquedaDto> {
  constructor(
    @InjectRepository(ContactoEmpresaEntity, NOMBRE_CADENA_CONEXION)
    public contactoEmpresaEntityRepository: Repository<ContactoEmpresaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        contactoEmpresaEntityRepository,
      _connection,
        ContactoEmpresaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
