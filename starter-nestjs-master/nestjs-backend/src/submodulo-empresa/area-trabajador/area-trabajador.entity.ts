import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { AreaPisoEntity } from '../area-piso/area-piso.entity';
import { ContactoEmpresaEntity } from '../contacto-empresa/contacto-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'AREA_TRABAJADOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class AreaTrabajadorEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_AREA_TRABAJADOR',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'descripcion_ubicacion',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  descripcionUbicacion: string;

  @ManyToOne((type) => AreaPisoEntity, (r) => r.areasTrabajador, {
    nullable: false,
  })
  areaPiso: AreaPisoEntity | number;

  @ManyToOne((type) => ContactoEmpresaEntity, (r) => r.areasTrabajador, {
    nullable: false,
  })
  contactoEmpresa: ContactoEmpresaEntity | number;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
