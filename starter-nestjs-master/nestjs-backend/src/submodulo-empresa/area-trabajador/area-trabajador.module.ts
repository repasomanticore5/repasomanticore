import { Module } from '@nestjs/common';
import { AreaTrabajadorController } from './area-trabajador.controller';
import { AREA_TRABAJADOR_IMPORTS } from './constantes/area-trabajador.imports';
import { AREA_TRABAJADOR_PROVIDERS } from './constantes/area-trabajador.providers';

@Module({
  imports: [...AREA_TRABAJADOR_IMPORTS],
  providers: [...AREA_TRABAJADOR_PROVIDERS],
  exports: [...AREA_TRABAJADOR_PROVIDERS],
  controllers: [AreaTrabajadorController],
})
export class AreaTrabajadorModule {}
