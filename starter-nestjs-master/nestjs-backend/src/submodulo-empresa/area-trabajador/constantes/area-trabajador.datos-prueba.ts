import { AreaTrabajadorCrearDto } from '../dto/area-trabajador.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const AREA_TRABAJADOR_DATOS_PRUEBA: ((
  idAreaPiso: number,
  idContactoEmpresa: number,
) => AreaTrabajadorCrearDto)[] = [
  (idAreaPiso: number, idContactoEmpresa: number) => {
    const dato = new AreaTrabajadorCrearDto();
    dato.areaPiso = idAreaPiso;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.descripcionUbicacion = 'puesto 24';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
