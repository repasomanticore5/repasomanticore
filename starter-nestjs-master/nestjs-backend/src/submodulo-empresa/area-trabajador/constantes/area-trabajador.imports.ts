import { TypeOrmModule } from '@nestjs/typeorm';
import { AreaTrabajadorEntity } from '../area-trabajador.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const AREA_TRABAJADOR_IMPORTS = [
  TypeOrmModule.forFeature([AreaTrabajadorEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
