import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_AREA_TRABAJADOR: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'area-trabajador';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/area-trabajador/, // /area-trabajador
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/area-trabajador\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /area-trabajador?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/area-trabajador\/\d+\/modificar-habilitado/, // /area-trabajador/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/area-trabajador\/\d+/, // /area-trabajador/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
