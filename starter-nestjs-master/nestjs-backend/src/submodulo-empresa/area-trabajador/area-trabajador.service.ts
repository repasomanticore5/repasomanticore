import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { AreaTrabajadorEntity } from './area-trabajador.entity';
import {AreaTrabajadorBusquedaDto} from './dto/area-trabajador.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class AreaTrabajadorService extends ServicioComun<AreaTrabajadorEntity, AreaTrabajadorBusquedaDto> {
  constructor(
    @InjectRepository(AreaTrabajadorEntity, NOMBRE_CADENA_CONEXION)
    public areaTrabajadorEntityRepository: Repository<AreaTrabajadorEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        areaTrabajadorEntityRepository,
      _connection,
        AreaTrabajadorEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
