import {IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class AreaTrabajadorCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcionUbicacion: string;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    areaPiso: number;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    contactoEmpresa: number;
}

