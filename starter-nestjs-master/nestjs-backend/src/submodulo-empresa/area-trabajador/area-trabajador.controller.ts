import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AreaTrabajadorService } from './area-trabajador.service';
import { AreaTrabajadorHabilitadoDto } from './dto/area-trabajador.habilitado.dto';
import { AREA_TRABAJADOR_OCC } from './constantes/area-trabajador.occ';
import { AreaTrabajadorCrearDto } from './dto/area-trabajador.crear.dto';
import { AreaTrabajadorActualizarDto } from './dto/area-trabajador.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { AreaTrabajadorEntity } from './area-trabajador.entity';
import { AreaTrabajadorBusquedaDto } from './dto/area-trabajador.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'area-trabajador';

@Controller(nombreControlador)
export class AreaTrabajadorController extends ControladorComun {
  constructor(
    private readonly _areaTrabajadorService: AreaTrabajadorService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _areaTrabajadorService,
      _seguridadService,
      _auditoriaService,
      AREA_TRABAJADOR_OCC(
        AreaTrabajadorHabilitadoDto,
        AreaTrabajadorCrearDto,
        AreaTrabajadorActualizarDto,
        AreaTrabajadorBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: AreaTrabajadorBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._areaTrabajadorService.areaTrabajadorEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<AreaTrabajadorEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._areaTrabajadorService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._areaTrabajadorService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<AreaTrabajadorEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // if (parametrosConsulta.busqueda) {
        //   consultaWhere.push({
        //     contactoEmpresa: {
        //       datosUsuario: {
        //         nombres: Like('%' + parametrosConsulta.busqueda + '%'),
        //       },
        //     },
        //   });
        //   consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({
        //         descripcionUbicacion: Like('%' + parametrosConsulta.busqueda + '%')
        //     });
        //     consulta.where = consultaWhere;
        // }
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<AreaTrabajadorEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'areaPiso',
          consultaWhere,
          parametrosConsulta.areaPiso,
        );

        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._areaTrabajadorService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS
      qb.leftJoinAndSelect(aliasTabla + '.contactoEmpresa', 'contactoEmpresa');
      qb.leftJoinAndSelect('contactoEmpresa.datosUsuario', 'datosUsuario');
      // qb.leftJoinAndSelect(aliasTabla + '.areaPiso', 'areaPiso');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES
      if (parametrosConsulta.busqueda && parametrosConsulta.areaPiso) {
        qb.andWhere(
          '(datosUsuario.nombres LIKE :busqueda OR ' +
            'datosUsuario.apellidos LIKE :busqueda OR ' +
            'datosUsuario.identificacionPais LIKE :busqueda)',
          {
            busqueda: `%${parametrosConsulta.busqueda.trim()}%`,
          },
        );
      }

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
