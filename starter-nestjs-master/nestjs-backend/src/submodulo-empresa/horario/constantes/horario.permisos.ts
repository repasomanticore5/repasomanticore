import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_HORARIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'horario';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/horario/, // /horario
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/horario\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /horario?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/horario\/\d+\/modificar-habilitado/, // /horario/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/horario\/\d+/, // /horario/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
