import { TypeOrmModule } from '@nestjs/typeorm';
import { HorarioEntity } from '../horario.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const HORARIO_IMPORTS = [
  TypeOrmModule.forFeature([HorarioEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
