import { HorarioCrearDto } from '../dto/horario.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const HORARIO_DATOS_PRUEBA: ((
  idEmpresa: number,
) => HorarioCrearDto)[] = [
  (idEmpresa) => {
    const dato = new HorarioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.descripcion = 'horario regular lunes -vienes';
    dato.tipo = 'D';
    dato.fechaInicia = '2021-01-01';
    dato.fechaFinaliza = '2021-12-31';
    dato.horaInicia = '09:00';
    dato.horaFinaliza = '20:00';
    dato.lunes = 1;
    dato.martes = 1;
    dato.miercoles = 1;
    dato.jueves = 1;
    dato.viernes = 1;
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    return dato;
  },
];
