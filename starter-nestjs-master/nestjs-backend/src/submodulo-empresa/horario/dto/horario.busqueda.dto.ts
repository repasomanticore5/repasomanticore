import { IsIn, IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class HorarioBusquedaDto extends BusquedaComunProyectoDto {

  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsString()
  @Expose()
  descripcion: string;

  @IsOptional()
  @IsString()
  @IsIn(['D', 'F'])
  @Expose()
  tipo: 'D' | 'F';

  @IsOptional()
  @IsNumberString()
  @Expose()
  empresa: string;
}
