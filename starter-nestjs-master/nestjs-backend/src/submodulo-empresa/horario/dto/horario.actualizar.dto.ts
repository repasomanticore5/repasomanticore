import { IsIn, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class HorarioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsOptional()
  @IsString()
  @IsIn(['D', 'F'])
  @Expose()
  tipo: 'D' | 'F';

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  lunes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  martes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  miercoles: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  jueves: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  viernes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  sabado: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  domingo: 1 | 0;

  @IsOptional()
  @IsString()
  @Expose()
  fechaInicia: string;

  @IsOptional()
  @IsString()
  @Expose()
  fechaFinaliza: string;

  @IsOptional()
  @IsString()
  @Expose()
  horaInicia: string;

  @IsOptional()
  @IsString()
  @Expose()
  horaFinaliza: string;
}
