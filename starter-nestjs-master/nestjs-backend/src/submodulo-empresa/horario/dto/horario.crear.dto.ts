import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class HorarioCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @IsIn(['D', 'F'])
  @Expose()
  tipo: 'D' | 'F';

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  lunes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  martes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  miercoles: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  jueves: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  viernes: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  sabado: 1 | 0;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  domingo: 1 | 0;

  @IsOptional()
  @IsString()
  @Expose()
  fechaInicia: string;

  @IsOptional()
  @IsString()
  @Expose()
  fechaFinaliza: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  horaInicia: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  horaFinaliza: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  empresa: number;
}
