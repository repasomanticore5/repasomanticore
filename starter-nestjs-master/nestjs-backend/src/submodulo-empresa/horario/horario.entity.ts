import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { HorarioServicioEntity } from '../horario-servicio/horario-servicio.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'HORARIO')
@Index([
  'sisHabilitado',
  'sisCreado',
  'sisModificado',
  'fechaInicia',
  'fechaFinaliza',
  'horaInicia',
  'horaFinaliza',
])
export class HorarioEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_HORARIO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  descripcion: string;

  @Index()
  @Column({
    name: 'tipo',
    type: 'varchar',
    length: 2,
    nullable: false,
    comment: 'D: dia (lun, mart, mier, ..), F: fecha (fecha inicio, fecha fin)',
  })
  tipo: 'D' | 'F';

  @Column({
    name: 'lunes',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  lunes: 1 | 0 = 0;

  @Column({
    name: 'martes',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  martes: 1 | 0 = 0;

  @Column({
    name: 'miercoles',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  miercoles: 1 | 0 = 0;

  @Column({
    name: 'jueves',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  jueves: 1 | 0 = 0;

  @Column({
    name: 'viernes',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  viernes: 1 | 0 = 0;

  @Column({
    name: 'sabado',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  sabado: 1 | 0 = 0;

  @Column({
    name: 'domingo',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  domingo: 1 | 0 = 0;

  @Column({
    name: 'fecha_inicia',
    type: 'date',
    nullable: true,
  })
  fechaInicia: Date = null;

  @Column({
    name: 'fecha_finaliza',
    type: 'date',
    nullable: true,
  })
  fechaFinaliza: Date = null;

  @Column({
    name: 'hora_inicia',
    type: 'time',
    nullable: true,
  })
  horaInicia: string = null;

  @Column({
    name: 'hora_finaliza',
    type: 'time',
    nullable: true,
  })
  horaFinaliza: string = null;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  @ManyToOne(() => EmpresaEntity, (r) => r.horarios, { nullable: true })
  empresa: EmpresaEntity | number;

  @OneToMany(() => HorarioServicioEntity, (r) => r.horario)
  horariosServicio: HorarioServicioEntity[];
}
