import { Module } from '@nestjs/common';
import { HorarioController } from './horario.controller';
import { HORARIO_IMPORTS } from './constantes/horario.imports';
import { HORARIO_PROVIDERS } from './constantes/horario.providers';

@Module({
  imports: [...HORARIO_IMPORTS],
  providers: [...HORARIO_PROVIDERS],
  exports: [...HORARIO_PROVIDERS],
  controllers: [HorarioController],
})
export class HorarioModule {}
