import { BodegaCrearDto } from '../dto/bodega.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const BODEGA_DATOS_PRUEBA: ((idEdificio: number, idContactoEmpresa?: number) => BodegaCrearDto)[] = [
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 1 Manticore Labs';
    dato.codigo = 'Bod01ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 2 Manticore Labs';
    dato.codigo = 'Bod02ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 3 Manticore Labs';
    dato.codigo = 'Bod03ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 4 Manticore Labs';
    dato.codigo = 'Bod04ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 5 Manticore Labs';
    dato.codigo = 'Bod05ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number, idContactoEmpresa?: number) => {
    const dato = new BodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Bodega 6 Manticore Labs';
    dato.codigo = 'Bod06ML';
    dato.esPercha = 0;
    dato.edificio = idEdificio;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
