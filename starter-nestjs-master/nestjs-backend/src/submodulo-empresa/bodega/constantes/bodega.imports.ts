import { TypeOrmModule } from '@nestjs/typeorm';
import { BodegaEntity } from '../bodega.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const BODEGA_IMPORTS = [
  TypeOrmModule.forFeature([BodegaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
