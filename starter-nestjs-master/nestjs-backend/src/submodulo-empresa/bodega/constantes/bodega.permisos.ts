import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_BODEGA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'bodega';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/bodega/, // /bodega
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/bodega\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /bodega?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/bodega\/\d+\/modificar-habilitado/, // /bodega/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/bodega\/\d+/, // /bodega/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
