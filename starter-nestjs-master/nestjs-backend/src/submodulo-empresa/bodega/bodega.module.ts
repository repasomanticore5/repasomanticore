import { Module } from '@nestjs/common';
import { BodegaController } from './bodega.controller';
import { BODEGA_IMPORTS } from './constantes/bodega.imports';
import { BODEGA_PROVIDERS } from './constantes/bodega.providers';

@Module({
  imports: [...BODEGA_IMPORTS],
  providers: [...BODEGA_PROVIDERS],
  exports: [...BODEGA_PROVIDERS],
  controllers: [BodegaController],
})
export class BodegaModule {}
