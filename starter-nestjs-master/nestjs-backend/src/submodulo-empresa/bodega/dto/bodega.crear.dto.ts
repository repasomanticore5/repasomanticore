import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class BodegaCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(60)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(30)
  @Expose()
  codigo: string;

  @IsNotEmpty()
  @IsIn([0, 1])
  @Expose()
  esPercha: 0 | 1;

  @IsOptional()
  @IsInt()
  @Expose()
  contactoEmpresa: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  edificio: number;
}
