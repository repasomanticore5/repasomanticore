import {
  IsIn,
  IsInt,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';

export class BodegaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(60)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(30)
  @Expose()
  codigo: string;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  esPercha: 0 | 1;

  @IsOptional()
  @IsInt()
  @Expose()
  contactoEmpresa: number;

  @IsOptional()
  @IsInt()
  @Expose()
  edificio: number;
}
