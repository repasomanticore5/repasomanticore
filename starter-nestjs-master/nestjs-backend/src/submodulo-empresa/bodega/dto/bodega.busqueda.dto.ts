import { IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class BodegaBusquedaDto extends BusquedaComunProyectoDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  edificio: string;

  @IsOptional()
  @Expose()
  esPercha: string;

  @IsOptional()
  @Expose()
  findById: string;
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
