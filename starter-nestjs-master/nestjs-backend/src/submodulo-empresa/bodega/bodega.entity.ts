import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ContactoEmpresaEntity } from '../contacto-empresa/contacto-empresa.entity';
import { EdificioEntity } from '../edificio/edificio.entity';
import { BodegaTipoEntity } from '../bodega-tipo/bodega-tipo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BODEGA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BodegaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_BODEGA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 60,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: 30,
    nullable: false,
    unique: true,
  })
  codigo: string;

  @Column({
    name: 'es_percha',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esPercha: 0 | 1 = 0;

  @ManyToOne(() => ContactoEmpresaEntity, (r) => r.bodegas, { nullable: true })
  contactoEmpresa: ContactoEmpresaEntity | number;

  @ManyToOne(() => EdificioEntity, (r) => r.bodegas, { nullable: false })
  edificio: EdificioEntity | number;

  @OneToMany(() => BodegaTipoEntity, (r) => r.bodega)
  bodegasTipo: BodegaTipoEntity[];
}
