import {
  IsInt,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';

export class EstablecimientoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(10)
  @Expose()
  codigo: string;

  @IsOptional()
  @IsInt()
  @Expose()
  horasAtencion: number;

  @IsOptional()
  @IsInt()
  @Expose()
  edificio: number;
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
}
