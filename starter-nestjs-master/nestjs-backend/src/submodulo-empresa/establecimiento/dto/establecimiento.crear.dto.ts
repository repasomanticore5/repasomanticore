import {
  IsInt,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class EstablecimientoCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(10)
  @Expose()
  codigo: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  horasAtencion: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  edificio: number;
}
