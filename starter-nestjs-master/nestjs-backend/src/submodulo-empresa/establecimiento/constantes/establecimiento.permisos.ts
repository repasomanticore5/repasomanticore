import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_ESTABLECIMIENTO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'establecimiento';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/establecimiento/, // /establecimiento
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/establecimiento\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /establecimiento?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/establecimiento\/\d+\/modificar-habilitado/, // /establecimiento/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/establecimiento\/\d+/, // /establecimiento/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
