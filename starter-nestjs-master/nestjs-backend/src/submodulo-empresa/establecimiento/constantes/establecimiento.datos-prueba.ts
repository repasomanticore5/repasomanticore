import { EstablecimientoCrearDto } from '../dto/establecimiento.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ESTABLECIMIENTO_DATOS_PRUEBA: ((
  idEdificio: number,
) => EstablecimientoCrearDto)[] = [
  (idEdificio: number) => {
    const dato = new EstablecimientoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.codigo = 'EST001' + idEdificio;
    dato.nombre = 'ESTABLECIMIENTO 1-' + idEdificio;
    dato.edificio = idEdificio;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEdificio: number) => {
    const dato = new EstablecimientoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.codigo = 'EST002' + idEdificio;
    dato.nombre = 'ESTABLECIMIENTO 2-' + idEdificio;
    dato.edificio = idEdificio;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
