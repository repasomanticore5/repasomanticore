import { TypeOrmModule } from '@nestjs/typeorm';
import { EstablecimientoEntity } from '../establecimiento.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const ESTABLECIMIENTO_IMPORTS = [
  TypeOrmModule.forFeature([EstablecimientoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
