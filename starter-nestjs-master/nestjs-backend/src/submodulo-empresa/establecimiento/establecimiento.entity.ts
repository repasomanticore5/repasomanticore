import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EdificioEntity } from '../edificio/edificio.entity';
import { ServicioEstablecimientoEntity } from '../servicio-establecimiento/servicio-establecimiento.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ESTABLECIMIENTO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre', 'codigo'])
export class EstablecimientoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_ESTABLECIMIENTO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: 10,
    nullable: false,
    unique: true,
  })
  codigo: string;

  @Column({
    name: 'horas_atencion',
    type: 'int',
    default: 0,
    nullable: false,
  })
  horasAtencion = 0;

  @ManyToOne(() => EdificioEntity, (r) => r.establecimientos, {
    nullable: false,
  })
  edificio: EdificioEntity | number;

  @OneToMany(() => ServicioEstablecimientoEntity, (r) => r.establecimiento)
  serviciosEstablecimiento: ServicioEstablecimientoEntity[];
}
