import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { EstablecimientoEntity } from './establecimiento.entity';
import {EstablecimientoBusquedaDto} from './dto/establecimiento.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class EstablecimientoService extends ServicioComun<EstablecimientoEntity, EstablecimientoBusquedaDto> {
  constructor(
    @InjectRepository(EstablecimientoEntity, NOMBRE_CADENA_CONEXION)
    public establecimientoEntityRepository: Repository<EstablecimientoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        establecimientoEntityRepository,
      _connection,
        EstablecimientoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
