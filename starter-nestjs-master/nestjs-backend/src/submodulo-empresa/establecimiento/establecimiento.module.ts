import { Module } from '@nestjs/common';
import { EstablecimientoController } from './establecimiento.controller';
import { ESTABLECIMIENTO_IMPORTS } from './constantes/establecimiento.imports';
import { ESTABLECIMIENTO_PROVIDERS } from './constantes/establecimiento.providers';

@Module({
  imports: [...ESTABLECIMIENTO_IMPORTS],
  providers: [...ESTABLECIMIENTO_PROVIDERS],
  exports: [...ESTABLECIMIENTO_PROVIDERS],
  controllers: [EstablecimientoController],
})
export class EstablecimientoModule {}
