import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { DepartamentoTrabajadorService } from './departamento-trabajador.service';
import { DepartamentoTrabajadorHabilitadoDto } from './dto/departamento-trabajador.habilitado.dto';
import { DEPARTAMENTO_TRABAJADOR_OCC } from './constantes/departamento-trabajador.occ';
import { DepartamentoTrabajadorCrearDto } from './dto/departamento-trabajador.crear.dto';
import { DepartamentoTrabajadorActualizarDto } from './dto/departamento-trabajador.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { DepartamentoTrabajadorEntity } from './departamento-trabajador.entity';
import { DepartamentoTrabajadorBusquedaDto } from './dto/departamento-trabajador.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'departamento-trabajador';

@Controller(nombreControlador)
export class DepartamentoTrabajadorController extends ControladorComun {
  constructor(
    private readonly _departamentoTrabajadorService: DepartamentoTrabajadorService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _departamentoTrabajadorService,
      _seguridadService,
      _auditoriaService,
      DEPARTAMENTO_TRABAJADOR_OCC(
        DepartamentoTrabajadorHabilitadoDto,
        DepartamentoTrabajadorCrearDto,
        DepartamentoTrabajadorActualizarDto,
        DepartamentoTrabajadorBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: DepartamentoTrabajadorBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._departamentoTrabajadorService.departamentoTrabajadorEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<DepartamentoTrabajadorEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._departamentoTrabajadorService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._departamentoTrabajadorService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<DepartamentoTrabajadorEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<DepartamentoTrabajadorEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'departamentoEmpresa',
          consultaWhere,
          parametrosConsulta.departamentoEmpresa,
        );
        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._departamentoTrabajadorService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      qb.leftJoinAndSelect(aliasTabla + '.contactoEmpresa', 'contactoEmpresa');
      qb.leftJoinAndSelect('contactoEmpresa.datosUsuario', 'datosUsuario');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES
      if (parametrosConsulta.busqueda) {
        qb.andWhere(
          'datosUsuario.nombres LIKE :busqueda OR ' +
            'datosUsuario.apellidos LIKE :busqueda OR ' +
            'datosUsuario.identificacionPais LIKE :busqueda',
          {
            busqueda: `%${parametrosConsulta.busqueda.trim()}%`,
          },
        );
      }
      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
