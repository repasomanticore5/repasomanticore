import { DepartamentoTrabajadorCrearDto } from '../dto/departamento-trabajador.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';
import { ContactoEmpresaEntity } from '../../contacto-empresa/contacto-empresa.entity';
import { DepartamentoEmpresaEntity } from '../../departamento-empresa/departamento-empresa.entity';

export const DEPARTAMENTO_TRABAJADOR_DATOS_PRUEBA: ((
  idContactoEmpresa: number,
  idDepartamentoEmpresa: number,
) => DepartamentoTrabajadorCrearDto)[] = [
  (idContactoEmpresa: number, idDepartamentoEmpresa: number) => {
    const dato = new DepartamentoTrabajadorCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.descripcion = 'Trabajador del departamento de TI';
    dato.contactoEmpresa = idContactoEmpresa;
    dato.departamentoEmpresa = idDepartamentoEmpresa;
    return dato;
  },
];
