import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartamentoTrabajadorEntity } from '../departamento-trabajador.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const DEPARTAMENTO_TRABAJADOR_IMPORTS = [
  TypeOrmModule.forFeature([DepartamentoTrabajadorEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
