import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_DEPARTAMENTO_TRABAJADOR: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'departamento-trabajador';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/departamento-trabajador/, // /departamento-trabajador
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/departamento-trabajador\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /departamento-trabajador?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/departamento-trabajador\/\d+\/modificar-habilitado/, // /departamento-trabajador/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/departamento-trabajador\/\d+/, // /departamento-trabajador/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
