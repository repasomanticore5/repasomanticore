import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class DepartamentoTrabajadorCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsOptional()
  @IsInt()
  @Expose()
  contactoEmpresa: number;

  @IsOptional()
  @IsInt()
  @Expose()
  departamentoEmpresa: number;
}
