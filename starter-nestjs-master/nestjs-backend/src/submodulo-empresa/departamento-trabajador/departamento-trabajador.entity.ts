import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ContactoEmpresaEntity } from '../contacto-empresa/contacto-empresa.entity';
import { DepartamentoEmpresaEntity } from '../departamento-empresa/departamento-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DEPARTAMENTO_TRABAJADOR')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class DepartamentoTrabajadorEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_DEPARTAMENTO_TRABAJADOR',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  @ManyToOne(() => ContactoEmpresaEntity, (r) => r.departamentosTrabajador, {
    nullable: true,
  })
  contactoEmpresa: ContactoEmpresaEntity | number;

  @ManyToOne(
    () => DepartamentoEmpresaEntity,
    (r) => r.departamentosTrabajador,
    { nullable: true },
  )
  departamentoEmpresa: DepartamentoEmpresaEntity | number;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
