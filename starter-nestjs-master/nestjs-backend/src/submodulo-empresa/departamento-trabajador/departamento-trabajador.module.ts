import { Module } from '@nestjs/common';
import { DepartamentoTrabajadorController } from './departamento-trabajador.controller';
import { DEPARTAMENTO_TRABAJADOR_IMPORTS } from './constantes/departamento-trabajador.imports';
import { DEPARTAMENTO_TRABAJADOR_PROVIDERS } from './constantes/departamento-trabajador.providers';

@Module({
  imports: [...DEPARTAMENTO_TRABAJADOR_IMPORTS],
  providers: [...DEPARTAMENTO_TRABAJADOR_PROVIDERS],
  exports: [...DEPARTAMENTO_TRABAJADOR_PROVIDERS],
  controllers: [DepartamentoTrabajadorController],
})
export class DepartamentoTrabajadorModule {}
