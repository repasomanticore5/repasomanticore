import { Module } from '@nestjs/common';
import { BodegaTipoController } from './bodega-tipo.controller';
import { BODEGA_TIPO_IMPORTS } from './constantes/bodega-tipo.imports';
import { BODEGA_TIPO_PROVIDERS } from './constantes/bodega-tipo.providers';

@Module({
  imports: [...BODEGA_TIPO_IMPORTS],
  providers: [...BODEGA_TIPO_PROVIDERS],
  exports: [...BODEGA_TIPO_PROVIDERS],
  controllers: [BodegaTipoController],
})
export class BodegaTipoModule {}
