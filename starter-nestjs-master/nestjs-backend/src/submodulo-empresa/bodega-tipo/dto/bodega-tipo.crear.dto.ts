import {IsInt, IsNotEmpty} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class BodegaTipoCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BASQUE

    @IsNotEmpty()
    @IsInt()
    @Expose()
    bodega: number;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    tipoBodega: number;
}
