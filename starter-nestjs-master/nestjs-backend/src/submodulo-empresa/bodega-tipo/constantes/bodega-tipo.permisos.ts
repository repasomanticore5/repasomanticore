import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_BODEGA_TIPO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'bodega-tipo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/bodega-tipo/, // /bodega-tipo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/bodega-tipo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /bodega-tipo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/bodega-tipo\/\d+\/modificar-habilitado/, // /bodega-tipo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/bodega-tipo\/\d+/, // /bodega-tipo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
