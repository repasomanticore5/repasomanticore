import { TypeOrmModule } from '@nestjs/typeorm';
import { BodegaTipoEntity } from '../bodega-tipo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const BODEGA_TIPO_IMPORTS = [
  TypeOrmModule.forFeature([BodegaTipoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
