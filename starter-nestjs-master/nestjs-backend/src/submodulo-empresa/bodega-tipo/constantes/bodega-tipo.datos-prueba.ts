import { BodegaTipoCrearDto } from '../dto/bodega-tipo.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const BODEGA_TIPO_DATOS_PRUEBA: ((idBodega: number, idTipoBodega: number) => BodegaTipoCrearDto)[] = [
  (idBodega: number, idTipoBodega: number) => {
    const dato = new BodegaTipoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.bodega = idBodega;
    dato.tipoBodega = idTipoBodega;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
