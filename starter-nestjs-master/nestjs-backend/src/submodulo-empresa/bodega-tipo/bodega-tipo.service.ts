import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {BodegaTipoEntity} from './bodega-tipo.entity';
import {BodegaTipoBusquedaDto} from './dto/bodega-tipo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class BodegaTipoService extends ServicioComun<BodegaTipoEntity, BodegaTipoBusquedaDto> {
    constructor(
        @InjectRepository(BodegaTipoEntity, NOMBRE_CADENA_CONEXION)
        public bodegaTipoEntityRepository: Repository<BodegaTipoEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            bodegaTipoEntityRepository,
            _connection,
            BodegaTipoEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar
        );
    }

    async eliminar(id: number): Promise<{ mensaje: string }> {
        try {
            await this.bodegaTipoEntityRepository
                .createQueryBuilder('bodegaTipo')
                .delete()
                .where('id = :idA', {idA: id})
                .execute();

            return {
                mensaje: 'Ok',
            }
        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar bodega tipo',
                error: e,
                data: {id}
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar bodega tipo',
            });
        }
    }
}
