import {
    Controller, Delete,
    Get,
    HttpCode, InternalServerErrorException, Param,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {BodegaTipoService} from './bodega-tipo.service';
import {BodegaTipoHabilitadoDto} from './dto/bodega-tipo.habilitado.dto';
import {BODEGA_TIPO_OCC} from './constantes/bodega-tipo.occ';
import {BodegaTipoCrearDto} from './dto/bodega-tipo.crear.dto';
import {BodegaTipoActualizarDto} from './dto/bodega-tipo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {BodegaTipoEntity} from './bodega-tipo.entity';
import {BodegaTipoBusquedaDto} from './dto/bodega-tipo.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'bodega-tipo';

@Controller(nombreControlador)
export class BodegaTipoController extends ControladorComun {
    constructor(
        private readonly _bodegaTipoService: BodegaTipoService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _bodegaTipoService,
            _seguridadService,
            _auditoriaService,
            BODEGA_TIPO_OCC(
                BodegaTipoHabilitadoDto,
                BodegaTipoCrearDto,
                BodegaTipoActualizarDto,
                BodegaTipoBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: BodegaTipoBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._bodegaTipoService.bodegaTipoEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<BodegaTipoEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._bodegaTipoService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._bodegaTipoService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<BodegaTipoEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<BodegaTipoEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                consulta.where = this.setearFiltro(
                    'bodega',
                    consultaWhere,
                    parametrosConsulta.bodega,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._bodegaTipoService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            qb.leftJoinAndSelect(aliasTabla + '.tipoBodega', 'tipoBodega');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            if (parametrosConsulta.bodegaTipoPorBodega) {
                qb.andWhere(aliasTabla + '.bodega = :idBodega', {
                    idBodega: parametrosConsulta.bodegaTipoPorBodega,
                })
            }
            //
            if (parametrosConsulta.busqueda) {
                qb.andWhere('tipoBodega.nombre LIKE :nombreTB', {
                    nombreTB: `%${parametrosConsulta.busqueda}%`,
                });
            }

            if (parametrosConsulta.findById) {
                qb.andWhere(aliasTabla + '.id = :idBT', {
                    idBT: parametrosConsulta.findById,
                });
            }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }

    @Delete(':id')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async eliminar(
        @Param('id') id: string,
    ) {
        try {
            return await this._bodegaTipoService
                .eliminar(+id);
        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar bodega tipo',
                error: e,
                data: {id}
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar bodega tipo',
            });
        }

    }
}
