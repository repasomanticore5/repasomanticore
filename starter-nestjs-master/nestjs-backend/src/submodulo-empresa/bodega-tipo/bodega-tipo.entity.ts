import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {BodegaEntity} from '../bodega/bodega.entity';
import {TipoBodegaEntity} from '../tipo-bodega/tipo-bodega.entity';
import {CaracteristicaBodegaTipoEntity} from '../caracteristica-bodega-tipo/caracteristica-bodega-tipo.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'BODEGA_TIPO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class BodegaTipoEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_BODEGA_TIPO',
        unsigned: true,
    })
    id: number;

    @ManyToOne(
        () => BodegaEntity,
        (r) => r.bodegasTipo,
        {nullable: false}
    )
    bodega: BodegaEntity | number;

    @ManyToOne(
        () => TipoBodegaEntity,
        (r) => r.bodegasTipo,
        {nullable: false}
    )
    tipoBodega: TipoBodegaEntity | number;

    @OneToMany(
        () => CaracteristicaBodegaTipoEntity,
        (r) => r.bodegaTipo
    )
    caracteristicasBodegaTipo: CaracteristicaBodegaTipoEntity[];
}
