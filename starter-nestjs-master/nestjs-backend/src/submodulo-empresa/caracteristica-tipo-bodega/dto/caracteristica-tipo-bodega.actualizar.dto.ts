import {IsInt, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class CaracteristicaTipoBodegaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsInt()
    @Expose()
    caracteristicaBodega?: number;

    @IsOptional()
    @IsInt()
    @Expose()
    tipoBodega?: number;
}
