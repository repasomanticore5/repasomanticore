import {IsInt, IsNotEmpty} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class CaracteristicaTipoBodegaCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsInt()
    @Expose()
    caracteristicaBodega: number;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    tipoBodega: number;
}
