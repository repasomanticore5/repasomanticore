import { Module } from '@nestjs/common';
import { CaracteristicaTipoBodegaController } from './caracteristica-tipo-bodega.controller';
import { CARACTERISTICA_TIPO_BODEGA_IMPORTS } from './constantes/caracteristica-tipo-bodega.imports';
import { CARACTERISTICA_TIPO_BODEGA_PROVIDERS } from './constantes/caracteristica-tipo-bodega.providers';

@Module({
  imports: [...CARACTERISTICA_TIPO_BODEGA_IMPORTS],
  providers: [...CARACTERISTICA_TIPO_BODEGA_PROVIDERS],
  exports: [...CARACTERISTICA_TIPO_BODEGA_PROVIDERS],
  controllers: [CaracteristicaTipoBodegaController],
})
export class CaracteristicaTipoBodegaModule {}
