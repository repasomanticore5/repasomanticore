import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {CaracteristicaBodegaEntity} from '../caracteristica-bodega/caracteristica-bodega.entity';
import {CaracteristicaBodegaTipoEntity} from '../caracteristica-bodega-tipo/caracteristica-bodega-tipo.entity';
import {TipoBodegaEntity} from '../tipo-bodega/tipo-bodega.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CARACTERISTICA_TIPO_BODEGA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class CaracteristicaTipoBodegaEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_CARACTERISTICA_TIPO_BODEGA',
        unsigned: true,
    })
    id: number;

    @ManyToOne(
        () => CaracteristicaBodegaEntity,
        (r) => r.caracteristicasTipoBodega,
        { nullable: false }
    )
    caracteristicaBodega: CaracteristicaBodegaEntity | number;

    @ManyToOne(
        () => TipoBodegaEntity,
        (r) => r.caracteristicasTipoBodega,
        { nullable: false }
    )
    tipoBodega: TipoBodegaEntity | number;

    @OneToMany(
        () => CaracteristicaBodegaTipoEntity,
        (r) => r.bodegaTipo
    )
    caracteristicasBodegaTipo: CaracteristicaBodegaTipoEntity[];
}
