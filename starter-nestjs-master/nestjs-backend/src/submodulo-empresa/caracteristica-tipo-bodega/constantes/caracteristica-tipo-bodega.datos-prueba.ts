import { CaracteristicaTipoBodegaCrearDto } from '../dto/caracteristica-tipo-bodega.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const CARACTERISTICA_TIPO_BODEGA_DATOS_PRUEBA: ((idCaracteristicaBodega: number, idTipoBodega: number) => CaracteristicaTipoBodegaCrearDto)[] = [
  (idCaracteristicaBodega: number, idTipoBodega: number) => {
    const dato = new CaracteristicaTipoBodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.caracteristicaBodega = idCaracteristicaBodega;
    dato.tipoBodega = idTipoBodega;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
