import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CARACTERISTICA_TIPO_BODEGA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'caracteristica-tipo-bodega';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/caracteristica-tipo-bodega/, // /caracteristica-tipo-bodega
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/caracteristica-tipo-bodega\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /caracteristica-tipo-bodega?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-tipo-bodega\/\d+\/modificar-habilitado/, // /caracteristica-tipo-bodega/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-tipo-bodega\/\d+/, // /caracteristica-tipo-bodega/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
