import { TypeOrmModule } from '@nestjs/typeorm';
import { FormulaArtEntity } from '../formula-art.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const FORMULA_ART_IMPORTS = [
  TypeOrmModule.forFeature([FormulaArtEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
