import { FormulaArtCrearDto } from '../dto/formula-art.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const FORMULA_ART_DATOS_PRUEBA: ((idPrefijo: number, idSufijo: number, idEmpresa: number) => FormulaArtCrearDto)[] = [
  (idPrefijo: number, idSufijo: number, idEmpresa: number) => {
    const dato = new FormulaArtCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'COMBO NAVIDEÑO';
    dato.codigoGeneral = 'III KKK LLL';
    dato.estadoFormulacion = 'EC';
    dato.tipo = 'CB';
    dato.formulaPrefijo = idPrefijo;
    dato.formulaSufijo = idSufijo;
    dato.empresa = idEmpresa;
    return dato;
  },
  (idPrefijo: number, idSufijo: number, idEmpresa: number) => {
    const dato = new FormulaArtCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'COMBO BF';
    dato.codigoGeneral = 'MMM NNN OOO';
    dato.estadoFormulacion = 'EC';
    dato.tipo = 'CB';
    dato.formulaPrefijo = idPrefijo;
    dato.formulaSufijo = idSufijo;
    dato.empresa = idEmpresa;
    return dato;
  },
  (idPrefijo: number, idSufijo: number, idEmpresa: number) => {
    const dato = new FormulaArtCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'COMBO FQ';
    dato.codigoGeneral = 'PPP QQQ RRR';
    dato.estadoFormulacion = 'EC';
    dato.tipo = 'CB';
    dato.formulaPrefijo = idPrefijo;
    dato.formulaSufijo = idSufijo;
    dato.empresa = idEmpresa;
    return dato;
  },
];
