import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_FORMULA_ART: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'formula-art';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/formula-art/, // /formula-art
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/formula-art\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /formula-art?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-art\/\d+\/modificar-habilitado/, // /formula-art/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-art\/\d+/, // /formula-art/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
