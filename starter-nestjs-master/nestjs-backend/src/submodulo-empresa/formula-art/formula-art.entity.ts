import {
  Column,
  Entity,
  Index, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn, TreeChildren, TreeParent,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { FormulaSufijoEntity } from '../formula-sufijo/formula-sufijo.entity';
import { FormulaPrefijoEntity } from '../formula-prefijo/formula-prefijo.entity';
import { IngredienteFormulaArtEntity } from '../ingrediente-formula-art/ingrediente-formula-art.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'FORMULA_ART')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre', 'estadoFormulacion', 'tipo', 'nivel'])
export class FormulaArtEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_FORMULA_ART',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'codigo_general',
    type: 'text',
    nullable: false,
  })
  codigoGeneral: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  @Column({
    name: 'estado_formulacion',
    type: 'varchar',
    default: 'EC',
    length: 3,
    comment: '',
    nullable: false,
  })
  estadoFormulacion: 'EC' | 'FI' | 'PA' = 'EC';

  @Column({
    name: 'tipo',
    type: 'varchar',
    length: 3,
    comment: 'RE: receta, CB: combo',
    nullable: false,
  })
  tipo: 'RE' | 'CB';

  @Column({
    name: 'nivel',
    type: 'int',
    default: 1,
    nullable: false,
  })
  nivel: number;

  @Column({
    name: 'es_prod_terminado',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esProdTerminado: 0 | 1 = 0;

  @Column({
    name: 'es_semielaborado',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esSemielaborado: 0 | 1 = 0;

  @TreeParent()
  formulaArtPadre: FormulaArtEntity | number;

  @ManyToOne(
    () => EmpresaEntity,
    (r) => r.formulasArt,
    { nullable: true },
  )
  empresa: EmpresaEntity | number;

  @ManyToOne(
    () => FormulaSufijoEntity,
    (r) => r.formulasArt,
    { nullable: true },
  )
  formulaSufijo: FormulaSufijoEntity | number;

  @ManyToOne(
    () => FormulaPrefijoEntity,
    (r) => r.formulasArt,
    { nullable: true },
  )
  formulaPrefijo: FormulaPrefijoEntity | number;

  @TreeChildren()
  formulaArtHijo: FormulaArtEntity[];

  @OneToMany(
    () => IngredienteFormulaArtEntity,
    (r) => r.formulaArt,
  )
  ingredientesFormulaArt: IngredienteFormulaArtEntity[];
}
