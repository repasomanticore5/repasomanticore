import { Module } from '@nestjs/common';
import { FormulaArtController } from './formula-art.controller';
import { FORMULA_ART_IMPORTS } from './constantes/formula-art.imports';
import { FORMULA_ART_PROVIDERS } from './constantes/formula-art.providers';

@Module({
  imports: [...FORMULA_ART_IMPORTS],
  providers: [...FORMULA_ART_PROVIDERS],
  exports: [...FORMULA_ART_PROVIDERS],
  controllers: [FormulaArtController],
})
export class FormulaArtModule {}
