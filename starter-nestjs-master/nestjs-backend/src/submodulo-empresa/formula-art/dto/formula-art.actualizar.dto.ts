import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class FormulaArtActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @Expose()
  codigoGeneral: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripion: string;

  @IsOptional()
  @IsString()
  @IsIn(['EC', 'FI', 'PA'])
  @Expose()
  estadoFormulacion: 'EC' | 'FI' | 'PA';

  @IsOptional()
  @IsString()
  @IsIn(['RE', 'CB'])
  @Expose()
  tipo: 'RE' | 'CB';

  @IsOptional()
  @IsInt()
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @IsIn([0, 1])
  @Expose()
  esProdTerminado: 0 | 1 = 0;

  @IsOptional()
  @IsInt()
  @IsIn([0, 1])
  @Expose()
  esSemielaborado: 0 | 1 = 0;
}
