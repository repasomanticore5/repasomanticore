import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class FormulaArtCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  codigoGeneral: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripion: string;

  @IsNotEmpty()
  @IsString()
  @IsIn(['EC', 'FI', 'PA'])
  @Expose()
  estadoFormulacion: 'EC' | 'FI' | 'PA';

  @IsNotEmpty()
  @IsString()
  @IsIn(['RE', 'CB'])
  @Expose()
  tipo: 'RE' | 'CB';

  @IsNotEmpty()
  @IsInt()
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @IsIn([0, 1])
  @Expose()
  esProdTerminado: 0 | 1 = 0;

  @IsOptional()
  @IsInt()
  @IsIn([0, 1])
  @Expose()
  esSemielaborado: 0 | 1 = 0;

  @IsOptional()
  @IsInt()
  @Expose()
  formulaArtPadre: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresa: number;

  @IsOptional()
  @IsInt()
  @Expose()
  formulaSufijo: number;

  @IsOptional()
  @IsInt()
  @Expose()
  formulaPrefijo: number;
}
