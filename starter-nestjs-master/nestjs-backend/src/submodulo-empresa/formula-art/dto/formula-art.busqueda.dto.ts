import { IsIn, IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class FormulaArtBusquedaDto extends BusquedaComunProyectoDto {

  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsString()
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @Expose()
  codigoGeneral: string;

  @IsOptional()
  @IsString()
  @IsIn(['EC', 'FI', 'PA'])
  @Expose()
  estadoFormulacion: 'EC' | 'FI' | 'PA';
}
