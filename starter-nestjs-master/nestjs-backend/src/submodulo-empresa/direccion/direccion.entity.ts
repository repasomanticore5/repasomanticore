import {
    Column,
    Entity,
    Index, JoinColumn,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EdificioEntity } from '../edificio/edificio.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DIRECCION')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class DireccionEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_DIRECCION',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'numero_calle',
    type: 'varchar',
    length: 10,
    nullable: false,
  })
  numeroCalle: string;

  @Column({
    name: 'calle_principal',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  callePrincipal: string;

  @Column({
    name: 'calle_secundaria',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  calleSecundaria: string;

  @Column({
    name: 'nombre_edificio',
    type: 'varchar',
    length: 200,
    nullable: true,
  })
  nombreEdificio: string = null;

  @Column({
    name: 'piso',
    type: 'varchar',
    length: 3,
    nullable: true,
  })
  piso: string = null;

  @Column({
    name: 'referencia',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  referencia: string = null;

  @Column({
    name: 'codigo_postal',
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  codigoPostal: string = null;

  @Column({
    name: 'es_principal',
    type: 'tinyint',
    default: 0,
    comment: '1: si, 0: no',
    nullable: false,
  })
  esPrincipal: 1 | 0 = 0;

  @OneToOne(() => EdificioEntity, (r) => r.direccion, { nullable: false })
  @JoinColumn()
  edificio: EdificioEntity;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
