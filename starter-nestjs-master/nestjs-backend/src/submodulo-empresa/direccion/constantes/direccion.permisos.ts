import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_DIRECCION: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'direccion';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/direccion/, // /direccion
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/direccion\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /direccion?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/direccion\/\d+\/modificar-habilitado/, // /direccion/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/direccion\/\d+/, // /direccion/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
