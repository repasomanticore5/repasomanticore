import { DireccionCrearDto } from '../dto/direccion.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const DIRECCION_DATOS_PRUEBA: ((
  idEdificio: number,
) => DireccionCrearDto)[] = [
  (idEdificio: number) => {
    const dato = new DireccionCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.numeroCalle = '66';
    dato.callePrincipal = 'Avenida siempre viva';
    dato.calleSecundaria = 'Hell';
    dato.esPrincipal = 1;
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.edificio = idEdificio;
    return dato;
  },
];
