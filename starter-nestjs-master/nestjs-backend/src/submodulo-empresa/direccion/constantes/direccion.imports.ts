import { TypeOrmModule } from '@nestjs/typeorm';
import { DireccionEntity } from '../direccion.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const DIRECCION_IMPORTS = [
  TypeOrmModule.forFeature([DireccionEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
