import { Module } from '@nestjs/common';
import { DireccionController } from './direccion.controller';
import { DIRECCION_IMPORTS } from './constantes/direccion.imports';
import { DIRECCION_PROVIDERS } from './constantes/direccion.providers';

@Module({
  imports: [...DIRECCION_IMPORTS],
  providers: [...DIRECCION_PROVIDERS],
  exports: [...DIRECCION_PROVIDERS],
  controllers: [DireccionController],
})
export class DireccionModule {}
