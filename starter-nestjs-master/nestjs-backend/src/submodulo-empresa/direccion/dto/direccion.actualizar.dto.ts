import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';

export class DireccionActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(10)
  @Expose()
  numeroCalle: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  callePrincipal: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  calleSecundaria: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  nombreEdificio: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(3)
  @Expose()
  piso: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  referencia: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  codigoPostal: string;

  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  esPrincipal: 0 | 1;

  @IsOptional()
  @IsInt()
  @Expose()
  edificio: number;
}
