import {
    IsIn, IsInt,
    IsNotEmpty,
    IsOptional,
    IsString,
    MaxLength,
    MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class DireccionCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(10)
  @Expose()
  numeroCalle: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  callePrincipal: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  calleSecundaria: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(200)
  @Expose()
  nombreEdificio: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(3)
  @Expose()
  piso: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  referencia: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  @Expose()
  codigoPostal: string;

  @IsNotEmpty()
  @IsIn([0, 1])
  @Expose()
  esPrincipal: 0 | 1;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  edificio: number;
}
