import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { AreaPisoEntity } from '../area-piso/area-piso.entity';
import { EdificioEntity } from '../edificio/edificio.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'PISO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre'])
export class PisoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_PISO',
    unsigned: true,
  })
  id: number;

  @Index()
  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'orden_piso',
    type: 'int',
    nullable: false,
  })
  ordenPiso: number;

  @ManyToOne(() => EdificioEntity, (r) => r.pisos, { nullable: true })
  edificio: EdificioEntity | number;

  @OneToMany(() => AreaPisoEntity, (areaPiso) => areaPiso.piso)
  areasPiso: AreaPisoEntity[];
}
