import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class PisoCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  ordenPiso: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  edificio: number;
}
