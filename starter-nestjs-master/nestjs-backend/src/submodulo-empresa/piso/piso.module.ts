import { Module } from '@nestjs/common';
import { PisoController } from './piso.controller';
import { PISO_IMPORTS } from './constantes/piso.imports';
import { PISO_PROVIDERS } from './constantes/piso.providers';

@Module({
  imports: [...PISO_IMPORTS],
  providers: [...PISO_PROVIDERS],
  exports: [...PISO_PROVIDERS],
  controllers: [PisoController],
})
export class PisoModule {}
