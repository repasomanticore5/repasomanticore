import { PisoCrearDto } from '../dto/piso.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const PISO_DATOS_PRUEBA: ((idEdificio: number) => PisoCrearDto)[] = [
  (idEdificio: number) => {
    const dato = new PisoCrearDto();
    dato.ordenPiso = 1;
    dato.nombre = 'piso 1';
    dato.edificio = idEdificio;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
