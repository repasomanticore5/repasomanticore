import { TypeOrmModule } from '@nestjs/typeorm';
import { PisoEntity } from '../piso.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const PISO_IMPORTS = [
  TypeOrmModule.forFeature([PisoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
