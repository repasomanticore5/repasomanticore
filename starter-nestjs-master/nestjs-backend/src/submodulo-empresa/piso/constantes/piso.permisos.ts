import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_PISO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'piso';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/piso/, // /piso
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/piso\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /piso?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/piso\/\d+\/modificar-habilitado/, // /piso/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/piso\/\d+/, // /piso/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
