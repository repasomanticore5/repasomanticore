import {IsInt, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class CaracteristicaBodegaTipoCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsNumberString()
    @Expose()
    valorInicial: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    valorFinal?: string;

    @IsOptional()
    @IsInt()
    @Expose()
    bodegaTipo: number;

    @IsOptional()
    @IsInt()
    @Expose()
    caracteristicaTipoBodega: number;
}
