import {IsNumberString, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class CaracteristicaBodegaTipoBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    bodegaTipo: string;

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
