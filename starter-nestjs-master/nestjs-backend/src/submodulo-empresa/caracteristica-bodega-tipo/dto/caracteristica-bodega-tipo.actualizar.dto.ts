import {IsInt, IsNumberString, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class CaracteristicaBodegaTipoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;


    @IsOptional()
    @IsNumberString()
    @Expose()
    valorInicial?: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    valorFinal?: string;

    @IsOptional()
    @IsInt()
    @Expose()
    bodegaTipo?: number;
}
