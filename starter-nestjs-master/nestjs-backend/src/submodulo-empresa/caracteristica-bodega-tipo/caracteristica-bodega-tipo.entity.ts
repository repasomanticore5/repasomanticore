import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {BodegaTipoEntity} from '../bodega-tipo/bodega-tipo.entity';
import {CaracteristicaTipoBodegaEntity} from '../caracteristica-tipo-bodega/caracteristica-tipo-bodega.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CARACTERISTICA_BODEGA_TIPO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class CaracteristicaBodegaTipoEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_CARACTERISTICA_BODEGA_TIPO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'valor_inicial',
        type: 'decimal',
        scale: 4,
        precision: 10,
        nullable: false,
    })
    valorInicial: number;

    @Column({
        name: 'valor_final',
        type: 'decimal',
        scale: 4,
        precision: 10,
        nullable: true,
    })
    valorFinal: number = null;

    // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
    // entidadRelacionPapa: EntidadRelacionPapa;

    @ManyToOne(
        () => BodegaTipoEntity,
        (r) => r.caracteristicasBodegaTipo,
        {nullable: true}
    )
    bodegaTipo: BodegaTipoEntity | number;

    @ManyToOne(
        () => CaracteristicaTipoBodegaEntity,
        (r) => r.caracteristicasBodegaTipo,
        {nullable: true}
    )
    caracteristicaTipoBodega: CaracteristicaTipoBodegaEntity | number;
}
