import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { CaracteristicaBodegaTipoEntity } from './caracteristica-bodega-tipo.entity';
import {CaracteristicaBodegaTipoBusquedaDto} from './dto/caracteristica-bodega-tipo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class CaracteristicaBodegaTipoService extends ServicioComun<CaracteristicaBodegaTipoEntity, CaracteristicaBodegaTipoBusquedaDto> {
  constructor(
    @InjectRepository(CaracteristicaBodegaTipoEntity, NOMBRE_CADENA_CONEXION)
    public caracteristicaBodegaTipoEntityRepository: Repository<CaracteristicaBodegaTipoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        caracteristicaBodegaTipoEntityRepository,
      _connection,
        CaracteristicaBodegaTipoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
