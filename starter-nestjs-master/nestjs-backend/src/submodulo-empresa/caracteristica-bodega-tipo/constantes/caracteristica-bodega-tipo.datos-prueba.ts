import {CaracteristicaBodegaTipoCrearDto} from '../dto/caracteristica-bodega-tipo.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const CARACTERISTICA_BODEGA_TIPO_DATOS_PRUEBA: (
    (idCaracteristicaTipoBodega: number, idBodegaTipo: number) => CaracteristicaBodegaTipoCrearDto)[] = [
  (idCaracteristicaTipoBodega: number, idBodegaTipo: number) => {
    const dato = new CaracteristicaBodegaTipoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    const valor1 = Math.random()*100;
    const valor2 = Math.random()*100;
    if (valor1 > valor2) {
      dato.valorInicial = valor2.toString();
      dato.valorFinal = valor1.toString();
    } else {
      dato.valorInicial = valor1.toString();
      dato.valorFinal = valor2.toString();
    }
    dato.bodegaTipo = idBodegaTipo;
    dato.caracteristicaTipoBodega = idCaracteristicaTipoBodega;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
