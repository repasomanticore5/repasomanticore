import { TypeOrmModule } from '@nestjs/typeorm';
import { CaracteristicaBodegaTipoEntity } from '../caracteristica-bodega-tipo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const CARACTERISTICA_BODEGA_TIPO_IMPORTS = [
  TypeOrmModule.forFeature([CaracteristicaBodegaTipoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
