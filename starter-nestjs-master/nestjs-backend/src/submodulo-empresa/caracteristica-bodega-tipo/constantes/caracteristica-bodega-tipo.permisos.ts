import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CARACTERISTICA_BODEGA_TIPO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'caracteristica-bodega-tipo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/caracteristica-bodega-tipo/, // /caracteristica-bodega-tipo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/caracteristica-bodega-tipo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /caracteristica-bodega-tipo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-bodega-tipo\/\d+\/modificar-habilitado/, // /caracteristica-bodega-tipo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-bodega-tipo\/\d+/, // /caracteristica-bodega-tipo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
