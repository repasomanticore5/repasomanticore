import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {CaracteristicaBodegaTipoService} from './caracteristica-bodega-tipo.service';
import {CaracteristicaBodegaTipoHabilitadoDto} from './dto/caracteristica-bodega-tipo.habilitado.dto';
import {CARACTERISTICA_BODEGA_TIPO_OCC} from './constantes/caracteristica-bodega-tipo.occ';
import {CaracteristicaBodegaTipoCrearDto} from './dto/caracteristica-bodega-tipo.crear.dto';
import {CaracteristicaBodegaTipoActualizarDto} from './dto/caracteristica-bodega-tipo.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {CaracteristicaBodegaTipoEntity} from './caracteristica-bodega-tipo.entity';
import {CaracteristicaBodegaTipoBusquedaDto} from './dto/caracteristica-bodega-tipo.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'caracteristica-bodega-tipo';

@Controller(nombreControlador)
export class CaracteristicaBodegaTipoController extends ControladorComun {
    constructor(
        private readonly _caracteristicaBodegaTipoService: CaracteristicaBodegaTipoService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _caracteristicaBodegaTipoService,
            _seguridadService,
            _auditoriaService,
            CARACTERISTICA_BODEGA_TIPO_OCC(
                CaracteristicaBodegaTipoHabilitadoDto,
                CaracteristicaBodegaTipoCrearDto,
                CaracteristicaBodegaTipoActualizarDto,
                CaracteristicaBodegaTipoBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: CaracteristicaBodegaTipoBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            // QUERY BUILDER https://typeorm.io/#/select-query-builder
            const qb = this._caracteristicaBodegaTipoService.caracteristicaBodegaTipoEntityRepository
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<CaracteristicaBodegaTipoEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._caracteristicaBodegaTipoService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._caracteristicaBodegaTipoService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<CaracteristicaBodegaTipoEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<CaracteristicaBodegaTipoEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                consulta.where = this.setearFiltro(
                    'bodegaTipo',
                    consultaWhere,
                    parametrosConsulta.bodegaTipo,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._caracteristicaBodegaTipoService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            if (parametrosConsulta.busqueda) {
                qb.andWhere(aliasTabla + '.valorInicial LIKE :valorInicial', {
                    valorInicial: `%${parametrosConsulta.busqueda}%`,
                })
            }

            if (parametrosConsulta.busqueda) {
                qb.orWhere(aliasTabla + '.valorFinal LIKE :valorFinal', {
                    valorFinal: `%${parametrosConsulta.busqueda}%`,
                })
            }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }
}
