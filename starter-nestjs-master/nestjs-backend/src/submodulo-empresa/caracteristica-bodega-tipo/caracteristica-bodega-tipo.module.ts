import { Module } from '@nestjs/common';
import { CaracteristicaBodegaTipoController } from './caracteristica-bodega-tipo.controller';
import { CARACTERISTICA_BODEGA_TIPO_IMPORTS } from './constantes/caracteristica-bodega-tipo.imports';
import { CARACTERISTICA_BODEGA_TIPO_PROVIDERS } from './constantes/caracteristica-bodega-tipo.providers';

@Module({
  imports: [...CARACTERISTICA_BODEGA_TIPO_IMPORTS],
  providers: [...CARACTERISTICA_BODEGA_TIPO_PROVIDERS],
  exports: [...CARACTERISTICA_BODEGA_TIPO_PROVIDERS],
  controllers: [CaracteristicaBodegaTipoController],
})
export class CaracteristicaBodegaTipoModule {}
