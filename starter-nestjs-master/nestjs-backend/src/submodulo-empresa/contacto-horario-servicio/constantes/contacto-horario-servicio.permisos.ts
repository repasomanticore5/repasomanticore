import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CONTACTO_HORARIO_SERVICIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'contacto-horario-servicio';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/contacto-horario-servicio/, // /contacto-horario-servicio
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/contacto-horario-servicio\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /contacto-horario-servicio?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/contacto-horario-servicio\/\d+\/modificar-habilitado/, // /contacto-horario-servicio/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/contacto-horario-servicio\/\d+/, // /contacto-horario-servicio/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
