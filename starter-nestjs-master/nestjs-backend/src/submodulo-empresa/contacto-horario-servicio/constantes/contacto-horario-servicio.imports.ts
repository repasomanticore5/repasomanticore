import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactoHorarioServicioEntity } from '../contacto-horario-servicio.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const CONTACTO_HORARIO_SERVICIO_IMPORTS = [
  TypeOrmModule.forFeature([ContactoHorarioServicioEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
