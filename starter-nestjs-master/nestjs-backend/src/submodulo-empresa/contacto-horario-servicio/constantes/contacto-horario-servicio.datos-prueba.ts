import {ContactoHorarioServicioCrearDto} from '../dto/contacto-horario-servicio.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const CONTACTO_HORARIO_SERVICIO_DATOS_PRUEBA: (
    (idHorarioServicio: number, idContactoEmpresa: number) => ContactoHorarioServicioCrearDto)[] = [
  (idHorarioServicio: number, idContactoEmpresa: number) => {
    const dato = new ContactoHorarioServicioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.contactoEmpresa = idContactoEmpresa;
    dato.horarioServicio = idHorarioServicio;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
