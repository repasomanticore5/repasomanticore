import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {ContactoHorarioServicioService} from './contacto-horario-servicio.service';
import {ContactoHorarioServicioHabilitadoDto} from './dto/contacto-horario-servicio.habilitado.dto';
import {CONTACTO_HORARIO_SERVICIO_OCC} from './constantes/contacto-horario-servicio.occ';
import {ContactoHorarioServicioCrearDto} from './dto/contacto-horario-servicio.crear.dto';
import {ContactoHorarioServicioActualizarDto} from './dto/contacto-horario-servicio.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {ContactoHorarioServicioEntity} from './contacto-horario-servicio.entity';
import {ContactoHorarioServicioBusquedaDto} from './dto/contacto-horario-servicio.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'contacto-horario-servicio';

@Controller(nombreControlador)
export class ContactoHorarioServicioController extends ControladorComun {
    constructor(
        private readonly _contactoHorarioServicioService: ContactoHorarioServicioService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _contactoHorarioServicioService,
            _seguridadService,
            _auditoriaService,
            CONTACTO_HORARIO_SERVICIO_OCC(
                ContactoHorarioServicioHabilitadoDto,
                ContactoHorarioServicioCrearDto,
                ContactoHorarioServicioActualizarDto,
                ContactoHorarioServicioBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: ContactoHorarioServicioBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._contactoHorarioServicioService.contactoHorarioServicioEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<ContactoHorarioServicioEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._contactoHorarioServicioService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._contactoHorarioServicioService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<ContactoHorarioServicioEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<ContactoHorarioServicioEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._contactoHorarioServicioService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);
            
            // RELACIONES CON PAPAS o HIJOS
            
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            
            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }
}
