import {IsInt, IsNotEmpty, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class ContactoHorarioServicioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;


    @IsOptional()
    @IsInt()
    @Expose()
    contactoEmpresa: number;

    @IsOptional()
    @IsInt()
    @Expose()
    horarioServicio: number;
}
