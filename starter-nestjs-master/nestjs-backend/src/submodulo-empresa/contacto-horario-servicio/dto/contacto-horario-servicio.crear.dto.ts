import {IsInt, IsNotEmpty, IsOptional} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class ContactoHorarioServicioCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsInt()
    @Expose()
    contactoEmpresa: number;

    @IsOptional()
    @IsInt()
    @Expose()
    horarioServicio: number;
}
