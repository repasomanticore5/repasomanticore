import { Module } from '@nestjs/common';
import { ContactoHorarioServicioController } from './contacto-horario-servicio.controller';
import { CONTACTO_HORARIO_SERVICIO_IMPORTS } from './constantes/contacto-horario-servicio.imports';
import { CONTACTO_HORARIO_SERVICIO_PROVIDERS } from './constantes/contacto-horario-servicio.providers';

@Module({
  imports: [...CONTACTO_HORARIO_SERVICIO_IMPORTS],
  providers: [...CONTACTO_HORARIO_SERVICIO_PROVIDERS],
  exports: [...CONTACTO_HORARIO_SERVICIO_PROVIDERS],
  controllers: [ContactoHorarioServicioController],
})
export class ContactoHorarioServicioModule {}
