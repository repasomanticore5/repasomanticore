import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {ContactoEmpresaEntity} from '../contacto-empresa/contacto-empresa.entity';
import {HorarioServicioEntity} from '../horario-servicio/horario-servicio.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CONTACTO_HORARIO_SERVICIO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ContactoHorarioServicioEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_CONTACTO_HORARIO_SERVICIO',
        unsigned: true,
    })
    id: number;

    @ManyToOne(
        () => ContactoEmpresaEntity,
        (r) => r.contactosHorariosServicio,
        { nullable: false }
    )
    contactoEmpresa: ContactoEmpresaEntity | number;

    @ManyToOne(
        () => HorarioServicioEntity,
        (r) => r.contactosHorariosServicio,
        { nullable: true }
    )
    horarioServicio: HorarioServicioEntity | number;

}
