import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class CodigoPaisCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(70)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(3)
  @Expose()
  codigoIso3166: string;
}
