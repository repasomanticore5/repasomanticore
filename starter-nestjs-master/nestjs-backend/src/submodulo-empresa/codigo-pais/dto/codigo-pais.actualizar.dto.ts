import {IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class CodigoPaisActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(70)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(3)
    @Expose()
    codigoIso3166: string;
}
