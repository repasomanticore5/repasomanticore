import { TypeOrmModule } from '@nestjs/typeorm';
import { CodigoPaisEntity } from '../codigo-pais.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const CODIGO_PAIS_IMPORTS = [
  TypeOrmModule.forFeature([CodigoPaisEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
