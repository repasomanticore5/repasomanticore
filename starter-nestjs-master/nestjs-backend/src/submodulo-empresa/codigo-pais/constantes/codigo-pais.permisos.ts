import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CODIGO_PAIS: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'codigo-pais';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/codigo-pais/, // /codigo-pais
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/codigo-pais\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /codigo-pais?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/codigo-pais\/\d+\/modificar-habilitado/, // /codigo-pais/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/codigo-pais\/\d+/, // /codigo-pais/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
