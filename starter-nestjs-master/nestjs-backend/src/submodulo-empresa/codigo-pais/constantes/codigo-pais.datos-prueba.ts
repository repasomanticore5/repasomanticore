import { CodigoPaisCrearDto } from '../dto/codigo-pais.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const CODIGO_PAIS_DATOS_PRUEBA: (() => CodigoPaisCrearDto)[] = [
  () => {
    const dato = new CodigoPaisCrearDto();
    dato.nombre = 'USA';
    dato.codigoIso3166 = '840';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new CodigoPaisCrearDto();
    dato.nombre = 'ECUADOR';
    dato.codigoIso3166 = '218';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
