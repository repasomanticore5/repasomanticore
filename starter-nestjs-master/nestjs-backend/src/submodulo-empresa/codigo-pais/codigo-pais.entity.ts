import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CODIGO_PAIS')
@Index([
  'sisHabilitado',
  'sisCreado',
  'sisModificado',
  'nombre',
  'codigoIso3166',
])
export class CodigoPaisEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_CODIGO_PAIS',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 70,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'codigo_iso_3166',
    type: 'varchar',
    length: 3,
    nullable: false,
    unique: true,
  })
  codigoIso3166: string;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  @OneToMany(() => EmpresaEntity, (r) => r.codigoPais)
  empresas: EmpresaEntity[];
}
