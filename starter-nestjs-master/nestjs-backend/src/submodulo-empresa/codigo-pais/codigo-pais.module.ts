import { Module } from '@nestjs/common';
import { CodigoPaisController } from './codigo-pais.controller';
import { CODIGO_PAIS_IMPORTS } from './constantes/codigo-pais.imports';
import { CODIGO_PAIS_PROVIDERS } from './constantes/codigo-pais.providers';

@Module({
  imports: [...CODIGO_PAIS_IMPORTS],
  providers: [...CODIGO_PAIS_PROVIDERS],
  exports: [...CODIGO_PAIS_PROVIDERS],
  controllers: [CodigoPaisController],
})
export class CodigoPaisModule {}
