import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class ServicioEstablecimientoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  precioEstablecimiento: string;

  @IsOptional()
  @IsInt()
  @Expose()
  establecimiento: number;

  @IsOptional()
  @IsInt()
  @Expose()
  articuloEmpresa: number;
}
