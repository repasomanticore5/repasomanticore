import { IsInt, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class ServicioEstablecimientoCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  precioEstablecimiento: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  establecimiento: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  articuloEmpresa: number;
}
