import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EstablecimientoEntity } from '../establecimiento/establecimiento.entity';
import { HorarioServicioEntity } from '../horario-servicio/horario-servicio.entity';
import { ArticuloEmpresaEntity } from '../../submodulo-articulo-empresa/articulo-empresa/articulo-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'SERVICIO_ESTABLECIMIENTO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'establecimiento'])
export class ServicioEstablecimientoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_SERVICIO_ESTABLECIMIENTO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'precio_establecimiento',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  precioEstablecimiento: string;

  @ManyToOne(() => EstablecimientoEntity, (r) => r.serviciosEstablecimiento, {
    nullable: false,
  })
  establecimiento: EstablecimientoEntity | number;

  @ManyToOne(() => ArticuloEmpresaEntity, (r) => r.serviciosEstablecimiento, {
    nullable: false,
  })
  articuloEmpresa: ArticuloEmpresaEntity | number;

  @OneToMany(() => HorarioServicioEntity, (r) => r.servicioEstablecimiento)
  horariosServicio: HorarioServicioEntity[];
}
