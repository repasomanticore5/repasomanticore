import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ServicioEstablecimientoService } from './servicio-establecimiento.service';
import { ServicioEstablecimientoHabilitadoDto } from './dto/servicio-establecimiento.habilitado.dto';
import { SERVICIO_ESTABLECIMIENTO_OCC } from './constantes/servicio-establecimiento.occ';
import { ServicioEstablecimientoCrearDto } from './dto/servicio-establecimiento.crear.dto';
import { ServicioEstablecimientoActualizarDto } from './dto/servicio-establecimiento.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { ServicioEstablecimientoEntity } from './servicio-establecimiento.entity';
import { ServicioEstablecimientoBusquedaDto } from './dto/servicio-establecimiento.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'servicio-establecimiento';

@Controller(nombreControlador)
export class ServicioEstablecimientoController extends ControladorComun {
  constructor(
    private readonly _servicioEstablecimientoService: ServicioEstablecimientoService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _servicioEstablecimientoService,
      _seguridadService,
      _auditoriaService,
      SERVICIO_ESTABLECIMIENTO_OCC(
        ServicioEstablecimientoHabilitadoDto,
        ServicioEstablecimientoCrearDto,
        ServicioEstablecimientoActualizarDto,
        ServicioEstablecimientoBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: ServicioEstablecimientoBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._servicioEstablecimientoService.servicioEstablecimientoEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<ServicioEstablecimientoEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._servicioEstablecimientoService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._servicioEstablecimientoService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<ServicioEstablecimientoEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<ServicioEstablecimientoEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'establecimiento',
          consultaWhere,
          parametrosConsulta.establecimiento,
        );
        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._servicioEstablecimientoService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      qb.leftJoinAndSelect(aliasTabla + '.articuloEmpresa', 'articuloEmpresa');
      qb.leftJoinAndSelect('articuloEmpresa.articuloAE', 'articuloAE');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      if (parametrosConsulta.busqueda) {
        qb.andWhere('articuloAE.nombre LIKE :busqueda', {
          busqueda: `%${parametrosConsulta.busqueda.trim()}%`,
        });
      }

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
