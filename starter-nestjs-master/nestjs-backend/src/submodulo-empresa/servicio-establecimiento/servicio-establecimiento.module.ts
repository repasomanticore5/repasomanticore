import { Module } from '@nestjs/common';
import { ServicioEstablecimientoController } from './servicio-establecimiento.controller';
import { SERVICIO_ESTABLECIMIENTO_IMPORTS } from './constantes/servicio-establecimiento.imports';
import { SERVICIO_ESTABLECIMIENTO_PROVIDERS } from './constantes/servicio-establecimiento.providers';

@Module({
  imports: [...SERVICIO_ESTABLECIMIENTO_IMPORTS],
  providers: [...SERVICIO_ESTABLECIMIENTO_PROVIDERS],
  exports: [...SERVICIO_ESTABLECIMIENTO_PROVIDERS],
  controllers: [ServicioEstablecimientoController],
})
export class ServicioEstablecimientoModule {}
