import { ServicioEstablecimientoCrearDto } from '../dto/servicio-establecimiento.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const SERVICIO_ESTABLECIMIENTO_DATOS_PRUEBA: ((
  idEstablecimiento: number,
  idArticuloEmpresa: number,
) => ServicioEstablecimientoCrearDto)[] = [
  (idEstablecimiento: number, idArticuloEmpresa: number) => {
    const dato = new ServicioEstablecimientoCrearDto();
    dato.establecimiento = idEstablecimiento;
    dato.precioEstablecimiento = '50.00';
    dato.articuloEmpresa = idArticuloEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
