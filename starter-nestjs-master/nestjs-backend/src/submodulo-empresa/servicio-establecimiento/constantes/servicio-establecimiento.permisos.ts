import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_SERVICIO_ESTABLECIMIENTO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'servicio-establecimiento';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/servicio-establecimiento/, // /servicio-establecimiento
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/servicio-establecimiento\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /servicio-establecimiento?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/servicio-establecimiento\/\d+\/modificar-habilitado/, // /servicio-establecimiento/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/servicio-establecimiento\/\d+/, // /servicio-establecimiento/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
