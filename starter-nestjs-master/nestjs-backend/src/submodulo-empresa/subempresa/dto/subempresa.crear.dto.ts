import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class SubempresaCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsInt()
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresaPadre: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresaHija: number;
}
