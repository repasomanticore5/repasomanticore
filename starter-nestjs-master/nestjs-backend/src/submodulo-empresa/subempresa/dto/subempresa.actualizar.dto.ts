import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class SubempresaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresaPadre: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresaHija: number;
}
