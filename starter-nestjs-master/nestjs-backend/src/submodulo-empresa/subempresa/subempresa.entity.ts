import {
  Column,
  Entity,
  Index, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'SUBEMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class SubempresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_SUBEMPRESA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nivel',
    type: 'int',
    nullable: false,
  })
  nivel: number;

  @ManyToOne(
    () => EmpresaEntity,
    (r) => r.subempresasPadres,
    {
      nullable: true,
    },
  )
  empresaPadre: EmpresaEntity | number;

  @ManyToOne(
    () => EmpresaEntity,
    (r) => r.subempresasHijas,
    {
      nullable: true,
    },
  )
  empresaHija: EmpresaEntity | number;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
