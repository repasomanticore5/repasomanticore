import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { SubempresaEntity } from './subempresa.entity';
import {SubempresaBusquedaDto} from './dto/subempresa.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class SubempresaService extends ServicioComun<SubempresaEntity, SubempresaBusquedaDto> {
  constructor(
    @InjectRepository(SubempresaEntity, NOMBRE_CADENA_CONEXION)
    public subempresaEntityRepository: Repository<SubempresaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        subempresaEntityRepository,
      _connection,
        SubempresaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
