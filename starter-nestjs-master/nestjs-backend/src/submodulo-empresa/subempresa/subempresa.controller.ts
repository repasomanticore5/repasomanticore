import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {SubempresaService} from './subempresa.service';
import {SubempresaHabilitadoDto} from './dto/subempresa.habilitado.dto';
import {SUBEMPRESA_OCC} from './constantes/subempresa.occ';
import {SubempresaCrearDto} from './dto/subempresa.crear.dto';
import {SubempresaActualizarDto} from './dto/subempresa.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {SubempresaEntity} from './subempresa.entity';
import {SubempresaBusquedaDto} from './dto/subempresa.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'subempresa';

@Controller(nombreControlador)
export class SubempresaController extends ControladorComun {
    constructor(
        private readonly _subempresaService: SubempresaService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _subempresaService,
            _seguridadService,
            _auditoriaService,
            SUBEMPRESA_OCC(
                SubempresaHabilitadoDto,
                SubempresaCrearDto,
                SubempresaActualizarDto,
                SubempresaBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: SubempresaBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._subempresaService.subempresaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<SubempresaEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._subempresaService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._subempresaService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<SubempresaEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<SubempresaEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._subempresaService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);
            
            // RELACIONES CON PAPAS o HIJOS
            
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES
            
            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }
}
