import { TypeOrmModule } from '@nestjs/typeorm';
import { SubempresaEntity } from '../subempresa.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const SUBEMPRESA_IMPORTS = [
  TypeOrmModule.forFeature([SubempresaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
