import { SubempresaCrearDto } from '../dto/subempresa.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const SUBEMPRESA_DATOS_PRUEBA: (() => SubempresaCrearDto)[] = [
  () => {
    const dato = new SubempresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new SubempresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new SubempresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
