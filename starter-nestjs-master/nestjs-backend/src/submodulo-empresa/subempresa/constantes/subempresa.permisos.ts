import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_SUBEMPRESA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'subempresa';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/subempresa/, // /subempresa
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/subempresa\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /subempresa?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/subempresa\/\d+\/modificar-habilitado/, // /subempresa/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/subempresa\/\d+/, // /subempresa/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
