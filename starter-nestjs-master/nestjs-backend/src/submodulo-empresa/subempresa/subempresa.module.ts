import { Module } from '@nestjs/common';
import { SubempresaController } from './subempresa.controller';
import { SUBEMPRESA_IMPORTS } from './constantes/subempresa.imports';
import { SUBEMPRESA_PROVIDERS } from './constantes/subempresa.providers';

@Module({
  imports: [...SUBEMPRESA_IMPORTS],
  providers: [...SUBEMPRESA_PROVIDERS],
  exports: [...SUBEMPRESA_PROVIDERS],
  controllers: [SubempresaController],
})
export class SubempresaModule {}
