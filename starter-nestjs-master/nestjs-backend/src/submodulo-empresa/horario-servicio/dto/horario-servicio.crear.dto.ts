import { IsIn, IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class HorarioServicioCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsIn([1, 0])
  @Expose()
  esHorarioEspecial: 0 | 1 = 0;

  @IsOptional()
  @IsIn([1, 0])
  @Expose()
  puedeAtender: 0 | 1;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  horario: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  servicioEstablecimiento: number;
}
