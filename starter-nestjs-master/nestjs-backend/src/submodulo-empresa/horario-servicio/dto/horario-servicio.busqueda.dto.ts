import { IsInt, IsNumber, IsNumberString, IsOptional, IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class HorarioServicioBusquedaDto extends BusquedaComunProyectoDto {

  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsInt()
  @Expose()
  horario: number;

  @IsOptional()
  @IsNumberString()
  @Expose()
  servicioEstablecimiento: number;

  @IsOptional()
  @IsString()
  @Expose()
  horarioRegistrado: string;
}
