import { IsIn, IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class HorarioServicioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsIn([1, 0])
  @Expose()
  esHorarioEspecial: 0 | 1 = 0;

  @IsOptional()
  @IsIn([1, 0])
  @Expose()
  puedeAtender: 0 | 1;

  @IsOptional()
  @IsInt()
  @Expose()
  horario: number;

  @IsOptional()
  @IsInt()
  @Expose()
  servicioEstablecimiento: number;
}
