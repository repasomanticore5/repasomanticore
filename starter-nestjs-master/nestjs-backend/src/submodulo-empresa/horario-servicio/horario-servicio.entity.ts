import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ContactoHorarioServicioEntity } from '../contacto-horario-servicio/contacto-horario-servicio.entity';
import { HorarioEntity } from '../horario/horario.entity';
import { ServicioEstablecimientoEntity } from '../servicio-establecimiento/servicio-establecimiento.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'HORARIO_SERVICIO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'horario'])
export class HorarioServicioEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_HORARIO_SERVICIO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'es_horario_especial',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esHorarioEspecial: 0 | 1 = 0;

  @Column({
    name: 'puede_atender',
    type: 'tinyint',
    default: 1,
    nullable: false,
  })
  puedeAtender: 0 | 1 = 1;

  @ManyToOne(() => HorarioEntity, (r) => r.horariosServicio, { nullable: true })
  horario: HorarioEntity | number;

  @ManyToOne(() => ServicioEstablecimientoEntity, (r) => r.horariosServicio, {
    nullable: true,
  })
  servicioEstablecimiento: ServicioEstablecimientoEntity | number;

  @OneToMany((type) => ContactoHorarioServicioEntity, (r) => r.horarioServicio)
  contactosHorariosServicio: ContactoHorarioServicioEntity[];
}
