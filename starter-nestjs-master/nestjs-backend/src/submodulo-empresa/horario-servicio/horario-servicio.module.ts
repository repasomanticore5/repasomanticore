import { Module } from '@nestjs/common';
import { HorarioServicioController } from './horario-servicio.controller';
import { HORARIO_SERVICIO_IMPORTS } from './constantes/horario-servicio.imports';
import { HORARIO_SERVICIO_PROVIDERS } from './constantes/horario-servicio.providers';

@Module({
  imports: [...HORARIO_SERVICIO_IMPORTS],
  providers: [...HORARIO_SERVICIO_PROVIDERS],
  exports: [...HORARIO_SERVICIO_PROVIDERS],
  controllers: [HorarioServicioController],
})
export class HorarioServicioModule {}
