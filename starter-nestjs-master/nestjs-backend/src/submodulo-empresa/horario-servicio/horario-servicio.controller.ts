import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { HorarioServicioService } from './horario-servicio.service';
import { HorarioServicioHabilitadoDto } from './dto/horario-servicio.habilitado.dto';
import { HORARIO_SERVICIO_OCC } from './constantes/horario-servicio.occ';
import { HorarioServicioCrearDto } from './dto/horario-servicio.crear.dto';
import { HorarioServicioActualizarDto } from './dto/horario-servicio.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { HorarioServicioEntity } from './horario-servicio.entity';
import { HorarioServicioBusquedaDto } from './dto/horario-servicio.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'horario-servicio';

@Controller(nombreControlador)
export class HorarioServicioController extends ControladorComun {
  constructor(
    private readonly _horarioServicioService: HorarioServicioService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _horarioServicioService,
      _seguridadService,
      _auditoriaService,
      HORARIO_SERVICIO_OCC(
        HorarioServicioHabilitadoDto,
        HorarioServicioCrearDto,
        HorarioServicioActualizarDto,
        HorarioServicioBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: HorarioServicioBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._horarioServicioService.horarioServicioEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<HorarioServicioEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._horarioServicioService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._horarioServicioService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<HorarioServicioEntity>[];
        /* if( parametrosConsulta.busqueda){
           consultaWhere.push({
             horario: {
               descripcion: Like('%' + parametrosConsulta.busqueda + '%')
             }
           });
         }*/
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<HorarioServicioEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'servicioEstablecimiento',
          consultaWhere,
          parametrosConsulta.servicioEstablecimiento,
        );

        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._horarioServicioService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS
      qb.leftJoinAndSelect(aliasTabla + '.horario', 'horario');
      qb.leftJoinAndSelect(aliasTabla + '.servicioEstablecimiento', 'servicioEstablecimiento');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES
      if (parametrosConsulta.busqueda) {
        qb.andWhere(
          'horario.descripcion LIKE :busqueda',
          {
            busqueda: `%${parametrosConsulta.busqueda.trim()}%`,
          },
        );
      }


      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }
      if( parametrosConsulta.horarioRegistrado) {
        const {ids} = JSON.parse(parametrosConsulta.horarioRegistrado);
        if( ids.length > 0) {
          qb.andWhere(aliasTabla + `.id NOT IN (${ids})`);
        }
      }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip)
        .take(consulta.take)
        .getManyAndCount();
    }
  }
}
