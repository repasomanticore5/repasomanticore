import { HorarioServicioCrearDto } from '../dto/horario-servicio.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const HORARIO_SERVICIO_DATOS_PRUEBA: ((
  idHorario: number,
  idServicioEstablecimiento: number,
) => HorarioServicioCrearDto)[] = [
  (idHorario: number, idServicioEstablecimiento: number) => {
    const dato = new HorarioServicioCrearDto();
    dato.servicioEstablecimiento = idServicioEstablecimiento;
    dato.horario = idHorario;
    dato.esHorarioEspecial = 0;
    dato.puedeAtender = 1;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
