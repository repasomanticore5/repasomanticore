import { TypeOrmModule } from '@nestjs/typeorm';
import { HorarioServicioEntity } from '../horario-servicio.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const HORARIO_SERVICIO_IMPORTS = [
  TypeOrmModule.forFeature([HorarioServicioEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
