import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_HORARIO_SERVICIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'horario-servicio';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/horario-servicio/, // /horario-servicio
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/horario-servicio\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /horario-servicio?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/horario-servicio\/\d+\/modificar-habilitado/, // /horario-servicio/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/horario-servicio\/\d+/, // /horario-servicio/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
