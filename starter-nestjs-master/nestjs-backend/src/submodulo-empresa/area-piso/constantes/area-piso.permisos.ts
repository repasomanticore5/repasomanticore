import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_AREA_PISO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'area-piso';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/area-piso/, // /area-piso
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/area-piso\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /area-piso?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/area-piso\/\d+\/modificar-habilitado/, // /area-piso/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/area-piso\/\d+/, // /area-piso/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
