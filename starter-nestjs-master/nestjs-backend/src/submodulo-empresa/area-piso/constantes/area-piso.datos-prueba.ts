import { AreaPisoCrearDto } from '../dto/area-piso.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const AREA_PISO_DATOS_PRUEBA: ((
  idPiso: number,
) => AreaPisoCrearDto)[] = [
  (idPiso: number) => {
    const dato = new AreaPisoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Area piso ' + idPiso;
    dato.piso = idPiso;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
