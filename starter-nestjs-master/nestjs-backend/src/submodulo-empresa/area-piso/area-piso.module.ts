import { Module } from '@nestjs/common';
import { AreaPisoController } from './area-piso.controller';
import { AREA_PISO_IMPORTS } from './constantes/area-piso.imports';
import { AREA_PISO_PROVIDERS } from './constantes/area-piso.providers';

@Module({
  imports: [...AREA_PISO_IMPORTS],
  providers: [...AREA_PISO_PROVIDERS],
  exports: [...AREA_PISO_PROVIDERS],
  controllers: [AreaPisoController],
})
export class AreaPisoModule {}
