import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { AreaTrabajadorEntity } from '../area-trabajador/area-trabajador.entity';
import { PisoEntity } from '../piso/piso.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'AREA_PISO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre', 'nivel'])
export class AreaPisoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_AREA_PISO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  @Column({
    name: 'nivel',
    type: 'int',
    default: 0,
    nullable: false,
    unsigned: true,
  })
  nivel = 0;

  @TreeParent()
  areaPisoPadre: AreaPisoEntity | number;

  @ManyToOne((type) => PisoEntity, (r) => r.areasPiso, {
    nullable: true,
  })
  piso: PisoEntity | number;

  @OneToMany((type) => AreaTrabajadorEntity, (r) => r.areaPiso)
  areasTrabajador: AreaTrabajadorEntity[];

  @TreeChildren()
  areaPisoHijos: AreaPisoEntity[];
}
