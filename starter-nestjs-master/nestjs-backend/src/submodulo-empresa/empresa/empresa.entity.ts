import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { CaracteristicaBodegaEntity } from '../caracteristica-bodega/caracteristica-bodega.entity';
import { CodigoPaisEntity } from '../codigo-pais/codigo-pais.entity';
import { ContactoEmpresaEntity } from '../contacto-empresa/contacto-empresa.entity';
import { DepartamentoEmpresaEntity } from '../departamento-empresa/departamento-empresa.entity';
import { EdificioEntity } from '../edificio/edificio.entity';
import { SubempresaEntity } from '../subempresa/subempresa.entity';
import { EmpresaIndustriaEntity } from '../empresa-industria/empresa-industria.entity';
import { HorarioEntity } from '../horario/horario.entity';
import { TipoCargoEntity } from '../tipo-cargo/tipo-cargo.entity';
import { FormulaArtEntity } from '../formula-art/formula-art.entity';
import { FormulaSufijoEntity } from '../formula-sufijo/formula-sufijo.entity';
import { FormulaPrefijoEntity } from '../formula-prefijo/formula-prefijo.entity';
import { TipoBodegaEntity } from '../tipo-bodega/tipo-bodega.entity';
import { ArticuloEntity } from '../../submodulo-articulo/articulo/articulo.entity';
import { ArticuloEmpresaEntity } from '../../submodulo-articulo-empresa/articulo-empresa/articulo-empresa.entity';
import { RolEntity } from '../../submodulo-roles/rol/rol.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'EMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class EmpresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_EMPRESA',
    unsigned: true,
  })
  id: number;

  @Index()
  @Column({
    name: 'nombre_comercial',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  nombreComercial: string = null;

  @Index()
  @Column({
    name: 'razon_social',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  razonSocial: string;

  @Index()
  @Column({
    name: 'ruc',
    type: 'varchar',
    length: 13,
    nullable: false,
  })
  ruc: string;

  @Column({
    name: 'direccion_matriz',
    type: 'varchar',
    length: 200,
    nullable: false,
  })
  direccionMatriz: string;

  @Column({
    name: 'telefono',
    type: 'varchar',
    length: 10,
    nullable: false,
  })
  telefono: string;

  @Column({
    name: 'correo',
    type: 'varchar',
    length: 60,
    nullable: true,
  })
  correo: string = null;

  @Column({
    name: 'tipo_contribuyente',
    type: 'varchar',
    length: 3,
    nullable: false,
    default: 'PN',
    comment: 'PN: persona natural, SO: sociendades, ES: especial',
  })
  tipoContribuyente: 'PN' | 'SO' | 'SE' = 'PN';

  @Column({
    name: 'contribuyente_especial',
    type: 'varchar',
    length: 5,
    nullable: true,
  })
  contribuyenteEspecial: string = null;

  @Column({
    name: 'obligado_contabilidad',
    type: 'tinyint',
    nullable: false,
    default: 0,
    comment: '1: si, 0: no',
  })
  obligadoContabilidad: 0 | 1 = 0;

  @Index()
  @Column({
    name: 'codigo_auxiliar',
    type: 'varchar',
    length: 10,
    nullable: true,
  })
  codigoAuxiliar: string = null;

  @ManyToOne(() => CodigoPaisEntity, (r) => r.empresas, { nullable: true })
  codigoPais: CodigoPaisEntity | number;

  @OneToMany(() => CaracteristicaBodegaEntity, (r) => r.empresa)
  caracteristicasBodega: CaracteristicaBodegaEntity[];

  @OneToMany(() => ContactoEmpresaEntity, (r) => r.empresa)
  contactosEmpresa: ContactoEmpresaEntity[];

  @OneToMany(() => DepartamentoEmpresaEntity, (r) => r.empresa)
  departamentosEmpresa: DepartamentoEmpresaEntity[];

  @OneToMany(() => EdificioEntity, (r) => r.empresa)
  edificios: EdificioEntity[];

  @OneToMany(() => SubempresaEntity, (r) => r.empresaPadre)
  subempresasPadres: SubempresaEntity[];

  @OneToMany(() => SubempresaEntity, (r) => r.empresaHija)
  subempresasHijas: SubempresaEntity[];

  @OneToMany(() => EmpresaIndustriaEntity, (r) => r.empresa)
  empresasIndustria: EmpresaIndustriaEntity[];

  @OneToMany(() => HorarioEntity, (r) => r.empresa)
  horarios: HorarioEntity[];

  @OneToMany(() => FormulaArtEntity, (r) => r.empresa)
  formulasArt: FormulaArtEntity[];

  @OneToMany(() => FormulaSufijoEntity, (r) => r.empresa)
  formulasSufijo: FormulaSufijoEntity[];

  @OneToMany(() => FormulaPrefijoEntity, (r) => r.empresa)
  formulasPrefijo: FormulaPrefijoEntity[];

  @OneToMany(() => TipoBodegaEntity, (r) => r.empresa)
  tiposBodega: TipoBodegaEntity[];

  @OneToMany(() => TipoCargoEntity, (r) => r.empresa)
  tiposCargo: TipoCargoEntity[];

  @OneToMany(() => ArticuloEmpresaEntity, (r) => r.empresaAE)
  articulosEmpresaE: ArticuloEmpresaEntity[];

  @OneToMany(() => RolEntity, (rol) => rol.empresaR)
  rolesE: RolEntity[];
}
