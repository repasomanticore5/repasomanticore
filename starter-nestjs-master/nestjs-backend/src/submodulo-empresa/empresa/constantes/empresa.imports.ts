import { TypeOrmModule } from '@nestjs/typeorm';
import { EmpresaEntity } from '../empresa.entity';
import {NOMBRE_CADENA_CONEXION} from "../../../constantes/nombre-cadena-conexion";
import {AuditoriaModule} from "../../../auditoria/auditoria.module";
import {SeguridadModule} from "../../../seguridad/seguridad.module";

export const EMPRESA_IMPORTS = [
  TypeOrmModule.forFeature([EmpresaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
