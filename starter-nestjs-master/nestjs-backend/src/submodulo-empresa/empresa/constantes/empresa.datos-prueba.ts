import { EmpresaCrearDto } from '../dto/empresa.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const EMPRESA_DATOS_PRUEBA: (() => EmpresaCrearDto)[] = [
  () => {
    const dato = new EmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombreComercial = 'ORQUESTA SINFÓNICA NACIONAL';
    dato.razonSocial = 'ORQUESTA SINFÓNICA NACIONAL';
    dato.ruc = '1760006270001';
    dato.direccionMatriz = 'Leonidas Plaza N19-34 18 de Septiembre';
    dato.telefono = '0999999999';
    return dato;
  },
  () => {
    const dato = new EmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombreComercial = 'Empresa Electrica Regional Centro Sur Ca';
    dato.razonSocial = 'Empresa Electrica Regional Centro Sur Ca';
    dato.ruc = '0190003809001';
    dato.direccionMatriz = 'Av. Max Uhle S-n Av. Pumapungo';
    dato.telefono = '0999999998';
    return dato;
  },
  () => {
    const dato = new EmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombreComercial = 'Empresa Publica Municipal del Cuerpo de Bomberos de Rumiñahui';
    dato.razonSocial = 'Empresa Publica Municipal del Cuerpo de Bomberos de Rumiñahui';
    dato.ruc = '1768185220001';
    dato.direccionMatriz = 'Shyris s/n Altar';
    dato.telefono = '0999999997';
    return dato;
  },
];
