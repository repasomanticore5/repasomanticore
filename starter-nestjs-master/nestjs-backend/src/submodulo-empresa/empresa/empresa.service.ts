import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { EmpresaEntity } from './empresa.entity';
import { EmpresaBusquedaDto } from './dto/empresa.busqueda.dto';
import { ServicioComun } from '@manticore-labs/nest-2021';
import { NOMBRE_CADENA_CONEXION } from '../../constantes/nombre-cadena-conexion';
import { AuditoriaService } from '../../auditoria/auditoria.service';

@Injectable()
export class EmpresaService extends ServicioComun<
  EmpresaEntity,
  EmpresaBusquedaDto
> {
  constructor(
    @InjectRepository(EmpresaEntity, NOMBRE_CADENA_CONEXION)
    public empresaEntityRepository: Repository<EmpresaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      empresaEntityRepository,
      _connection,
      EmpresaEntity,
      'id',
      _auditoriaService,
      // Por defecto NO se transforma todos los campos a mayúsculas.
      // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
      // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
      (objetoATransformar, metodo) => objetoATransformar,
    );
  }
}
