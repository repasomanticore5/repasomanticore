import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class EmpresaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  nombreComercial: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  razonSocial: string;

  @IsOptional()
  @IsString()
  @MinLength(10)
  @MaxLength(13)
  @Expose()
  ruc: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(200)
  @Expose()
  direccionMatriz: string;

  @IsOptional()
  @IsString()
  @MinLength(8)
  @MaxLength(10)
  @Expose()
  telefono: string;

  @IsOptional()
  @IsString()
  @MinLength(5)
  @MaxLength(60)
  @Expose()
  correo: string;

  @IsOptional()
  @IsString()
  @MaxLength(3)
  @IsIn(['PN', 'SO', 'SE'])
  @Expose()
  tipoContribuyente: 'PN' | 'SO' | 'SE';

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(5)
  @Expose()
  contribuyenteEspecial: string;

  @IsOptional()
  @IsInt()
  @IsIn([0, 1])
  @Expose()
  obligadoContabilidad: 0 | 1;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(10)
  @Expose()
  codigoAuxiliar: string;

  @IsOptional()
  @IsInt()
  @Expose()
  codigoPais: number;
}
