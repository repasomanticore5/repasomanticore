import {IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {BusquedaComunProyectoDto} from '../../../abstractos/busqueda-comun-proyecto-dto';

export class EmpresaBusquedaDto extends BusquedaComunProyectoDto {
    @IsOptional()
    @IsString()
    @Expose()
    busquedaAutocomplete: string;

    @IsOptional()
    @IsNumberString()
    @Expose()
    findById: string;
}
