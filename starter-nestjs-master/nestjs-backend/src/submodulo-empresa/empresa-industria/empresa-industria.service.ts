import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { EmpresaIndustriaEntity } from './empresa-industria.entity';
import {EmpresaIndustriaBusquedaDto} from './dto/empresa-industria.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class EmpresaIndustriaService extends ServicioComun<EmpresaIndustriaEntity, EmpresaIndustriaBusquedaDto> {
  constructor(
    @InjectRepository(EmpresaIndustriaEntity, NOMBRE_CADENA_CONEXION)
    public empresaIndustriaEntityRepository: Repository<EmpresaIndustriaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        empresaIndustriaEntityRepository,
      _connection,
        EmpresaIndustriaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
