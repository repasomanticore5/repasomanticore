import { Module } from '@nestjs/common';
import { EmpresaIndustriaController } from './empresa-industria.controller';
import { EMPRESA_INDUSTRIA_IMPORTS } from './constantes/empresa-industria.imports';
import { EMPRESA_INDUSTRIA_PROVIDERS } from './constantes/empresa-industria.providers';

@Module({
  imports: [...EMPRESA_INDUSTRIA_IMPORTS],
  providers: [...EMPRESA_INDUSTRIA_PROVIDERS],
  exports: [...EMPRESA_INDUSTRIA_PROVIDERS],
  controllers: [EmpresaIndustriaController],
})
export class EmpresaIndustriaModule {}
