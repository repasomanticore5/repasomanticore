import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { TipoIndustriaEntity } from '../tipo-industria/tipo-industria.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'EMPRESA_INDUSTRIA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'esPrincipal'])
export class EmpresaIndustriaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_EMPRESA_INDUSTRIA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'es_principal',
    type: 'tinyint',
    default: 0,
    nullable: false,
  })
  esPrincipal: 0 | 1 = 0;

  @ManyToOne(() => EmpresaEntity, (r) => r.empresasIndustria, {
    nullable: true,
  })
  empresa: EmpresaEntity | number;

  @ManyToOne(() => TipoIndustriaEntity, (r) => r.empresasIndustria, {
    nullable: true,
  })
  tipoIndustria: TipoIndustriaEntity | number;

  // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
  // entidadRelacionesHijos: EntidadRelacionHijo[];
}
