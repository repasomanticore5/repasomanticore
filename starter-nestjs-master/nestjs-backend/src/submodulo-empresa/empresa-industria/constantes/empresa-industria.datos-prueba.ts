import { EmpresaIndustriaCrearDto } from '../dto/empresa-industria.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const EMPRESA_INDUSTRIA_DATOS_PRUEBA: ((
  idEmpresa: number,
  idTipoIndustria: number,
) => EmpresaIndustriaCrearDto)[] = [
  (idEmpresa: number, idTipoIndustria: number) => {
    const dato = new EmpresaIndustriaCrearDto();
    dato.esPrincipal = 1;
    dato.empresa = idEmpresa;
    dato.tipoIndustria = idTipoIndustria;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
