import { TypeOrmModule } from '@nestjs/typeorm';
import { EmpresaIndustriaEntity } from '../empresa-industria.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const EMPRESA_INDUSTRIA_IMPORTS = [
  TypeOrmModule.forFeature([EmpresaIndustriaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
