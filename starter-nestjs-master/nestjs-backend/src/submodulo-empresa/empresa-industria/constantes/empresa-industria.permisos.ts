import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_EMPRESA_INDUSTRIA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'empresa-industria';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/empresa-industria/, // /empresa-industria
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/empresa-industria\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /empresa-industria?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/empresa-industria\/\d+\/modificar-habilitado/, // /empresa-industria/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/empresa-industria\/\d+/, // /empresa-industria/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
