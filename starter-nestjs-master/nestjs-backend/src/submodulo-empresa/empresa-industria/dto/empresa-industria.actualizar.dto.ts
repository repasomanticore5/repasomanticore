import { IsIn, IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export class EmpresaIndustriaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR
  @IsOptional()
  @IsIn([0, 1])
  @Expose()
  esPrincipal: 0 | 1;

  @IsOptional()
  @IsInt()
  @Expose()
  tipoIndustria: number;
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
}
