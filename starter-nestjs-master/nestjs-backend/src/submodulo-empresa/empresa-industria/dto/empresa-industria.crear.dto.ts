import { IsIn, IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class EmpresaIndustriaCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsIn([0, 1])
  @Expose()
  esPrincipal: 0 | 1;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  empresa: number;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  tipoIndustria: number;
}
