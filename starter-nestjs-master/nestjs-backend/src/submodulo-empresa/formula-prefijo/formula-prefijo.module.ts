import { Module } from '@nestjs/common';
import { FormulaPrefijoController } from './formula-prefijo.controller';
import { FORMULA_PREFIJO_IMPORTS } from './constantes/formula-prefijo.imports';
import { FORMULA_PREFIJO_PROVIDERS } from './constantes/formula-prefijo.providers';

@Module({
  imports: [...FORMULA_PREFIJO_IMPORTS],
  providers: [...FORMULA_PREFIJO_PROVIDERS],
  exports: [...FORMULA_PREFIJO_PROVIDERS],
  controllers: [FormulaPrefijoController],
})
export class FormulaPrefijoModule {}
