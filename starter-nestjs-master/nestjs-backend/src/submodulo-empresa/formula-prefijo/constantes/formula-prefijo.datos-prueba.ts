import { FormulaPrefijoCrearDto } from '../dto/formula-prefijo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const FORMULA_PREFIJO_DATOS_PRUEBA: ((idEmpresa: number) => FormulaPrefijoCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new FormulaPrefijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'G1';
    dato.descripcion = 'Formula 1';
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new FormulaPrefijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'G2';
    dato.descripcion = 'Formula 2';
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new FormulaPrefijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'G3';
    dato.descripcion = 'Formula 3';
    dato.empresa = idEmpresa;
    return dato;
  },
];
