import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_FORMULA_PREFIJO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'formula-prefijo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/formula-prefijo/, // /formula-prefijo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/formula-prefijo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /formula-prefijo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-prefijo\/\d+\/modificar-habilitado/, // /formula-prefijo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-prefijo\/\d+/, // /formula-prefijo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
