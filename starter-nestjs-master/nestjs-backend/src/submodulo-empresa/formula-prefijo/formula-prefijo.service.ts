import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { FormulaPrefijoEntity } from './formula-prefijo.entity';
import {FormulaPrefijoBusquedaDto} from './dto/formula-prefijo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class FormulaPrefijoService extends ServicioComun<FormulaPrefijoEntity, FormulaPrefijoBusquedaDto> {
  constructor(
    @InjectRepository(FormulaPrefijoEntity, NOMBRE_CADENA_CONEXION)
    public formulaPrefijoEntityRepository: Repository<FormulaPrefijoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        formulaPrefijoEntityRepository,
      _connection,
        FormulaPrefijoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
