import {
  Column,
  Entity,
  Index, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { FormulaArtEntity } from '../formula-art/formula-art.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'FORMULA_PREFIJO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre'])
export class FormulaPrefijoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_FORMULA_PREFIJO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  @ManyToOne(
    () => EmpresaEntity,
    (r) => r.formulasPrefijo,
    { nullable: true },
  )
  empresa: EmpresaEntity | number;

  @OneToMany(
    () => FormulaArtEntity,
    (r) => r.formulaPrefijo,
  )
  formulasArt: FormulaArtEntity[];
}
