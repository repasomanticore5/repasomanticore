import { DatosContactoCrearDto } from '../dto/datos-contacto.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const DATOS_CONTACTO_DATOS_PRUEBA: ((idContactoEmpresa: number) => DatosContactoCrearDto)[] = [
  (idContactoEmpresa: number) => {
    const dato = new DatosContactoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.llave = 'telefono';
    dato.valor = '0965326521';
    dato.esPrincipal = 0;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idContactoEmpresa: number) => {
    const dato = new DatosContactoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.llave = 'telefono';
    dato.valor = '022633594';
    dato.esPrincipal = 0;
    dato.contactoEmpresa = idContactoEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
