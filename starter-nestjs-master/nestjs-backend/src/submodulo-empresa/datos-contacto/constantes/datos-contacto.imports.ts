import { TypeOrmModule } from '@nestjs/typeorm';
import { DatosContactoEntity } from '../datos-contacto.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const DATOS_CONTACTO_IMPORTS = [
  TypeOrmModule.forFeature([DatosContactoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
