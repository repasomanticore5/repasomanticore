import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_DATOS_CONTACTO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'datos-contacto';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/datos-contacto/, // /datos-contacto
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/datos-contacto\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /datos-contacto?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/datos-contacto\/\d+\/modificar-habilitado/, // /datos-contacto/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/datos-contacto\/\d+/, // /datos-contacto/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
