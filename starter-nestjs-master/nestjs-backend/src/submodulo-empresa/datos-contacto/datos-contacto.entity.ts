import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {ContactoEmpresaEntity} from '../contacto-empresa/contacto-empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DATOS_CONTACTO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'llave', 'valor'])
export class DatosContactoEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_DATOS_CONTACTO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'llave',
        type: 'varchar',
        length: 20,
        nullable: false,
    })
    llave: string;

    @Column({
        name: 'valor',
        type: 'varchar',
        length: 255,
        nullable: false,
    })
    valor: string;

    @Column({
        name: 'es_principal',
        type: 'tinyint',
        default: 0,
        nullable: false,
    })
    esPrincipal: 1 | 0 = 0;

    @ManyToOne(
        () => ContactoEmpresaEntity,
        (r) => r.datosContacto,
        { nullable: false }
    )
    contactoEmpresa: ContactoEmpresaEntity | number;

    // @OneToMany(() => EntidadRelacionHijo, (r) => r.nombreRelacionHijo)
    // entidadRelacionesHijos: EntidadRelacionHijo[];
}
