import {IsIn, IsInt, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class DatosContactoActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsString()
    @MinLength(5)
    @MaxLength(20)
    @Expose()
    llave: string;

    @IsOptional()
    @IsString()
    @MinLength(1)
    @MaxLength(255)
    @Expose()
    valor: string;

    @IsOptional()
    @IsIn([0, 1])
    @Expose()
    esPrincipal: string;

    @IsOptional()
    @IsInt()
    @Expose()
    contactoEmpresa: number;
}
