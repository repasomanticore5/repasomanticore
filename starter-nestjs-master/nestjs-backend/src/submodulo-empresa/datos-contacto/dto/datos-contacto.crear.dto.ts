import {IsIn, IsInt, IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class DatosContactoCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsString()
    @MinLength(5)
    @MaxLength(20)
    @Expose()
    llave: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(1)
    @MaxLength(255)
    @Expose()
    valor: string;

    @IsNotEmpty()
    @IsIn([0, 1])
    @Expose()
    esPrincipal: 0 | 1;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    contactoEmpresa: number;
}
