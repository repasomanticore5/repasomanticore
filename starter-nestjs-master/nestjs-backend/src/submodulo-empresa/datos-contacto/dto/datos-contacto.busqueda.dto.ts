import { IsIn, IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export class DatosContactoBusquedaDto extends BusquedaComunProyectoDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  contactoEmpresa: string;

  @IsOptional()
  // @IsIn([1, 0])
  @Expose()
  esPrincipal?: 0 | 1;
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
