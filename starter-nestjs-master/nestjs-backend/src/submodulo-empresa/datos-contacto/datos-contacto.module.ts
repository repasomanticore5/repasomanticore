import { Module } from '@nestjs/common';
import { DatosContactoController } from './datos-contacto.controller';
import { DATOS_CONTACTO_IMPORTS } from './constantes/datos-contacto.imports';
import { DATOS_CONTACTO_PROVIDERS } from './constantes/datos-contacto.providers';

@Module({
  imports: [...DATOS_CONTACTO_IMPORTS],
  providers: [...DATOS_CONTACTO_PROVIDERS],
  exports: [...DATOS_CONTACTO_PROVIDERS],
  controllers: [DatosContactoController],
})
export class DatosContactoModule {}
