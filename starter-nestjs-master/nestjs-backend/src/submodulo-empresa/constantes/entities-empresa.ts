import {HorarioServicioEntity} from '../horario-servicio/horario-servicio.entity';
import {TipoIndustriaEntity} from '../tipo-industria/tipo-industria.entity';
import {DatosContactoEntity} from '../datos-contacto/datos-contacto.entity';
import {CaracteristicaTipoBodegaEntity} from '../caracteristica-tipo-bodega/caracteristica-tipo-bodega.entity';
import {ContactoEmpresaEntity} from '../contacto-empresa/contacto-empresa.entity';
import {EdificioEntity} from '../edificio/edificio.entity';
import {CodigoPaisEntity} from '../codigo-pais/codigo-pais.entity';
import {CaracteristicaBodegaTipoEntity} from '../caracteristica-bodega-tipo/caracteristica-bodega-tipo.entity';
import {AreaPisoEntity} from '../area-piso/area-piso.entity';
import {TipoCargoEntity} from '../tipo-cargo/tipo-cargo.entity';
import {FormulaArtEntity} from '../formula-art/formula-art.entity';
import {TipoBodegaEntity} from '../tipo-bodega/tipo-bodega.entity';
import {FormulaSufijoEntity} from '../formula-sufijo/formula-sufijo.entity';
import {EmpresaEntity} from '../empresa/empresa.entity';
import {HorarioEntity} from '../horario/horario.entity';
import {EstablecimientoEntity} from '../establecimiento/establecimiento.entity';
import {BodegaEntity} from '../bodega/bodega.entity';
import {FormulaPrefijoEntity} from '../formula-prefijo/formula-prefijo.entity';
import {PisoEntity} from '../piso/piso.entity';
import {AreaTrabajadorEntity} from '../area-trabajador/area-trabajador.entity';
import {DepartamentoEmpresaEntity} from '../departamento-empresa/departamento-empresa.entity';
import {IngredienteFormulaArtEntity} from '../ingrediente-formula-art/ingrediente-formula-art.entity';
import {CaracteristicaBodegaEntity} from '../caracteristica-bodega/caracteristica-bodega.entity';
import {BodegaTipoEntity} from '../bodega-tipo/bodega-tipo.entity';
import {DepartamentoTrabajadorEntity} from '../departamento-trabajador/departamento-trabajador.entity';
import {ContactoHorarioServicioEntity} from '../contacto-horario-servicio/contacto-horario-servicio.entity';
import {DireccionEntity} from '../direccion/direccion.entity';
import {SubempresaEntity} from '../subempresa/subempresa.entity';
import {EmpresaIndustriaModule} from '../empresa-industria/empresa-industria.module';
import {EmpresaIndustriaEntity} from '../empresa-industria/empresa-industria.entity';
import {ServicioEstablecimientoEntity} from '../servicio-establecimiento/servicio-establecimiento.entity';

export const ENTITIES_EMPRESA = [
    AreaPisoEntity,
    AreaTrabajadorEntity,
    BodegaEntity,
    BodegaTipoEntity,
    CaracteristicaBodegaEntity,
    CaracteristicaBodegaTipoEntity,
    CaracteristicaTipoBodegaEntity,
    CodigoPaisEntity,
    ContactoEmpresaEntity,
    ContactoHorarioServicioEntity,
    DatosContactoEntity,
    DepartamentoEmpresaEntity,
    DepartamentoTrabajadorEntity,
    DireccionEntity,
    EdificioEntity,
    EmpresaEntity,
    EmpresaIndustriaEntity,
    EstablecimientoEntity,
    FormulaArtEntity,
    FormulaPrefijoEntity,
    FormulaSufijoEntity,
    HorarioEntity,
    HorarioServicioEntity,
    IngredienteFormulaArtEntity,
    PisoEntity,
    ServicioEstablecimientoEntity,
    SubempresaEntity,
    TipoBodegaEntity,
    TipoCargoEntity,
    TipoIndustriaEntity,
];
