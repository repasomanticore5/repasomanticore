import {AreaPisoModule} from '../area-piso/area-piso.module';
import {AreaTrabajadorModule} from '../area-trabajador/area-trabajador.module';
import {CaracteristicaBodegaModule} from '../caracteristica-bodega/caracteristica-bodega.module';
import {CaracteristicaBodegaTipoModule} from '../caracteristica-bodega-tipo/caracteristica-bodega-tipo.module';
import {CaracteristicaTipoBodegaModule} from '../caracteristica-tipo-bodega/caracteristica-tipo-bodega.module';
import {CodigoPaisModule} from '../codigo-pais/codigo-pais.module';
import {ContactoEmpresaModule} from '../contacto-empresa/contacto-empresa.module';
import {DatosContactoModule} from '../datos-contacto/datos-contacto.module';
import {DepartamentoEmpresaModule} from '../departamento-empresa/departamento-empresa.module';
import {EdificioModule} from '../edificio/edificio.module';
import {EmpresaModule} from '../empresa/empresa.module';
import {EstablecimientoModule} from '../establecimiento/establecimiento.module';
import {FormulaArtModule} from '../formula-art/formula-art.module';
import {FormulaPrefijoModule} from '../formula-prefijo/formula-prefijo.module';
import {FormulaSufijoModule} from '../formula-sufijo/formula-sufijo.module';
import {HorarioModule} from '../horario/horario.module';
import {HorarioServicioModule} from '../horario-servicio/horario-servicio.module';
import {IngredienteFormulaArtModule} from '../ingrediente-formula-art/ingrediente-formula-art.module';
import {PisoModule} from '../piso/piso.module';
import {TipoBodegaModule} from '../tipo-bodega/tipo-bodega.module';
import {TipoCargoModule} from '../tipo-cargo/tipo-cargo.module';
import {TipoIndustriaModule} from '../tipo-industria/tipo-industria.module';
import {BodegaModule} from '../bodega/bodega.module';
import {BodegaTipoModule} from '../bodega-tipo/bodega-tipo.module';
import {ContactoHorarioServicioModule} from '../contacto-horario-servicio/contacto-horario-servicio.module';
import {DepartamentoTrabajadorModule} from '../departamento-trabajador/departamento-trabajador.module';
import {DireccionModule} from '../direccion/direccion.module';
import {SubempresaModule} from '../subempresa/subempresa.module';
import {EmpresaIndustriaModule} from '../empresa-industria/empresa-industria.module';
import {ServicioEstablecimientoModule} from '../servicio-establecimiento/servicio-establecimiento.module';

export const MODULOS_EMPRESA = [
    AreaPisoModule,
    AreaTrabajadorModule,
    BodegaModule,
    BodegaTipoModule,
    CaracteristicaBodegaModule,
    CaracteristicaBodegaTipoModule,
    CaracteristicaTipoBodegaModule,
    CodigoPaisModule,
    ContactoEmpresaModule,
    ContactoHorarioServicioModule,
    DatosContactoModule,
    DepartamentoEmpresaModule,
    DepartamentoTrabajadorModule,
    DireccionModule,
    EdificioModule,
    EmpresaModule,
    EmpresaIndustriaModule,
    EstablecimientoModule,
    FormulaArtModule,
    FormulaPrefijoModule,
    FormulaSufijoModule,
    HorarioModule,
    HorarioServicioModule,
    IngredienteFormulaArtModule,
    PisoModule,
    ServicioEstablecimientoModule,
    SubempresaModule,
    TipoBodegaModule,
    TipoCargoModule,
    TipoIndustriaModule,
];
