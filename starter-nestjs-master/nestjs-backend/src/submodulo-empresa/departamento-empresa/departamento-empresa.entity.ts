import {
  Column,
  Entity,
  Index, ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn, TreeChildren, TreeParent,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaEntity } from '../empresa/empresa.entity';
import { DepartamentoTrabajadorEntity } from '../departamento-trabajador/departamento-trabajador.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DEPARTAMENTO_EMPRESA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nivel'])
export class DepartamentoEmpresaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_DEPARTAMENTO_EMPRESA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  @Column({
    name: 'nivel',
    type: 'int',
    default: 0,
    nullable: false,
  })
  nivel: number;


  @TreeParent()
  departamentoEmpresaPadre: DepartamentoEmpresaEntity | number;

  @ManyToOne(
    () => EmpresaEntity,
    (r) => r.departamentosEmpresa,
    { nullable: true },
  )
  empresa: EmpresaEntity | number;

  @TreeChildren()
  departamentoEmpresaHijo: DepartamentoEmpresaEntity[];

  @OneToMany(
    () => DepartamentoTrabajadorEntity,
    (r) => r.departamentoEmpresa,
  )
  departamentosTrabajador: DepartamentoTrabajadorEntity[];
}
