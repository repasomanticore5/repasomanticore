import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_DEPARTAMENTO_EMPRESA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'departamento-empresa';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/departamento-empresa/, // /departamento-empresa
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/departamento-empresa\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /departamento-empresa?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/departamento-empresa\/\d+\/modificar-habilitado/, // /departamento-empresa/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/departamento-empresa\/\d+/, // /departamento-empresa/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
