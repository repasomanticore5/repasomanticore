import { DepartamentoEmpresaCrearDto } from '../dto/departamento-empresa.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const DEPARTAMENTO_EMPRESA_DATOS_PRUEBA: ((
  idEmpresa: number,
) => DepartamentoEmpresaCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new DepartamentoEmpresaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.empresa = idEmpresa;
    dato.nombre = 'TI';
    dato.descripcion = 'Departamento de TI';
    return dato;
  },
];
