import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartamentoEmpresaEntity } from '../departamento-empresa.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const DEPARTAMENTO_EMPRESA_IMPORTS = [
  TypeOrmModule.forFeature([DepartamentoEmpresaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
