import { Module } from '@nestjs/common';
import { DepartamentoEmpresaController } from './departamento-empresa.controller';
import { DEPARTAMENTO_EMPRESA_IMPORTS } from './constantes/departamento-empresa.imports';
import { DEPARTAMENTO_EMPRESA_PROVIDERS } from './constantes/departamento-empresa.providers';

@Module({
  imports: [...DEPARTAMENTO_EMPRESA_IMPORTS],
  providers: [...DEPARTAMENTO_EMPRESA_PROVIDERS],
  exports: [...DEPARTAMENTO_EMPRESA_PROVIDERS],
  controllers: [DepartamentoEmpresaController],
})
export class DepartamentoEmpresaModule {}
