import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { DepartamentoEmpresaService } from './departamento-empresa.service';
import { DepartamentoEmpresaHabilitadoDto } from './dto/departamento-empresa.habilitado.dto';
import { DEPARTAMENTO_EMPRESA_OCC } from './constantes/departamento-empresa.occ';
import { DepartamentoEmpresaCrearDto } from './dto/departamento-empresa.crear.dto';
import { DepartamentoEmpresaActualizarDto } from './dto/departamento-empresa.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { DepartamentoEmpresaEntity } from './departamento-empresa.entity';
import { DepartamentoEmpresaBusquedaDto } from './dto/departamento-empresa.busqueda.dto';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'departamento-empresa';

@Controller(nombreControlador)
export class DepartamentoEmpresaController extends ControladorComun {
  constructor(
    private readonly _departamentoEmpresaService: DepartamentoEmpresaService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _departamentoEmpresaService,
      _seguridadService,
      _auditoriaService,
      DEPARTAMENTO_EMPRESA_OCC(
        DepartamentoEmpresaHabilitadoDto,
        DepartamentoEmpresaCrearDto,
        DepartamentoEmpresaActualizarDto,
        DepartamentoEmpresaBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: DepartamentoEmpresaBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._departamentoEmpresaService.departamentoEmpresaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<DepartamentoEmpresaEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._departamentoEmpresaService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._departamentoEmpresaService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<DepartamentoEmpresaEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        if (parametrosConsulta.busqueda) {
          consultaWhere.push({ nombre: Like('%' + parametrosConsulta.busqueda + '%') });
          consulta.where = consultaWhere;
        }

  /*      console.log(consulta, consultaWhere)
        if (parametrosConsulta.nivel) {
          consultaWhere.push({ nivel: +parametrosConsulta.nivel });
          consulta.where = consultaWhere;
         }*/
        consultaWhere = consulta.where as FindConditions<DepartamentoEmpresaEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );

        consulta.where = this.setearFiltro(
          'empresa',
          consultaWhere,
          parametrosConsulta.empresa,
        );
        consulta.where = this.setearFiltro(
          'nivel',
          consultaWhere,
          parametrosConsulta.nivel,
        );
        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._departamentoEmpresaService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      // qb.leftJoinAndSelect(aliasTabla + '.empresa', 'empresa');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip)
        .take(consulta.take)
        .getManyAndCount();
    }
  }
}
