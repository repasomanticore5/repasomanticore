import { IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, Min, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class DepartamentoEmpresaCrearDto extends HabilitadoDtoComun {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(30)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @Expose()
  departamentoEmpresaPadre: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresa: number;
}
