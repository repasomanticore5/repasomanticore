import { IsInt, IsOptional, IsString, MaxLength, Min, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class DepartamentoEmpresaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(30)
  @Expose()
  nombre: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  descripcion: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  @Expose()
  nivel: number;

  @IsOptional()
  @IsInt()
  @Expose()
  departamentoEmpresaPadre: number;

  @IsOptional()
  @IsInt()
  @Expose()
  empresa: number;
}
