import { IsInt, IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, Min, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class DepartamentoEmpresaBusquedaDto extends BusquedaComunProyectoDto {

  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsNumberString()
  @Expose()
  empresa: string;

  @IsOptional()
  @IsString()
  @Expose()
  nombre: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  @Expose()
  nivel: number;
}
