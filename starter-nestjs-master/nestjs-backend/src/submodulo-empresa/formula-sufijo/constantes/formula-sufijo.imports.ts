import { TypeOrmModule } from '@nestjs/typeorm';
import { FormulaSufijoEntity } from '../formula-sufijo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const FORMULA_SUFIJO_IMPORTS = [
  TypeOrmModule.forFeature([FormulaSufijoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
