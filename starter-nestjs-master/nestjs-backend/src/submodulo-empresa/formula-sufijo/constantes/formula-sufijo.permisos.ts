import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_FORMULA_SUFIJO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'formula-sufijo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/formula-sufijo/, // /formula-sufijo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/formula-sufijo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /formula-sufijo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-sufijo\/\d+\/modificar-habilitado/, // /formula-sufijo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/formula-sufijo\/\d+/, // /formula-sufijo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
