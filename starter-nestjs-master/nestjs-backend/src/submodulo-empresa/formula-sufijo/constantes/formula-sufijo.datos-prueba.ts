import { FormulaSufijoCrearDto } from '../dto/formula-sufijo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const FORMULA_SUFIJO_DATOS_PRUEBA: ((idEmpresa: number) => FormulaSufijoCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new FormulaSufijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'AA';
    dato.descripcion = 'Doble A';
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new FormulaSufijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'AAA';
    dato.descripcion = 'Triple A';
    dato.empresa = idEmpresa;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new FormulaSufijoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.nombre = 'ZZ';
    dato.descripcion = 'Doble Z';
    dato.empresa = idEmpresa;
    return dato;
  },
];
