import { Module } from '@nestjs/common';
import { FormulaSufijoController } from './formula-sufijo.controller';
import { FORMULA_SUFIJO_IMPORTS } from './constantes/formula-sufijo.imports';
import { FORMULA_SUFIJO_PROVIDERS } from './constantes/formula-sufijo.providers';

@Module({
  imports: [...FORMULA_SUFIJO_IMPORTS],
  providers: [...FORMULA_SUFIJO_PROVIDERS],
  exports: [...FORMULA_SUFIJO_PROVIDERS],
  controllers: [FormulaSufijoController],
})
export class FormulaSufijoModule {}
