import { TypeOrmModule } from '@nestjs/typeorm';
import { CaracteristicaBodegaEntity } from '../caracteristica-bodega.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const CARACTERISTICA_BODEGA_IMPORTS = [
  TypeOrmModule.forFeature([CaracteristicaBodegaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
