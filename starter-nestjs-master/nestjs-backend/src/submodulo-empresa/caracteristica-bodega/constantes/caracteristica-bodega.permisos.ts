import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_CARACTERISTICA_BODEGA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'caracteristica-bodega';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/caracteristica-bodega/, // /caracteristica-bodega
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/caracteristica-bodega\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /caracteristica-bodega?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-bodega\/\d+\/modificar-habilitado/, // /caracteristica-bodega/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/caracteristica-bodega\/\d+/, // /caracteristica-bodega/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
