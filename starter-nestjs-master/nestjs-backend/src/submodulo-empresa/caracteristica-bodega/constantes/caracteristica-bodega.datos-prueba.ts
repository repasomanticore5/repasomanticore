import { CaracteristicaBodegaCrearDto } from '../dto/caracteristica-bodega.crear.dto';
import {ActivoInactivo} from '../../../enums/activo-inactivo';

export const CARACTERISTICA_BODEGA_DATOS_PRUEBA: ((idEmpresa: number) => CaracteristicaBodegaCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new CaracteristicaBodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Medidas cuarto frío';
    dato.descripcion = 'cuarto frio de 32m2';
    dato.empresa = idEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new CaracteristicaBodegaCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Medidas cuarto caliente';
    dato.descripcion = 'cuarto caliente de 32m2';
    dato.empresa = idEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
