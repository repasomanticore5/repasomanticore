import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {EmpresaEntity} from '../empresa/empresa.entity';
import {CaracteristicaTipoBodegaEntity} from '../caracteristica-tipo-bodega/caracteristica-tipo-bodega.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'CARACTERISTICA_BODEGA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class CaracteristicaBodegaEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_CARACTERISTICA_BODEGA',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'nombre',
        type: 'varchar',
        length: 60,
        nullable: false,
    })
    nombre: string;

    @Column({
        name: 'descripcion',
        type: 'varchar',
        length: 255,
        nullable: true,
    })
    descripcion: string = null;

    @ManyToOne(
        () => EmpresaEntity,
        (r) => r.caracteristicasBodega,
        {nullable: false}
    )
    empresa: EmpresaEntity | number;

    @OneToMany(
        () => CaracteristicaTipoBodegaEntity,
        (r) => r.caracteristicaBodega,
    )
    caracteristicasTipoBodega: CaracteristicaTipoBodegaEntity[];
}
