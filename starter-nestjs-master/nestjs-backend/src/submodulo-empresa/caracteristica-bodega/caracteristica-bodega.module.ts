import { Module } from '@nestjs/common';
import { CaracteristicaBodegaController } from './caracteristica-bodega.controller';
import { CARACTERISTICA_BODEGA_IMPORTS } from './constantes/caracteristica-bodega.imports';
import { CARACTERISTICA_BODEGA_PROVIDERS } from './constantes/caracteristica-bodega.providers';

@Module({
  imports: [...CARACTERISTICA_BODEGA_IMPORTS],
  providers: [...CARACTERISTICA_BODEGA_PROVIDERS],
  exports: [...CARACTERISTICA_BODEGA_PROVIDERS],
  controllers: [CaracteristicaBodegaController],
})
export class CaracteristicaBodegaModule {}
