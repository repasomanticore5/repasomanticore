import {IsInt, IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class CaracteristicaBodegaCrearDto extends HabilitadoDtoComun {
    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    empresa: number;

}
