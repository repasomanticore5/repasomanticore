import {IsInt, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class CaracteristicaBodegaActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR

  // @IsOptional()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(255)
    @Expose()
    descripcion: string;

    @IsOptional()
    @IsInt()
    @Expose()
    empresa: number;
}
