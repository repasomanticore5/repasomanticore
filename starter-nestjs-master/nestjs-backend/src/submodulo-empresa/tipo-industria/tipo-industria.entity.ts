import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { EmpresaIndustriaEntity } from '../empresa-industria/empresa-industria.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_INDUSTRIA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre'])
export class TipoIndustriaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_TIPO_INDUSTRIA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 60,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  descripcion: string = null;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  @OneToMany(() => EmpresaIndustriaEntity, (r) => r.tipoIndustria)
  empresasIndustria: EmpresaIndustriaEntity[];
}
