import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_INDUSTRIA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'tipo-industria';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tipo-industria/, // /tipo-industria
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tipo-industria\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-industria?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-industria\/\d+\/modificar-habilitado/, // /tipo-industria/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-industria\/\d+/, // /tipo-industria/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
