import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoIndustriaEntity } from '../tipo-industria.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const TIPO_INDUSTRIA_IMPORTS = [
  TypeOrmModule.forFeature([TipoIndustriaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
