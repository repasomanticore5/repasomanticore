import { TipoIndustriaCrearDto } from '../dto/tipo-industria.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TIPO_INDUSTRIA_DATOS_PRUEBA: (() => TipoIndustriaCrearDto)[] = [
  () => {
    const dato = new TipoIndustriaCrearDto();
    dato.nombre = 'servicio de delivery';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TipoIndustriaCrearDto();
    dato.nombre = 'educación';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new TipoIndustriaCrearDto();
    dato.nombre = 'tecnología';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
