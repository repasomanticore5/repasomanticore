import { Module } from '@nestjs/common';
import { TipoIndustriaController } from './tipo-industria.controller';
import { TIPO_INDUSTRIA_IMPORTS } from './constantes/tipo-industria.imports';
import { TIPO_INDUSTRIA_PROVIDERS } from './constantes/tipo-industria.providers';

@Module({
  imports: [...TIPO_INDUSTRIA_IMPORTS],
  providers: [...TIPO_INDUSTRIA_PROVIDERS],
  exports: [...TIPO_INDUSTRIA_PROVIDERS],
  controllers: [TipoIndustriaController],
})
export class TipoIndustriaModule {}
