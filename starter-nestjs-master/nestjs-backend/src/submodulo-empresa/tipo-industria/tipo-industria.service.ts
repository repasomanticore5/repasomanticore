import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TipoIndustriaEntity } from './tipo-industria.entity';
import {TipoIndustriaBusquedaDto} from './dto/tipo-industria.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class TipoIndustriaService extends ServicioComun<TipoIndustriaEntity, TipoIndustriaBusquedaDto> {
  constructor(
    @InjectRepository(TipoIndustriaEntity, NOMBRE_CADENA_CONEXION)
    public tipoIndustriaEntityRepository: Repository<TipoIndustriaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        tipoIndustriaEntityRepository,
      _connection,
        TipoIndustriaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
