import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { BodegaTipoEntity } from '../bodega-tipo/bodega-tipo.entity';
import { CaracteristicaTipoBodegaEntity } from '../caracteristica-tipo-bodega/caracteristica-tipo-bodega.entity';
import { EmpresaEntity } from '../empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_BODEGA')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre'])
export class TipoBodegaEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_TIPO_BODEGA',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 60,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  descripcion: string = null;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;
  @ManyToOne(() => EmpresaEntity, (r) => r.tiposBodega, { nullable: false })
  empresa: EmpresaEntity | number;

  @OneToMany(() => BodegaTipoEntity, (r) => r.tipoBodega)
  bodegasTipo: BodegaTipoEntity[];

  @OneToMany(() => CaracteristicaTipoBodegaEntity, (r) => r.tipoBodega)
  caracteristicasTipoBodega: CaracteristicaTipoBodegaEntity[];
}
