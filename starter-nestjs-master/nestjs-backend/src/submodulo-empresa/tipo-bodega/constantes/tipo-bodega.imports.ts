import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoBodegaEntity } from '../tipo-bodega.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const TIPO_BODEGA_IMPORTS = [
  TypeOrmModule.forFeature([TipoBodegaEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
