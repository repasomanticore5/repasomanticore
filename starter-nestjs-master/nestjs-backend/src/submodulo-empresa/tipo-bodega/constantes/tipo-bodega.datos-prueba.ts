import { TipoBodegaCrearDto } from '../dto/tipo-bodega.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TIPO_BODEGA_DATOS_PRUEBA: ((
  idEmpresa: number,
) => TipoBodegaCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new TipoBodegaCrearDto();
    dato.nombre = 'bodega de almacenamiento';
    dato.descripcion = 'bodega para almacenamiento de articulos';
    dato.empresa = idEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new TipoBodegaCrearDto();
    dato.nombre = 'bodega de refrigeración';
    dato.descripcion = 'bodega para refrigeración de articulos';
    dato.empresa = idEmpresa; // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
