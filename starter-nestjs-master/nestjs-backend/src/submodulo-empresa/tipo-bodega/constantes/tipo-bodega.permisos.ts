import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_BODEGA: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'tipo-bodega';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tipo-bodega/, // /tipo-bodega
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tipo-bodega\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-bodega?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-bodega\/\d+\/modificar-habilitado/, // /tipo-bodega/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-bodega\/\d+/, // /tipo-bodega/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
