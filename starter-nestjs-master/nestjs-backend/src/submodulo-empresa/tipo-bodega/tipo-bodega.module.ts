import { Module } from '@nestjs/common';
import { TipoBodegaController } from './tipo-bodega.controller';
import { TIPO_BODEGA_IMPORTS } from './constantes/tipo-bodega.imports';
import { TIPO_BODEGA_PROVIDERS } from './constantes/tipo-bodega.providers';

@Module({
  imports: [...TIPO_BODEGA_IMPORTS],
  providers: [...TIPO_BODEGA_PROVIDERS],
  exports: [...TIPO_BODEGA_PROVIDERS],
  controllers: [TipoBodegaController],
})
export class TipoBodegaModule {}
