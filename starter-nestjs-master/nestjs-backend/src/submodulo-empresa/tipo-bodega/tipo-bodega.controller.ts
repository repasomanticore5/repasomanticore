import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {TipoBodegaService} from './tipo-bodega.service';
import {TipoBodegaHabilitadoDto} from './dto/tipo-bodega.habilitado.dto';
import {TIPO_BODEGA_OCC} from './constantes/tipo-bodega.occ';
import {TipoBodegaCrearDto} from './dto/tipo-bodega.crear.dto';
import {TipoBodegaActualizarDto} from './dto/tipo-bodega.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {TipoBodegaEntity} from './tipo-bodega.entity';
import {TipoBodegaBusquedaDto} from './dto/tipo-bodega.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';

const nombreControlador = 'tipo-bodega';

@Controller(nombreControlador)
export class TipoBodegaController extends ControladorComun {
    constructor(
        private readonly _tipoBodegaService: TipoBodegaService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _tipoBodegaService,
            _seguridadService,
            _auditoriaService,
            TIPO_BODEGA_OCC(
                TipoBodegaHabilitadoDto,
                TipoBodegaCrearDto,
                TipoBodegaActualizarDto,
                TipoBodegaBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: TipoBodegaBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._tipoBodegaService.tipoBodegaEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<TipoBodegaEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._tipoBodegaService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._tipoBodegaService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta,
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<TipoBodegaEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({
                        nombre: Like('%' + parametrosConsulta.busqueda + '%'),
                    });
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.nombreAutocomplete) {
                    consultaWhere.push({nombre: Like('%' + parametrosConsulta.nombreAutocomplete + '%')});
                    consulta.where = consultaWhere;
                }
                consultaWhere = consulta.where as FindConditions<TipoBodegaEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                consulta.where = this.setearFiltro(
                    'empresa',
                    consultaWhere,
                    parametrosConsulta.empresa,
                );
                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._tipoBodegaService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            if (parametrosConsulta.tipoBodegaRegistrado) {
                const {ids} = JSON.parse(parametrosConsulta.tipoBodegaRegistrado);
                if (ids.length > 0) {
                    qb.andWhere(aliasTabla + `.id NOT IN (${ids})`);
                }
            }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
        }
    }
}
