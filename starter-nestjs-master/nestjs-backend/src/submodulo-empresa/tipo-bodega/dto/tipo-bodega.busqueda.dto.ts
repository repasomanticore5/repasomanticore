import { IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class TipoBodegaBusquedaDto extends BusquedaComunProyectoDto {

  @IsOptional()
  @IsNumberString()
  @Expose()
  id: string;

  // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA

  @IsOptional()
  @IsNumberString()
  @Expose()
  empresa: string;

  @IsOptional()
  @IsString()
  @Expose()
  nombreAutocomplete: string;

  @IsOptional()
  @IsString()
  @Expose()
  tipoBodegaRegistrado: string;
}
