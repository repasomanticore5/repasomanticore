import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TipoBodegaEntity } from './tipo-bodega.entity';
import {TipoBodegaBusquedaDto} from './dto/tipo-bodega.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class TipoBodegaService extends ServicioComun<TipoBodegaEntity, TipoBodegaBusquedaDto> {
  constructor(
    @InjectRepository(TipoBodegaEntity, NOMBRE_CADENA_CONEXION)
    public tipoBodegaEntityRepository: Repository<TipoBodegaEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        tipoBodegaEntityRepository,
      _connection,
        TipoBodegaEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
