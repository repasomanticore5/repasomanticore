import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ContactoEmpresaEntity } from '../contacto-empresa/contacto-empresa.entity';
import { EmpresaEntity } from '../empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'TIPO_CARGO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado', 'nombre', 'codigo'])
export class TipoCargoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_TIPO_CARGO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 30,
    nullable: false,
  })
  nombre: string;

  @Column({
    name: 'codigo',
    type: 'varchar',
    length: 10,
    nullable: true,
    unique: true,
  })
  codigo: string = null;

  // @ManyToOne(() => EntidadRelacionPapa, (r) => r.nombreRelacionPapa, { nullable: false })
  // entidadRelacionPapa: EntidadRelacionPapa;

  @OneToMany(() => ContactoEmpresaEntity, (r) => r.tipoCargo)
  contactosEmpresa: ContactoEmpresaEntity[];

  @ManyToOne(() => EmpresaEntity, (r) => r.tiposCargo, {
    nullable: false,
  })
  empresa: EmpresaEntity | number;
}
