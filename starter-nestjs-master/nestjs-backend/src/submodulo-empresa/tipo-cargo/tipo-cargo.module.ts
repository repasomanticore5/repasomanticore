import { Module } from '@nestjs/common';
import { TipoCargoController } from './tipo-cargo.controller';
import { TIPO_CARGO_IMPORTS } from './constantes/tipo-cargo.imports';
import { TIPO_CARGO_PROVIDERS } from './constantes/tipo-cargo.providers';

@Module({
  imports: [...TIPO_CARGO_IMPORTS],
  providers: [...TIPO_CARGO_PROVIDERS],
  exports: [...TIPO_CARGO_PROVIDERS],
  controllers: [TipoCargoController],
})
export class TipoCargoModule {}
