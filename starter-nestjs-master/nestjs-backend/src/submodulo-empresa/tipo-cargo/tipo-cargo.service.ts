import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TipoCargoEntity } from './tipo-cargo.entity';
import {TipoCargoBusquedaDto} from './dto/tipo-cargo.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';

@Injectable()
export class TipoCargoService extends ServicioComun<TipoCargoEntity, TipoCargoBusquedaDto> {
  constructor(
    @InjectRepository(TipoCargoEntity, NOMBRE_CADENA_CONEXION)
    public tipoCargoEntityRepository: Repository<TipoCargoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        tipoCargoEntityRepository,
      _connection,
        TipoCargoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
