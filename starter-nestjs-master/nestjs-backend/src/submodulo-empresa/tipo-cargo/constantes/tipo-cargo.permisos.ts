import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_TIPO_CARGO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'tipo-cargo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/tipo-cargo/, // /tipo-cargo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/tipo-cargo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /tipo-cargo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-cargo\/\d+\/modificar-habilitado/, // /tipo-cargo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/tipo-cargo\/\d+/, // /tipo-cargo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
