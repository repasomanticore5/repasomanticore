import { TipoCargoCrearDto } from '../dto/tipo-cargo.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const TIPO_CARGO_DATOS_PRUEBA: ((
  idEmpresa: number,
) => TipoCargoCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new TipoCargoCrearDto();
    dato.empresa = idEmpresa;
    dato.nombre = 'vendedor';
    dato.codigo = 'TC001' + idEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new TipoCargoCrearDto();
    dato.empresa = idEmpresa;
    dato.nombre = 'supervisor';
    dato.codigo = 'TC002' + idEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new TipoCargoCrearDto();
    dato.empresa = idEmpresa;
    dato.nombre = 'desarrollador';
    dato.codigo = 'TC003' + idEmpresa;
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
