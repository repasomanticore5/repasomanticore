import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoCargoEntity } from '../tipo-cargo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';
import {SeguridadModule} from '../../../seguridad/seguridad.module';

export const TIPO_CARGO_IMPORTS = [
  TypeOrmModule.forFeature([TipoCargoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
