import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {NombrePermisoEntity} from '../nombre-permiso/nombre-permiso.entity';
import {RolEntity} from '../rol/rol.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'PERMISO_ROL')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class PermisoRolEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_PERMISO_ROL',
        unsigned: true,
    })
    id: number;

    @ManyToOne(
        () => NombrePermisoEntity,
        (nombrePermiso) => nombrePermiso.permisoRolNP,
        { nullable: false },
    )
    nombrePermisoPR: NombrePermisoEntity | number;

    @ManyToOne(
        () => RolEntity,
        (rol) => rol.permisoRolR,
        { nullable: false },
    )
    rolPR: RolEntity | number;
}
