import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class PermisoRolCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsNumber()
  @Expose()
  nombrePermisoPR: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  rolPR: number;
}
