import {IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { Expose } from 'class-transformer';

export class PermisoRolActualizarDto {
    @IsOptional()
    @IsNumber()
    @Expose()
    nombrePermisoPR: number;


    @IsOptional()
    @IsNumber()
    @Expose()
    rolPR: number;
}
