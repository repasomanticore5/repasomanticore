import { TypeOrmModule } from '@nestjs/typeorm';
import { PermisoRolEntity } from '../permiso-rol.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const PERMISO_ROL_IMPORTS = [
  TypeOrmModule.forFeature([PermisoRolEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
