import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_PERMISO_ROL: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'permiso-rol';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/permiso-rol/, // /permiso-rol
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/permiso-rol\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /permiso-rol?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/permiso-rol\/\d+\/modificar-habilitado/, // /permiso-rol/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/permiso-rol\/\d+/, // /permiso-rol/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
