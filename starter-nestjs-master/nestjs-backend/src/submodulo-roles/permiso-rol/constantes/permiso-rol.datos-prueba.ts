import { PermisoRolCrearDto } from '../dto/permiso-rol.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const PERMISO_ROL_DATOS_PRUEBA: ((
  idRol: number,
  idNombrePemiso: number,
) => PermisoRolCrearDto)[] = [
  (idRol: number, idNombrePemiso: number) => {
    const dato = new PermisoRolCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombrePermisoPR = idNombrePemiso;
    dato.rolPR = idRol;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
