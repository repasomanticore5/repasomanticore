import {
  Controller, Delete,
  Get,
  HttpCode, InternalServerErrorException, Param,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import {PermisoRolService} from './permiso-rol.service';
import {PermisoRolHabilitadoDto} from './dto/permiso-rol.habilitado.dto';
import {PERMISO_ROL_OCC} from './constantes/permiso-rol.occ';
import {PermisoRolCrearDto} from './dto/permiso-rol.crear.dto';
import {PermisoRolActualizarDto} from './dto/permiso-rol.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {PermisoRolEntity} from './permiso-rol.entity';
import {PermisoRolBusquedaDto} from './dto/permiso-rol.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';
import {NombrePermisoEntity} from '../nombre-permiso/nombre-permiso.entity';

const nombreControlador = 'permiso-rol';

@Controller(nombreControlador)
export class PermisoRolController extends ControladorComun {
  constructor(
      private readonly _permisoRolService: PermisoRolService,
      private readonly _seguridadService: SeguridadService,
      private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        _permisoRolService,
        _seguridadService,
        _auditoriaService,
        PERMISO_ROL_OCC(
            PermisoRolHabilitadoDto,
            PermisoRolCrearDto,
            PermisoRolActualizarDto,
            PermisoRolBusquedaDto,
        ),
        NUMERO_REGISTROS_DEFECTO,
        NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Delete(':idPermisoRol')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async eliminar(
      @Param('idPermisoRol') id: string,
  ) {
    try {
      console.log({id})
      return await this._permisoRolService
          .eliminar(+id);

    } catch (e) {
      console.error({
        mensaje: 'Error al eliminar permiso rol',
        error: e,
      });
      throw new InternalServerErrorException({
        mensaje: 'Error al eliminar permiso rol',
      });
    }
  }


  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
      @Query() parametrosConsulta: PermisoRolBusquedaDto,
      @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
        request,
        parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._permisoRolService.permisoRolEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
          .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<PermisoRolEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
          consulta,
          parametrosConsulta.skip,
          parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._permisoRolService.establecerOrden(
          parametrosConsulta.sortField,
          parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._permisoRolService.establecerBusquedaPorId(
          parametrosConsulta,
          consulta
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<PermisoRolEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        if (parametrosConsulta.busqueda) {
          // consultaWhere.push({nombrePermisoPR: {nombre: Like('%' + parametrosConsulta.busqueda + '%')}});
          // consultaWhere.push({rolPR.nombre: Like('%' + parametrosConsulta.busqueda + '%')});
          consulta.where = consultaWhere;
        }
        // if (parametrosConsulta.busqueda) {
        //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
        //     consulta.where = consultaWhere;
        // }
        consultaWhere = consulta.where as FindConditions<PermisoRolEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
            'sisHabilitado',
            consultaWhere,
            parametrosConsulta.sisHabilitado,
        );

        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._permisoRolService.convertirEnMayusculas(consulta);
      }

      // @ts-ignore
      // console.log({b: consulta.where.nombrePermisoPR.nombre});
      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      qb.leftJoinAndSelect(aliasTabla + '.nombrePermisoPR', 'nombrePermisoPR');
      qb.leftJoinAndSelect(aliasTabla + '.rolPR', 'rolPR');
      qb.leftJoinAndSelect('rolPR.empresaR', 'empresaR');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      if (parametrosConsulta.busqueda) {
        // qb.orWhere({rolPR['nombre'] : ''})
          qb.orWhere('nombrePermisoPR.nombre like :nombre', {
              nombre: `%${parametrosConsulta.busqueda}%`,
          })
          qb.orWhere('rolPR.nombre like :nombre', {
              nombre: '%' + parametrosConsulta.busqueda + '%',
          })
      }
      // if (parametrosConsulta.busqueda) {
      //   const consultaWhere = consulta.where as FindConditions<PermisoRolEntity>[];
      //   consultaWhere.push({nombrePermisoPR: {nombre: Like('%' + parametrosConsulta.busqueda + '%')}});
      //   // consultaWhere.push({rolPR.nombre: Like('%' + parametrosConsulta.busqueda + '%')});
      //   consulta.where = consultaWhere;
      //   qb.where(consulta.where);
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip)
          .take(consulta.take)
          .getManyAndCount();
    }
  }
}
