import { Module } from '@nestjs/common';
import { PermisoRolController } from './permiso-rol.controller';
import { PERMISO_ROL_IMPORTS } from './constantes/permiso-rol.imports';
import { PERMISO_ROL_PROVIDERS } from './constantes/permiso-rol.providers';

@Module({
  imports: [...PERMISO_ROL_IMPORTS],
  providers: [...PERMISO_ROL_PROVIDERS],
  exports: [...PERMISO_ROL_PROVIDERS],
  controllers: [PermisoRolController],
})
export class PermisoRolModule {}
