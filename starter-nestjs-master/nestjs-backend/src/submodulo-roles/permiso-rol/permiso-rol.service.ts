import {Injectable, InternalServerErrorException} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { PermisoRolEntity } from './permiso-rol.entity';
import {PermisoRolBusquedaDto} from './dto/permiso-rol.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class PermisoRolService extends ServicioComun<PermisoRolEntity, PermisoRolBusquedaDto> {
  constructor(
    @InjectRepository(PermisoRolEntity, NOMBRE_CADENA_CONEXION)
    public permisoRolEntityRepository: Repository<PermisoRolEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        permisoRolEntityRepository,
      _connection,
        PermisoRolEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }

  async eliminar(id: number): Promise<{ mensaje: string }> {
    try {
      console.log('eliminar')
      await this.permisoRolEntityRepository
          .createQueryBuilder('permisoRol')
          .delete()
          .where('id = :id', {id})
          .execute();
      return {
        mensaje: 'Ok',
      }
    } catch (e) {
      console.error({
        mensaje: 'Error al eliminar permiso rol',
        error: e,
      });
      throw new InternalServerErrorException({
        mensaje: 'Error al eliminar permiso rol',
      });
    }
  }
}
