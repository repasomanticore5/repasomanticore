import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class NombrePermisoActualizarDto {

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(70)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsNumber()
    @Expose()
    modulo: number;
}
