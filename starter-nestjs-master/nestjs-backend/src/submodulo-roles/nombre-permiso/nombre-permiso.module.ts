import { Module } from '@nestjs/common';
import { NombrePermisoController } from './nombre-permiso.controller';
import { NOMBRE_PERMISO_IMPORTS } from './constantes/nombre-permiso.imports';
import { NOMBRE_PERMISO_PROVIDERS } from './constantes/nombre-permiso.providers';

@Module({
  imports: [...NOMBRE_PERMISO_IMPORTS],
  providers: [...NOMBRE_PERMISO_PROVIDERS],
  exports: [...NOMBRE_PERMISO_PROVIDERS],
  controllers: [NombrePermisoController],
})
export class NombrePermisoModule {}
