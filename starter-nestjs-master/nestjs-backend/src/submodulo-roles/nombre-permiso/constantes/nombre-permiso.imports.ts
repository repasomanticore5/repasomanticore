import { TypeOrmModule } from '@nestjs/typeorm';
import { NombrePermisoEntity } from '../nombre-permiso.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const NOMBRE_PERMISO_IMPORTS = [
  TypeOrmModule.forFeature([NombrePermisoEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
