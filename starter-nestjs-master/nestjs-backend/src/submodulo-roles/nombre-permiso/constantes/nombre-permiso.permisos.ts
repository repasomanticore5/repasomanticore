import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_NOMBRE_PERMISO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'nombre-permiso';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/nombre-permiso/, // /nombre-permiso
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/nombre-permiso\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /nombre-permiso?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/nombre-permiso\/\d+\/modificar-habilitado/, // /nombre-permiso/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/nombre-permiso\/\d+/, // /nombre-permiso/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
