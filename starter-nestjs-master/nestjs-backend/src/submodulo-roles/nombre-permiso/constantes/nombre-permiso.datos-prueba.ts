import { NombrePermisoCrearDto } from '../dto/nombre-permiso.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const NOMBRE_PERMISO_DATOS_PRUEBA: ((
  idModulo: number,
) => NombrePermisoCrearDto)[] = [
  (idModulo: number) => {
    const dato = new NombrePermisoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'crear';
    dato.moduloNP = idModulo;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idModulo: number) => {
    const dato = new NombrePermisoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'editar';
    dato.moduloNP = idModulo;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idModulo: number) => {
    const dato = new NombrePermisoCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'habilitar';
    dato.sisHabilitado = ActivoInactivo.Activo;
    dato.moduloNP = idModulo;
    return dato;
  },
];
