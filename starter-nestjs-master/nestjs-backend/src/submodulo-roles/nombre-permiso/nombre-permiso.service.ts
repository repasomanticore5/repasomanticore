import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { NombrePermisoEntity } from './nombre-permiso.entity';
import {NombrePermisoBusquedaDto} from './dto/nombre-permiso.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class NombrePermisoService extends ServicioComun<NombrePermisoEntity, NombrePermisoBusquedaDto> {
  constructor(
    @InjectRepository(NombrePermisoEntity, NOMBRE_CADENA_CONEXION)
    public nombrePermisoEntityRepository: Repository<NombrePermisoEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        nombrePermisoEntityRepository,
      _connection,
        NombrePermisoEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
