import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ModuloEntity } from '../modulo/modulo.entity';
import { PermisoRolEntity } from '../permiso-rol/permiso-rol.entity';

@Entity(PREFIJO_BASE + 'NOMBRE_PERMISO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class NombrePermisoEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_NOMBRE_PERMISO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 70,
    nullable: false,
  })
  nombre: string;

  @ManyToOne(() => ModuloEntity, (modulo) => modulo.nombrePermisoM, {
    nullable: false,
  })
  moduloNP: ModuloEntity | number;

  @OneToMany(() => PermisoRolEntity, (permisoRol) => permisoRol.nombrePermisoPR)
  permisoRolNP: PermisoRolEntity[];
}
