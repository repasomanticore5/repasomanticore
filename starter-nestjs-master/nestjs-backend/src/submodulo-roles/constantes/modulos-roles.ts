import { DatosUsuarioModule } from '../datos-usuario/datos-usuario.module';
import { DispositivosMovilesModule } from '../dispositivos-moviles/dispositivos-moviles.module';
import { ModuloModule } from '../modulo/modulo.module';
import { NombrePermisoModule } from '../nombre-permiso/nombre-permiso.module';
import { PermisoRolModule } from '../permiso-rol/permiso-rol.module';
import { RolModule } from '../rol/rol.module';
import { RolPorUsuarioModule } from '../rol-por-usuario/rol-por-usuario.module';

export const MODULOS_ROLES = [
  DatosUsuarioModule,
  DispositivosMovilesModule,
  ModuloModule,
  NombrePermisoModule,
  PermisoRolModule,
  RolModule,
  RolPorUsuarioModule,
];
