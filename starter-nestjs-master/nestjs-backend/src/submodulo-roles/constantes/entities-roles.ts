import { DatosUsuarioEntity } from '../datos-usuario/datos-usuario.entity';
import { DispositivosMovilesEntity } from '../dispositivos-moviles/dispositivos-moviles.entity';
import { ModuloEntity } from '../modulo/modulo.entity';
import { NombrePermisoEntity } from '../nombre-permiso/nombre-permiso.entity';
import { PermisoRolEntity } from '../permiso-rol/permiso-rol.entity';
import { RolEntity } from '../rol/rol.entity';
import { RolPorUsuarioEntity } from '../rol-por-usuario/rol-por-usuario.entity';

export const ENTITIES_ROLES = [
  DatosUsuarioEntity,
  DispositivosMovilesEntity,
  ModuloEntity,
  NombrePermisoEntity,
  PermisoRolEntity,
  RolEntity,
  RolPorUsuarioEntity,
];
