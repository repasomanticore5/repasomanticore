import { DatosUsuarioEntity } from '../datos-usuario/datos-usuario.entity';
import { PermisoRolEntity } from '../permiso-rol/permiso-rol.entity';
import { NombrePermisoEntity } from '../nombre-permiso/nombre-permiso.entity';
import { RolEntity } from '../rol/rol.entity';
import { RolPorUsuarioEntity } from '../rol-por-usuario/rol-por-usuario.entity';
import { DispositivosMovilesEntity } from '../dispositivos-moviles/dispositivos-moviles.entity';
import { ModuloEntity } from '../modulo/modulo.entity';

export interface DatosPruebaRolesInterface {
  datosUsuario: DatosUsuarioEntity[];
  modulo: ModuloEntity[];
  permisoRol: PermisoRolEntity[];
  nombrePermiso: NombrePermisoEntity[];
  rol: RolEntity[];
  rolPorUsuario: RolPorUsuarioEntity[];
  dispMovilesUsuario: DispositivosMovilesEntity[];
}
