export const CONFIG_DATOS_PRUEBA_ROLES = {
  submoduloRoles: true,
  datosUsuario: true,
  modulo: true,
  permisoRol: true,
  nombrePermiso: true,
  rol: true,
  rolPorUsuario: true,
  dispMovilesUsuario: true,
};
