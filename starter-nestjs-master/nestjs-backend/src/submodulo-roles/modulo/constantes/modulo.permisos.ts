import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_MODULO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'modulo';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/modulo/, // /modulo
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/modulo\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /modulo?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/modulo\/\d+\/modificar-habilitado/, // /modulo/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/modulo\/\d+/, // /modulo/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
