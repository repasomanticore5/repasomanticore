import { TypeOrmModule } from '@nestjs/typeorm';
import { ModuloEntity } from '../modulo.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const MODULO_IMPORTS = [
  TypeOrmModule.forFeature([ModuloEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
