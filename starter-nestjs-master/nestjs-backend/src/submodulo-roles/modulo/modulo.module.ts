import { Module } from '@nestjs/common';
import { ModuloController } from './modulo.controller';
import { MODULO_IMPORTS } from './constantes/modulo.imports';
import { MODULO_PROVIDERS } from './constantes/modulo.providers';

@Module({
  imports: [...MODULO_IMPORTS],
  providers: [...MODULO_PROVIDERS],
  exports: [...MODULO_PROVIDERS],
  controllers: [ModuloController],
})
export class ModuloModule {}
