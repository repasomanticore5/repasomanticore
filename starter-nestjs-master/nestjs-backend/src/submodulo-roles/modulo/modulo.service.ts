import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ModuloEntity } from './modulo.entity';
import {ModuloBusquedaDto} from './dto/modulo.busqueda.dto';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {ServicioComun} from '@manticore-labs/nest-2021';

@Injectable()
export class ModuloService extends ServicioComun<ModuloEntity, ModuloBusquedaDto> {
  constructor(
    @InjectRepository(ModuloEntity, NOMBRE_CADENA_CONEXION)
    public moduloEntityRepository: Repository<ModuloEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        moduloEntityRepository,
      _connection,
        ModuloEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
