import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {Expose} from 'class-transformer';
import {HabilitadoDtoComun} from '../../../abstractos/habilitado-dto-comun';

export class ModuloCrearDto extends HabilitadoDtoComun {
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(50)
    @Expose()
    nombreModulo: string;
}
