import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class ModuloActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(50)
    @Expose()
    nombreModulo: string;
}
