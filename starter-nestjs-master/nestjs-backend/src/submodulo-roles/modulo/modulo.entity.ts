import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { NombrePermisoEntity } from '../nombre-permiso/nombre-permiso.entity';

@Entity(PREFIJO_BASE + 'MODULO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class ModuloEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_MODULO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre_modulo',
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  nombreModulo: string;

  @OneToMany(
    () => NombrePermisoEntity,
    (nombrePermiso) => nombrePermiso.moduloNP,
  )
  nombrePermisoM: NombrePermisoEntity[];
}
