import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectConnection, InjectRepository} from '@nestjs/typeorm';
import {Connection, Repository} from 'typeorm';
import {RolPorUsuarioEntity} from './rol-por-usuario.entity';
import {RolPorUsuarioBusquedaDto} from './dto/rol-por-usuario.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {RolPorUsuarioCrearDto} from './dto/rol-por-usuario.crear.dto';

@Injectable()
export class RolPorUsuarioService extends ServicioComun<RolPorUsuarioEntity, RolPorUsuarioBusquedaDto> {
    constructor(
        @InjectRepository(RolPorUsuarioEntity, NOMBRE_CADENA_CONEXION)
        public rolPorUsuarioEntityRepository: Repository<RolPorUsuarioEntity>,
        @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            rolPorUsuarioEntityRepository,
            _connection,
            RolPorUsuarioEntity,
            'id',
            _auditoriaService,
            // Por defecto NO se transforma todos los campos a mayúsculas.
            // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
            // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
            (objetoATransformar, metodo) => objetoATransformar
        );
    }

    async saveManyRolesPorUsuarioEmpresa(rolesPorUsuario: RolPorUsuarioCrearDto[]): Promise<RolPorUsuarioEntity[]> {
        try {
            const rolesAsignadosPorEmpresa = rolesPorUsuario
                .map(
                    async (rolPorUsuario) => {
                        const rolPorUsuarioCreado = await this.rolPorUsuarioEntityRepository
                            .createQueryBuilder('rolPorUsuario')
                            .where('rolPorUsuario.empresaId = :idEmp', {idEmp: rolPorUsuario.empresaId})
                            .andWhere('rolPorUsuario.rolPU = :idA', {idA: rolPorUsuario.rolPU})
                            .andWhere('rolPorUsuario.datosUsuarioRPU = :idUs', {idUs: rolPorUsuario.datosUsuarioRPU})
                            .getOne();
                    }
                )
            const rolesAsignados = await Promise.all(rolesAsignadosPorEmpresa);
            const arrLimpio = rolesAsignados
                .filter(
                    (rolPorUsuario) => rolPorUsuario !== undefined
                )
            if (arrLimpio.length > 0) {
                console.error({
                    mensaje: 'Error, el rol ya se encuentra asignado al usuario',
                });
                throw new InternalServerErrorException({
                    mensaje: 'Error, el rol ya se encuentra asignado al usuario',
                });
            }
            const respuestaSave = await this.rolPorUsuarioEntityRepository.save(rolesPorUsuario);
            const rolesMapeados = respuestaSave
                .map(
                    async (rolPorUsuario) => {
                        const rolPorUsuarioCreado = await this.rolPorUsuarioEntityRepository
                            .createQueryBuilder('rolPorUsuario')
                            .leftJoinAndSelect(
                                'rolPorUsuario.rolPU',
                                'rol',
                                'rolPorUsuario.rolPU = rol.id'
                            )
                            .where('rolPorUsuario.id = :idA', {idA: rolPorUsuario.id})
                            .getOne();
                        return {
                            ...rolPorUsuarioCreado,
                            habilitado: true,
                        }
                    }
                );
            return Promise.all(rolesMapeados);
        } catch (e) {
            console.error({
                mensaje: 'Error al crear varios roles por usuario',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al crear varios roles por usuario',
            });
        }
    }

    async eliminar(id: number): Promise<{ mensaje: string }> {
        try {
            await this.rolPorUsuarioEntityRepository
                .createQueryBuilder('rolPorUsuario')
                .delete()
                .where('id = :idA', {idA: id})
                .execute();
            return {
                mensaje: 'Ok',
            }
        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar role por usuario',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar role por usuario',
            });
        }
    }
}
