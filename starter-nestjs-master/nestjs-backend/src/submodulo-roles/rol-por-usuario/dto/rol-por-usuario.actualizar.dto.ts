import {
  IsInt,
  IsNumber,
  IsOptional,
} from 'class-validator';
import { Expose } from 'class-transformer';

export class RolPorUsuarioActualizarDto {
  @IsOptional()
  @IsInt()
  @Expose()
  empresaId: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  rolPU: number;

  @IsOptional()
  @IsNumber()
  @Expose()
  datosUsuarioRPU: number;
}
