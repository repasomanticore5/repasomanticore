import { IsInt, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class RolPorUsuarioCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsInt()
  @Expose()
  empresaId: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  rolPU: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  datosUsuarioRPU: number;
}
