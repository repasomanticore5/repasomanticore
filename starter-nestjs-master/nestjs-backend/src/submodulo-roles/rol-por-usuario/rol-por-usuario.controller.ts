import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get,
    HttpCode, InternalServerErrorException, Param, Post,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {RolPorUsuarioService} from './rol-por-usuario.service';
import {RolPorUsuarioHabilitadoDto} from './dto/rol-por-usuario.habilitado.dto';
import {ROL_POR_USUARIO_OCC} from './constantes/rol-por-usuario.occ';
import {RolPorUsuarioCrearDto} from './dto/rol-por-usuario.crear.dto';
import {RolPorUsuarioActualizarDto} from './dto/rol-por-usuario.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {RolPorUsuarioEntity} from './rol-por-usuario.entity';
import {RolPorUsuarioBusquedaDto} from './dto/rol-por-usuario.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'rol-por-usuario';

@Controller(nombreControlador)
export class RolPorUsuarioController extends ControladorComun {
    constructor(
        private readonly _rolPorUsuarioService: RolPorUsuarioService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _rolPorUsuarioService,
            _seguridadService,
            _auditoriaService,
            ROL_POR_USUARIO_OCC(
                RolPorUsuarioHabilitadoDto,
                RolPorUsuarioCrearDto,
                RolPorUsuarioActualizarDto,
                RolPorUsuarioBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: RolPorUsuarioBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._rolPorUsuarioService.rolPorUsuarioEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<RolPorUsuarioEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._rolPorUsuarioService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._rolPorUsuarioService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<RolPorUsuarioEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.idUsuario) {
                    consultaWhere.push({datosUsuarioRPU: +parametrosConsulta.idUsuario});
                    consulta.where = consultaWhere;
                }
                // if (parametrosConsulta.busqueda) {
                //     consultaWhere.push({cedula: Like('%' + parametrosConsulta.busqueda + '%')});
                //     consulta.where = consultaWhere;
                // }
                consultaWhere = consulta.where as FindConditions<RolPorUsuarioEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );
                consulta.where = this.setearFiltro(
                    'datosUsuarioRPU',
                    consultaWhere,
                    parametrosConsulta.datosUsuarioRPU,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._rolPorUsuarioService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            qb.leftJoinAndSelect(aliasTabla + '.rolPU', 'rol');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }

    @Post('save-many/roles-usuario-empresa')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async saveManyRolesPorUsuarioEmpresa(
        @Body('rolesPorUsuario') rolesPorUsuario: RolPorUsuarioCrearDto[],
    ) {
        if (rolesPorUsuario.length <= 0) {
            console.error({
                mensaje: 'Error, no envia parametros para crear',
            });
            throw new BadRequestException({
                mensaje: 'Error, no envia parametros para crear',
            });
        }

        try {
            return await this._rolPorUsuarioService
                .saveManyRolesPorUsuarioEmpresa(rolesPorUsuario);
        } catch (e) {
            console.error({
                mensaje: 'Error al crear varios roles por usuario',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al crear varios roles por usuario',
            });
        }
    }

    @Delete(':idRolPorUsuario')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async eliminar(
        @Param('idRolPorUsuario') id: string,
    ) {
        try {
            return await this._rolPorUsuarioService
                .eliminar(+id);

        } catch (e) {
            console.error({
                mensaje: 'Error al eliminar role por usuario',
                error: e,
            });
            throw new InternalServerErrorException({
                mensaje: 'Error al eliminar role por usuario',
            });
        }
    }
}
