import { Module } from '@nestjs/common';
import { RolPorUsuarioController } from './rol-por-usuario.controller';
import { ROL_POR_USUARIO_IMPORTS } from './constantes/rol-por-usuario.imports';
import { ROL_POR_USUARIO_PROVIDERS } from './constantes/rol-por-usuario.providers';

@Module({
  imports: [...ROL_POR_USUARIO_IMPORTS],
  providers: [...ROL_POR_USUARIO_PROVIDERS],
  exports: [...ROL_POR_USUARIO_PROVIDERS],
  controllers: [RolPorUsuarioController],
})
export class RolPorUsuarioModule {}
