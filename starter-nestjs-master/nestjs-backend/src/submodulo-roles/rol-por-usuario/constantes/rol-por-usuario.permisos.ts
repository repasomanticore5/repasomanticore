import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_ROL_POR_USUARIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'rol-por-usuario';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/rol-por-usuario/, // /rol-por-usuario
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/rol-por-usuario\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /rol-por-usuario?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/rol-por-usuario\/\d+\/modificar-habilitado/, // /rol-por-usuario/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/rol-por-usuario\/\d+/, // /rol-por-usuario/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
