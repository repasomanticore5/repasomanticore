import { RolPorUsuarioCrearDto } from '../dto/rol-por-usuario.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ROL_POR_USUARIO_DATOS_PRUEBA: ((
  idDatosUsuario: number,
  idRol: number,
) => RolPorUsuarioCrearDto)[] = [
  (idDatosUsuario: number, idRol: number) => {
    const dato = new RolPorUsuarioCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.datosUsuarioRPU = idDatosUsuario;
    dato.rolPU = idRol;
    dato.empresaId = 1;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
