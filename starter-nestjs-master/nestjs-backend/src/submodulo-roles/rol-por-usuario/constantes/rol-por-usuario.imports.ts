import { TypeOrmModule } from '@nestjs/typeorm';
import { RolPorUsuarioEntity } from '../rol-por-usuario.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const ROL_POR_USUARIO_IMPORTS = [
  TypeOrmModule.forFeature([RolPorUsuarioEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
