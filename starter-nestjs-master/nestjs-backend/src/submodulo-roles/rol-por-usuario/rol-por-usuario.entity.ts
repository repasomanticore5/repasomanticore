import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {RolEntity} from '../rol/rol.entity';
import {DatosUsuarioEntity} from '../datos-usuario/datos-usuario.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ROL_POR_USUARIO')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class RolPorUsuarioEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_ROL_POR_USUARIO',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'empresa_id',
        type: 'int',
        nullable: false,
    })
    empresaId: number;

    @ManyToOne(
        () => RolEntity,
        (rol) => rol.rolPorUsuarioR,
        { nullable: false },
    )
    rolPU: RolEntity | number;

    @ManyToOne(
        () => DatosUsuarioEntity,
        (datosUsuario) => datosUsuario.rolPorUsuarioDU,
        { nullable: false },
    )
    datosUsuarioRPU: DatosUsuarioEntity | number;
}
