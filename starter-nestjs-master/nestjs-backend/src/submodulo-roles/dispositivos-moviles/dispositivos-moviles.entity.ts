import {
    Column,
    Entity,
    Index, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {PREFIJO_BASE} from '../../constantes/prefijo-base';
import {EntidadComunProyecto} from '../../abstractos/entidad-comun-proyecto';
import {DatosUsuarioEntity} from '../datos-usuario/datos-usuario.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DISPOSITIVOS_MOVILES')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class DispositivosMovilesEntity extends EntidadComunProyecto {
    @PrimaryGeneratedColumn({
        name: 'ID_DISPOSITIVOS_MOVILES',
        unsigned: true,
    })
    id: number;

    @Column({
        name: 'nombre',
        type: 'varchar',
        length: 60,
        nullable: false,
    })
    nombre: string;

    @Column({
        name: 'codigo_iso_3166',
        type: 'varchar',
        length: 3,
        nullable: false,
    })
    codigoIso: string;

    @ManyToOne(
        () => DatosUsuarioEntity,
        (datosUsuario) => datosUsuario.dispMovilesUsuario,
        { nullable: false },
    )
    datosUsuarioDMU: DatosUsuarioEntity | number;
}
