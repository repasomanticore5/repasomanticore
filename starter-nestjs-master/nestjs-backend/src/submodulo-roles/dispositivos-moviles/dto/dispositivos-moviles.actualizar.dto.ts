import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class DispositivosMovilesActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(60)
    @Expose()
    nombre: string;

    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(3)
    @Expose()
    codigoIso: string;


    @IsOptional()
    @IsNumber()
    @Expose()
    datosUsuarioDMU: number;
}
