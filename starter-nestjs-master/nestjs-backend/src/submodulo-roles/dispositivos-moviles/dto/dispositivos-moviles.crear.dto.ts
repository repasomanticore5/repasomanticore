import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class DispositivosMovilesCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(60)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(3)
  @Expose()
  codigoIso: string;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  datosUsuarioDMU: number;
}
