import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {DispositivosMovilesService} from './dispositivos-moviles.service';
import {DispositivosMovilesHabilitadoDto} from './dto/dispositivos-moviles.habilitado.dto';
import {DISPOSITIVOS_MOVILES_OCC} from './constantes/dispositivos-moviles.occ';
import {DispositivosMovilesCrearDto} from './dto/dispositivos-moviles.crear.dto';
import {DispositivosMovilesActualizarDto} from './dto/dispositivos-moviles.actualizar.dto';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {DispositivosMovilesEntity} from './dispositivos-moviles.entity';
import {DispositivosMovilesBusquedaDto} from './dto/dispositivos-moviles.busqueda.dto';
import {ControladorComun} from '@manticore-labs/nest-2021';
import {AuditoriaService} from '../../auditoria/auditoria.service';
import {SeguridadService} from '../../seguridad/seguridad.service';
import {NUMERO_REGISTROS_DEFECTO} from '../../constantes/numero-registros-defecto';
import {NUMERO_MAXIMO_REGISTROS_CONSULTARSE} from '../../constantes/numero-maximo-registros-consultarse';
import {SeguridadGuard} from '../../guards/seguridad/seguridad.guard';

const nombreControlador = 'dispositivos-moviles';

@Controller(nombreControlador)
export class DispositivosMovilesController extends ControladorComun {
    constructor(
        private readonly _dispositivosMovilesService: DispositivosMovilesService,
        private readonly _seguridadService: SeguridadService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _dispositivosMovilesService,
            _seguridadService,
            _auditoriaService,
            DISPOSITIVOS_MOVILES_OCC(
                DispositivosMovilesHabilitadoDto,
                DispositivosMovilesCrearDto,
                DispositivosMovilesActualizarDto,
                DispositivosMovilesBusquedaDto,
            ),
            NUMERO_REGISTROS_DEFECTO,
            NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(
        @Query() parametrosConsulta: DispositivosMovilesBusquedaDto,
        @Req() request,
    ) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            const aliasTabla = nombreControlador;
            const qb = this._dispositivosMovilesService.dispositivosMovilesEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
                .createQueryBuilder(aliasTabla);

            let consulta: FindManyOptions<DispositivosMovilesEntity> = {
                where: [],
            };
            // Setear Skip y Take (limit y offset)
            consulta = this.setearSkipYTake(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            // Definir el orden según los parámetros sortField y sortOrder
            const orden = this._dispositivosMovilesService.establecerOrden(
                parametrosConsulta.sortField,
                parametrosConsulta.sortOrder,
            );
            // Búsqueda por el identificador
            const consultaPorId = this._dispositivosMovilesService.establecerBusquedaPorId(
                parametrosConsulta,
                consulta
            );
            if (consultaPorId) {
                // Si busca solo por Identificador no necesitamos hacer nada mas
                consulta = consultaPorId;
            } else {
                // Especifica todas las busquedas dentro de la entidad AND y OR
                let consultaWhere = consulta.where as FindConditions<DispositivosMovilesEntity>[];
                // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
                // se llama 'busqueda'.
                // EJ: (nombre = busqueda) OR (cedula = busqueda)
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({nombre: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                if (parametrosConsulta.busqueda) {
                    consultaWhere.push({codigoIso: Like('%' + parametrosConsulta.busqueda + '%')});
                    consulta.where = consultaWhere;
                }
                consultaWhere = consulta.where as FindConditions<DispositivosMovilesEntity>[];
                // Aquí van las condiciones AND de los filtros
                // EJ:
                // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
                // Notese que se van a repetir estas condiciones en todos los 'OR'
                consulta.where = this.setearFiltro(
                    'sisHabilitado',
                    consultaWhere,
                    parametrosConsulta.sisHabilitado,
                );

                consulta.where = this.setearFiltro(
                    'datosUsuarioDMU',
                    consultaWhere,
                    parametrosConsulta.datosUsuarioDMU,
                );

                // OPCIONAL si desea convertir en mayusculas todos los strings
                // consulta = this._dispositivosMovilesService.convertirEnMayusculas(consulta);
            }

            qb.where(consulta.where);

            // RELACIONES CON PAPAS o HIJOS

            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
            // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
            // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
            // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

            // CONSULTAS AVANZADAS EN OTRAS RELACIONES

            // if (parametrosConsulta.nombreCampoRelacionUno) {
            //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
            //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
            //     })
            // }
            //
            // if (parametrosConsulta.nombreOtroCampoCualquiera) {
            //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
            //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
            //     });
            // }

            if (orden) {
                qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
            }

            return qb.skip(consulta.skip)
                .take(consulta.take)
                .getManyAndCount();
        }
    }
}
