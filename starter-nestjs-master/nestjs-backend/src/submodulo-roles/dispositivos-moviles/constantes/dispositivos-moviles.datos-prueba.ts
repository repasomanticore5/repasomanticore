import { DispositivosMovilesCrearDto } from '../dto/dispositivos-moviles.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const DISPOSITIVOS_MOVILES_DATOS_PRUEBA: ((
  idDatosUsuario: number,
) => DispositivosMovilesCrearDto)[] = [
  (idDatosUsuario: number) => {
    const dato = new DispositivosMovilesCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'dispositivo 1' + idDatosUsuario;
    dato.codigoIso = '218';
    dato.datosUsuarioDMU = idDatosUsuario;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
