import {ObjetoSeguridadPermisos} from "@manticore-labs/nest-2021";

export const PERMISOS_DISPOSITIVOS_MOVILES: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'dispositivos-moviles';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/dispositivos-moviles/, // /dispositivos-moviles
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/dispositivos-moviles\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /dispositivos-moviles?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/dispositivos-moviles\/\d+\/modificar-habilitado/, // /dispositivos-moviles/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/dispositivos-moviles\/\d+/, // /dispositivos-moviles/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
