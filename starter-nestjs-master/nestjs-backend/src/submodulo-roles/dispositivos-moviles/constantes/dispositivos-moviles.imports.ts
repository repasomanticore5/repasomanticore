import { TypeOrmModule } from '@nestjs/typeorm';
import { DispositivosMovilesEntity } from '../dispositivos-moviles.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const DISPOSITIVOS_MOVILES_IMPORTS = [
  TypeOrmModule.forFeature([DispositivosMovilesEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
