import { Module } from '@nestjs/common';
import { DispositivosMovilesController } from './dispositivos-moviles.controller';
import { DISPOSITIVOS_MOVILES_IMPORTS } from './constantes/dispositivos-moviles.imports';
import { DISPOSITIVOS_MOVILES_PROVIDERS } from './constantes/dispositivos-moviles.providers';

@Module({
  imports: [...DISPOSITIVOS_MOVILES_IMPORTS],
  providers: [...DISPOSITIVOS_MOVILES_PROVIDERS],
  exports: [...DISPOSITIVOS_MOVILES_PROVIDERS],
  controllers: [DispositivosMovilesController],
})
export class DispositivosMovilesModule {}
