import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { DispositivosMovilesEntity } from './dispositivos-moviles.entity';
import {DispositivosMovilesBusquedaDto} from './dto/dispositivos-moviles.busqueda.dto';
import {ServicioComun} from '@manticore-labs/nest-2021';
import {NOMBRE_CADENA_CONEXION} from '../../constantes/nombre-cadena-conexion';
import {AuditoriaService} from '../../auditoria/auditoria.service';

@Injectable()
export class DispositivosMovilesService extends ServicioComun<DispositivosMovilesEntity, DispositivosMovilesBusquedaDto> {
  constructor(
    @InjectRepository(DispositivosMovilesEntity, NOMBRE_CADENA_CONEXION)
    public dispositivosMovilesEntityRepository: Repository<DispositivosMovilesEntity>,
    @InjectConnection(NOMBRE_CADENA_CONEXION) readonly _connection: Connection,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
        dispositivosMovilesEntityRepository,
      _connection,
        DispositivosMovilesEntity,
      'id',
      _auditoriaService,
        // Por defecto NO se transforma todos los campos a mayúsculas.
        // Si DESEA transformar todos los campos a mayúsculas comente la siguiente línea
        // O implemente sus metodos para transformación en búsqueda, beforeInsert y beforeUpdate
        (objetoATransformar, metodo) => objetoATransformar
    );
  }
}
