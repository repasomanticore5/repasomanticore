import {
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import { HabilitadoDtoComun } from '../../../abstractos/habilitado-dto-comun';

export class RolCrearDto extends HabilitadoDtoComun {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(70)
  @Expose()
  nombre: string;

  @IsNotEmpty()
  @IsInt()
  @Expose()
  empresaR: number;
}
