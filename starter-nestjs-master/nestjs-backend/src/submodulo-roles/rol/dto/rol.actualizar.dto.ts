import {IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { Expose } from 'class-transformer';

export class RolActualizarDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(70)
    @Expose()
    nombre: string;
}
