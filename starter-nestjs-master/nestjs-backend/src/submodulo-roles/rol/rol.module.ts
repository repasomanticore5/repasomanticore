import { Module } from '@nestjs/common';
import { RolController } from './rol.controller';
import { ROL_IMPORTS } from './constantes/rol.imports';
import { ROL_PROVIDERS } from './constantes/rol.providers';

@Module({
  imports: [...ROL_IMPORTS],
  providers: [...ROL_PROVIDERS],
  exports: [...ROL_PROVIDERS],
  controllers: [RolController],
})
export class RolModule {}
