import {
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { PermisoRolEntity } from '../permiso-rol/permiso-rol.entity';
import { RolPorUsuarioEntity } from '../rol-por-usuario/rol-por-usuario.entity';
import { EmpresaEntity } from '../../submodulo-empresa/empresa/empresa.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'ROL')
@Index(['sisHabilitado', 'sisCreado', 'sisModificado'])
export class RolEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_ROL',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
    length: 70,
    nullable: false,
  })
  nombre: string;

  @ManyToOne(() => EmpresaEntity, (empresa) => empresa.rolesE, {
    nullable: false,
  })
  empresaR: EmpresaEntity | number;

  @OneToMany(() => PermisoRolEntity, (permisoRol) => permisoRol.rolPR)
  permisoRolR: PermisoRolEntity[];

  @OneToMany(() => RolPorUsuarioEntity, (rolPorUsuario) => rolPorUsuario.rolPU)
  rolPorUsuarioR: RolPorUsuarioEntity[];
}
