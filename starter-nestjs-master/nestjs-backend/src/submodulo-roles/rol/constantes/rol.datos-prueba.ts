import { RolCrearDto } from '../dto/rol.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const ROL_DATOS_PRUEBA: ((idEmpresa: number) => RolCrearDto)[] = [
  (idEmpresa: number) => {
    const dato = new RolCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Superusuario';
    dato.empresaR = idEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  (idEmpresa: number) => {
    const dato = new RolCrearDto();
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.nombre = 'Administrador del sistema';
    dato.empresaR = idEmpresa;
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
