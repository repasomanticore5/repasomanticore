import {ObjetoSeguridadPermisos} from '@manticore-labs/nest-2021';

export const PERMISOS_ROL: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'rol';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/rol/, // /rol
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/rol\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /rol?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/rol\/\d+\/modificar-habilitado/, // /rol/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/rol\/\d+/, // /rol/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
