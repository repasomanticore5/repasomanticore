import { TypeOrmModule } from '@nestjs/typeorm';
import { RolEntity } from '../rol.entity';
import {NOMBRE_CADENA_CONEXION} from '../../../constantes/nombre-cadena-conexion';
import {SeguridadModule} from '../../../seguridad/seguridad.module';
import {AuditoriaModule} from '../../../auditoria/auditoria.module';

export const ROL_IMPORTS = [
  TypeOrmModule.forFeature([RolEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
