import {IsIn, IsNumberString, IsOptional, IsString} from 'class-validator';
import {Expose} from 'class-transformer';
import { BusquedaComunProyectoDto } from '../../../abstractos/busqueda-comun-proyecto-dto';

export class DatosUsuarioBusquedaDto extends BusquedaComunProyectoDto {

    @IsOptional()
    @IsNumberString()
    @Expose()
    id: string;

    @IsOptional()
    @IsIn(['M', 'F', 'O'])
    @Expose()
    sexo: 'M' | 'F' | 'O';

    // AQUI TODOS LOS CAMPOS PARA EL DTO DE BÚSQUEDA
}
