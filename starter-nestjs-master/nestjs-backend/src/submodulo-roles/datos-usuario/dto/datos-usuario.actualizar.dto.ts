import { IsIn, IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { Expose } from 'class-transformer';

export class DatosUsuarioActualizarDto {
  // AQUI TODOS LOS CAMPOS PARA EL DTO DE ACTUALIZAR
  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(70)
  @Expose()
  nombres: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(60)
  @Expose()
  apellidos: string;

  @IsOptional()
  @IsString()
  @MinLength(5)
  @MaxLength(60)
  @Expose()
  email: string;

  @IsOptional()
  @IsString()
  @Expose()
  fechaNacimiento: string;

  @IsOptional()
  @IsIn(['M', 'F', 'O'])
  @Expose()
  sexo: 'M' | 'F' | 'O';

  @IsOptional()
  @IsNumberString()
  @MinLength(1)
  @MaxLength(15)
  @Expose()
  identificacionPais: string;

  @IsOptional()
  // @IsNumberString()
  @MinLength(3)
  @MaxLength(10)
  @Expose()
  codigoPais: string;

  @IsOptional()
  @IsNumberString()
  @MinLength(10)
  @MaxLength(12)
  @Expose()
  telefono: string;

  @IsOptional()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  @Expose()
  uid: string;
  // @IsNotEmpty()
  // @IsString()
  // @MinLength(10)
  // @MaxLength(60)
  // @Expose()
  // prefijoCampoEjemplo: string;
}
