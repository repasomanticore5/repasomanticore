import { DatosUsuarioCrearDto } from '../dto/datos-usuario.crear.dto';
import { ActivoInactivo } from '../../../enums/activo-inactivo';

export const DATOS_USUARIO_DATOS_PRUEBA: (() => DatosUsuarioCrearDto)[] = [
  () => {
    const dato = new DatosUsuarioCrearDto();
    dato.nombres = 'Cristian';
    dato.apellidos = 'Lara';
    dato.codigoPais = '218';
    dato.telefono = '0992885963';
    dato.email = 'cris.lara@manticore-labs.com';
    dato.fechaNacimiento = '1981-03-01';
    dato.sexo = 'M';
    dato.identificacionPais = '1724155977';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
  () => {
    const dato = new DatosUsuarioCrearDto();
    dato.nombres = 'Oscar';
    dato.apellidos = 'Sambache';
    dato.codigoPais = '218';
    dato.telefono = '0992885963';
    dato.identificacionPais = '1724155914';
    dato.email = 'oscar.sambache@manticore-labs.com';
    dato.fechaNacimiento = '1995-03-01';
    dato.sexo = 'M';
    // LLENAR DATOS DE PRUEBA
    // dato.nombreCampo = 'Valor campo';
    dato.sisHabilitado = ActivoInactivo.Activo;
    return dato;
  },
];
