import { ObjetoSeguridadPermisos } from '@manticore-labs/nest-2021';

export const PERMISOS_DATOS_USUARIO: (
) => {permisos:ObjetoSeguridadPermisos[], nombreEntidad:string} = () => {
  const nombreEntidad = 'datos-usuario';
  const permisos: ObjetoSeguridadPermisos[] = [
    {
      nombrePermiso: nombreEntidad + 'Crear',
      metodoHTTP: 'POST',
      urlExpressionRegular: /\/datos-usuario/, // /datos-usuario
    },
    {
      nombrePermiso: nombreEntidad + 'Buscar',
      metodoHTTP: 'GET',
      urlExpressionRegular: /\/datos-usuario\??(([a-zA-Z0-9]+)=[a-zA-Z0-9]+&?)*/, // /datos-usuario?asdas=asdasd
    },
    {
      nombrePermiso: nombreEntidad + 'EditarHabilitado',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/datos-usuario\/\d+\/modificar-habilitado/, // /datos-usuario/1/modificar-habilitado
    },
    {
      nombrePermiso: nombreEntidad + 'Editar',
      metodoHTTP: 'PUT',
      urlExpressionRegular: /\/datos-usuario\/\d+/, // /datos-usuario/1
    },
  ];
  return {
    permisos,
    nombreEntidad
  };
};
