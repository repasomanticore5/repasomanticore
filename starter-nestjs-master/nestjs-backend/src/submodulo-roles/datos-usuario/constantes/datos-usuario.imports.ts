import { TypeOrmModule } from '@nestjs/typeorm';
import { DatosUsuarioEntity } from '../datos-usuario.entity';
import { NOMBRE_CADENA_CONEXION } from '../../../constantes/nombre-cadena-conexion';
import { AuditoriaModule } from '../../../auditoria/auditoria.module';
import { SeguridadModule } from '../../../seguridad/seguridad.module';

export const DATOS_USUARIO_IMPORTS = [
  TypeOrmModule.forFeature([DatosUsuarioEntity], NOMBRE_CADENA_CONEXION),
  SeguridadModule,
  AuditoriaModule,
];
