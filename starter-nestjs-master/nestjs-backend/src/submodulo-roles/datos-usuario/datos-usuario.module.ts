import { Module } from '@nestjs/common';
import { DatosUsuarioController } from './datos-usuario.controller';
import { DATOS_USUARIO_IMPORTS } from './constantes/datos-usuario.imports';
import { DATOS_USUARIO_PROVIDERS } from './constantes/datos-usuario.providers';

@Module({
  imports: [...DATOS_USUARIO_IMPORTS],
  providers: [...DATOS_USUARIO_PROVIDERS],
  exports: [...DATOS_USUARIO_PROVIDERS],
  controllers: [DatosUsuarioController],
})
export class DatosUsuarioModule {}
