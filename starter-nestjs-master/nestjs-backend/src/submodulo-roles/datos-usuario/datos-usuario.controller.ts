import {
  Controller,
  Get,
  HttpCode,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { DatosUsuarioService } from './datos-usuario.service';
import { DatosUsuarioHabilitadoDto } from './dto/datos-usuario.habilitado.dto';
import { DATOS_USUARIO_OCC } from './constantes/datos-usuario.occ';
import { DatosUsuarioCrearDto } from './dto/datos-usuario.crear.dto';
import { DatosUsuarioActualizarDto } from './dto/datos-usuario.actualizar.dto';
import { FindConditions, FindManyOptions, Like } from 'typeorm';
import { DatosUsuarioEntity } from './datos-usuario.entity';
import { DatosUsuarioBusquedaDto } from './dto/datos-usuario.busqueda.dto';
import { AuditoriaService } from '../../auditoria/auditoria.service';
import { SeguridadGuard } from '../../guards/seguridad/seguridad.guard';
import { NUMERO_MAXIMO_REGISTROS_CONSULTARSE } from '../../constantes/numero-maximo-registros-consultarse';
import { SeguridadService } from '../../seguridad/seguridad.service';
import { ControladorComun } from '@manticore-labs/nest-2021';
import { NUMERO_REGISTROS_DEFECTO } from '../../constantes/numero-registros-defecto';

const nombreControlador = 'datos-usuario';

@Controller(nombreControlador)
export class DatosUsuarioController extends ControladorComun {
  constructor(
    private readonly _datosUsuarioService: DatosUsuarioService,
    private readonly _seguridadService: SeguridadService,
    private readonly _auditoriaService: AuditoriaService,
  ) {
    super(
      _datosUsuarioService,
      _seguridadService,
      _auditoriaService,
      DATOS_USUARIO_OCC(
        DatosUsuarioHabilitadoDto,
        DatosUsuarioCrearDto,
        DatosUsuarioActualizarDto,
        DatosUsuarioBusquedaDto,
      ),
      NUMERO_REGISTROS_DEFECTO,
      NUMERO_MAXIMO_REGISTROS_CONSULTARSE,
    );
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async obtener(
    @Query() parametrosConsulta: DatosUsuarioBusquedaDto,
    @Req() request,
  ) {
    const respuesta = await this.validarBusquedaDto(
      request,
      parametrosConsulta,
    );
    if (respuesta === 'ok') {
      const aliasTabla = nombreControlador;
      const qb = this._datosUsuarioService.datosUsuarioEntityRepository // QUERY BUILDER https://typeorm.io/#/select-query-builder
        .createQueryBuilder(aliasTabla);

      let consulta: FindManyOptions<DatosUsuarioEntity> = {
        where: [],
      };
      // Setear Skip y Take (limit y offset)
      consulta = this.setearSkipYTake(
        consulta,
        parametrosConsulta.skip,
        parametrosConsulta.take,
      );
      // Definir el orden según los parámetros sortField y sortOrder
      const orden = this._datosUsuarioService.establecerOrden(
        parametrosConsulta.sortField,
        parametrosConsulta.sortOrder,
      );
      // Búsqueda por el identificador
      const consultaPorId = this._datosUsuarioService.establecerBusquedaPorId(
        parametrosConsulta,
        consulta,
      );
      if (consultaPorId) {
        // Si busca solo por Identificador no necesitamos hacer nada mas
        consulta = consultaPorId;
      } else {
        // Especifica todas las busquedas dentro de la entidad AND y OR
        let consultaWhere = consulta.where as FindConditions<DatosUsuarioEntity>[];
        // Aquí se definen todos los campos a buscar con OR. El campo por defecto de búsqueda
        // se llama 'busqueda'.
        // EJ: (nombre = busqueda) OR (cedula = busqueda)
        if (parametrosConsulta.busqueda) {
          consultaWhere.push({
            nombres: Like('%' + parametrosConsulta.busqueda + '%'),
          });
          consulta.where = consultaWhere;
        }
        if (parametrosConsulta.busqueda) {
          consultaWhere.push({
            apellidos: Like('%' + parametrosConsulta.busqueda + '%'),
          });
          consulta.where = consultaWhere;
        }
        if (parametrosConsulta.busqueda) {
          consultaWhere.push({
            identificacionPais: Like('%' + parametrosConsulta.busqueda + '%'),
          });
          consulta.where = consultaWhere;
        }
        consultaWhere = consulta.where as FindConditions<DatosUsuarioEntity>[];
        // Aquí van las condiciones AND de los filtros
        // EJ:
        // Buscar por (nombre = busqueda AND sisHabilitado = habilitado) O (Cedula = busqueda AND sisHabilitado = habilitado)
        // Notese que se van a repetir estas condiciones en todos los 'OR'
        consulta.where = this.setearFiltro(
          'sisHabilitado',
          consultaWhere,
          parametrosConsulta.sisHabilitado,
        );
        consulta.where = this.setearFiltro(
          'sexo',
          consultaWhere,
          parametrosConsulta.sexo,
        );

        // OPCIONAL si desea convertir en mayusculas todos los strings
        // consulta = this._datosUsuarioService.convertirEnMayusculas(consulta);
      }

      qb.where(consulta.where);

      // RELACIONES CON PAPAS o HIJOS

      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionUno', 'nombreRelacionUno');
      // qb.leftJoinAndSelect(aliasTabla + '.nombreRelacionDos', 'nombreRelacionDos');
      // qb.leftJoinAndSelect('nombreRelacionUno.campoOtraRelacionEnCampoRelacionUno', 'campoOtraRelacionEnCampoRelacionUno');
      // qb.leftJoinAndSelect('nombreRelacionDos.campoOtraRelacionEnCampoRelacionDos', 'campoOtraRelacionEnCampoRelacionDos');

      // CONSULTAS AVANZADAS EN OTRAS RELACIONES

      // if (parametrosConsulta.nombreCampoRelacionUno) {
      //     qb.andWhere('nombreRelacionUno.nombreCampoRelacionUno = :nombreCampoRelacionUno', {
      //         nombreCampoRelacionUno: parametrosConsulta.nombreCampoRelacionUno,
      //     })
      // }
      //
      // if (parametrosConsulta.nombreOtroCampoCualquiera) {
      //     qb .andWhere('campoOtraRelacionEnCampoRelacionUno.nombreOtroCampoCualquiera = :nombreOtroCampoCualquiera', {
      //         nombreOtroCampoCualquiera: parametrosConsulta.nombreOtroCampoCualquiera,
      //     });
      // }

      if (orden) {
        qb.orderBy(aliasTabla + '.' + orden.campo, orden.valor);
      }

      return qb.skip(consulta.skip).take(consulta.take).getManyAndCount();
    }
  }
}
