import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PREFIJO_BASE } from '../../constantes/prefijo-base';
import { EntidadComunProyecto } from '../../abstractos/entidad-comun-proyecto';
import { ContactoEmpresaEntity } from '../../submodulo-empresa/contacto-empresa/contacto-empresa.entity';
import { RolPorUsuarioEntity } from '../rol-por-usuario/rol-por-usuario.entity';
import { DispositivosMovilesEntity } from '../dispositivos-moviles/dispositivos-moviles.entity';

// const PREFIJO_TABLA = ''; // EJ: PREFIJO_
// const nombreCampoEjemplo = PREFIJO_TABLA + 'EJEMPLO';

@Entity(PREFIJO_BASE + 'DATOS_USUARIO')
@Index([
  'sisHabilitado',
  'sisCreado',
  'sisModificado',
  'nombres',
  'apellidos',
  'sexo',
  'identificacionPais',
  'codigoPais',
  'telefono',
  'uid',
])
export class DatosUsuarioEntity extends EntidadComunProyecto {
  @PrimaryGeneratedColumn({
    name: 'ID_DATOS_USUARIO',
    unsigned: true,
  })
  id: number;

  @Column({
    name: 'nombres',
    type: 'varchar',
    length: 70,
    nullable: false,
  })
  nombres: string = null;

  @Column({
    name: 'apellidos',
    type: 'varchar',
    length: 60,
    nullable: false,
  })
  apellidos: string = null;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 60,
    nullable: true,
  })
  email: string = null;

  @Column({
    name: 'fecha_nacimiento',
    type: 'date',
    nullable: true,
  })
  fechaNacimiento: Date = null;

  @Column({
    name: 'sexo',
    type: 'char',
    nullable: false,
    default: 0,
    comment: 'M: masculino, F: femenino, O: otro',
  })
  sexo: 'M' | 'F' | 'O';

  @Column({
    name: 'identificacion_pais',
    type: 'varchar',
    length: 13,
    nullable: false,
  })
  identificacionPais: string = null;

  @Column({
    name: 'codigo_pais',
    type: 'varchar',
    length: 10,
    nullable: false,
  })
  codigoPais: string = null;

  @Column({
    name: 'telefono',
    type: 'varchar',
    length: 12,
    nullable: false,
  })
  telefono: string;

  @Column({
    name: 'uid',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  uid: string;
  // @Column({
  //     name: nombreCampoEjemplo,
  //     type: 'varchar',
  //     length: '60',
  //     nullable: false,
  // })
  // prefijoCampoEjemplo: string;

  @OneToMany(() => ContactoEmpresaEntity, (r) => r.datosUsuario)
  contactosEmpresa: ContactoEmpresaEntity[];

  @OneToMany(
    () => RolPorUsuarioEntity,
    (rolPorUsuario) => rolPorUsuario.datosUsuarioRPU,
  )
  rolPorUsuarioDU: RolPorUsuarioEntity[];

  @OneToMany(
    () => DispositivosMovilesEntity,
    (dispositivosMoviles) => dispositivosMoviles.datosUsuarioDMU,
  )
  dispMovilesUsuario: DispositivosMovilesEntity[];
}
