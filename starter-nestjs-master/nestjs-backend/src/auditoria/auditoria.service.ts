import {Injectable} from '@nestjs/common';
import {ServicioAuditoriaComun} from '@manticore-labs/nest-2021';
import {Request} from 'express';

@Injectable()
export class AuditoriaService implements ServicioAuditoriaComun {
    [key: string]: any;

    generarAuditoriaErrorValidacionDto(request: Request | { [p: string]: any }, metodo: string | 'crear' | 'modificarHabilitado' | 'actualizar', datos: any, id: string | undefined): Promise<void> {
        console.error(JSON.stringify(datos, null, 3))
        return Promise.resolve(undefined);
    }

    generarAuditoriaServicio(request: Request | { [p: string]: any }, metodo: string | 'crear' | 'modificarHabilitado' | 'actualizar', datos: any, id: string | undefined): Promise<void> {
        return Promise.resolve(undefined);
    }

}
