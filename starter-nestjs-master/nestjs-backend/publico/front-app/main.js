(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/dev-11/Documents/Gitlab/submodulos/starter-angular/angular-frontend/src/main.ts */"zUnb");


/***/ }),

/***/ "09py":
/*!*********************************************************************************!*\
  !*** ./src/app/modulos/ejemplo-nest/miga-pan-general/ejemplo-nest-migas-pan.ts ***!
  \*********************************************************************************/
/*! exports provided: EJEMPLO_NEST_MIGAS_PAN */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EJEMPLO_NEST_MIGAS_PAN", function() { return EJEMPLO_NEST_MIGAS_PAN; });
const EJEMPLO_NEST_MIGAS_PAN = (componente, items) => {
    const restoDeMigas = [
        {
            label: 'Entrenador',
            disabled: true,
            expanded: true,
            items: [
                {
                    label: 'Pokemon',
                    disabled: true,
                    expanded: true,
                }
            ]
        },
    ];
    return {
        id: 'ejemplo-nest',
        icon: 'pi pi-pw',
        expanded: true,
        label: 'Ejemplo Nest Pokemon',
        routerLink: ['/ejemplo-nest'],
        items: restoDeMigas,
    };
};


/***/ }),

/***/ "0kRC":
/*!********************************************************!*\
  !*** ./src/app/servicios/cargando/cargando.service.ts ***!
  \********************************************************/
/*! exports provided: CargandoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CargandoService", function() { return CargandoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class CargandoService {
    constructor() {
        this.cambioBloqueado = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.estaBloqueado = false;
        this.activo = false;
    }
    bloquear() {
    }
    desbloquear() {
    }
    habilitarCargando() {
        this.activo = true;
    }
    deshabilitarCargando() {
        this.activo = false;
    }
}
CargandoService.ɵfac = function CargandoService_Factory(t) { return new (t || CargandoService)(); };
CargandoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: CargandoService, factory: CargandoService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "5Znw":
/*!****************************************************************!*\
  !*** ./src/app/servicios/menu-general/menu-general.service.ts ***!
  \****************************************************************/
/*! exports provided: MenuGeneralService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuGeneralService", function() { return MenuGeneralService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _modulos_ejemplo_nest_miga_pan_general_ejemplo_nest_migas_pan__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../modulos/ejemplo-nest/miga-pan-general/ejemplo-nest-migas-pan */ "09py");



class MenuGeneralService {
    constructor() {
        this.aparecer = true;
        this.itemsDeMenu = {
            pokemon: [],
        };
        this.cambioMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onInit();
    }
    onInit() {
        this.itemsDeMenu.pokemon = [
            Object(_modulos_ejemplo_nest_miga_pan_general_ejemplo_nest_migas_pan__WEBPACK_IMPORTED_MODULE_1__["EJEMPLO_NEST_MIGAS_PAN"])(this),
        ];
    }
    modificarPorIndice(nombreObjeto, indice, item) {
        const itemAny = this.itemsDeMenu;
        itemAny[nombreObjeto][indice] = item;
        this.cambioMenu.emit(true);
    }
}
MenuGeneralService.ɵfac = function MenuGeneralService_Factory(t) { return new (t || MenuGeneralService)(); };
MenuGeneralService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: MenuGeneralService, factory: MenuGeneralService.ɵfac });


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    url: 'http://localhost:3000',
    firebase: {
        apiKey: "AIzaSyD1s-DooP0MzL6qOhwYkTvVL0Bxihofzos",
        authDomain: "sos-encargos-test.firebaseapp.com",
        projectId: "sos-encargos-test",
        storageBucket: "sos-encargos-test.appspot.com",
        messagingSenderId: "143073202598",
        appId: "1:143073202598:web:0fd131fc31f0b8d1e5bf67",
        measurementId: "G-7RQR9Q38JP"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "KHIb":
/*!**************************************************************************************!*\
  !*** ./src/app/modal/modal-crear-editar-comun/modal-crear-editar-comun.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ModalCrearEditarComunComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCrearEditarComunComponent", function() { return ModalCrearEditarComunComponent; });
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_ripple__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/ripple */ "Q4Mo");
/* harmony import */ var man_lab_ng__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! man-lab-ng */ "6/bn");







function ModalCrearEditarComunComponent_div_0_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "app-ml-contenedor-campos-formulario", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("todosCamposValidos", function ModalCrearEditarComunComponent_div_0_div_1_div_3_Template_app_ml_contenedor_campos_formulario_todosCamposValidos_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r3.formularioValido($event); })("cambioCampo", function ModalCrearEditarComunComponent_div_0_div_1_div_3_Template_app_ml_contenedor_campos_formulario_cambioCampo_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r5.cambioCampo($event); })("cambioAutocomplete", function ModalCrearEditarComunComponent_div_0_div_1_div_3_Template_app_ml_contenedor_campos_formulario_cambioAutocomplete_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r6.cambioAutocomplete($event); })("cambioStepperEnModal", function ModalCrearEditarComunComponent_div_0_div_1_div_3_Template_app_ml_contenedor_campos_formulario_cambioStepperEnModal_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r7.cambioStepperEnModal($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("min-height", ctx_r2.data.componente.minHeightModal ? ctx_r2.data.componente.minHeightModal : "400px");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx_r2.data.registro ? ctx_r2.data.descripcionActualizar : ctx_r2.data.descripcionCrear, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("gruposFormularioFuncion", ctx_r2.data.gruposFormularioCrearEditar)("tituloFormulario", ctx_r2.data.rutaServicio.tituloFormulario)("descripcionFormulario", ctx_r2.data.rutaServicio.descripcionFormulario)("vertical", ctx_r2.data.rutaServicio.vertical)("tituloFormularioConGrupo", ctx_r2.data.rutaServicio.tituloFormularioConGrupo)("descripcionFormularioConGrupo", ctx_r2.data.rutaServicio.descripcionFormularioConGrupo)("selectedStepperIndex", ctx_r2.data.rutaServicio.stepperActualCrearEditar)("camposFormularioFuncion", ctx_r2.data.camposFormulario)("componente", ctx_r2);
} }
function ModalCrearEditarComunComponent_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "h1", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ModalCrearEditarComunComponent_div_0_div_1_div_3_Template, 9, 12, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ModalCrearEditarComunComponent_div_0_div_1_Template_button_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r8.guardarEditar(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r1.data.registro ? ctx_r1.data.tituloActualizar : ctx_r1.data.tituloCrear);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r1.data.componente.ocultarFormulario);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r1.data.componente.botonAceptarModalDisabled);
} }
function ModalCrearEditarComunComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ModalCrearEditarComunComponent_div_0_div_1_Template, 10, 3, "div", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.data.camposFormulario);
} }
class ModalCrearEditarComunComponent {
    constructor(data) {
        this.data = data;
    }
    ngOnInit() {
    }
    cambioAutocomplete(evento) {
        this.data.componente.formularioCrearEditarCambioAutocomplete(evento, this.data.registro);
    }
    formularioValido(estaValido) {
        this.data.componente.formularioCrearEditarValido(estaValido, this.data.registro);
    }
    cambioCampo(eventoCambioCampo) {
        if (eventoCambioCampo.campoFormulario) {
            if (this.data.componente.nombreServicio) {
                this.data.componente[this.data.componente.nombreServicio]
                    .camposSeleccionadosFormulario[eventoCambioCampo.campoFormulario.nombreCampo] = eventoCambioCampo.valor;
            }
            this.data.componente.formularioCrearEditarCambioCampo(eventoCambioCampo, this.data.registro);
        }
    }
    guardarEditar() {
        this.data.componente.crearEditar(this.data.registro);
    }
    cambioStepperEnModal(indice) {
        this.data.rutaServicio.cambioStepperEnModal(indice);
    }
}
ModalCrearEditarComunComponent.ɵfac = function ModalCrearEditarComunComponent_Factory(t) { return new (t || ModalCrearEditarComunComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"])); };
ModalCrearEditarComunComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ModalCrearEditarComunComponent, selectors: [["app-modal-crear-editar-s-logistico"]], decls: 1, vars: 1, consts: [[4, "ngIf"], ["mat-dialog-title", ""], ["mat-dialog-content", "", 3, "min-height", 4, "ngIf"], ["mat-dialog-actions", ""], [1, "row"], [1, "col-sm-6"], ["pButton", "", "pRipple", "", "type", "button", "mat-dialog-close", "", "label", "Cancelar", 1, "p-button-raised", "p-button-secondary", "p-button-text"], ["pButton", "", "pRipple", "", "type", "button", "label", "Aceptar", 1, "p-button-raised", "p-button-success", 3, "disabled", "click"], ["mat-dialog-content", ""], [1, "campo-formulario", 3, "gruposFormularioFuncion", "tituloFormulario", "descripcionFormulario", "vertical", "tituloFormularioConGrupo", "descripcionFormularioConGrupo", "selectedStepperIndex", "camposFormularioFuncion", "componente", "todosCamposValidos", "cambioCampo", "cambioAutocomplete", "cambioStepperEnModal"]], template: function ModalCrearEditarComunComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, ModalCrearEditarComunComponent_div_0_Template, 2, 1, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.data);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogTitle"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogActions"], primeng_button__WEBPACK_IMPORTED_MODULE_3__["ButtonDirective"], primeng_ripple__WEBPACK_IMPORTED_MODULE_4__["Ripple"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogClose"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogContent"], man_lab_ng__WEBPACK_IMPORTED_MODULE_5__["MlContenedorCamposFormularioComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtb2RhbC1jcmVhci1lZGl0YXItY29tdW4uY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "KwZT":
/*!****************************************************************!*\
  !*** ./src/app/servicios/notificacion/notificacion.service.ts ***!
  \****************************************************************/
/*! exports provided: NotificacionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacionService", function() { return NotificacionService; });
/* harmony import */ var _notificacion_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notificacion.module */ "xUck");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");



class NotificacionService {
    constructor(_messageService) {
        this._messageService = _messageService;
    }
    anadir(notificacion) {
        this._messageService
            .add({
            severity: notificacion.severidad,
            summary: notificacion.titulo,
            detail: notificacion.detalle
        });
    }
}
NotificacionService.ɵfac = function NotificacionService_Factory(t) { return new (t || NotificacionService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"])); };
NotificacionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: NotificacionService, factory: NotificacionService.ɵfac, providedIn: _notificacion_module__WEBPACK_IMPORTED_MODULE_0__["NotificacionModule"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _servicios_cargando_cargando_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./servicios/cargando/cargando.service */ "0kRC");
/* harmony import */ var _servicios_notificacion_notificacion_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servicios/notificacion/notificacion.service */ "KwZT");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _servicios_menu_general_menu_general_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./servicios/menu-general/menu-general.service */ "5Znw");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/toast */ "Gxio");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/confirmdialog */ "Nf9I");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/sidenav */ "XhcP");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var ngx_auth_firebaseui__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-auth-firebaseui */ "eJ7z");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");




















function AppComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "p-panelMenu", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx_r1.menuGeneralService.itemsDeMenu.pokemon);
} }
const _c0 = function () { return { width: "50vw" }; };
class AppComponent {
    constructor(cargandoService, _notificacionService, _httpClient, _router, dialog, menuGeneralService, firestore, auth) {
        this.cargandoService = cargandoService;
        this._notificacionService = _notificacionService;
        this._httpClient = _httpClient;
        this._router = _router;
        this.dialog = dialog;
        this.menuGeneralService = menuGeneralService;
        this.firestore = firestore;
        this.auth = auth;
        this.title = 'angular-frontend';
        this.showFiller = false;
    }
    ngOnInit() {
        this.cambioMenu();
        this.firestore.collection('usuario').get().subscribe((data) => {
            console.log('HABLA');
            data.forEach((a) => {
                console.log(a.data());
            });
        }, (error) => {
            console.log(error);
        });
    }
    login() {
        this.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_0__["default"].auth.EmailAuthProvider());
    }
    logout() {
        this.auth.signOut();
    }
    printUser(event) {
        console.log(event);
    }
    printError(event) {
        console.error(event);
    }
    cambioMenu() {
        this.menuGeneralService
            .cambioMenu
            .subscribe(() => {
            this.menuGeneralService.aparecer = false;
            setTimeout(() => {
                this.menuGeneralService.aparecer = true;
            }, 1);
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_cargando_cargando_service__WEBPACK_IMPORTED_MODULE_2__["CargandoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_notificacion_notificacion_service__WEBPACK_IMPORTED_MODULE_3__["NotificacionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_servicios_menu_general_menu_general_service__WEBPACK_IMPORTED_MODULE_7__["MenuGeneralService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestore"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_fire_auth__WEBPACK_IMPORTED_MODULE_9__["AngularFireAuth"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 24, vars: 9, consts: [[3, "blocked"], [2, "color", "white"], ["rejectButtonStyleClass", "p-button-text", 3, "baseZIndex"], ["color", "primary"], ["mat-icon-button", "", "aria-label", "Example icon-button with menu icon", 1, "example-icon", 3, "click"], [1, "example-spacer2"], [1, "example-spacer"], [1, "version"], ["href", "https://manticore-labs.com", "target", "_blank"], ["src", "https://manticore-labs.com/wp-content/uploads/2019/09/logo-manticore-png.png", "alt", "Logo Manticore Labs", "title", "Manticore Labs", 1, "logo-manticore-labs"], ["autosize", ""], ["mode", "side", 1, "example-sidenav"], ["drawer", ""], [4, "ngIf"], [1, "sidenav-content-ml"], [1, "container"], [3, "signInTabText", "onSuccess", "onError"], [3, "model"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Cargando...");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "p-toast");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "p-confirmDialog", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "mat-toolbar", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_Template_button_click_6_listener() { return ctx.showFiller = !ctx.showFiller; })("click", function AppComponent_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](18); return _r0.toggle(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "span", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, " V 1.0.0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "mat-drawer-container", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "mat-drawer", 11, 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, AppComponent_div_19_Template, 2, 1, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "ngx-auth-firebaseui", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onSuccess", function AppComponent_Template_ngx_auth_firebaseui_onSuccess_23_listener($event) { return ctx.printUser($event); })("onError", function AppComponent_Template_ngx_auth_firebaseui_onError_23_listener($event) { return ctx.printError($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.cargandoService.activo);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](8, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("baseZIndex", 10000);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"]("example-container ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.menuGeneralService.aparecer);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("signInTabText", "VALES VERGA");
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_10__["BlockUI"], primeng_toast__WEBPACK_IMPORTED_MODULE_11__["Toast"], primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_12__["ConfirmDialog"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_13__["MatToolbar"], _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__["MatIcon"], _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_16__["MatDrawerContainer"], _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_16__["MatDrawer"], _angular_common__WEBPACK_IMPORTED_MODULE_17__["NgIf"], ngx_auth_firebaseui__WEBPACK_IMPORTED_MODULE_18__["AuthComponent"], primeng_panelmenu__WEBPACK_IMPORTED_MODULE_19__["PanelMenu"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "Yp09":
/*!*********************************************************!*\
  !*** ./src/app/imports/imports-componente-principal.ts ***!
  \*********************************************************/
/*! exports provided: IMPORTS_COMPONENTE_PRINCIPAL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IMPORTS_COMPONENTE_PRINCIPAL", function() { return IMPORTS_COMPONENTE_PRINCIPAL; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app-routing.module */ "vY5A");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/toast */ "Gxio");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/sidenav */ "XhcP");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _modal_modal_crear_editar_comun_modal_crear_editar_comun_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../modal/modal-crear-editar-comun/modal-crear-editar-comun.module */ "nGSx");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var man_lab_ng__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! man-lab-ng */ "6/bn");
/* harmony import */ var _servicios_notificacion_notificacion_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../servicios/notificacion/notificacion.module */ "xUck");
/* harmony import */ var _servicios_componente_principal_componente_principal_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../servicios/componente-principal/componente-principal.module */ "cmAm");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/confirmdialog */ "Nf9I");





















const IMPORTS_COMPONENTE_PRINCIPAL = [
    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
    primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_20__["ConfirmDialogModule"],
    _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
    primeng_blockui__WEBPACK_IMPORTED_MODULE_4__["BlockUIModule"],
    primeng_toast__WEBPACK_IMPORTED_MODULE_5__["ToastModule"],
    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_6__["MatSidenavModule"],
    _angular_material_list__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
    _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_9__["MatToolbarModule"],
    _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
    man_lab_ng__WEBPACK_IMPORTED_MODULE_17__["MlCampoFormularioModule"],
    primeng_autocomplete__WEBPACK_IMPORTED_MODULE_11__["AutoCompleteModule"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
    _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
    _modal_modal_crear_editar_comun_modal_crear_editar_comun_module__WEBPACK_IMPORTED_MODULE_14__["ModalCrearEditarComunModule"],
    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__["MatDialogModule"],
    primeng_panelmenu__WEBPACK_IMPORTED_MODULE_16__["PanelMenuModule"],
    _servicios_notificacion_notificacion_module__WEBPACK_IMPORTED_MODULE_18__["NotificacionModule"],
    _servicios_componente_principal_componente_principal_module__WEBPACK_IMPORTED_MODULE_19__["ComponentePrincipalModule"],
];


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _imports_imports_componente_principal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./imports/imports-componente-principal */ "Yp09");
/* harmony import */ var _imports_providers_componente_principal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./imports/providers-componente-principal */ "dCnX");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire */ "spgP");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../environments/environment */ "AytR");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_fire_analytics__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/analytics */ "h+eY");
/* harmony import */ var ngx_auth_firebaseui__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-auth-firebaseui */ "eJ7z");
/* harmony import */ var _angular_material_extensions_password_strength__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular-material-extensions/password-strength */ "Rw1y");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/confirmdialog */ "Nf9I");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/toast */ "Gxio");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/sidenav */ "XhcP");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var man_lab_ng__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! man-lab-ng */ "6/bn");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _modal_modal_crear_editar_comun_modal_crear_editar_comun_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./modal/modal-crear-editar-comun/modal-crear-editar-comun.module */ "nGSx");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var _servicios_notificacion_notificacion_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./servicios/notificacion/notificacion.module */ "xUck");
/* harmony import */ var _servicios_componente_principal_componente_principal_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./servicios/componente-principal/componente-principal.module */ "cmAm");


































class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineInjector"]({ providers: [
        ..._imports_providers_componente_principal__WEBPACK_IMPORTED_MODULE_2__["PROVIDERS_COMPONENTE_PRINCIPAL"]
    ], imports: [[
            ..._imports_imports_componente_principal__WEBPACK_IMPORTED_MODULE_1__["IMPORTS_COMPONENTE_PRINCIPAL"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_3__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].firebase),
            _angular_fire_analytics__WEBPACK_IMPORTED_MODULE_6__["AngularFireAnalyticsModule"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestoreModule"],
            _angular_material_extensions_password_strength__WEBPACK_IMPORTED_MODULE_8__["MatPasswordStrengthModule"].forRoot(),
            ngx_auth_firebaseui__WEBPACK_IMPORTED_MODULE_7__["NgxAuthFirebaseUIModule"].forRoot(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].firebase, () => 'your_app_name_factory', {
                enableFirestoreSync: true,
                toastMessageOnAuthSuccess: false,
                toastMessageOnAuthError: false,
                authGuardFallbackURL: '/loggedout',
                authGuardLoggedInURL: '/loggedin',
                passwordMaxLength: 60,
                passwordMinLength: 8,
                // Same as password but for the name
                nameMaxLength: 50,
                nameMinLength: 2,
                // If set, sign-in/up form is not available until email has been verified.
                // Plus protected routes are still protected even though user is connected.
                guardProtectedRoutesUntilEmailIsVerified: true,
                enableEmailVerification: true,
                useRawUserCredential: true,
            })
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__["AppComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__["BrowserModule"], primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_11__["ConfirmDialogModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_12__["AppRoutingModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["BrowserAnimationsModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbModule"], primeng_blockui__WEBPACK_IMPORTED_MODULE_15__["BlockUIModule"], primeng_toast__WEBPACK_IMPORTED_MODULE_16__["ToastModule"], _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_17__["MatSidenavModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_18__["MatListModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__["MatIconModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_20__["MatToolbarModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_21__["MatButtonModule"], man_lab_ng__WEBPACK_IMPORTED_MODULE_22__["MlCampoFormularioModule"], primeng_autocomplete__WEBPACK_IMPORTED_MODULE_23__["AutoCompleteModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_24__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_24__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_25__["HttpClientModule"], _modal_modal_crear_editar_comun_modal_crear_editar_comun_module__WEBPACK_IMPORTED_MODULE_26__["ModalCrearEditarComunModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__["MatDialogModule"], primeng_panelmenu__WEBPACK_IMPORTED_MODULE_28__["PanelMenuModule"], _servicios_notificacion_notificacion_module__WEBPACK_IMPORTED_MODULE_29__["NotificacionModule"], _servicios_componente_principal_componente_principal_module__WEBPACK_IMPORTED_MODULE_30__["ComponentePrincipalModule"], _angular_fire__WEBPACK_IMPORTED_MODULE_3__["AngularFireModule"], _angular_fire_analytics__WEBPACK_IMPORTED_MODULE_6__["AngularFireAnalyticsModule"],
        _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestoreModule"], _angular_material_extensions_password_strength__WEBPACK_IMPORTED_MODULE_8__["MatPasswordStrengthModule"], ngx_auth_firebaseui__WEBPACK_IMPORTED_MODULE_7__["NgxAuthFirebaseUIModule"]] }); })();


/***/ }),

/***/ "cmAm":
/*!*******************************************************************************!*\
  !*** ./src/app/servicios/componente-principal/componente-principal.module.ts ***!
  \*******************************************************************************/
/*! exports provided: ComponentePrincipalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentePrincipalModule", function() { return ComponentePrincipalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class ComponentePrincipalModule {
}
ComponentePrincipalModule.ɵfac = function ComponentePrincipalModule_Factory(t) { return new (t || ComponentePrincipalModule)(); };
ComponentePrincipalModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: ComponentePrincipalModule });
ComponentePrincipalModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({});


/***/ }),

/***/ "dCnX":
/*!***********************************************************!*\
  !*** ./src/app/imports/providers-componente-principal.ts ***!
  \***********************************************************/
/*! exports provided: PROVIDERS_COMPONENTE_PRINCIPAL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PROVIDERS_COMPONENTE_PRINCIPAL", function() { return PROVIDERS_COMPONENTE_PRINCIPAL; });
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _servicios_cargando_cargando_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../servicios/cargando/cargando.service */ "0kRC");
/* harmony import */ var _servicios_menu_general_menu_general_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../servicios/menu-general/menu-general.service */ "5Znw");



const PROVIDERS_COMPONENTE_PRINCIPAL = [
    primeng_api__WEBPACK_IMPORTED_MODULE_0__["MessageService"],
    _servicios_cargando_cargando_service__WEBPACK_IMPORTED_MODULE_1__["CargandoService"],
    _servicios_menu_general_menu_general_service__WEBPACK_IMPORTED_MODULE_2__["MenuGeneralService"],
    primeng_api__WEBPACK_IMPORTED_MODULE_0__["ConfirmationService"],
];


/***/ }),

/***/ "nGSx":
/*!***********************************************************************************!*\
  !*** ./src/app/modal/modal-crear-editar-comun/modal-crear-editar-comun.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ModalCrearEditarComunModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCrearEditarComunModule", function() { return ModalCrearEditarComunModule; });
/* harmony import */ var _modal_crear_editar_comun_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-crear-editar-comun.component */ "KHIb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_ripple__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/ripple */ "Q4Mo");
/* harmony import */ var man_lab_ng__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! man-lab-ng */ "6/bn");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "fXoL");







class ModalCrearEditarComunModule {
}
ModalCrearEditarComunModule.ɵfac = function ModalCrearEditarComunModule_Factory(t) { return new (t || ModalCrearEditarComunModule)(); };
ModalCrearEditarComunModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({ type: ModalCrearEditarComunModule });
ModalCrearEditarComunModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({ imports: [[
            man_lab_ng__WEBPACK_IMPORTED_MODULE_5__["MlCampoFormularioModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            primeng_button__WEBPACK_IMPORTED_MODULE_3__["ButtonModule"],
            primeng_ripple__WEBPACK_IMPORTED_MODULE_4__["RippleModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](ModalCrearEditarComunModule, { declarations: [_modal_crear_editar_comun_component__WEBPACK_IMPORTED_MODULE_0__["ModalCrearEditarComunComponent"]], imports: [man_lab_ng__WEBPACK_IMPORTED_MODULE_5__["MlCampoFormularioModule"],
        _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_3__["ButtonModule"],
        primeng_ripple__WEBPACK_IMPORTED_MODULE_4__["RippleModule"]], exports: [_modal_crear_editar_comun_component__WEBPACK_IMPORTED_MODULE_0__["ModalCrearEditarComunComponent"]] }); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _modulos_ejemplo_nest_miga_pan_general_ejemplo_nest_migas_pan__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modulos/ejemplo-nest/miga-pan-general/ejemplo-nest-migas-pan */ "09py");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    {
        path: Object(_modulos_ejemplo_nest_miga_pan_general_ejemplo_nest_migas_pan__WEBPACK_IMPORTED_MODULE_1__["EJEMPLO_NEST_MIGAS_PAN"])(undefined).id,
        loadChildren: () => __webpack_require__.e(/*! import() | modulos-ejemplo-nest-ejemplo-nest-module */ "modulos-ejemplo-nest-ejemplo-nest-module").then(__webpack_require__.bind(null, /*! ./modulos/ejemplo-nest/ejemplo-nest.module */ "KnF+"))
            .then(m => m.EjemploNestModule)
    },
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "xUck":
/*!***************************************************************!*\
  !*** ./src/app/servicios/notificacion/notificacion.module.ts ***!
  \***************************************************************/
/*! exports provided: NotificacionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacionModule", function() { return NotificacionModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class NotificacionModule {
}
NotificacionModule.ɵfac = function NotificacionModule_Factory(t) { return new (t || NotificacionModule)(); };
NotificacionModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NotificacionModule });
NotificacionModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({});


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map